<?php
class Features extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $base = base_url() . PUBLIC_DIR . "assets/";
    $this->load->library('viewer');
  }
  public function index()
  {
      $this->viewer->fview('general/features');
  }
}
?>

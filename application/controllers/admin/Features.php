<?php
class Features extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $base = base_url() . PUBLIC_DIR . "assets/";
    $this->load->library('viewer');
    $this->load->helper(array('form', 'url'));
  }
  public function index()
  {
      $this->viewer->aview('features/view');
  }

  function do_upload()
  {
    $title = $this->input->post('title');
    $link = $this->input->post('link');
    $start_date = $this->input->post('start');
    $end_date = $this->input->post('end');
    $config = array(
      'upload_path'=>'./public/assets/features',
      'allowed_types'=>'gif|jpg|jpeg|png',
      'overwrite'=>TRUE,
      'max_size'=>'2048'
    );
    $this->load->library('upload', $config);
    if (!is_dir($config['upload_path']))
    {
      echo "Directory does not exist";
    }
    if ( ! $this->upload->do_upload('userfile'))
               {
                      echo "file cannot be uploaded";
               }
               else
               {
                      $this->upload->do_upload('userfile');
                       $data = array('upload_data' => $this->upload->data());
                       $pic_name = $data['upload_data']['file_name'];
                        print_r($pic_name);
                       $do_upload = $this->create_features($title,$pic_name,$link,$start_date,$end_date);
                       if($do_upload)
                       {
                        redirect('admin/Features');
                        echo "Records persisted";
                         return 1;
                       }
                       else
                       {
                         echo "Records not persisted";
                        redirect('admin/Features');
                         return 0;
                       }

                       echo "file uploaded";
               }
  }
  public function create_features($title,$image_url,$link,$start_date,$end_date)
  {
    $data = array(
      'title'=>$title,
      'image_url'=>$image_url,
      'link'=>$link,
      'start_date'=>$start_date,
      'end_date' => $end_date
    );
    $query = $this->db->insert('features',$data);
    return $query;
  }
  //Retrieve features on a daily basis
  public function retrieve_features()
  {
    $date = date('Y/M/D');
    // $query = $this->db->query("SELECT * FROM features WHERE date = $date")->result_array();
    $query = $this->db->query("SELECT * FROM features WHERE end_date <= $date")->result_array();
    return $query;
  }
}
?>

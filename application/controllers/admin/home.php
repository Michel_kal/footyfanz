<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
        }
    }
 public function index_static($param = '') {
     $data['menu']="1-1";
      $data['articles']="5";
      $data['cats']="5";
      $data['weekly']="5";
      $data['views']="5";
      $data['users']="5";
      $data['ads']="5";
     
 $this->viewer->aview('home.php', $data);
 }
    public function index($param = '') {
        $time = getTimestamp();
        $timestamp_7 = strtotime($time) - (7 * 24 * 60 * 60);
        $data = array();
        $data['menu'] = "1-1";
        $artcles = $this->db->query("SELECT count(*) AS cnt FROM post WHERE status='1' AND is_deleted='0' AND post_type='N'")->result_array();
        $data['news'] = $artcles[0]['cnt'];
         $artcles = $this->db->query("SELECT count(*) AS cnt FROM post WHERE status='1' AND is_deleted='0' AND post_type='A'")->result_array();
        $data['artcles'] = $artcles[0]['cnt'];
        $artcles = $this->db->query("SELECT count(*) AS cnt FROM post WHERE status='1' AND is_deleted='0' AND post_type='N' AND sponser_status='1'")->result_array();
        $data['news_sp'] = $artcles[0]['cnt'];
        $artcles = $this->db->query("SELECT count(*) AS cnt FROM post WHERE status='1' AND is_deleted='0' AND post_type='N' AND featured_status='1'")->result_array();
        $data['news_fe'] = $artcles[0]['cnt'];
        
        $users = $this->db->query("SELECT count(*) AS cnt FROM users WHERE status='1' AND user_role='U' AND is_deleted='0' ")->result_array();
        $data['users'] = $users[0]['cnt'];
        
        $users = $this->db->query("SELECT count(*) AS cnt FROM users WHERE status='1' AND user_role='U' AND is_deleted='0' AND premium='1'")->result_array();
        $data['users_pre'] = $users[0]['cnt'];
        
        $lgs = $this->db->query("SELECT count(*) AS cnt FROM master_league WHERE status='1'")->result_array();
        $data['league'] = $lgs[0]['cnt'];
        $clb = $this->db->query("SELECT count(*) AS cnt FROM master_club WHERE status='1'")->result_array();
        $data['club'] = $clb[0]['cnt'];
        
        $ads = $this->db->query("SELECT count(*) AS cnt FROM ads WHERE status='1' AND start_date <= '" . date("Y-m-d") . "' AND end_date >= '" . date("Y-m-d") . "'")->result_array();
        $data['ads'] = $ads[0]['cnt'];

        $artcles_view = $this->db->query("SELECT count(*) AS cnt FROM post_views WHERE created_at>='" . date("Y-m-d", $timestamp_7) . "'")->result_array();
        $data['weekly'] = $artcles_view[0]['cnt'];

        $views = $this->db->query("SELECT count(*) AS cnt FROM post_views")->result_array();
        $data['views'] = $views[0]['cnt'];
       

        $data['css'] = array("plugins/bootstrap-daterangepicker/daterangepicker.css");
        $data['js'] = array("../../assets/plugins/bootstrap-daterangepicker/date.js", "../../assets/plugins/bootstrap-daterangepicker/daterangepicker.js", "home.js", "../../assets/plugins/highchart/highcharts.js", "../../assets/plugins/highchart/highcharts-more.js", "../../assets/plugins/highchart/modules/exporting.js");



        $this->viewer->aview('home.php', $data);
    }

    public function status_graph() {
        $data = $this->input->post();
        // Graph Data
        $views = $this->db->query("SELECT count(*) AS cnt,DATE_FORMAT(`created_at`,'%Y-%m-%d') as `date` FROM post_views WHERE DATE_FORMAT(`created_at`,'%Y-%m-%d') >= '" . $this->input->post('dt_from') . "' AND DATE_FORMAT(`created_at`,'%Y-%m-%d') <= '" . $this->input->post('dt_to') . "' GROUP BY DATE_FORMAT(`created_at`,'%Y-%m-%d')");
        $data['graph'] = $views->result_array();
        $this->viewer->aview('graph.php', $data, false);
    }

}

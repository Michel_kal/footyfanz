<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Images extends CI_Controller {

    public $typemenu = array("square" => "1", "small" => "2", "medium" => "3", "large" => "4");

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("auth_model");
        $this->load->model("master_model");
        $this->load->model("poll_model");
        $this->load->model("image_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
        }
    }

    public function index($param = '') {
        if ($param == "bg") {
            $data['menu'] = "12-1";
        }
        if ($param == "head") {
            $data['menu'] = "11-1";
        }
        $data['type'] = $param;
        $data['js'] = array("master.js", "jquery.form.js", "../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js");
        $data['css'] = array("plugins/bootstrap-colorpicker/css/colorpicker.css");
        $this->viewer->aview('images/index.php', $data);
    }

    public function updatecolor() {
        $this->db->update("images", array("color" => $this->input->post("value")), array("pk_image_id" => $this->input->post("id")));
        echo json_encode(array('status' => '1', 'title' => "Background", 'text' => "Color has been updated"));
    }

    public function uploadmedia() {

        if (isset($_FILES['logo']) and $_FILES['logo']['error'] == 0) {
            $name = $_FILES['logo']['name'];
            $pathinfo = pathinfo($name);
            $realname = $pathinfo['basename'];
            if (strtolower($pathinfo['extension']) == "php") {
                echo json_encode(array("status" => "0", "msg" => "Invalid file selected"));
                exit();
            }

            $CI = & get_instance();
            $config['allowed_types'] = "*";
            $config['upload_path'] = MEDIA_DIR;
            $config['max_size'] = '0';
            $config['max_width'] = '0';
            $config['max_height'] = '0';
            $config['file_name'] = "media_" . strtotime("now") . rand("10", "99");
            $CI->load->library('upload', $config);
            $CI->upload->initialize($config);
            if (!$CI->upload->do_upload("logo")) {
                echo json_encode(array("status" => "0", "msg" => $CI->upload->display_errors()));
            } else {
                $data = $CI->upload->data();
                $filename = $data['file_name'];
                $data = array('image_type' => $this->input->post("type"), "path" => $filename);
                $this->db->insert("images", $data);
                echo json_encode(array("status" => "1", "msg" => "File uploaded successfully"));
            }
        } else {
            echo json_encode(array("status" => "0", "msg" => "No file selected"));
        }
    }

    public function edit_poll($id) {
        $data['menu'] = "9-1";
        $data['poll'] = $this->db->get_where("polls", array("pk_poll_id" => $id))->row_array();
        $this->viewer->aview('poll/edit_poll.php', $data);
    }

    public function image_list() {
        $page = $this->input->post('page');
        $type = $this->input->get("imagetype");
        $perpage = PAGING_MED;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }
        $data = $this->image_model->getImageList($page, $perpage, $searchKey, $this->input->get("imagetype"));
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]['placement'] = $this->poll_model->getPlacement($type, $value['pk_image_id']);
        }
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $data['type'] = $type;
        $this->viewer->aview('images/image_list.php', $data, false);
    }

    public function get_clubs() {
        $league_id = $this->input->post("league_id");
        $data['curr_clubs'] = explode(",", $this->input->post('curr_clubs'));
        $data['clubs'] = $this->master_model->getClubKVByLeague($league_id, '1');
        $data['type'] = 'check_selected';
        $this->viewer->aview('utility/clubs.php', $data, false);
    }

    public function actions() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->update("polls", array('status' => $status), array("pk_poll_id" => $id));
        if ($status == '1') {
            echo json_encode(array('status' => '1', 'title' => "Poll status", 'text' => "Poll has been activated"));
        } else {
            echo json_encode(array('status' => '1', 'title' => "Poll status", 'text' => "Poll has been deactivated"));
        }
    }

    public function deleteaction() {
        $id = $this->input->post('id');
        $status = $this->input->post('action');
        $this->db->delete("images", array("pk_image_id" => $id));
        @unlink(MEDIA_DIR . $this->input->post('image'));
        echo json_encode(array('status' => '1', 'title' => "Image status", 'text' => "Image has been deleted"));
    }

}

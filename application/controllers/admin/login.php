<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->model('auth_model');
        $this->load->helper('cookie');
    }

    public function index($param = '') {
        $this->viewer->aview('login/index.php', array(), false);
    }

    public function verify() {
        $username = $this->input->post("txt_username");
        $password = $this->input->post("txt_password");
        $data = $this->auth_model->verifyAdmin($username, $password);
        if ($data) {
            $result = array('status' => 'success', 'page' => 'admin');
            $newdata = array(
                'admin_id' => $data['pk_user_id'],
                'admin_name' => $data['firstname']." ".$data['lastname'],
                'admin_email' => $data['email'],
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            echo json_encode($result);
        } else {
            echo json_encode(array('status' => 'invalid', 'msg' => "Wrong Username or Password"));
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect("admin");
    }

}

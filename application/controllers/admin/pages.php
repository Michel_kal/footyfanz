<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("cms_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
            exit();
        }
    }

    public function index($param = '') {
        $data['menu'] = "9-1";
        $data['pages'] = $this->db->get("pages")->result_array();
        $this->viewer->aview('pages/index.php', $data);
    }

    public function pages() {
        $data['pages'] = $this->db->get("menus")->result_array();
        $this->viewer->aview('pages/pages.php', $data);
    }

    public function page_add() {
        $data = array();
        $this->viewer->aview('modal/page_add.php', $data, FALSE);
    }

    public function page_edit($id) {
        $data['page'] = $this->db->get_where("pages", array("pk_page_id" => $id))->result_array();
        $this->viewer->aview('modal/page_edit.php', $data, FALSE);
    }

    public function page_add_action() {
        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $this->input->post('slug'),
            'menu_order' => $this->input->post('order'),
            'header_meta' => '<meta name="description" content=" " />
<meta name="keywords" content="" />'
        );
        $this->db->insert("pages", $data);
    }

    public function pade_delete() {
        $this->db->delete("pages", array('pk_page_id' => $this->input->post('id')));
    }

    public function page_status($id, $status) {
        $this->db->update("pages", array("status" => $status), array('pk_page_id' => $id));
    }

    public function page_edit_action() {
        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $this->input->post('slug'),
            'menu_order' => $this->input->post('order')
        );
        $this->db->update("pages", $data, array('pk_page_id' => $this->input->post('pk_page_id')));
    }

    public function editor($id) {
        $page = $this->db->get_where("pages", array("pk_page_id" => $id))->result_array();
        $data['content'] = $page[0];
        $data['menu'] = "9-1";
        $data['js'] = array("../plugins/ckeditor/ckeditor.js");
        $this->viewer->aview('pages/editor.php', $data);
    }

    public function saveeditor() {
        $id = $this->input->post("pk_page_id");
        $content = $this->input->post("content");
        $header_meta = $this->input->post("header_meta");
        $this->db->update("pages", array("header_meta" => $header_meta, "content" => $content), array('pk_page_id' => $id));
    }

}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Placement extends CI_Controller {

    public $typemenu = array("poll" => "9", "head" => "11", "bg" => "12");

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("auth_model");
        $this->load->model("master_model");
        $this->load->model("ads_model");
        $this->load->model("cms_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
        }
    }

    public function manager($type = "") {
        $data['menu'] = $this->typemenu[$type] . "-1";
        $data['type'] = $type;
        $this->viewer->aview('ads/index.php', $data);
    }

    public function manage($type, $id, $index) {
        $data['sub_types'] = array('2' => "All", '1' => "Home", '3' => "Newsfeed", '4' => 'Articles', '5' => 'Clubs', '6' => 'League', '7' => "Footyfanz+");
        $data['menu'] = "7-" . $this->typemenu[$type];
        $data['type'] = $type;
        $data['id'] = $id;
        if ($type == "poll") {
            $data['data'] = $this->db->get_where("polls", array('pk_poll_id' => $id))->row_array();
        }
        if ($type == "bg" or $type == "head") {
            $data['data'] = $this->db->get_where("images", array('pk_image_id' => $id))->row_array();
        }


        $data['location'] = $this->getLocation($type, $id);
        $data['current_club_league'] = "0";
        if (isset($data['location'][5])) {
            if (is_array($data['location'][5])) {
                $ldata = $this->db->get_where("master_club", array('pk_club_id' => $data['location'][5][0]))->result_array();
                if (count($ldata) > 0) {
                    $data['current_club_league'] = $ldata[0]['fk_league_id'];
                }
            }
        }
        $data['leagues'] = $this->master_model->getLeagueKV("1");
        $data['index'] = $index;
        $this->viewer->aview('placement/manage.php', $data);
    }

    function getLocation($type, $id) {

        $arr = $this->db->query("SELECT * FROM content_placement WHERE content_type='$type' AND fk_ad_id='$id' ORDER BY type")->result_array();
        $loc = array();
        foreach ($arr as $key => $ar) {
            $loc[$ar['type']][] = $ar['ref_id'];
        }

        return $loc;
    }

    public function get_clubs() {
        $league_id = $this->input->post("league_id");
        $data['curr_clubs'] = explode(",", $this->input->post('curr_clubs'));
        $data['clubs'] = $this->master_model->getClubKVByLeague($league_id, '1');
        $data['type'] = 'check_selected';
        $this->viewer->aview('utility/clubs.php', $data, false);
    }

    public function update_location() {
        $post = $this->input->post();
        $save_data = array();
        $contype = $this->input->post("type");
        $id = $this->input->post("id");
        $post['ad_id'] = $id;
        $sub_types = $post['sub_type'];
        $this->db->delete("content_placement", array("fk_ad_id" => $id, "content_type" => $contype));
        if (is_array($sub_types) and count($sub_types) > 0) {

            foreach ($sub_types as $type) {
                if ($type < 5) {
                    $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => 0, "content_type" => $contype);
                } else if ($type == 5) {
                    if ($post['club_league'] == 0) {
                        $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => 0, "content_type" => $contype);
                    } else {
                        if (isset($post['club']) and count($post['club']) > 0) {
                            $clubs = $post['club'];
                        } else {
                            $clubs_by_league = $this->master_model->getClubKVByLeague($post['club_league'], '1');
                            $clubs = array_keys($clubs_by_league);
                        }
                        if (is_array($clubs) and count($clubs) > 0) {
                            foreach ($clubs as $club) {
                                $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => $club, "content_type" => $contype);
                            }
                        }
                    }
                } else if ($type == 6) {
                    if (isset($post['leagues']) and count($post['leagues']) > 0) {
                        foreach ($post['leagues'] as $league) {
                            $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => $league, "content_type" => $contype);
                        }
                    }
                } else if ($type == 7) {
                    $save_data[] = array("fk_ad_id" => $post['ad_id'], "type" => $type, "ref_id" => 0, "content_type" => $contype);
                }
            }
            $this->db->insert_batch("content_placement", $save_data);
//            print_r($save_data);
        }
    }

}

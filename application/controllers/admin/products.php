<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model('master_model');
        $this->load->model('product_model');
        if ($this->session->userdata('logged_in') == "") {
            echo '<script>window.location="' . base_url() . 'admin/";</script>';
            exit();
        }
    }

    public function index() {
        $categories = $this->master_model->getCategoriesKV("1");
        $this->viewer->aview('products/index.php', array('menu' => "4-2", 'categories' => $categories));
    }

    public function product_loader() {
        $data = $this->input->post();
        $this->viewer->aview('products/product_loader.php', $data, false);
    }

    public function product_list() {
        $page = $this->input->post('page');
        $category = $this->input->get('category');
        $status = $this->input->get('status');
        $top = $this->input->get('top');
        $perpage = PAGING_MED;
        if (isset($_GET['sk'])) {
            $searchKey = $_GET['sk'];
        } else {
            $searchKey = "";
        }

        $data = $this->product_model->getProductsList($page, $perpage, $searchKey, $status, $category, $top);
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->aview('products/product_list.php', $data, false);
    }

    public function add() {
        $data['categories'] = $this->master_model->getCategoriesKV("1");
        $data['menu'] = "4-1";
        $this->viewer->aview('products/add.php', $data);
    }

    public function product_save() {
        $id = $this->input->post('pk_product_id');
        if (isset($_FILES['logo']) and $_FILES['logo']['error'] == 0) {
            $config['path'] = PIMAGE_DIR . "/";
            $config['type'] = 'gif|jpg|png';
            $config['width'] = "300";
            $config['prefix'] = "logo";
            $config['file_name'] = "logo";
            $file = uploadFile($config);
            $data['image'] = $file;
        }
        $data['title'] = $this->input->post('title');
        $data['fk_category_id'] = $this->input->post('fk_category_id');
        $data['description'] = $this->input->post('description');
        $data['price_label'] = $this->input->post('price_label');
        if ($id == "") {
            $id = $this->db->insert("products", $data);
        } else {
            if (isset($data['image'])) {
                $product = $this->product_model->getProductsById($id);
                @unlink(PIMAGE_DIR . $product['image']);
            }
            $this->db->update("products", $data, array("pk_product_id" => $id));
        }
    }

    public function edit_form($id) {
        $data['categories'] = $this->master_model->getCategoriesKV("1");
        $data['menu'] = "4";
        $data['product'] = $this->product_model->getProductsById($id);
        $this->viewer->aview('products/edit.php', $data);
    }

    public function product_available($status) {
        $id = $this->input->post("id");
        $this->db->update("products", array("available" => $status), array("pk_product_id" => $id));
        if ($status == 1) {
            $res = array("title" => "Product", "text" => "Product has been Available");
        } else {
            $res = array("title" => "Product", "text" => "Product has been Not Available");
        }
        echo json_encode($res);
    }

    public function product_status($status) {
        $id = $this->input->post("id");
        $this->db->update("products", array("status" => $status), array("pk_product_id" => $id));
        if ($status == 1) {
            $res = array("title" => "Product", "text" => "Product has been Activated");
        } else {
            $res = array("title" => "Product", "text" => "Product has been Not Deactivated");
        }
        echo json_encode($res);
    }

    public function product_delete() {
        $id = $this->input->post("id");
        $product = $this->product_model->getProductsById($id);
        @unlink(PIMAGE_DIR . $product['image']);
        $this->db->delete("products", array("pk_product_id" => $id));
        $res = array("title" => "Product", "text" => "Product has been Deleted");
    }

    public function price_delete() {
        $id = $this->input->post("id");
        $this->db->delete("prices", array("pk_price_id" => $id));
        $res = array("title" => "Product", "text" => "Price has been Deleted");
    }

    public function add_price($id) {
        if ($id == "") {
            exit();
        }
        $this->viewer->aview('products/price_add.php', array("id" => $id), false);
    }

    public function price_save() {
        $save = array(
            'fk_product_id' => $this->input->post("id"),
            'size' => $this->input->post("product_size"),
            'mrp' => $this->input->post("product_mrp"),
            'price' => $this->input->post("product_price")
        );
        $this->db->insert("prices", $save);
        echo json_encode(array("title" => "Product Price", "text" => "Price saved successfully."));
    }

}

<?php

class Seo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        $this->load->model("master_model");
    }

    public function sitemapnews($page = "1") {
        $page = $page - 1;
        $per_page = "50000";
        $start = $page * $per_page;
        $posts = $this->db->query("SELECT pk_post_id,slug FROM post  WHERE post_type='N' AND status=1 AND is_deleted=0 LIMIT $start,$per_page");
        $data = $posts->result_array();
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->viewer->aview("seo/sitemapnews.php", array('data' => array("post" => $data)), false);
    }

    public function sitemaparticle($page = "1") {
        $page = $page - 1;
        $per_page = "50000";
        $start = $page * $per_page;
        $posts = $this->db->query("SELECT pk_post_id,slug FROM post  WHERE post_type='A' AND status=1 AND is_deleted=0 LIMIT $start,$per_page");
        $data = $posts->result_array();
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->viewer->aview("seo/sitemaparticle.php", array('data' => array("post" => $data)), false);
    }

    public function sitemapuser($page = "1") {
        $page = $page - 1;
        $per_page = "50000";
        $start = $page * $per_page;
        $posts = $this->db->query("SELECT pk_user_id,slug FROM users WHERE status=1 AND is_deleted=0 LIMIT $start,$per_page");
        $data = $posts->result_array();
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->viewer->aview("seo/sitemapuser.php", array('data' => array("post" => $data)), false);
    }

    public function sitemapclub($page = "1") {
        $page = $page - 1;
        $per_page = "50000";
        $start = $page * $per_page;
        $posts = $this->db->query("SELECT pk_club_id,slug FROM users WHERE status=1 AND is_deleted=0 LIMIT $start,$per_page");
        $data = $posts->result_array();
        header("Content-Type: text/xml;charset=iso-8859-1");
        $this->viewer->aview("seo/sitemapclub.php", array('data' => array("post" => $data, "categories" => $categories)), false);
    }

}

?>

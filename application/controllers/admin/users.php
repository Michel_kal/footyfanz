<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");

        $this->load->model("master_model");
        $this->load->model("user_model");
        $this->load->model("login_model");
        if ($this->session->userdata('admin_id') == "") {
            redirect("admin/login");
            exit();
        }
    }

    public function index($param = '') {
        $data['menu'] = "8-1";
        $data['js'] = "master.js";
//        $profile1 = $this->user_model->getUserHighlights("3");
//        print_r($profile1);

        $this->viewer->aview('users/index.php', $data);
    }

    public function user_add($param = '') {
        $this->viewer->aview('modal/user_add.php', array(), false);
    }

    public function add($param = "") {
        $name = $this->input->post("name");
        $email = $this->input->post("email");
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        $tag = $this->input->post("tag");
        if ($username != "") {
            $exist = $this->login_model->checkUsernameExist($username);
            if ($exist) {
                echo json_encode(array("status" => "0", "msg" => "Username already registered"));
            } else {
                if (preg_match('/[^a-z_\-0-9]/i', $username)) {
                    echo json_encode(array("status" => "0", "msg" => "Username should be alphanumeric only."));
                } else {
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        echo json_encode(array('status' => '0', "msg" => 'Invalid email format'));
                        exit();
                    }
                    if ($this->login_model->checkEmailExist($email)) {
                        echo json_encode(array("status" => "0", "msg" => "Email already registered"));
                        exit();
                    }
                    $code = time();
                    $data = array(
                        'name' => $name,
                        'email' => $email,
                        'vefitation_code' => $code,
                        'verification_status' => "1",
                        'username' => $username,
                        'password' => $password,
                        'tagboard_allowed' => '1'
                    );

                    $this->db->insert("users", $data);
                    $id = $this->db->insert_id();
                    $this->db->insert("tagboards", array("user_id" => $id, "tag" => $tag, 'publish_status' => "1", "approved_status" => "1"));
                    //$this->sendVerificationMail($name, $email, $code);
                    //$this->start_login($id, "skm");
                    echo json_encode(array("status" => "1", "msg" => "Registration successfull.", "id" => $id));
                }
            }
        }
    }

    public function updatestatus($status) {

        $user = $this->db->get_where('users', array("pk_user_id" => $this->input->post("id")))->result_array();

        $this->db->update('users', array("status" => $status), "pk_user_id =" . $this->input->post("id"));
        if ($status == 1) {
            $res = array("title" => "User Update", "text" => "User has been Activated");
        } else if ($status == 2) {
            $res = array("title" => "User Update", "text" => "User has been Deactivated");
        }
        if ($status == 1 and $user[0]['status'] == 0) {
            $res = array("title" => "User Update", "text" => "User's Email has been Verified");
        }


        echo json_encode($res);
    }

    public function premstatus($status) {

        $this->db->update('users', array("premium" => $status), "pk_user_id =" . $this->input->post("id"));
        if ($status == 1) {
            $res = array("title" => "User Update", "text" => "User has been Marked as Premium");
        } else {
            $res = array("title" => "User Update", "text" => "User has been Removed from Premium");
        }

        echo json_encode($res);
    }

    public function ffstatus($status) {
        $this->db->update('users', array("ff_plus" => $status), "pk_user_id =" . $this->input->post("id"));
        if ($status == 1) {
            $res = array("title" => "User Update", "text" => "User has been Marked as FootyFanz+ Member");
        } else {
            $res = array("title" => "User Update", "text" => "User has been Removed from FootyFanz+ Membership");
        }

        echo json_encode($res);
    }

    public function ffverifystatus($status) {
        $this->db->update('users', array("ff_verify" => $status), "pk_user_id =" . $this->input->post("id"));
        if ($status == 1) {
            $res = array("title" => "User Update", "text" => "User has been Verified as FootyFanz+");
        } else {
            $res = array("title" => "User Update", "text" => "User has been Unverified as FootyFanz+");
        }

        echo json_encode($res);
    }

    public function resendmail() {
        $user = $this->db->get_where('users', array("pk_user_id" => $this->input->post("id")))->row_array();
        $email = $user['email'];
        $data['name'] = $user['firstname'] . " " . $user['lastname'];
        $data['email'] = $user['email'];
        $data['vcode'] = $user['email_vcode'];

        $data_eamil = $this->viewer->emailview("signup.php", array('data' => $data, "verification" => ""));

        $this->load->library('email');
        $config = getEmailConfig();
        $this->email->initialize($config);
        $this->email->from(MAIL_USER, SITE_NAME);
        $this->email->subject(SITE_NAME . " Verification");
        $this->email->message($data_eamil);
        $this->email->to($email);
        $this->email->send();
        $res = array("title" => "Email Verification", "text" => "Verification email send");
        echo json_encode($res);
    }

    public function profile($user_id = '') {
        if ($user_id == '') {
            exit();
        }
        $data['menu'] = "8";
        $data['clubs'] = $this->master_model->getClubKV();
//        $data['user'] = $this->user_model->getUserById($user_id);
        $data['pos'] = $this->user_model->getUserPosition($user_id);
        $data['user'] = $this->user_model->getUserDetails($user_id);
        $data['league'] = $this->master_model->getLeagueByID($data['user']['fk_league_id']);
        $data['uh'] = $this->user_model->getUserHighlights($user_id);
        $data['followers'] = $this->user_model->getFollowers($user_id);
        $data['followings'] = $this->user_model->getFollowings($user_id);
        $this->viewer->aview('users/profileview.php', $data);
    }

    public function loader() {
        $this->viewer->aview('users/loader.php', $this->input->post(), false);
    }

    public function loaderhash() {
        $this->viewer->aview('users/loaderhash.php', $this->input->post(), false);
    }

    public function listing() {
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $searchKey = isset($_GET['sk']) ? $_GET['sk'] : "";
        $vtype = isset($_GET['vtype']) ? $_GET['vtype'] : "";
        $data = $this->user_model->getUserList($page, $perpage, $searchKey, $vtype);
        $data['clubs'] = $this->master_model->getClubKV();
        $data['page'] = getPaginationFooter($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->aview('users/listing.php', $data, false);
    }

    public function deleteuser() {
        $id = $this->input->post('id');
        $this->db->delete("users", array("pk_user_id" => $id));
        echo json_encode(array('status' => '1', 'title' => "User status", 'text' => "User has been deleted"));
    }

    public function downloaduser() {
        $data = $this->db->query("SELECT users.firstname,users.lastname,users.email, user_profile.gender, user_profile.phone_no, master_club.title FROM users "
                        . "LEFT OUTER JOIN user_profile ON user_profile.fk_user_id=users.pk_user_id "
                        . " LEFT OUTER JOIN master_club ON master_club.pk_club_id=user_profile.fk_club_id  "
                        . " WHERE users.is_deleted='0'")->result_array();

        $exp = array();
        foreach ($data as $key => $dt) {
            $exp[$key]['firstname'] = $dt['firstname'];
            $exp[$key]['lastname'] = $dt['lastname'];
            $exp[$key]['email'] = $dt['email'];
            $exp[$key]['gender'] = $dt['gender'];
            $exp[$key]['phone_no'] = $dt['phone_no'];
            $exp[$key]['title'] = $dt['title'];
        }

        function cleanData(&$str) {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if (strstr($str, '"'))
                $str = '"' . str_replace('"', '""', $str) . '"';
        }

        // filename for download
        $filename = "users_" . date('Ymd') . ".xls";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");

        $flag = false;
        foreach ($exp as $row) {
            if (!$flag) {
                // display field/column names as first row
                echo implode("\t", array_keys($row)) . "\r\n";
                $flag = true;
            }
            array_walk($row, __NAMESPACE__ . '\cleanData');
            echo implode("\t", array_values($row)) . "\r\n";
        }
        exit;
    }

}

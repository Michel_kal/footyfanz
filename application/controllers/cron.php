<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
        $this->load->model('user_model');
    }

    public function getClubWiseUsersPoint(){
        $dm= explode("-", date("Y-m"));
        $y=$dm[0];
        $m=$dm[1];
        if($m>1){
            $m=$m-1;
        }else{
            $m=12;
            $y=$y-1;
        }
        $m=str_pad($m, 2, "0", STR_PAD_LEFT);
        
//        echo "$y-$m";
//        exit();
        // AND DATE_FORMAT(created_at,'%Y-%m') = '$y-$m'
         $points_month = $this->db->query("SELECT ("
                 . "SELECT sum(points) as points FROM user_points WHERE user_points.fk_user_id=user_profile.fk_user_id AND DATE_FORMAT(created_at,'%Y-%m') = '$y-$m'"
                 . ") AS points,fk_user_id,fk_club_id FROM user_profile WHERE user_profile.fk_club_id>0 ORDER BY points DESC")->result_array();        
         $records=array();
         foreach($points_month as $pt){
             if($pt['points']>0){
                 $records[$pt['fk_club_id']][$pt['fk_user_id']]=$pt['points'];
             }             
         }
         $this->db->delete("laurals",array("l_month"=>$m, "l_year"=>$y));
         $insertAll=array();
         foreach($records as $club=>$rc){
             $count=1;
             foreach($rc as $user=>$points){
                if($count>3){
                    continue;
                }
                $insertAll[]=array(
                    "l_month"=>$m,
                    "l_year"=>$y,
                    "points"=>$points,
                    "fk_user_id"=>$user,
                    "position"=>$count
                );
                $count++;
            }
         }
         if(count($insertAll)>0){
             $this->db->insert_batch('laurals',$insertAll);
         }         
    }
}

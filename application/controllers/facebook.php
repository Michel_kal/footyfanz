<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Facebook extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('master_model');
        $this->load->model('login_model');
    }

    public function connect() {
        $app_id = FB_APP;
        $app_secret = FB_SECRET;
        $my_url = $this->input->get('url');
        if ($my_url == '') {
            $my_url = base_url();
        }
        $fb_url = base_url() . "facebook/connect";
        $code = $this->input->get('code');
        $rstate = $this->input->get('state');
        $sstate = $this->session->userdata('state');
        
        
        if (empty($code)) {
            $state = md5(uniqid(rand(), TRUE)); //CSRF protection
            $this->session->set_userdata(array('state' => $state));
            
            $dialog_url = "https://www.facebook.com/dialog/oauth?client_id="
                    . $app_id . "&redirect_uri=" . urlencode($fb_url) . "&state="
                    . $state . "&scope=email,public_profile";
            
       
            redirect($dialog_url);
        }
        //echo $code;

        if ($rstate == $sstate) {
            $token_url = "https://graph.facebook.com/oauth/access_token?"
                    . "client_id=" . $app_id . "&redirect_uri=" . urlencode($fb_url)
                    . "&client_secret=" . $app_secret . "&code=" . $code;

            $response = file_get_contents($token_url);


            $params = null;
            parse_str($response, $params);
            $graph_url = "https://graph.facebook.com/me?access_token="
                    . $params['access_token'] . "&fields=first_name,last_name,email,name";
            $user = json_decode(file_get_contents($graph_url));
            if(isset($user->email)){
                $res = $this->db->get_where('users', array('email' => $user->email));
            }else{
                $res = $this->db->get_where('users', array('fb_id' => $user->id));
            }

            
            //   var_dump($user);
//            debug($res);
            // exit();

            if ($res->num_rows > 0) {
                $users = $res->result_array();
                $data = $users[0];

                $profile = $this->db->get_where("user_profile", array("fk_user_id" => $data['pk_user_id']))->result_array();
                if ($profile[0]['profile_pic'] == "") {
                    $img = $this->saveProfileImage("https://graph.facebook.com/" . $user->id . "/picture?type=large");
                    $this->db->update("user_profile", array("profile_pic" => $img), array("fk_user_id" => $data['pk_user_id']));
                }
                if($data['status']!="2"){
                    $this->start_login($data['pk_user_id'], "skm");
                    echo '<script>window.opener.afterLogin("1","' . $data['pk_user_id'] . '");</script>';
                }else{
                   echo '<script>window.opener.afterLogin("0","0");</script>';
                }
            } else {
                $img = $this->saveProfileImage("https://graph.facebook.com/" . $user->id . "/picture?type=large");

                
                
                $parts = explode(" ", $user->name);
                $lastname = array_pop($parts);
                $firstname = implode(" ", $parts);
                if(isset($user->email)){
                     $emailsplit = explode("@", $user->email);
                }else{
                    $user->email="";
                    $emailsplit = explode("@", $user->name);
                    $emailsplit[0]= $emailsplit[0];
                }
               

                $newdata = array(
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    "username" => ($this->login_model->checkUsernameExist($emailsplit[0])) ? ($emailsplit[0] . time()) : $emailsplit[0],
                    'email' => $user->email,
                    'password' => rand("1111", "9999"),
                    'user_role' => 'U',
                    'fb_id' => $user->id,
                    'status' => "1",
                    'created_at' => getTimestamp()
                );

                $this->db->insert("users", $newdata);
                $user_id = $this->db->insert_id();
                $this->load->library('slug');
                $this->slug->set_config(array('table' => 'users', 'field' => 'slug', 'title' => 'username', 'id' => 'pk_user_id'));
                $rslug = $this->slug->create_uri($newdata['username'], $user_id);
                $this->db->update("users", array('slug' => $rslug), array('pk_user_id' => $user_id));
                $profile = array(
                    'fk_user_id' => $user_id,
                    'gender' => 'Male',
                    'profile_pic' => $img
                );
                $this->db->insert("user_profile", $profile);
                $this->start_login($user_id, "skm");
                echo '<script>window.opener.afterLogin("1","' . $user_id . '");</script>';
            }
        } else {
            echo("The state does not match. You may be a victim of CSRF.");
        }
    }

    public function view($id) {
        $id = _decode($id);
        echo $id;
    }

    public function start_login($id, $pass) {
        if ($pass != "skm") {
            exit();
        }
        $data = $this->login_model->getUser($id);
        if (is_file(PUBLIC_DIR . PROFILE_IMG_DIR . $data['profile_pic'])) {
            $data['image'] = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $data['profile_pic'];
        } else {
            $data['image'] = site_url() . "public/fassets/images/user.jpg";
        }
        $sessiondata = array(
            'user_id' => $data['pk_user_id'],
            'email' => $data['email'],
            'name' => $data['firstname'] . " " . $data['lastname'],
            'image' => $data['image'],
            'club_id' => $data['fk_club_id'],
            'club_slug' => $data['club_slug'],
            'league_id' => $data['fk_league_id'],
            'site_league' => $data['fk_league_id'],
            "profile_status" => $data['profile_status'],
            "club_image" => $data['club_image'],
            "utype" => $data['user_role'],
            "slug" => $data['slug']
        );

        $this->session->set_userdata($sessiondata);
        return $data['profile_status'];
        // print_r($sessiondata);
        // exit();
    }

    public function saveProfileImage($url) {
        $content = file_get_contents($url);
//Store in the filesystem.
        $img = time() . ".jpg";
        $fp = fopen(PUBLIC_DIR . PROFILE_IMG_DIR . $img, "w");
        fwrite($fp, $content);
        fclose($fp);
        @copy(PUBLIC_DIR . PROFILE_IMG_DIR . $img, IMG_PROFILE ."thumb/". $image_name);
        resizeFile(IMG_PROFILE ."thumb/". $image_name, 50);
        return $img;
    }

}

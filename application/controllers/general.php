<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class General extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
    }

    public function page_404($param = '') {
        $this->viewer->fview('general/page_404.php', array());
    }

    public function terms($param = '') {
        $this->viewer->fview('general/terms.php', array('tmenu' => '1'));
    }

    public function legal($param = '') {
        $this->viewer->fview('general/legal.php', array('tmenu' => '2'));
    }

    public function privacy($param = '') {
        $this->viewer->fview('general/privacy.php', array('tmenu' => '3'));
    }

    public function feedback($param = '') {
        $this->viewer->fview('general/feedback.php', array('tmenu' => '4'));
    }

    public function faq($param = '') {
        $this->viewer->fview('general/faq.php', array('tmenu' => '4'));
    }

    public function contact() {
        $data = $this->input->post();
        $this->sendContactEmail($data);
        echo json_encode(array('status' => 'success', 'msg' => (($data['type'] == 'F') ? 'Feedback ' : 'Contact') . ' has been send to College Media.'));
    }

    public function sendContactEmail($data) {

        $data_eamil = $this->viewer->emailview("contactus.php", array('data' => $data));
        //echo $data_eamil;
        $this->load->library('email');
        $config = getEmailConfig();
        $this->email->initialize($config);
        $this->email->from(ADMIN_EMAIL, SITE_NAME);
        $this->email->subject(SITE_NAME . (($data['type'] == 'F') ? '- Feedback' : '- Contact'));
        $this->email->message($data_eamil);
        $this->email->to(CONTACT_EMAIL);
        $this->email->send();
        $this->email->to('rowdypundir@gmail.com');
        $this->email->send();
    }

    public function captcha($type) {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        for ($i = 0; $i < 5; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        $this->session->set_userdata(array($type => $string));
        $im = imagecreatetruecolor(65, 34);
        $bg = imagecolorallocate($im, 165, 86, 22); //background color blue
        $fg = imagecolorallocate($im, 255, 255, 255); //text color white
        imagefill($im, 0, 0, $bg);
        imagestring($im, 12, 8, 8, $string, $fg);
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-type: image/png');
        imagepng($im);
        imagedestroy($im);
    }

}

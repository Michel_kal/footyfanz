<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
        $this->load->model('article_model');
        $this->load->model('gallery_model');
        $this->load->model('poll_model');
    }

    public function index($param = '') {
        if ($this->session->userdata('user_id') == "") {
            redirect("home/signup/index");
        }
        $league_filter = "(fk_league_id='" . $this->session->userdata('site_league') . "' OR fk_league_id='0') AND ";
        $data = array('css' => "update_assets/css/jquery-confirm.min.css", 'js' => 'update_assets/js/jquery-confirm.min.js');
        $data['hp_news'] = $this->article_model->getHomePageArticle("$league_filter post_type='N' ORDER BY fk_league_id DESC, pk_post_id DESC LIMIT 0,4");
        $data['hp_videos'] = $this->gallery_model->getGalleryVideos(50);
        $league_id = ($this->session->userdata('site_league') > 0) ? $this->session->userdata('site_league') : 1;
        $wrt_data = $this->article_model->getWritersByPointsFilter($league_id, 0, 13);
        $data['hp_writers'] = $wrt_data['wdata'];
        $fan_data = $this->article_model->getTopFanzFilter($league_id, 0, 17);
        $data['hp_fanz'] = $fan_data['fan_data'];
        $data['home_page'] = "1";
        $data['team_points'] = $this->article_model->getClubPoints(20, $league_id);
        //$data['poll_data'] = $this->poll_model->getPoll("type='V'");
//AND created_at>=CURRENT_TIMESTAMP - INTERVAL 1 MONTH
        $data['poll_data'] = $this->poll_model->getPoll("type='V'", "2", "0");
        $pollfeed = $this->poll_model->getPoll("type='V'", "1", "0");
        if ($pollfeed) {
            $data['poll_data'] = $pollfeed;
        }
        $data['league_id'] = $league_id;

        $ffplus = $this->db->query("SELECT users.*,user_profile.profile_pic,(SELECT SUM(user_points.points) as pnt FROM user_points WHERE user_points.fk_user_id = users.pk_user_id) as points FROM users LEFT OUTER JOIN user_profile ON user_profile.fk_user_id=users.pk_user_id WHERE users.fk_league_id='$league_id' AND ff_plus='1' ORDER BY points LIMIT 20")->result_array();
        
        foreach ($ffplus as $key => $user) {
            $ffplus[$key]['profile_pic'] = (is_file(IMG_PROFILE . $user['profile_pic'])) ? site_url() . IMG_PROFILE . $user['profile_pic'] : site_url() . "public/fassets/images/user.jpg";
            $ffplus[$key]['name'] = $user['firstname'] . ' ' . $user['lastname'];
            $ffplus[$key]['art_title'] = "";
            $f_article = $this->article_model->getArticleIdByUserId($user['pk_user_id']);
            if ($f_article) {
                $ffplus[$key]['article_pic'] = $f_article['image'];
                $ffplus[$key]['art_title'] = $f_article['title'];
            } else {
                unset($ffplus[$key]);
            }
        }
        $ffplus = array_merge(array(), $ffplus);
        //print_r($ffplus);
        $data['ffplus'] = array_slice($ffplus, 0, 3);
        //$data['ffplus'] = $ffplus;


        /*print_r($data['club_list']);
        exit();*/

        $this->viewer->fview('home/index.php', $data);
    }

    public function save_poll() {
        $this->poll_model->saveVoting($this->input->post('id'), $this->input->post('voting_option'));
    }

    public function login($param = '') {

        $this->viewer->loginview('home/login.php', array("nextpage" => $param));
    }

    public function signup($param = '') {
        $clubs = $this->master_model->getClubKV("1");
        $this->viewer->loginview('home/signup.php', array("clubs" => $clubs));
    }

    public function contact($param = '') {
        $clubs = $this->master_model->getClubKV("1");
        $this->viewer->fview('home/contact.php', array("clubs" => $clubs));
    }

    public function contactsend() {
        $data = $this->input->post();
        $data['type'] = "C";
        $data_eamil = $this->viewer->emailview("contactus.php", array('data' => $data));

        //echo $data_eamil;
        $this->load->library('email');
        $config = getEmailConfig();
        $this->email->initialize($config);
        $this->email->from(ADMIN_EMAIL, SITE_NAME);
        $this->email->subject(SITE_NAME . (($data['type'] == 'F') ? '- Feedback' : '- Contact'));
        $this->email->message($data_eamil);
        $this->email->to(CONTACT_EMAIL);
        $this->email->send();

        echo $data_eamil;
    }

    public function getclubs($league_id) {
        $data = $this->master_model->getHeaderLeagueClub($league_id);
        $this->session->set_userdata('site_league', $league_id);
        echo json_encode($data);
    }

    public function setleague($league_id) {
        $this->session->set_userdata('site_league', $league_id);
        echo $this->session->userdata('site_league');
    }

    public function verification($param = '') {
        $this->viewer->loginview('home/verification.php', array());
    }

    public function upload() {
        if (isset($_FILES['upload']) and $_FILES['upload']['error'] == 0) {
            $path_config = "public/uploads/editor/";
            $config['path'] = $path_config;
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "400";
            $config['prefix'] = $this->session->userdata("user_id");
            $config['file_name'] = 'upload';
            $file = uploadFile($config);
            if ($file) {
                echo json_encode(array(
                    "uploaded" => "1",
                    "fileName" => $file,
                    "url" => site_url() . $path_config . $file,
                    ));
            }
        }
    }

    public function ckupload() {
        if (($_FILES['upload'] == "none") OR (empty($_FILES['upload']['name']))) {
            $message = "No file uploaded.";
        } else if ($_FILES['upload']["size"] == 0) {
            $message = "The file is of zero length.";
        } else if (($_FILES['upload']["type"] != "image/pjpeg") AND ($_FILES['upload']["type"] != "image/jpeg") AND ($_FILES['upload']["type"] != "image/png")) {
            $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        } else if (!is_uploaded_file($_FILES['upload']["tmp_name"])) {
            $message = "You may be attempting to hack our server. We're on to you;
            expect a knock on the door sometime soon.";
        } else {
            $message = "";
            $path_config = "public/uploads/editor/";
            $config['path'] = $path_config;
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "400";
            $config['prefix'] = $this->session->userdata("user_id");
            $config['file_name'] = 'upload';
            $file = uploadFile($config);
            if ($file) {
                $url = site_url() . $path_config . $file;
            } else {
                $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
            }
        }

        $funcNum = $_GET['CKEditorFuncNum'];
        echo "<script type = 'text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum , '$url ', '$message');</script>";
    }

    public function delete_account() {
        $user_id = $this->session->userdata("user_id");
        $this->db->delete("users", array("pk_user_id" => $user_id));
        $this->db->delete("user_profile", array("fk_user_id" => $user_id));
        $this->db->delete("user_albums", array("fk_user_id" => $user_id));
        $this->db->delete("user_points", array("fk_user_id" => $user_id));
        $this->db->delete("user_uploads", array("fk_user_id" => $user_id));
        $this->db->delete("post_views", array("fk_user_id" => $user_id));
        $this->db->delete("post", array("fk_user_id" => $user_id));
        $this->db->delete("notifications", array("fk_user_id" => $user_id));
        $this->db->delete("notifications", array("sent_by" => $user_id));
        $this->db->delete("news_feed", array("fk_user_id" => $user_id));
        $this->db->delete("news_feed", array("fk_feed_by" => $user_id));
        $this->db->delete("followers", array("fk_user_id" => $user_id));
        $this->db->delete("followers", array("fk_ref_id" => $user_id));
        $this->db->delete("feed_likes", array("fk_user_id" => $user_id));
        $this->db->delete("comments", array("fk_user_id" => $user_id));
    }

}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model("master_model");
        $this->load->model("article_model");
        $this->load->model("user_model");
        if (!($this->session->userdata("user_id") > 0)) {
            redirect("login");
        }
    }

    public function index($param = '') {
        $data = array('css' => "fassets/css/jquery-confirm.min.css", 'js' => 'fassets/js/jquery-confirm.min.js');
        $this->viewer->fview('notification/index.php', $data);
    }

    public function loadnotification() {
        $page = $this->input->post('page');
        $data = array();
        $data['notifications'] = $this->user_model->getNotifications($this->session->userdata("user_id"), $page, 25, true);
        $this->viewer->fview('notification/notifications.php', $data, false);
    }

    public function delete_notification() {
        $msg_id = $this->input->post('msg_id');
        $this->db->delete("notifications", array("pk_notification_id" => $msg_id));
    }

}

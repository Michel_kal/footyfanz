<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
    }

    public function pagecontent($slug) {
        $data = $this->db->get_where("pages", array("slug" => $slug))->result_array();
        if (count($data) == 0) {
            show_404();
        } else {
            $data[0]['cat_data']['header_meta'] = $data[0]['header_meta'];
            $this->viewer->fview('general/page.php', $data[0]);
        }
    }

}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model('auth_model');
        $this->load->model('master_model');
        $this->load->model('user_model');
        $this->load->model('poll_model');
    }

    public function newsfeed($slug = "", $feed_id = "0", $is_api = 0) {
        $news_feeds = array();
        $response = array('status'=>'0', 'error'=>TRUE, 'error_msg'=>'No news feed found...', 'news' => $news_feeds, 'msg'=>'No news feed found...');

        $user_id = $this->session->userdata("user_id");

        if ($is_api == 1) {// added for mobile app
            $user_id = 1;//$this->input->post('uid');
        }

        if ($user_id == "") {
            if ($is_api == 1) {
                echo json_encode($response);
                return;
            }

            redirect("login");
        }

        $self = $this->db->get_where("user_profile", array("fk_user_id" => $user_id))->result_array();
        if (!($self[0]['fk_club_id'] > 0)) {
            if ($is_api == 0) {// is web call not API call
                redirect("user/edit");
            }
        }

        $user_id = $this->user_model->getUserIDBySlug($slug);
        // if ($slug == "" or $slug == "0") {
        if (($slug == "" or $slug == "0") and ($is_api == 0)) {//no need for session on API call
            $user_id = $this->session->userdata("user_id");
        }
        // <link rel="stylesheet" type="text/css" href="../source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
        // <script type="text/javascript" src="../source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
        $css = $js = array();

        if ($is_api == 0) {// is web call not API call
            $css = array("fassets/css/jquery-confirm.min.css", "fassets/fancy_box/lib/jquery.fancybox.css?v=2.1.5", "fassets/fancy_box/lib/helpers/jquery.fancybox-buttons.css?v=1.0.5");
            $js = array('fassets/js/jquery-confirm.min.js', "fassets/fancy_box/lib/jquery.fancybox.js?v=2.1.5", "fassets/fancy_box/lib/jquery.mousewheel-3.0.6.pack.js", "fassets/fancy_box/lib/helpers/jquery.fancybox-media.js?v=1.0.6", "fassets/fancy_box/lib/helpers/jquery.fancybox-buttons.js?v=1.0.5");
        }

        $data = array('css' => $css, 'js' => $js);
        $data['ref_id'] = $feed_id;
        $data['pos'] = $this->user_model->getUserPosition($user_id);
        $data['ud'] = $this->user_model->getUserDetails($user_id);
        $data['uh'] = $this->user_model->getUserHighlights($user_id);
        $data['followers'] = $this->user_model->getFollowers($user_id);
        $data['followings'] = $this->user_model->getFollowings($user_id);
        $data['poll_data'] = $this->poll_model->getPoll("type='V'", "2", "0");
        $pollfeed = $this->poll_model->getPoll("type='V'", "3", "0");
        if ($pollfeed) {
            $data['poll_data'] = $pollfeed;
        }

        $gal = $this->user_model->getUserGallery($user_id, 1, 6);
        $data['user_gallery'] = $gal['data'];
        $data['user_id'] = $user_id;
        if ($this->session->userdata("user_id") == $user_id) {
            $data['follow_status'] = "NA";
        } else {
            $data['follow_status'] = $this->user_model->followStatus($user_id, $this->session->userdata("user_id"));
        }

        if ($is_api == 1) {// added for mobile app
            
            $newsfeed = array();
            if (!empty($data['user_gallery'])) {                
                foreach($data['user_gallery'] as $nf) {
                    // $newsfeed[] = array('image' => $nf['large_image']);
                    $newsfeed[] = array('image' => "http://10.0.2.2:85/footyfanz/". substr($nf['large_image'], strlen(base_url())));//for testing with mobile
                }
            }

            //echo '<pre>';print_r($newsfeed);exit;
            echo json_encode($newsfeed);
            return;
        }

        if ($user_id) {
            $this->viewer->fview('profile/newsfeed.php', $data);
        } else {
            $this->viewer->fview('general/page_404.php', array());
        }
    }

    public function gallery($slug = "") {
        if ($this->session->userdata('user_id') == "") {
            redirect("login");
        }
        $user_id = $this->user_model->getUserIDBySlug($slug);
        if ($slug == "") {
            $user_id = $this->session->userdata("user_id");
        }
        $css = array("fassets/fancy_box/lib/jquery.fancybox.css?v=2.1.5", "fassets/fancy_box/lib/helpers/jquery.fancybox-buttons.css?v=1.0.5", "fassets/css/jquery-confirm.min.css");
        $js = array("fassets/fancy_box/lib/jquery.fancybox.js?v=2.1.5", "fassets/fancy_box/lib/jquery.mousewheel-3.0.6.pack.js", "fassets/fancy_box/lib/helpers/jquery.fancybox-media.js?v=1.0.6", "fassets/fancy_box/lib/helpers/jquery.fancybox-buttons.js?v=1.0.5", "fassets/js/jquery-confirm.min.js");

        $data = array('css' => $css, 'js' => $js);
        $data['pos'] = $this->user_model->getUserPosition($user_id);
        $data['ud'] = $this->user_model->getUserDetails($user_id);
        $data['uh'] = $this->user_model->getUserHighlights($user_id);
        $data['followers'] = $this->user_model->getFollowers($user_id);
        $data['followings'] = $this->user_model->getFollowings($user_id);
        //$data['user_gallery'] = $this->user_model->getUserGallery($user_id,6);
        $data['user_id'] = $user_id;
        if ($this->session->userdata("user_id") == $user_id) {
            $data['follow_status'] = "NA";
        } else {
            $data['follow_status'] = $this->user_model->followStatus($user_id, $this->session->userdata("user_id"));
        }
        if ($user_id) {
            $this->viewer->fview('profile/gallery.php', $data);
        } else {
            $this->viewer->fview('general/page_404.php', array());
        }
    }

    public function gallery_data() {
        $user_id = $this->input->get("user_id");
        $page = $this->input->post('page');
        $perpage = PAGING_MED;
        $searchKey = (isset($_GET['sk'])) ? $_GET['sk'] : "";
        $data = $this->user_model->getUserGallery($user_id, $page, $perpage);
        $data['page'] = getPaginationFooterFront($page, $perpage, $data['count']);
        $data['search'] = $searchKey;
        $this->viewer->fview('articles/gallery_data.php', $data, false);
    }

    public function delete_data() {
        $id = $this->input->post('id');
        $query = "SELECT * FROM user_uploads WHERE pk_upload_id='$id'";
        $arr = $this->db->query($query)->row_array();
        @unlink(USER_GALLERY . $arr['file_name']);
        @unlink(USER_GALLERY . "thumb/" . $arr['file_name']);
        $this->db->delete("user_uploads", array("pk_upload_id" => $id));
    }

    public function uploadgalleryimage() {
        $file = "";
        if (isset($_FILES['fileimage']) and $_FILES['fileimage']['error'] == 0) {
            $config['path'] = USER_GALLERY;
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "800";
            $config['prefix'] = "img";
            $config['file_name'] = 'fileimage';
            $file = uploadFile($config);
            setImageRatio(USER_GALLERY . $file);
            @copy(USER_GALLERY . $file, USER_GALLERY . "thumb/" . $file);
            resizeFile(USER_GALLERY . "thumb/" . $file, 120);
            $data = array(
                "file_name" => $file,
                "fk_user_id" => $this->session->userdata("user_id"),
                "status" => "1",
                "file_type" => "I"
            );
            $this->db->insert("user_uploads", $data);
            echo json_encode(array("status" => "1"));
        } else {
            echo json_encode(array("status" => "0"));
        }
    }

    public function followers($slug = "") {
        $user_id = $this->user_model->getUserIDBySlug($slug);
        if ($user_id) {
            $data = array('css' => "fassets/css/jquery-confirm.min.css", 'js' => 'fassets/js/jquery-confirm.min.js');
            $data['pos'] = $this->user_model->getUserPosition($user_id);
            $data['ud'] = $this->user_model->getUserDetails($user_id);
            $data['uh'] = $this->user_model->getUserHighlights($user_id);
            $data['followers'] = $this->user_model->getFollowers($user_id);
            $data['followings'] = $this->user_model->getFollowings($user_id);
            if ($this->session->userdata("user_id") == $user_id) {
                $data['follow_status'] = "NA";
            } else {
                $data['follow_status'] = $this->user_model->followStatus($user_id, $this->session->userdata("user_id"));
            }
            $this->viewer->fview('profile/followers.php', $data);
        } else {
            $this->viewer->fview('general/page_404.php', array());
        }
    }

    public function follow() {
        if ($this->session->userdata("user_id") > 0) {
            $this->user_model->follow($this->input->post("following_id"), $this->session->userdata("user_id"));
            echo json_encode(array("status" => "1"));
        } else {
            echo json_encode(array("status" => "0"));
        }
    }

    public function unfollow() {
        if ($this->session->userdata("user_id") > 0) {
            $this->user_model->unfollow($this->input->post("following_id"), $this->session->userdata("user_id"));
            echo json_encode(array("status" => "1"));
        } else {
            echo json_encode(array("status" => "0"));
        }
    }

    public function send_message_form($id) {

        $this->viewer->fview('modals/send_message.php', array("id" => $id), false);
    }

    public function send_message() {
        $save_data = array(
            'title' => $this->input->post("title"),
            'content' => $this->input->post("content"),
            'fk_user_id' => $this->input->post("user_id"),
            'sent_by' => $this->session->userdata("user_id")
        );
        $this->db->insert("messages", $save_data);
    }

    public function feedsave() {
        $file = "";
        if ($this->input->post("filetype") == "I" and isset($_FILES['fileimage']) and $_FILES['fileimage']['error'] == 0) {
            $config['path'] = IMG_NEWSFEED;
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "800";
            $config['prefix'] = "img";
            $config['file_name'] = 'fileimage';
            $file = uploadFile($config);
        }
        if ($this->input->post("filetype") == "V" and isset($_FILES['filevideo']) and $_FILES['filevideo']['error'] == 0) {
            $config['path'] = VID_NEWSFEED;
            $config['type'] = 'video/mp4|mp4|avi|wav|flv|mov|wmv';
            $config['prefix'] = "vid";
            $config['file_name'] = 'filevideo';
            $file = uploadRawFile($config);
        }
        $save_data = array(
            'content' => $this->input->post("posttext"),
            'file_name' => $file,
            'file_type' => $this->input->post("filetype"),
            'fk_user_id' => $this->session->userdata("user_id"),
        );
        $this->db->insert("news_feed", $save_data);
        $post_id = $this->db->insert_id();
        //Sending Notification
        if ($this->input->post("posttext") != "") {
            $this->user_model->SavePoints($this->session->userdata("user_id"), $post_id, "NF_TEXT");
        }
        if ($this->input->post("filetype") == "I" and isset($_FILES['fileimage']) and $_FILES['fileimage']['error'] == 0) {
            $this->user_model->SavePoints($this->session->userdata("user_id"), $post_id, "NF_IMG");
        }
        if ($this->input->post("filetype") == "V" and isset($_FILES['filevideo']) and $_FILES['filevideo']['error'] == 0) {
            $this->user_model->SavePoints($this->session->userdata("user_id"), $post_id, "NF_VID");
        }


        $followers = $this->db->query("SELECT fk_user_id FROM followers WHERE fk_ref_id='" . $this->session->userdata('user_id') . "'")->result_array();
        $noti_data = array();
        foreach ($followers as $row) {
            $noti_data[] = array(
                "ref_id" => $post_id,
                "fk_user_id" => $row['fk_user_id'],
                "sent_by" => $this->session->userdata('user_id'),
                "content" => $this->session->userdata('name') . " posted a newsfeed.",
                "type" => "FEED_NEW"
            );
        }
        if (count($noti_data) > 0) {
            $this->db->insert_batch("notifications", $noti_data);
        }
    }
    public function getfeeds($is_api = 0) {
        $page = $this->input->post('page');
        $ref_id = $this->input->post('ref_id');
        $user_id = $this->input->post('user_id');
        $perpage = PAGING_MED;

        if ($is_api == 1) {// added for mobile app
            $page = 1;//testing
            $current_user_slug = 'admin';//testing
            // $current_user_slug = $this->input->post('usr_slug');//production
            $user_id = $this->user_model->getUserIDBySlug($current_user_slug);
        }

        // $perpage = 2;
        $data = $this->user_model->getNewsFeeds($user_id, $page, $perpage, 0, $ref_id);

        $response = array('status'=>'0', 'error'=>TRUE, 'error_msg'=>'No news feed found...', 'feed' => array(), 'msg'=>'No news feed found...');
        if ($is_api == 1) {
            //echo '<pre>'.print_r($data, TRUE).'</pre>';
            $response['feed'] = array();
            foreach ($data['data'] as $key => $feed) {
                // if ($feed['art_data']) {
                    $articleURL = ($feed['art_data']) ? site_url() . ARTICLE_TAG . "/" . $feed['art_data']['slug'] : NULL;
                    $response['feed'][$key] = array(
                        'id'=>$feed['pk_feed_id'], 'content'=>$feed['art_data']['content'],
                        'name'=>$feed['name'], 'image'=>$feed['art_data']['image'],
                        'status'=>wordLimiter(strip_tags($feed['art_data']['title'], 160)), 'url'=>$articleURL, 
                        'timeStamp'=>(DateTime::createFromFormat('Y-m-d H:i:s', $feed['created_at'], new DateTimeZone('UTC'))->getTimestamp())
                    );
                    // $response['feed'][$key] = array(
                        // 'id'=>$feed['pk_feed_id'], 'content'=>$feed['content'],
                        ////'name'=>$feed['name'], 'name_slug'=>$feed['slug'], 'image'=>str_replace('localhost', '192.168.42.131', $feed['thumb']),
                        // 'name'=>$feed['name'], 'name_slug'=>$feed['slug'], 'image'=>$feed['thumb'],
                        // 'status'=>wordLimiter(strip_tags($feed['content'], 160)), 'url'=>$articleURL, 
                        // 'timeStamp'=>(DateTime::createFromFormat('Y-m-d H:i:s', $feed['created_at'])->getTimestamp())
                    // );

                    if (is_file(PUBLIC_DIR . PROFILE_IMG_DIR . $feed['profile_pic'])) {
                        // $response['feed'][$key]['profilePic'] = "http://192.168.42.131/footyfanz/" . PUBLIC_DIR . PROFILE_IMG_DIR . $feed['profile_pic'];// for testing with mobile app LOCALLY
                        $response['feed'][$key]['profilePic'] = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $feed['profile_pic'];
                    } else {
                        // $response['feed'][$key]['profilePic'] = "http://192.168.42.131/footyfanz/" . "public/fassets/images/user.jpg";
                        $response['feed'][$key]['profilePic'] = site_url() . "public/fassets/images/user.jpg";
                    }
                // }
            }
            $response['feed_count'] = $data['count'];
            $response['feed_load'] = $data['load'];
            $response['status'] = "1";
            $response['error'] = FALSE;
            $response['error_msg'] = $response['msg'] = "";
            // echo '<pre>'.print_r($response, TRUE).'</pre>';
            // header('Content-Type', 'application/json');
            echo json_encode($response);
            return;
        }

        $this->viewer->fview('profile/newsfeed_data.php', $data, false);
    }

    // public function getfeeds() {
        // $page = $this->input->post('page');
        // $ref_id = $this->input->post('ref_id');
        // $user_id = $this->input->post('user_id');
        // $perpage = PAGING_MED;
        //// $perpage = 2;
        // $data = $this->user_model->getNewsFeeds($user_id, $page, $perpage, 0, $ref_id);
        // $this->viewer->fview('profile/newsfeed_data.php', $data, false);
    // }

    public function getlatestfeeds() {
        $user_id = $this->input->post('user_id');
        $data = $this->user_model->getNewsFeeds($user_id, 0, 0, $this->input->post('last_id'), 0);
        $this->viewer->fview('profile/newsfeed_data.php', $data, false);
    }

    public function getoldcomments() {
        $data = $this->user_model->getNewsFeedComments($this->input->post('feed_id'), $this->input->post('last_id'), "first");

        $this->viewer->fview('profile/comment_data.php', $data, false);
    }

    public function deletecomment() {
        $this->db->delete("comments", array("pk_comment_id" => $this->input->post('id')));
    }

    public function commentupdate() {
        // $this->db->delete("comments", array("pk_comment_id" => $this->input->post('id')));
        $save_data = array(
            "fk_user_id" => $this->session->userdata('user_id'),
            "content" => $this->input->post('com_text'),
        );
        $this->db->update("comments", $save_data, array("pk_comment_id" => $this->input->post('com_id')));
    }

    public function savecomment() {
        $save_data = array(
            "fk_ref_id" => $this->input->post('feed_id'),
            "fk_user_id" => $this->session->userdata('user_id'),
            "content" => $this->input->post('com_text'),
        );
        $this->db->insert("comments", $save_data);
        //Sending Notification
        $nf = $this->db->get_where("news_feed", array("pk_feed_id" => $this->input->post('feed_id')))->result_array();
        $noti_data = array(
            "ref_id" => $this->input->post('feed_id'),
            "fk_user_id" => $nf[0]['fk_user_id'],
            "sent_by" => $this->session->userdata('user_id'),
            "content" => $this->session->userdata('name') . " commented your post.",
            "type" => "COM_POST"
        );
        $this->user_model->SavePoints($this->session->userdata('user_id'), $this->input->post('feed_id'), "NF_COM");
        if ($nf[0]['fk_user_id'] != $this->session->userdata('user_id')) {
            $this->db->insert("notifications", $noti_data);
        }
        /*         * ****************************************** */
        $data = $this->user_model->getNewsFeedComments($this->input->post('feed_id'), $this->input->post('last_id'), "last");
        $this->user_model->SavePoints($this->session->userdata('user_id'), $this->input->post('feed_id'), "NF_COM", FALSE);
        $this->user_model->SavePoints($nf[0]['fk_user_id'], $this->input->post('feed_id'), "W_NF_COM", FALSE);
        $com_data['data'] = $this->load->view('front/profile/comment_data.php', $data, true);
        $com_data['count'] = $data['tot_comments'];
        $com_data['last_id'] = $data['last_id'];
        echo json_encode($com_data);
    }

    public function managelike() {
        $data = array(
            "fk_feed_id" => $this->input->post('feed_id'),
            "fk_user_id" => $this->session->userdata('user_id'),
        );
        //Sending Notification
        $nf = $this->db->get_where("news_feed", array("pk_feed_id" => $this->input->post('feed_id')))->result_array();
        $noti_data = array(
            "ref_id" => $this->input->post('feed_id'),
            "fk_user_id" => $nf[0]['fk_user_id'],
            "sent_by" => $this->session->userdata('user_id'),
            "content" => $this->session->userdata('name') . " liked your post.",
            "type" => "LIKE_POST"
        );
        $this->db->delete("notifications", array("type" => 'LIKE_POST', "ref_id" => $this->input->post('feed_id'), "fk_user_id" => $this->session->userdata('user_id')));
        /*         * ****************************************** */
        if ($this->input->post("like_status") == 0) {
            $this->db->insert("feed_likes", $data);

            if ($nf[0]['fk_user_id'] != $this->session->userdata('user_id')) {
                $this->db->insert("notifications", $noti_data);
            }
            $this->user_model->SavePoints($this->session->userdata('user_id'), $this->input->post('feed_id'), "NF_LIKE", true);
            $this->user_model->SavePoints($nf[0]['fk_user_id'], $this->input->post('feed_id'), "W_NF_LIKE", true);
        } else {
            $this->db->delete("feed_likes", $data);
        }
    }

    public function sharefeed() {
        $this->user_model->shareNewsFeeds($this->input->post('feed_id'), $this->session->userdata('user_id'));
        $this->user_model->SavePoints($this->session->userdata('user_id'), $this->input->post('feed_id'), "NF_SHARE");
        $nf = $this->db->get_where("news_feed", array("pk_feed_id" => $this->input->post('feed_id')))->result_array();
        $this->user_model->SavePoints($nf[0]['fk_user_id'], $this->input->post('feed_id'), "W_NF_SHARE");
    }

    public function delete_feed() {

        $this->user_model->deleteAllFeedData($this->input->post('id'));
    }

    public function savegalleryvideo() {
        $id = $this->input->post("id");
        $file = $this->saveYoutubeImage($id);
        $data = array(
            "file_name" => $file,
            "fk_user_id" => $this->session->userdata("user_id"),
            "status" => "1",
            "file_type" => "V"
        );
        $this->db->insert("user_uploads", $data);
        echo json_encode(array("status" => "1"));
    }

    public function saveYoutubeImage($vid) {
        $url = "http://img.youtube.com/vi/$vid/hqdefault.jpg";

        $content = file_get_contents($url);
//Store in the filesystem.
        $img = $vid . ".jpg";
        @unlink(USER_GALLERY . $img);
        @unlink(USER_GALLERY . "thumb/" . $img);
        $fp = fopen(USER_GALLERY . $img, "w");
        fwrite($fp, $content);
        fclose($fp);
        setImageRatio(USER_GALLERY . $img);
        @copy(USER_GALLERY . $img, USER_GALLERY . "thumb/" . $img);
        resizeFile(USER_GALLERY . "thumb/" . $img, 120);
        return $img;
    }

}
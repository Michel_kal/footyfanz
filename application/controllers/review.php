<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class review extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->model("review_model");
    }

    public function savereview() {
        $review = trim($this->input->post("txt_review"));
        $rate = $this->input->post("txt_rating");
        $type = $this->input->post("txt_type");
        $ref = $this->input->post("txt_ref");
        $user = $this->session->userdata("user_id");
        if ($review == "" or $rate == "") {
            $res = array("status" => "failed", "msg" => "Please enter Review and select Rating");
        } else {
            $this->review_model->saveReview($type, $ref, $user, $review);
            $this->review_model->saveRating($type, $ref, $user, $rate);
            $res = array("status" => "success", "msg" => "Review saved successfully");
        }
        echo json_encode($res);
    }

    public function getfestreviews($ref, $page) {
        $user_id = $this->session->userdata("user_id");
        $reviews = $this->review_model->getReviews("fest", $ref, $user_id, $page, 2);
        $this->viewer->fview('reviews/fest.php', array('reviews' => $reviews, "page" => $page + 1), false);
    }

}

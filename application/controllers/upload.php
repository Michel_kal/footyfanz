<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
    }

    public function video($param = '') {
        $this->load->model("master_model");
        $categories = $this->master_model->getCategoriesKV();
        $this->viewer->fview('upload/video.php', array('menu' => "0-0", 'categories' => $categories, 'css' => array('css/chosen.css', 'css/select2_metro.css')));
    }

    public function videoform() {
        $this->load->model("master_model");
        $categories = $this->master_model->getCategoriesKV();
        $tags = $this->master_model->getDistinctTags();
        $this->viewer->fview('upload/form.php', array('categories' => $categories, 'tags' => $tags), false);
    }

    public function video_save() {
        $this->load->model("master_model");
        $this->load->model("video_model");
        $vid = $this->input->post("vid");
        $user = $this->session->userdata("user_id");
        // print_r($this->input->post());
        if ($this->video_model->checkVIDExist($vid)) {
            $result = array('status' => 'failed', 'msg' => 'This video is already exist on College Media.');
        } elseif ($user == "") {
            $result = array('status' => 'failed', 'msg' => 'You are not logged in. Please login before save.');
        } else {
            $tags = $this->input->post("vtags");
            $time = getTimestamp();
            $category = $this->input->post("vcategory");
            $cats = ($category == "") ? '' : implode(",", $category);
            $data = array(
                'sid' => $user,
                'name' => $this->input->post("vtitle"),
                'descr' => $this->input->post("vdescription"),
                'duration' => $this->input->post("vduration"),
                'url' => $this->input->post("vid"),
                'collid' => $this->input->post("college_id"),
                'fstid' => $this->input->post("vfest"),
                'sts' => '0',
                'typ' => 'v',
                'cat' => $cats,
                'date' => $time,
                'time' => strtotime($time)
            );
            $this->db->insert("ko_post", $data);
            $id = $this->db->insert_id();
            $this->load->library('slug');
            $this->slug->set_config(array('table' => 'ko_post', 'field' => 'slug', 'title' => 'name'));
            $rslug = $this->slug->create_uri($data['name'], $id);
            $this->db->update("ko_post", array('slug' => $rslug), array('id' => $id));
            if (is_array($category)) {
                foreach ($category as $ca) {
                    $this->db->insert("ko_cat_post", array('category_id' => $ca, 'post_id' => $id));
                }
            }
            if ($tags != '') {
                $tags = explode(",", $tags);
                foreach ($tags as $tg) {
                    if ($tg != '') {
                        $this->db->insert("ko_tag_post", array('tag' => trim($tg), 'post_id' => $id, 'post_type' => 'v'));
                    }
                }
            }
            $result = array('status' => 'success', 'msg' => 'Video has been saved successfully. This video will be publicaly visible after College Media approval');
        }
        echo json_encode($result);
    }

    function collegehint() {
        $search = $this->input->post('key');
        $filter = "";
        if ($search != "") {
            $arr = getSearchKeys($this->db->escape_str($search), "ko_college.name");
            $filter = implode(" AND ", $arr);
            $filter = " AND " . $filter;
        }

        $data_res = $this->db->query("SELECT id,name FROM ko_college WHERE 1=1 $filter LIMIT 30");
        $data = $data_res->result_array();
        $jarr = array();
        foreach ($data as $dt) {
            $jarr[$dt['id']] = $dt['name'];
        }
        echo json_encode($jarr);
    }

    public function test() {
        $this->viewer->fview('user/test.php', array(), FALSE);
    }

}

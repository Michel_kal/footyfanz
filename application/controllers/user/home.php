<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->database();
        $this->load->helper("url");
        if ($this->session->userdata('user_id') == "") {
            redirect("user/login");
        }
    }

    public function index($param = '') {
        $time = getTimestamp();
        $imestamp_7 = strtotime($time) - (7 * 24 * 60 * 60);
        $data = array();
        $user = $this->session->userdata("user_id");
        $posts_active = $this->db->query("SELECT COUNT(*) AS cnt FROM ko_post WHERE sid='$user'");
        $p_active = $posts_active->result_array();
        $data['p_active'] = $p_active[0]['cnt'];


        $views = $this->db->query("SELECT COUNT(*) AS cnt FROM ko_view WHERE `date` >= '" . date("Y-m-d", $imestamp_7) . "' AND pid IN (SELECT id FROM ko_post WHERE sid='$user')");
        $v_active = $views->result_array();
        $data['v_active'] = $v_active[0]['cnt'];
        $data['menu'] = "1-1";
        $this->viewer->uview('home.php', $data);
    }

}

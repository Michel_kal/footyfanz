<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('viewer');
        $this->load->model("master_model");
        if ($this->session->userdata('user_id') == "") {
            redirect("user/login");
        }
    }

    public function index($param = '') {
        $this->load->model("user_model");

        $profile = $this->user_model->getUserById($this->session->userdata('user_id'));
        $profile['image'] = parseUserImage($profile['sid'], $profile['pic'], $profile['gender']);
        $css = array('plugins/dropzone/css/dropzone.css', 'plugins/jcrop/css/jquery.Jcrop.min.css', 'css/pages/image-crop.css');
        $js = array('../plugins/photobooth/webcam.js', '../plugins/dropzone/dropzone.js', '../plugins/jcrop/js/jquery.Jcrop.min.js', '../scripts/form-image-crop.js', 'user_profile.js');
        $this->viewer->uview('profile.php', array('menu' => "0-0", 'profile' => $profile, 'css' => $css, 'js' => $js));
    }

    public function profileview() {
        $this->load->model("user_model");
        $profile = $this->user_model->getUserById($this->session->userdata('user_id'));
        $profile['image'] = parseUserImage($profile['sid'], $profile['pic'], $profile['gender'], "large");
        $profile['states'] = $this->master_model->getStates();
        $profile['cities'] = $this->master_model->getCitiesByState($profile['state']);
        $this->viewer->uview('profileview.php', array('profile' => $profile), FALSE);
    }

    public function uploder() {
        $this->viewer->uview('modal/uploder.php', array(), FALSE);
    }

    public function camera_save() {
        $filename = PUBLIC_DIR . PROFILE_IMG_DIR . "/temp/" . $this->session->userdata('id') . "_" . strtotime("now") . '.jpg';
        $result = file_put_contents($filename, file_get_contents('php://input'));
        if (!$result) {
            print "ERROR: Failed to write data to $filename, check permissions\n";
            exit();
        }
        $last_image = $this->session->userdata('large_image_path');
        if (is_file($last_image)) {
            @unlink($last_image);
        }
        $this->session->set_userdata(array('large_image_path' => $filename));
        $url = base_url() . $filename;
        print "$url\n";
    }

    public function upload_image() {
        $this->remmoveOldFiles(PUBLIC_DIR . PROFILE_IMG_DIR . "/");
        $config['upload_path'] = PUBLIC_DIR . PROFILE_IMG_DIR . "/temp/";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $this->session->userdata('user_id') . "_" . strtotime("now");
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("file")) {
            echo "Error in file upload";
            echo $this->upload->display_errors('<p>', '</p>');
        } else {
            $data = $this->upload->data();
            $org_wid = $data['image_width'];
            $org_ht = $data['image_height'];
            $file_name = explode(".", $data['file_name']);
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['full_path'];
            $config['create_thumb'] = TRUE;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1000;
            $height = intval(($config['width'] * $org_ht) / $org_wid);
            $config['height'] = $height;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $thumb_name = PUBLIC_DIR . PROFILE_IMG_DIR . "/temp/" . $file_name[0] . "_thumb" . "." . $file_name[1];
            @unlink($data['full_path']);
            $last_image = $this->session->userdata('large_image_path');
            if (is_file($last_image)) {
                @unlink($data['full_path']);
            }
            $this->session->set_userdata(array('large_image_path' => $thumb_name));
            echo '<script> window.parent.after_upload_image(); </script>';
        }
    }

    public function crop_image_save() {
        $x1 = $this->input->post("x1");
        $y1 = $this->input->post("y1");
        $x2 = $this->input->post("x2");
        $y2 = $this->input->post("y2");
        $w = $this->input->post("w");
        $h = $this->input->post("h");
        $imgw = $this->input->post("imgw");
        $image = $this->session->userdata('large_image_path');
        $vals = @getimagesize($image);
        $ord_w = $vals[0];
        $ord_h = $vals[1];
        $local_ratio = $ord_w / $imgw;
        $x1 = intval($x1 * $local_ratio);
        $x2 = intval($x2 * $local_ratio);
        $y1 = intval($y1 * $local_ratio);
        $y2 = intval($y2 * $local_ratio);
        $this->load->model("user_model");
        $basename = basename($image);
        $this->user_model->updateUserImage($this->session->userdata("user_id"), $basename);
        $large_path = PUBLIC_DIR . PROFILE_IMG_DIR . "/large/" . $basename;
        $small_path = PUBLIC_DIR . PROFILE_IMG_DIR . "/small/" . $basename;
        $target_width = $x2 - $x1;
        $target_height = $y2 - $y1;
        $resize = array(
            'image_library' => 'gd2',
            'maintain_ratio' => FALSE,
            'width' => $target_width,
            'height' => $target_height,
            'x_axis' => $x1,
            'y_axis' => $y1,
            'source_image' => $image,
            'new_image' => $large_path
        );
        $this->load->library('image_lib', $resize);
        $this->image_lib->crop();



        $resize['new_image'] = $small_path;
        $this->image_lib->initialize($resize);
        $this->image_lib->crop();
        //******* Resize Image **********
        $resize = array(
            'image_library' => 'gd2',
            'maintain_ratio' => TRUE,
            'create_thumb' => FALSE,
            'width' => $target_width,
            'height' => $target_height
        );
        $resize['source_image'] = $resize['new_image'] = $large_path;
        $resize['width'] = 400;
        $height = intval(($resize['width'] * $target_height) / $target_width);
        $resize['height'] = $height;
        $this->image_lib->initialize($resize);
        $this->image_lib->resize();


        $resize['source_image'] = $resize['new_image'] = $small_path;
        $resize['width'] = 100;
        $height = intval(($resize['width'] * $target_height) / $target_width);
        $resize['height'] = $height;
        $this->image_lib->initialize($resize);
        $this->image_lib->resize();
        echo base_url() . $large_path;
    }

    private function remmoveOldFiles($folder) {
        if ($handle = opendir($folder)) {
            while (false !== ($file = readdir($handle))) {
                $filelastmodified = filemtime($file);
                if ((time() - $filelastmodified) > 24 * 3600) {
                    @unlink($file);
                }
            }
            closedir($handle);
        }
    }

    public function get_large_image() {
        echo base_url() . $this->session->userdata('large_image_path');
    }

    public function city_by_state() {
        $city_data = $this->master_model->getCitiesByState($this->input->post("id")); #Geting Active Cities List By State
        ?> <option value="">Select City</option> <?php
        foreach ($city_data as $row) {
            ?> <option value="<?php echo $row['city_id']; ?>"><?php echo $row['city_name']; ?></option> <?php
        }
    }

    public function updateUserProfile() {
        $id = $this->session->userdata('user_id');
        $dob = dmyToYmd($this->input->post("dob"));
        $update = array(
            'name' => $this->input->post("name"),
            'abt' => $this->input->post("abt"),
            'gender' => $this->input->post("gender"),
            'state' => $this->input->post("state"),
            'city' => $this->input->post("city"),
            'phone' => $this->input->post("phone"),
            'user_dob' => $dob,
            'dob' => date("m/d,Y", strtotime($dob)),
            'cid' => $this->input->post("cid"),
            'degree' => $this->input->post("degree"),
            'btch' => $this->input->post("btch"),
            'stream' => $this->input->post("stream"),
            'sch_name' => $this->input->post("sch_name"),
            'sch_city' => $this->input->post("sch_city"),
            'sch_batch' => $this->input->post("sch_batch"),
            'sch_medium' => $this->input->post("sch_medium"),
            'company' => $this->input->post("company"),
            'project' => $this->input->post("project"),
            'skills' => $this->input->post("skills"),
            'social' => serialize($this->input->post("social")),
        );
        $this->db->update("ko_user", $update, array("id" => $id));
    }

    function hint() {
        $search = $this->input->post('key');
        $filter = "";
        if ($search != "") {
            $arr = getSearchKeys($this->db->escape_str($search), "ko_college.name");
            $filter = implode(" AND ", $arr);
            $filter = " AND " . $filter;
        }

        $data_res = $this->db->query("SELECT id,name FROM ko_college WHERE 1=1 $filter LIMIT 15");
        $data = $data_res->result_array();
        $jarr = array();
        foreach ($data as $dt) {
            $jarr[$dt['id']] = $dt['name'];
        }
        echo json_encode($jarr);
    }

}

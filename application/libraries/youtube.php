<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Youtube {

    private $ci;

    function __construct() {
        $this->ci = & get_instance();
    }

    public function search_video($search) {
        $this->ci->load->library('google');
        $DEVELOPER_KEY = GOOGLEAPIKEY;
        $client = new Google_Client();
        $client->setDeveloperKey($DEVELOPER_KEY);
        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);
        $final = array();

        $searchResponse = $youtube->search->listSearch('id,snippet', array(
            'q' => $search,
            'maxResults' => 3
        ));
        foreach ($searchResponse['items'] as $searchResult) {
            switch ($searchResult['id']['kind']) {
                case 'youtube#video':
                    $final[] = array(
                        'id' => $searchResult['id']['videoId'],
                        'title' => $searchResult['snippet']['title'],
                        'desc' => $searchResult['snippet']['description'],
                        'image' => "http://i.ytimg.com/vi/" . $searchResult['id']['videoId'] . "/mqdefault.jpg",
                        'dur' => $this->getVideoDuration($searchResult['id']['videoId'])
                    );
                    break;
                case 'youtube#channel':
                    break;
                case 'youtube#playlist':
                    break;
            }
        }
        return $final;
    }

    function get_video($id = "") {
        $JSON = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id=" . $id . "&part=contentDetails,id,snippet&key=" . GOOGLEAPIKEY);
        $JSON_Data = json_decode($JSON, true);
        $final = array();
        if (isset($JSON_Data['items'][0])) {
            $data = $JSON_Data['items'][0];
            $final[] = array(
                'id' => $data['id'],
                'title' => $data['snippet']['title'],
                'dur' => $this->covtime($data['contentDetails']['duration']),
                'desc' => $data['snippet']['description'],
                'image' => "http://i.ytimg.com/vi/" . $data['id'] . "/mqdefault.jpg"
            );
        }
        return $final;
    }

    function validateVID($vid) {
        if (verifyURL("http://gdata.youtube.com/feeds/api/videos/$vid")) {
            $str = @fopen("http://gdata.youtube.com/feeds/api/videos/$vid", 1);
            if (trim($str) == "Invalid id") {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    function verifyURL($url) {
        $file = $url;
        $file_headers = @get_headers($file);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        } else {
            $exists = true;
        }
        return $exists;
    }

    function linkValid($link) {
        if (preg_match('/((http:\/\/)?(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/v\/)([\w-]{11}).*|http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*)/i', $link, $match)) {
            return true;
        } else {
            if (preg_match('/((https:\/\/)?(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/v\/)([\w-]{11}).*|http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*)/i', $link, $match)) {
                return true;
            } else {
                return false;
            }
        }
    }

    function getVidById($link) {
        $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
        if (empty($video_id[1]))
            $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

        $video_id = explode("&", $video_id[1]); // Deleting any other params
        $video_id = $video_id[0];
        $video_id = explode("?", $video_id);
        $video_id = $video_id[0];
        return $video_id;
    }

    function getVideoDuration($video_ID) {
        $JSON = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id=" . $video_ID . "&part=contentDetails&key=" . GOOGLEAPIKEY);
        $JSON_Data = json_decode($JSON, true);
        if (isset($JSON_Data['items'][0]['contentDetails']['duration'])) {

            $time = $JSON_Data['items'][0]['contentDetails']['duration'];
            return $this->covtime($time);
        } else {
            return;
        }
    }

    function covtime($youtube_time) {
        preg_match_all('/(\d+)/', $youtube_time, $parts);
        $parts[0] = array_reverse($parts[0]);
        $hours = isset($parts[0][2]) ? $parts[0][2] : 0;
        $minutes = isset($parts[0][1]) ? $parts[0][1] : 0;
        $seconds = isset($parts[0][0]) ? $parts[0][0] : 0;
        $total = ($hours * 60 * 60) + ($minutes * 60) + $seconds;
        return $total;
    }

}

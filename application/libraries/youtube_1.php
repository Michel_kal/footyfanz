<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Youtube {

    private $ci;

    function __construct() {
        $this->ci = & get_instance();
    }

    public function search_video($search) {
        $this->load->library('google');




        $data = file_get_contents("https://gdata.youtube.com/feeds/api/videos?q=" . urlencode($search) . "&orderby=published&start-index=1&max-results=10&v=2");
        $p = xml_parser_create();
        xml_parse_into_struct($p, $data, $vals, $index);
        //print_r($vals);
        $ids = array();
        $title = array();
        $duration = array();
        $desc = array();
        $i = 0;
        foreach ($vals as $vl) {
            if (isset($vl['tag']) and trim($vl['tag']) == "YT:VIDEOID") {
                $ids[] = $vl['value'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "MEDIA:TITLE") {
                $title[] = $vl['value'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "YT:DURATION") {
                $duration[] = $vl['attributes']['SECONDS'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "MEDIA:DESCRIPTION") {
                if (isset($vl['value'])) {
                    $desc[] = $vl['value'];
                } else {
                    $desc[] = "";
                }
            }
        }
        $final = array();
        foreach ($ids as $key => $id) {
            $final[] = array(
                'id' => $id,
                'title' => $title[$key],
                'dur' => $duration[$key],
                'desc' => $desc[$key],
                'image' => "http://i.ytimg.com/vi/" . $id . "/mqdefault.jpg"
            );
        }
        return $final;
    }

    function get_video($id = "") {
        $data = file_get_contents("http://gdata.youtube.com/feeds/api/videos/" . $id . "?v=2");
        $p = xml_parser_create();
        $pdata = xml_parse_into_struct($p, $data, $vals, $index);

        $ids = array();
        $title = array();
        $duration = array();
        $desc = array();
        $i = 0;
        foreach ($vals as $vl) {
            if (isset($vl['tag']) and trim($vl['tag']) == "YT:VIDEOID") {
                $ids[] = $vl['value'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "MEDIA:TITLE") {
                $title[] = $vl['value'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "YT:DURATION") {
                $duration[] = $vl['attributes']['SECONDS'];
            }
            if (isset($vl['tag']) and trim($vl['tag']) == "MEDIA:DESCRIPTION") {
                if (isset($vl['value'])) {
                    $desc[] = $vl['value'];
                } else {
                    $desc[] = "";
                }
            }
        }
        $final = array();
        foreach ($ids as $key => $id) {
            $final[] = array(
                'id' => $id,
                'title' => $title[$key],
                'dur' => $duration[$key],
                'desc' => $desc[$key],
                'image' => "http://i.ytimg.com/vi/" . $id . "/mqdefault.jpg"
            );
        }
        return $final;
    }

    function validateVID($vid) {
        if (verifyURL("http://gdata.youtube.com/feeds/api/videos/$vid")) {
            $str = @fopen("http://gdata.youtube.com/feeds/api/videos/$vid", 1);
            if (trim($str) == "Invalid id") {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    function verifyURL($url) {
        $file = $url;
        $file_headers = @get_headers($file);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        } else {
            $exists = true;
        }
        return $exists;
    }

    function linkValid($link) {
        if (preg_match('/((http:\/\/)?(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/v\/)([\w-]{11}).*|http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*)/i', $link, $match)) {
            return true;
        } else {
            if (preg_match('/((https:\/\/)?(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/v\/)([\w-]{11}).*|http:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*)/i', $link, $match)) {
                return true;
            } else {
                return false;
            }
        }
    }

    function getVidById($link) {
        $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
        if (empty($video_id[1]))
            $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

        $video_id = explode("&", $video_id[1]); // Deleting any other params
        $video_id = $video_id[0];
        $video_id = explode("?", $video_id);
        $video_id = $video_id[0];
        return $video_id;
    }

}

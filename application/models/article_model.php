<?php

class article_model extends CI_Model {

    function getArticleList($page, $per_page, $searchKey, $ftype = array()) {#echo '<pre>'; print_r(func_get_args()); echo '</pre>';
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($searchKey != "") {
            $arr = getSearchKeys($this->db->escape_str($searchKey), "post.title");
            $arruser1 = getSearchKeys2($this->db->escape_str($searchKey), "firstname");
            $arruser2 = getSearchKeys2($this->db->escape_str($searchKey), "lastname");
            $arruser3 = getSearchKeys2($this->db->escape_str($searchKey), "username");
            $arruser4 = getSearchKeys2($this->db->escape_str($searchKey), "email");
            $userfilter1 = implode(" OR ", $arruser1);
            $userfilter2 = implode(" OR ", $arruser2);
            $userfilter3 = implode(" OR ", $arruser3);
            $userfilter4 = implode(" OR ", $arruser4);

            $filter = implode(" AND ", $arr);
            $filter = " AND ((" . $filter . ")";
            $filter.="OR (post.fk_user_id IN(SELECT pk_user_id FROM users WHERE ($userfilter1) OR ($userfilter2)  OR ($userfilter3) OR ($userfilter4) OR username like '$searchKey%') OR post.title LIKE '%$searchKey%' OR post.content LIKE '%$searchKey%') )";
        }
        //echo $filter;
        if (isset($ftype['league_id']) and $ftype['league_id'] > 0) {
            $filter .= " AND post.fk_club_id IN (SELECT pk_club_id FROM master_club WHERE status='1' AND is_deleted='0' AND fk_league_id='" . $ftype['league_id'] . "')";
        }
        if (isset($ftype['club_id']) and $ftype['club_id'] > 0) {
            $filter .= " AND post.fk_club_id='" . $ftype['club_id'] . "'";
        }
        if (isset($ftype['user_id']) and $ftype['user_id'] > 0) {
            $filter .= " AND post.fk_user_id='" . $ftype['user_id'] . "'";
        }
        // if (isset($ftype['sponsered']) and $ftype['sponsered'] > 0) {
        if (isset($ftype['sponsored']) and $ftype['sponsored'] > 0) {
            $filter .= " AND post.sponser_status='" . $ftype['sponsored'] . "'";
        }
        if (isset($ftype['featured']) and $ftype['featured'] > 0) {
            $filter .= " AND post.featured_status='" . $ftype['featured'] . "'";
        }

        if (isset($ftype['post_type'])) {
            // $filter .= " AND post.post_type='" . $ftype['post_type'] . "'";
        }
        else if (isset($ftype['status'])) {
            $filter .= " AND status='" . $ftype['status'] . "'";
        }

        // echo "SELECT SQL_CALC_FOUND_ROWS post.* FROM `post` WHERE is_deleted='0' $filter ORDER BY created_at DESC  LIMIT $start,$per_page";
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS post.* FROM `post` WHERE is_deleted='0' $filter ORDER BY created_at DESC  LIMIT $start,$per_page");
        $arr = $data->result_array();
        $count = getFoundRows();

        foreach ($arr as $key => $ar) {
            $image = IMG_ARTICLE . $ar['file_name'];
            if (is_file($image)) {
                $arr[$key]['thumb'] = base_url() . $image;
            } else {
                $arr[$key]['thumb'] = base_url() . NO_IMAGE;
            }
            $arr[$key]['views'] = $this->getArticleViews($ar['pk_post_id']);
            if ($arr[$key]['views'] > 1000) {
                $arr[$key]['views'] = floor($arr[$key]['views'] / 1000) . "K";
            }
            $arr[$key]['comments'] = $this->getCommentCount($ar['pk_post_id'], $ftype["post_type"]);
            $user = $this->getArticlePostedBy($ar['fk_user_id']);
            $arr[$key]['posted_by'] = "";
            $arr[$key]['posted_by_email'] = "";
            if ($user) {
                $arr[$key]['posted_by'] = $user['name'];
                $arr[$key]['user_image'] = $user['image'];
                $arr[$key]['user_slug'] = $user['slug'];
                $arr[$key]['posted_by_email'] = $user['email'];
            }
        }

        return array('data' => $arr, 'count' => $count);
    }

    function getUserList($page, $per_page, $searchKey, $ftype = array()) {
        $this->load->model("user_model");
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($searchKey != "") {
            $arruser1 = getSearchKeys2($this->db->escape_str($searchKey), "firstname");
            $arruser2 = getSearchKeys2($this->db->escape_str($searchKey), "lastname");
            $arruser3 = getSearchKeys2($this->db->escape_str($searchKey), "username");
            $arruser4 = getSearchKeys2($this->db->escape_str($searchKey), "email");
            $userfilter1 = implode(" OR ", $arruser1);
            $userfilter2 = implode(" OR ", $arruser2);
            $userfilter3 = implode(" OR ", $arruser3);
            $userfilter4 = implode(" OR ", $arruser4);
            $filter.="AND (($userfilter1) OR ($userfilter2)  OR ($userfilter3) OR ($userfilter4))";
        }
        $res = $this->db->query("SELECT SQL_CALC_FOUND_ROWS users.*,up.profile_pic"
                . " FROM `users` INNER JOIN user_profile up ON users.pk_user_id=up.fk_user_id "
                . " WHERE is_deleted='0' $filter   ORDER BY firstname,lastname  LIMIT $start,$per_page");
        $arr = $res->result_array();
        $count = getFoundRows();
        $curr_user = $this->session->userdata("user_id");
        $data = array();
        foreach ($arr as $key => $ar) {
            $user_id = $ar['pk_user_id'];
            $points = $this->db->query("SELECT sum(points) as points FROM user_points WHERE fk_user_id='$user_id'")->result_array();
            $data[$key]['points'] = intval($points[0]['points']);
            $data[$key]['pos'] = $this->user_model->getUserPosition($user_id);
            $data[$key]['pos'] = $start + ($key + 1);
            $data[$key]['ud'] = $this->user_model->getUserDetails($user_id);
            $data[$key]['uh'] = $this->user_model->getUserHighlights($user_id);
        }

        return array('data' => $data, 'count' => $count);
    }

    function getArticleDetails($post_id) {
        $data = $this->db->query("SELECT *"
                . " FROM `post`"
                . " WHERE pk_post_id='$post_id'");
        $arr = $data->result_array();
        if (count($arr)) {
            $ar = $arr[0];
            $image = IMG_ARTICLE . $ar['file_name'];
            if (is_file($image)) {
                $ar['thumb'] = base_url() . $image;
            } else {
                $ar['thumb'] = base_url() . NO_IMAGE;
            }
            $ar['views'] = $this->getArticleViews($ar['pk_post_id']);
            $result = $this->db->query("SELECT title FROM master_club WHERE pk_club_id='" . $ar['fk_club_id'] . "'")->result_array();
            $ar['club'] = "";
            if (count($result) > 0) {
                $ar['club'] = $result[0]['title'];
            }
            $user = $this->getArticlePostedBy($ar['fk_user_id']);
            $ar['posted_by'] = "";
            $ar['posted_by_email'] = "";
            if ($user) {
                $ar['posted_by'] = $user['name'];
                $ar['posted_by_email'] = $user['email'];
            }
            return $ar;
        } else {
            return false;
        }
    }

    function getHomePageArticle($conditions = "1=1") {
        $data = $this->db->query("SELECT *"
                . " FROM `post`"
                . " WHERE status='1' AND is_deleted='0' AND $conditions");

        $arr = $data->result_array();
        if (count($arr) > 0) {
            foreach ($arr as $key => $ar) {
                $image = IMG_ARTICLE . $ar['file_name'];
                if (is_file($image)) {
                    $arr[$key]['image'] = base_url() . $image;
                } else {
                    $arr[$key]['image'] = base_url() . NO_IMAGE;
                }
                $arr[$key]['views'] = $this->getArticleViews($ar['pk_post_id']);
                if ($arr[$key]['views'] > 1000) {
                    $arr[$key]['views'] = floor($arr[$key]['views'] / 1000) . "K";
                }
                $arr[$key]['comments'] = $this->getCommentCount($ar['pk_post_id']);
                $user = $this->getArticlePostedBy($ar['fk_user_id']);
                $arr[$key]['posted_by'] = "";
                $arr[$key]['posted_by_email'] = "";
                if ($user) {
                    $arr[$key]['posted_by'] = $user['name'];
                    $arr[$key]['user_image'] = $user['image'];
                    $arr[$key]['user_slug'] = $user['slug'];
                    $arr[$key]['posted_by_email'] = $user['email'];
                }
            }
            return $arr;
        } else {
            return false;
        }
    }

    function getArticlePostedBy($user_id) {
        $user = $this->db->query("SELECT CONCAT(u.firstname,' ',u.lastname) as name,email,up.profile_pic,u.slug FROM users u LEFT JOIN user_profile up ON u.pk_user_id=up.fk_user_id WHERE u.pk_user_id='" . $user_id . "'")->result_array();
        if (count($user) > 0) {
            if (is_file(PUBLIC_DIR . PROFILE_IMG_DIR . $user[0]['profile_pic'])) {
                $user[0]['image'] = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $user[0]['profile_pic'];
            } else {
                $user[0]['image'] = site_url() . "public/fassets/images/user.jpg";
            }
            return $user[0];
        }
        return false;
    }

    public function getArticleViews($post_id) {
        $data = $this->db->query("SELECT count(*) as views"
                . " FROM `post_views`"
                . " WHERE fk_post_id='$post_id'");
        $arr = $data->result_array();
        return $arr[0]['views'];
    }

    public function getCommentCount($post_id, $post_type = "F") {
        $data = $this->db->query("SELECT count(*) as com_count"
                . " FROM `comments`"
                . " WHERE fk_ref_id='$post_id' AND type='$post_type'");
        $arr = $data->result_array();
        return $arr[0]['com_count'];
    }

    public function getArticleCategorySlug($cat_id) {
//$cat = $this->getCategoryById($cat_id);
    }

    public function saveTags($id, $tags) {
        $tag_data = array();
        if (is_array($tags) and count($tags) > 0) {
            $tag_data = $tags;
        } elseif (trim($tags) != '') {
            $tag_data = explode(",", $tags);
        }
        if (count($tag_data) > 0) {
            $data = array();
            foreach ($tag_data as $tag) {
                $data[] = array('tag' => $tag, 'fk_post_id' => $id);
            }
            $this->db->delete("post_tags", array('fk_post_id' => $id));
            $this->db->insert_batch('post_tags', $data);
        }
    }

    public function getTagsByPostID($id, $type = "String") {
        if ($id > 0) {
            if ($type == "String") {
                $result = $this->db->query("SELECT GROUP_CONCAT(tag SEPARATOR ', ') as tags FROM post_tags WHERE fk_post_id='$id' GROUP BY fk_post_id")->result_array();
                if (count($result) > 0) {
                    return $result[0]['tags'];
                }
                return "";
            } else {
                $result = $this->db->query("SELECT tag FROM post_tags WHERE fk_post_id='$id'")->result_array();
                return $result;
            }
        }
        return "";
    }

    public function getArticleCategories($cat_id) {
        $cat = $this->getCategoryById($cat_id);
        $cats = array();
        $cats[] = array('id' => $cat['pk_category_id'], 'name' => $cat['category'], 'slug' => $cat['slug']);
        if ($cat['parent'] > 0) {
            $cat = $this->getCategoryById($cat['parent']);
            $cats[] = array('id' => $cat['pk_category_id'], 'name' => $cat['category'], 'slug' => $cat['slug']);
        }
        return array_reverse($cats);
    }

    public function getCategoryById($cat_id) {
        $data = $this->db->get_where("categories", array('pk_category_id' => $cat_id))->result_array();
        return $data[0];
    }

    public function getArticleById($id) {
        $data = $this->db->get_where("post", array('pk_post_id' => $id))->result_array();
        $ar = $data[0];
        $ar['tags'] = $this->getTagsByPostID($id);
        $image = IMG_ARTICLE . $ar['file_name'];
        $ar['vid_link'] = "";
        $ar['vid'] = "";
        if ($ar['file_type'] == 'V') {
            $vid_data = explode(".", $ar['file_name']);
            $ar['vid_link'] = "http://www.youtube.com/embed/" . $vid_data[0];
            $ar['vid'] = $vid_data[0];
        }
        if (is_file($image)) {
            $ar['thumb'] = base_url() . $image;
        } else {
            $ar['thumb'] = base_url() . NO_IMAGE;
        }

        return $ar;
    }

    public function getHomePageArticles($section, $page, $per_page) {
        $page -= 1;
        $start = $page * $per_page;
        $query = "SELECT SQL_CALC_FOUND_ROWS * FROM articles WHERE status='1' AND homepage='1' AND display_column='$section' ORDER BY created_at DESC LIMIT $start,$per_page";
        $arr = $this->db->query($query)->result_array();
        $count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $image = IMG_ARTICLE . $ar['image'];
            if (is_file($image)) {
                $arr[$key]['thumb'] = base_url() . $image;
            } else {
                $arr[$key]['thumb'] = base_url() . NO_IMAGE;
            }
            $arr[$key]['categories'] = $this->getArticleCategories($ar['fk_category_id']);
        }
        return array('data' => $arr, 'count' => $count);
    }

    public function getArticleIdBySlug($slug) {
        $data = $this->db->get_where("post", array('slug' => $slug))->result_array();
        if (count($data) > 0) {
            return $data[0]['pk_post_id'];
        } else {
            return false;
        }
    }

    public function getArticleCommentCount($id, $type = 'A') {
        $data = $this->db->query("SELECT COUNT(pk_comment_id) cm_count FROM  `comments` WHERE fk_ref_id = '$id' AND type='$type'")->result_array();

        if (count($data) > 0) {
            return $data[0]['cm_count'];
        } else {
            return false;
        }
    }

    private function saveViews($post_id) {
        $filter = " AND ip_address='" . $_SERVER['REMOTE_ADDR'] . "' AND DATE_FORMAT(created_at,'%Y-%m-%d')='" . date('Y-m-d') . "'";
        if ($this->session->userdata("user_id") > 0) {
            $filter = " AND fk_user_id='" . $this->session->userdata("user_id") . "'";
        }
        $data_views = $this->db->query("SELECT count(*) as vcount FROM post_views WHERE fk_post_id =  '$post_id' $filter")->result_array();
        $data = array('fk_post_id' => $post_id, 'fk_user_id' => $this->session->userdata("user_id"), 'ip_address' => $_SERVER['REMOTE_ADDR']);
        if ($data_views[0]['vcount'] == 0) {
            $this->db->insert("post_views", $data);
        }
    }

    public function getArticleBySlug($slug, $type = 'A') {
        $id = $this->getArticleIdBySlug($slug, $type);
        $this->saveViews($id);
        if ($id > 0) {
            $data = $this->db->query("SELECT p.*, COUNT( pv.fk_post_id ) pv_count FROM  `post` p LEFT JOIN post_views pv ON p.pk_post_id = pv.fk_post_id WHERE p.pk_post_id =  '$id' AND p.status='1' AND is_deleted='0'  GROUP BY pv.fk_post_id")->result_array();
            if (count($data)) {
                $ar = $data[0];
                $ar['tags'] = $this->getTagsByPostID($id, "array");
                $ar['cm_count'] = $this->getArticleCommentCount($id, $type);
                if ($ar['pv_count'] > 1000) {
                    $ar['pv_count'] = floor($ar['pv_count'] / 1000) . "K";
                }
                $ar['vid_link'] = "";
                $ar['vid'] = "";
                if ($ar['file_type'] == 'V') {
                    $vid_data = explode(".", $ar['file_name']);
                    $ar['vid_link'] = "http://www.youtube.com/embed/" . $vid_data[0];
                    $ar['vid'] = $vid_data[0];
                }
                $image = IMG_ARTICLE . $ar['file_name'];
                if (is_file($image)) {
                    $ar['image'] = base_url() . $image;
                } else {
                    $ar['image'] = base_url() . NO_IMAGE;
                }

                return $ar;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getAllCategoryIdsInCategory($cat_id) {
        $data = $this->db->get_where("categories", array('parent' => $cat_id))->result_array();
        $cat_ids = array($cat_id);
        foreach ($data as $dt) {
            $cat_ids[] = $dt['pk_category_id'];
        }
        return $cat_ids;
    }

    public function getArticleBySearch($key, $limit) {
        $key = trim($key);

        $arr = getSearchKeys($this->db->escape_str($key), "articles.slug");
        $arr2 = getSearchKeys($this->db->escape_str($key), "title");
        $arr3 = getSearchKeys($this->db->escape_str($key), "heder_meta");
        $key_search = implode(" AND ", $arr);
        $key_search2 = implode(" AND ", $arr2);
        $key_search3 = implode(" AND ", $arr3);
        $filter = "";
        if ($key != "") {
            $filter = " AND ((" . $key_search . ") OR ($key_search2) OR ($key_search3)) ";
        }
        $data = $this->db->query("SELECT pk_article_id,title,image,content,articles.slug,fk_category_id,categories.category,categories.slug AS cslug FROM articles "
                . " LEFT OUTER JOIN categories ON categories.pk_category_id=articles.fk_category_id WHERE articles.status=1 $filter ORDER BY views DESC LIMIT 0,$limit");
        $arr = $data->result_array();
        return $arr;
    }

    public function getArticleBySearchPage($key, $page) {
        $start = ($page - 1) * PAGING_MED;
        $limit = PAGING_MED;
        $key = trim($key);
        $arr = getSearchKeys($this->db->escape_str($key), "articles.slug");
        $arr2 = getSearchKeys($this->db->escape_str($key), "title");
        $arr3 = getSearchKeys($this->db->escape_str($key), "heder_meta");
        $key_search = implode(" AND ", $arr);
        $key_search2 = implode(" AND ", $arr2);
        $key_search3 = implode(" AND ", $arr3);
        $filter = "";
        if ($key != "") {
            $filter = " AND ((" . $key_search . ") OR ($key_search2) OR ($key_search3)) ";
        }
        $data = $this->db->query("SELECT pk_article_id,title,image,content,articles.slug,fk_category_id,categories.category,categories.slug AS cslug FROM articles "
                . " LEFT OUTER JOIN categories ON categories.pk_category_id=articles.fk_category_id WHERE articles.status=1 $filter ORDER BY views DESC LIMIT $start,$limit");
        $arr = $data->result_array();
        return $arr;
    }

    public function getArticleIdById($id) {
        $arr = $this->db->get_where("articles", array('pk_article_id' => $id))->result_array();
        foreach ($arr as $key => $ar) {
            $image = IMG_ARTICLE . $ar['image'];
            if (is_file($image)) {
                $size = getimagesize($image);
                $arr[$key]['thumb'] = base_url() . $image;
                $arr[$key]['thumb_width'] = $size[0];
                $arr[$key]['thumb_height'] = $size[1];
            } else {
                $size = getimagesize(NO_IMAGE);
                $arr[$key]['thumb'] = base_url() . NO_IMAGE;
                $arr[$key]['thumb_width'] = $size[0];
                $arr[$key]['thumb_height'] = $size[1];
            }
            $arr[$key]['categories'] = $this->getArticleCategories($ar['fk_category_id']);
            $arr[$key]['user'] = $this->getAtricleUser($ar['fk_user_id'], $ar['fk_user_type']);
            $arr[$key]['cslug'] = $arr[$key]['categories'][0]['slug'];
        }
        if (count($arr) > 0) {
            return $arr[0];
        } else {
            return false;
        }
    }

    public function getAtricleUser($id, $type) {
        if ($type == "A") {
            $arr = $this->db->get_where("admins", array("pk_admin_id" => $id))->result_array();
            if (count($arr) > 0) {
                $arr[0]['thumb'] = base_url() . NO_USER;
                if (is_file(IMG_PROFILE . $arr[0]['image'])) {
                    $arr[0]['thumb'] = site_url() . IMG_PROFILE . $arr[0]['image'];
                }
            }
            return $arr[0];
        } else {
            $arr = $this->db->get_where("users", array("pk_user_id" => $id))->result_array();
            if (count($arr) > 0) {
                $arr[0]['thumb'] = base_url() . NO_USER;
                if (is_file(IMG_PROFILE . $arr[0]['image'])) {
                    $arr[0]['thumb'] = site_url() . IMG_PROFILE . $arr[0]['image'];
                }
            }
            return $arr[0];
        }
    }

    public function getComments($article_id, $page, $per_page, $ptype = 'A') {
        $tot_records = $page * $per_page;
        $page -= 1;
        $start = $page * $per_page;
        $query = "SELECT SQL_CALC_FOUND_ROWS comments.*,users.firstname,users.lastname,up.profile_pic FROM comments "
                . " LEFT JOIN users ON users.pk_user_id=comments.fk_user_id "
                . " LEFT JOIN user_profile up ON users.pk_user_id=up.fk_user_id"
                . " WHERE comments.type='$ptype' AND comments.fk_ref_id='" . $article_id . "' AND comments.status='1' ORDER BY pk_comment_id DESC LIMIT $start,$per_page";
        $arr = $this->db->query($query)->result_array();
        $count = getFoundRows();
        if (count($arr) > 0) {
            foreach ($arr as $key => $ar) {
                $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
                $image = PUBLIC_DIR . PROFILE_IMG_DIR . $ar['profile_pic'];
                if (is_file($image)) {
                    $arr[$key]['thumb'] = base_url() . $image;
                } else {
                    $arr[$key]['thumb'] = base_url() . "public/fassets/images/user.jpg";
                }
                $arr[$key]['remove_com'] = false;

                if ($ar['fk_user_id'] == $this->session->userdata("user_id") or $this->session->userdata("utype") == 'A') {
                    $arr[$key]['remove_com'] = true;
                }
            }
            $load = true;
            if ($tot_records >= $count) {
                $load = false;
            }
//        $arr = array_reverse($arr);
            return array('data' => $arr, 'count' => $count, 'load' => $load);
        } else {
            return false;
        }
    }

    public function addArticleViewCount($art_id, $user_id, $ip) {
        $date = getTimestamp(true);
        $data = $this->db->get_where("article_views", array("fk_article_id" => $art_id, "fk_user_id" => $user_id, "ip_address" => $ip, "created_at" => $date))->result_array();
        if (count($data) > 0) {
            $sql = "UPDATE article_views SET view_count=view_count+1 WHERE pk_view_id='" . $data[0]['pk_view_id'] . "'";
            $this->db->query($sql);
        } else {
            $save = array("fk_article_id" => $art_id, "fk_user_id" => $user_id, "ip_address" => $ip, "view_count" => "1", "created_at" => $date);
            $this->db->insert("article_views", $save);
        }
        $datasql = "SELECT SUM(view_count) AS sm FROM article_views WHERE fk_article_id='" . $art_id . "'";
        $data = $this->db->query($datasql)->result_array();
        $update = array("views" => $data[0]['sm']);
        $this->db->update("articles", $update, array("pk_article_id" => $art_id));
    }

    function getCommentList($article_id, $page, $per_page, $searchKey) {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($searchKey != "") {
            $arr = getSearchKeys($this->db->escape_str($searchKey), "comment");
            $filter = implode(" AND ", $arr);
            $filter = " AND " . $filter;
        }
        if ($article_id != "") {
            $filter = " AND comments.fk_ref_id='$article_id'" . $filter;
        }

        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS comments.*,users.firstname,users.lastname,users.email,up.profile_pic,up.phone_no,up.fk_club_id"
                . " FROM comments LEFT OUTER JOIN users ON users.pk_user_id=comments.fk_user_id "
                . " LEFT OUTER JOIN user_profile up ON users.pk_user_id=up.fk_user_id "
                . " WHERE  1=1 $filter  ORDER BY comments.created_at DESC  LIMIT $start,$per_page");
        $arr = $data->result_array();
        $count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            if (is_file(PROFILE_IMG_DIR . $ar['profile_pic'])) {
                $arr[$key]['thumb'] = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $ar['profile_pic'];
            } else {
                $arr[$key]['thumb'] = site_url() . "public/fassets/images/user.jpg";
            }
        }

        return array('data' => $arr, 'count' => $count);
    }

    public function saveYoutubeImage($vid) {
        $url = "http://img.youtube.com/vi/$vid/hqdefault.jpg";

        $content = file_get_contents($url);
//Store in the filesystem.
        $img = $vid . ".jpg";
        @unlink(IMG_ARTICLE . $img);
        @unlink(IMG_ARTICLE . "thumb/" . $img);
        $fp = fopen(IMG_ARTICLE . $img, "w");
        fwrite($fp, $content);
        fclose($fp);
        @copy(IMG_ARTICLE . $img, IMG_ARTICLE . "thumb/" . $img);
        resizeFile(IMG_ARTICLE . "thumb/" . $img, 120);
        //Saving Video Image to User Gallery
        $this->saveUserGalleryData($img, "V");
        return $img;
    }

    public function updateArticleImage($id, $name) {
        if (isset($_FILES[$name]) and $_FILES[$name]['error'] == 0) {
            $config['path'] = IMG_ARTICLE;
            $config['type'] = 'gif|jpg|png|jpeg';
            $config['width'] = "800";
            $config['prefix'] = $id;
            $config['file_name'] = $name;
            $file = uploadFile($config);
            if ($file) {
                setImageRatio(IMG_ARTICLE . $file);
                @copy(IMG_ARTICLE . $file, IMG_ARTICLE . "thumb/" . $file);
                resizeFile(IMG_ARTICLE . "thumb/" . $file, 120);
                $art = $this->db->get_where("post", array('pk_post_id' => $id, 'file_type' => 'I'))->result_array();
                if ($art[0][$name]) {
                    @unlink(IMG_ARTICLE . $art[0]['image']);
                    @unlink(IMG_ARTICLE . "thumb/" . $art[0]['image']);
                }
                $this->db->update("post", array($name => $file), array('pk_post_id' => $id));

                //Saving Image to User Gallery
                $this->saveUserGalleryData($file, "I");
            }
        }
    }

    public function saveUserGalleryData($file, $type) {
        @copy(IMG_ARTICLE . $file, USER_GALLERY . $file);
        @copy(IMG_ARTICLE . "thumb/" . $file, USER_GALLERY . "thumb/" . $file);

        $data = array(
            "file_name" => $file,
            "fk_user_id" => $this->session->userdata("user_id"),
            "status" => "1",
            "file_type" => $type
        );
        $this->db->insert("user_uploads", $data);
    }

    public function addFeedFromArticle($user_id, $article_id) {
        $save_data = array(
            'fk_user_id' => $user_id,
            'feed_ref_id' => $article_id,
        );
        $this->db->insert("news_feed", $save_data);
    }

    public function getClubArticles($club_id) {
//        $arr = $this->db->query("SELECT count(*) as art_count FROM  post WHERE post_type='A' and fk_club_id IN (SELECT u.pk_user_id FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id AND fk_club_id='$club_id' AND status='1' AND is_deleted='0')")->result_array();
        //$arr = $this->db->query("SELECT count(*) as art_count FROM  post WHERE post_type='A' and fk_club_id ='$club_id'")->result_array();
        $arr = $this->db->query("SELECT count(*) as art_count FROM  post WHERE  fk_club_id ='$club_id' AND is_deleted='0'")->result_array();
//        echo $arr[0]['art_count'];
        return $arr[0]['art_count'];
    }

    public function getClubArticleRead($club_id) {
        $arr = $this->db->query("SELECT count(*) art_reads FROM post_views WHERE fk_post_id IN(SELECT pk_post_id FROM post WHERE fk_club_id ='$club_id' AND is_deleted='0')")->result_array();
//        echo $arr[0]['art_count'];
//        $arr[0]['art_reads']="4500";
        if ($arr[0]['art_reads'] > 1000) {
            $count = intval($arr[0]['art_reads'] / 1000);
            return $count . "K";
        }
        return $arr[0]['art_reads'];
    }

    public function getWritersByPointsFilter($league_id, $club_id, $records) {
        $filter = "";
        if ($league_id > 0) {
            $filter.=" AND u.fk_league_id='$league_id' ";
        }
        if ($club_id > 0) {
            $filter.=" AND up.fk_club_id='$club_id' ";
        }
        $arr = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.firstname,u.lastname,u.slug,up.profile_pic,(SELECT SUM(points) as pnts FROM user_points WHERE fk_user_id=up.fk_user_id AND DATE_FORMAT(created_at,'%Y-%m') = '" . date("Y-m") . "' AND point_type IN ('W_ART_WRT','W_ART_COM','W_ART_READS','W_ART_SHARE','W_NF_COM','W_NF_LIKE')) as points "
                        . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id $filter"
                        . " WHERE u.status='1' AND u.is_deleted='0' ORDER BY points DESC LIMIT 0, $records")->result_array();
        $writer_count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            if (is_file(IMG_PROFILE . "thumb/" . $ar['profile_pic'])) {
                $arr[$key]['thumb'] = site_url() . IMG_PROFILE . "thumb/" . $ar['profile_pic'];
            } else {
                $arr[$key]['thumb'] = site_url() . "public/fassets/images/user.jpg";
            }
            $arr[$key]['points'] = intval($arr[$key]['points']);
        }
        $data = array('wdata' => false, 'wr_count' => "0");
        if (count($arr) > 0) {
            $data['wdata'] = $arr;
            $data['wr_count'] = $writer_count;
        }
        return $data;
    }

    public function getTopFanzFilter($league_id, $club_id, $records) {
        $filter = "";
        if ($league_id > 0) {
            $filter.=" AND u.fk_league_id='$league_id' ";
        }
        if ($club_id > 0) {
            $filter.=" AND up.fk_club_id='$club_id' ";
        }
        $arr = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.firstname,u.lastname,u.slug,up.profile_pic, "
                        . ""
                        . "(SELECT SUM(points) as pnts FROM user_points WHERE fk_user_id=up.fk_user_id AND DATE_FORMAT(created_at,'%Y-%m') = '" . date("Y-m") . "' AND point_type NOT IN ('W_ART_WRT','W_ART_COM','W_ART_READS','W_ART_SHARE','W_NF_COM','W_NF_LIKE')) as points "
                        . " "
                        . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id $filter "
                        . " WHERE u.status='1' AND u.is_deleted='0'  ORDER BY points DESC LIMIT 0, $records")->result_array();
        $fan_count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            if (is_file(IMG_PROFILE . "thumb/" . $ar['profile_pic'])) {
                $arr[$key]['thumb'] = site_url() . IMG_PROFILE . "thumb/" . $ar['profile_pic'];
            } else {
                $arr[$key]['thumb'] = site_url() . "public/fassets/images/user.jpg";
            }
            $arr[$key]['points'] = intval($arr[$key]['points']);
        }
        $data = array('fan_data' => false, 'fan_count' => "0");
        if (count($arr) > 0) {
            $data['fan_data'] = $arr;
            $data['fan_count'] = $fan_count;
        }
        return $data;
    }

    public function getWritersByPoints($club_id, $records) {
        $filter = "";
        if ($club_id > 0) {
            $filter.=" AND up.fk_club_id='$club_id' ";
        }
        $arr = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.firstname,u.lastname,u.slug,up.profile_pic,sum(points) as points "
                        . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id $filter"
                        . " INNER JOIN post  ON u.pk_user_id=post.fk_user_id "
                        . " LEFT JOIN user_points p ON u.pk_user_id=p.fk_user_id "
                        . " WHERE u.status='1' AND u.is_deleted='0' GROUP BY p.fk_user_id ORDER BY points DESC LIMIT 0, $records")->result_array();
        $writer_count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            if (is_file(IMG_PROFILE . "thumb/" . $ar['profile_pic'])) {
                $arr[$key]['thumb'] = site_url() . IMG_PROFILE . "thumb/" . $ar['profile_pic'];
            } else {
                $arr[$key]['thumb'] = site_url() . "public/fassets/images/user.jpg";
            }
            $arr[$key]['points'] = intval($arr[$key]['points']);
        }
        $data = array('wdata' => false, 'wr_count' => "0");
        if (count($arr) > 0) {
            $data['wdata'] = $arr;
            $data['wr_count'] = $writer_count;
        }
        return $data;
    }

    public function getTopFanz($club_id, $records) {



        $filter = "";
        if ($club_id > 0) {
            $filter.=" AND up.fk_club_id='$club_id' ";
        }
        $arr = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.firstname,u.lastname,u.slug,up.profile_pic,count(f.fk_user_id) as points "
                        . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id $filter "
                        . " LEFT JOIN followers f ON u.pk_user_id=f.fk_ref_id AND follow_type='U'"
                        . " WHERE u.status='1' AND u.is_deleted='0' GROUP BY u.pk_user_id ORDER BY points DESC LIMIT 0, $records")->result_array();
        $fan_count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            if (is_file(IMG_PROFILE . "thumb/" . $ar['profile_pic'])) {
                $arr[$key]['thumb'] = site_url() . IMG_PROFILE . "thumb/" . $ar['profile_pic'];
            } else {
                $arr[$key]['thumb'] = site_url() . "public/fassets/images/user.jpg";
            }
            $arr[$key]['points'] = intval($arr[$key]['points']);
        }
        $data = array('fan_data' => false, 'fan_count' => "0");
        if (count($arr) > 0) {
            $data['fan_data'] = $arr;
            $data['fan_count'] = $fan_count;
        }
        return $data;
    }

    function getTopFanzList($page, $per_page, $searchKey, $ftype = array()) {
        $this->load->model('user_model');
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";

        if (isset($ftype['league_id']) and $ftype['league_id'] > 0) {
            $filter .= " AND up.fk_club_id IN (SELECT pk_club_id FROM master_club WHERE status='1' AND is_deleted='0' AND fk_league_id='" . $ftype['league_id'] . "')";
        }
        if (isset($ftype['club_id']) and $ftype['club_id'] > 0) {
            $filter .= " AND up.fk_club_id='" . $ftype['club_id'] . "'";
        }
        $arr = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.pk_user_id,u.firstname,u.lastname,u.slug,up.profile_pic, (SELECT SUM(points) as pnts FROM user_points WHERE fk_user_id=up.fk_user_id   AND DATE_FORMAT(created_at,'%Y-%m') = '" . date("Y-m") . "' AND point_type NOT IN('W_ART_WRT','W_ART_COM','W_ART_READS','W_ART_SHARE','W_NF_COM','W_NF_LIKE')) as points "
                        . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id $filter "
                        . " WHERE u.status='1' AND u.is_deleted='0' ORDER BY points DESC LIMIT $start,$per_page")->result_array();

        $count = getFoundRows();
        $data = array();
        foreach ($arr as $key => $ar) {
            $user_id = $ar['pk_user_id'];
            $data[$key]['points'] = ($ar['points'] > 0) ? $ar['points'] : "0";
            //$data[$key]['pos'] = $this->user_model->getUserPosition($user_id);
            $data[$key]['pos'] = $start + ($key + 1);
            $data[$key]['ud'] = $this->user_model->getUserDetails($user_id);
            $data[$key]['uh'] = $this->user_model->getUserHighlights($user_id);
        }
        if (count($data) > 0) {
            return array('data' => $data, 'count' => $count);
        }
        return array('data' => false, 'count' => $count);
    }

    function getTopWritersList($page, $per_page, $searchKey, $ftype = array()) {
        $this->load->model('user_model');
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";

        if (isset($ftype['league_id']) and $ftype['league_id'] > 0) {
            $filter .= " AND up.fk_club_id IN (SELECT pk_club_id FROM master_club WHERE status='1' AND is_deleted='0' AND fk_league_id='" . $ftype['league_id'] . "')";
        }
        if (isset($ftype['club_id']) and $ftype['club_id'] > 0) {
            $filter .= " AND up.fk_club_id='" . $ftype['club_id'] . "'";
        }

        $arr = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.pk_user_id, (SELECT SUM(points) as pnts FROM user_points WHERE fk_user_id=up.fk_user_id  AND DATE_FORMAT(created_at,'%Y-%m') = '" . date("Y-m") . "' AND point_type IN ('W_ART_WRT','W_ART_COM','W_ART_READS','W_ART_SHARE','W_NF_COM','W_NF_LIKE')) as points "
                        . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id $filter"
                        . " WHERE u.status='1' AND u.is_deleted='0' ORDER BY points DESC LIMIT $start,$per_page")->result_array();

        $count = getFoundRows();
        $data = array();
        foreach ($arr as $key => $ar) {

            $user_id = $ar['pk_user_id'];
            $data[$key]['points'] = ($ar['points'] > 0) ? $ar['points'] : "0";
            //$data[$key]['pos'] = $this->user_model->getUserPosition($user_id);
            $data[$key]['pos'] = $start + ($key + 1);
            $data[$key]['ud'] = $this->user_model->getUserDetails($user_id);
            $data[$key]['uh'] = $this->user_model->getUserHighlights($user_id);
        }
        if (count($data) > 0) {
            return array('data' => $data, 'count' => $count);
        }
        return array('data' => false, 'count' => $count);
    }

    public function getClubPoints($records, $league_id = "") {
        if ($league_id != "") {
            $arr = $this->db->query("SELECT master_club.title,master_club.slug,(SELECT SUM((SELECT sum(points) FROM user_points where user_points.fk_user_id=user_profile.fk_user_id AND created_at>=CURRENT_TIMESTAMP - INTERVAL 1 MONTH )) AS pnt FROM user_profile WHERE user_profile.fk_club_id=master_club.pk_club_id) AS points FROM master_club WHERE fk_league_id='$league_id' ORDER BY points DESC LIMIT 0, $records")->result_array();
        } else {
            $arr = $this->db->query("SELECT master_club.title,master_club.slug,(SELECT SUM((SELECT sum(points) FROM user_points where user_points.fk_user_id=user_profile.fk_user_id AND created_at>=CURRENT_TIMESTAMP - INTERVAL 1 MONTH )) AS pnt FROM user_profile WHERE user_profile.fk_club_id=master_club.pk_club_id) AS points FROM master_club ORDER BY points DESC LIMIT 0, $records")->result_array();
        }


        return $arr;
    }

    public function getArticleIdByUserId($user_id) {
        $data = $this->db->query("SELECT p.* FROM  `post` p  WHERE p.fk_user_id =  '$user_id' AND p.status='1' AND is_deleted='0' ORDER BY pk_post_id DESC LIMIT 1")->result_array();
        if (count($data)) {
            $ar = $data[0];
            $ar['vid_link'] = "";
            $ar['vid'] = "";
            if ($ar['file_type'] == 'V') {
                $vid_data = explode(".", $ar['file_name']);
                $ar['vid_link'] = "http://www.youtube.com/embed/" . $vid_data[0];
                $ar['vid'] = $vid_data[0];
            }
            $image = IMG_ARTICLE . $ar['file_name'];
            if (is_file($image)) {
                $ar['image'] = base_url() . $image;
            } else {
                $ar['image'] = base_url() . NO_IMAGE;
            }

            return $ar;
        } else {
            return false;
        }
    }

}

?>

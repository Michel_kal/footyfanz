<?php

class email_model extends CI_Model {

    function getOptions() {
        $res = $this->db->get("emails");
        $data = $res->result_array();
        return $data;
    }

    function getEmail($id) {
        $res = $this->db->get_where("emails", array("pk_email_id" => $id));
        $data = $res->result_array();
        return $data[0];
    }

    function getEmailList($page, $per_page, $searchKey, $category = "") {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($searchKey != "") {
            $arr = getSearchKeys($this->db->escape_str($searchKey), "newsletter.email");
            $filter = implode(" AND ", $arr);
            $filter = " AND " . $filter;
        }
        if ($category > 0) {
            $filter .= " AND newsletter.fk_club_id='" . $category . "'";
        }
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS newsletter.*,master_club.title "
                . " FROM newsletter "
                . " LEFT OUTER JOIN master_club ON master_club.pk_club_id=newsletter.fk_club_id "
                . " WHERE  newsletter.status='1' $filter  ORDER BY created_at DESC  LIMIT $start,$per_page");
        $arr = $data->result_array();
        $count = getFoundRows();
        return array('data' => $arr, 'count' => $count);
    }

    function getEmailLogList($page, $per_page, $searchKey) {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($searchKey != "") {
            $arr = getSearchKeys($this->db->escape_str($searchKey), "newsletter_log.subject");
            $filter = implode(" AND ", $arr);
            $filter = " AND " . $filter;
        }
        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS newsletter_log.*,master_club.title "
                . " FROM newsletter_log "
                . " LEFT OUTER JOIN master_club ON master_club.pk_club_id=newsletter_log.fk_club_id "
                . " WHERE 1=1 $filter  ORDER BY created_at DESC  LIMIT $start,$per_page");
        $arr = $data->result_array();
        $count = getFoundRows();
        return array('data' => $arr, 'count' => $count);
    }

    function getEmailCountInCategory($cat_id,$league_id="") {
        $filter="";
        if ($cat_id > 0) {
            $filter=" AND fk_club_id='$cat_id' ";
           
        }
        if ($league_id > 0) {
            $filter.=" AND fk_league_id='$league_id' ";
           
        }
            $data = $this->db->query("SELECT COUNT(DISTINCT(email)) as cnt FROM newsletter WHERE status='1' $filter")->result_array();
       
        return $data[0]['cnt'];
    }

    function getEmailsInCategory($cat_id,$league_id="") {
          $filter="";
        if ($cat_id > 0) {
            $filter=" AND fk_club_id='$cat_id' ";
           
        }
        if ($league_id > 0) {
            $filter.=" AND fk_league_id='$league_id' ";
           
        }  
            $data = $this->db->query("SELECT DISTINCT(email) as email FROM newsletter WHERE status='1' $filter")->result_array();
      
        $emails = array();
        foreach ($data as $dt) {
            $emails[] = $dt['email'];
        }
        return $emails;
    }

}

?>
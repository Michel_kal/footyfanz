<?php

class gallery_model extends CI_Model {

    public function getGalleryVideos($limit = 5) {
        $data = $this->db->query("SELECT * FROM home_gallery WHERE status='1' ORDER BY pk_gallery_id DESC LIMIT 0,$limit")->result_array();

        foreach ($data as $key => $ar) {
            $vid_file = VID_GALLERY . $ar['file_name'];
            $data[$key]['vid_link'] = "";
            $data[$key]['vid'] = false;
            if ($ar['type'] == 'Y') {
                $vid_data = explode(".", $ar['file_name']);
                $data[$key]['vid_link'] = "http://www.youtube.com/embed/" . $vid_data[0];
                $data[$key]['vid_image'] = site_url() . IMG_GALLERY . $ar['file_name'];
                $data[$key]['vid'] = true;
            } else {
                if (is_file($vid_file)) {
                    $data[$key]['vid_link'] = base_url() . $vid_file;
                    $data[$key]['vid'] = true;
                }
            }
        }


        return $data;
    }

}

?>

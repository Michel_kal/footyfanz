<?php

class login_model extends CI_Model {

    function checkEmailExist($email, $ignore = "") {
        $data = $this->db->get_where("users", array("email" => $email))->result_array();
        if (count($data) > 0) {
            if ($data[0]['pk_user_id'] == $ignore) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    function checkUsernameExist($username, $ignore = "") {
        $data = $this->db->get_where("users", array("username" => $username))->result_array();
        if (count($data) > 0) {
            if ($data[0]['pk_user_id'] == $ignore) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

    function checkTwitterExist($twitter_id) {
        $data = $this->db->get_where("users", array("twitter_id" => $twitter_id))->result_array();
        if (count($data) > 0) {
            return $data[0]['id'];
        } else {
            return FALSE;
        }
    }

    function checkInstagramExist($ins_id) {
        $data = $this->db->get_where("users", array("instagram_id" => $ins_id))->result_array();
        if (count($data) > 0) {
            return $data[0]['id'];
        } else {
            return FALSE;
        }
    }

    function getUser($id) {
        $data = $this->db->get_where("users", array("pk_user_id" => $id))->result_array();
        $data1 = $this->db->get_where("user_profile", array("fk_user_id" => $id))->result_array();
        $finaldata = array_merge($data[0], $data1[0]);
        $club = $this->db->get_where("master_club", array("pk_club_id" => $finaldata['fk_club_id']))->result_array();
        $league_id = "0";
        $club_image = "";
        $club_slug="";
        if (count($club) > 0) {
            $league_id = $club[0]['fk_league_id'];
            $club_slug=$club[0]['slug'];
            $club_image = CLUB_IMG_DIR . $club[0]['logo'];
            if (is_file($club_image)) {
                $club_image = site_url() . $club_image;
            } else {
                $club_image = "";
            }
        }
        $finaldata['club_image'] = $club_image;
        $finaldata['club_slug'] = $club_slug;
        $finaldata['fk_league_id'] = $league_id;
        if ($finaldata['fk_club_id'] != "") {
            $finaldata['profile_status'] = "1";
        } else {
            $finaldata['profile_status'] = "0";
        }
        $finaldata['profile_status'] = "1";
        return $finaldata;
    }

}

?>

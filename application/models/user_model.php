<?php

class user_model extends CI_Model {

    public $ptype = array('ART_COM' => '1', 'NF_TEXT' => '3', 'NF_IMG' => '4', 'NF_VID' => '5', 'ART_SHARE' => '2', 'W_ART_WRT' => '7', 'W_ART_READS' => '1', 'ART_SHARE' => '2', 'W_ART_SHARE' => '3', 'W_ART_COM' => '1', 'NF_COM' => '1', 'NF_SHARE' => '2', 'W_NF_COM' => '1', 'NF_LIKE' => '1', 'W_NF_LIKE' => '1');
    public $writers = array('W_ART_WRT', 'W_ART_COM', 'W_ART_READS', 'W_ART_SHARE', 'W_NF_COM', 'W_NF_LIKE');
    public $ptype_detail = array(
        'ART_COM' => 'Comments you make on an article',
        'NF_TEXT' => 'TEXT posts made on the Newsfeed',
        'NF_IMG' => 'IMAGE posts made on the Newsfeed',
        'NF_VID' => 'VIDEO posts made on the Newsfeed',
        'ART_SHARE' => 'Share articles you read to social media',
        'W_ART_WRT' => 'Write an Article',
        'W_ART_READS' => 'Reads by Fanz on your Articles',
        'W_ART_SHARE' => 'Shares by fanz of your article to social media',
        'W_ART_COM' => 'Comments made by fanz on your article ',
        'NF_COM' => 'Comments made on your Newsfeed post',
        'NF_SHARE' => 'Share the newsfeed',
        'W_NF_COM' => 'Comments made on your newsfeed',
        'NF_LIKE' => 'Likes made on your Newsfeed post',
        'W_NF_LIKE' => 'Like made on your newsfeed'
    );

//ALTER TABLE `user_points` CHANGE `point_type` `point_type` ENUM( 'ART_COM', 'NF_TEXT', 'NF_COM', 'NF_IMG', 'NF_VID', 'ART_SHARE', 'W_ART_WRT', 'W_ART_READS', 'W_ART_SHARE', 'NF_SHARE', 'W_ART_COM','W_NF_LIKE','NF_LIKE','W_NF_COM' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL
//   For Fanz 
//1. VIDEO posts made on the Newsfeed                           (5 points)
//2. IMAGE posts made on the Newsfeed                           (4 points)
//3. TEXT posts made on the Newsfeed                              (3 points)
//4. Share articles you read to social media                         (2 points)
//5. Comments you make on an article                                 (1 point)
//6. Likes and Comments made on your Newsfeed post       (1 point)
//7. Likes and Comments you make on other posts              (1 point)
//
//For Writers
//1. Write an Article                                                                                 (7 points)
//2. Number of Shares by fanz of your article to social media               (3 points each)
//3. Number of Comments made by fanz on your article                       (1 point each)
//4. Number of Reads by Fanz on your Articles                                    (1 point each)
//5. Comments made on your newsfeed            (1 point each)
// COMMENT '"ART_COM","NF_TEXT","NF_IMG","NF_VID","ART_SHARE","W_ART_WRT","W_ART_READS","W_ART_SHARE"'


    function getPointList($page, $per_page, $search = "", $user_id) {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";

        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS user_points.*,CONCAT(users.firstname,' ',users.lastname) as user_name,users.slug AS userslug "
                . " FROM user_points "
                . " LEFT OUTER JOIN users ON users.pk_user_id=user_points.ref_user_id"
                . " WHERE user_points.fk_user_id='$user_id' ORDER BY pk_point_id desc LIMIT $start,$per_page");
        $arr = $data->result_array();
        foreach ($arr as $key => $user) {
            if (in_array($user["point_type"], $this->writers)) {
                $arr[$key]['writer'] = "1";
            } else {
                $arr[$key]['writer'] = "0";
            }
            $arr[$key]['type'] = $this->ptype_detail[$user["point_type"]];
        }
        return array('data' => $arr, 'count' => getFoundRows(), 'start' => ($start + 1));
    }

    function getUserList($page, $per_page, $search = "", $type = "1") {
        $page -= 1;
        $start = $page * $per_page;
        $filter = "";
        if ($search != "") {
            $sc1 = getSearchKeys($search, "u.firstname");
            $sc2 = getSearchKeys($search, "u.lastname");
            $sc3 = getSearchKeys($search, "u.email");
            $filter = " AND ((" . implode(" OR ", $sc1) . ") or (" . implode(" OR ", $sc2) . ") or (" . implode(" OR ", $sc3) . ")) ";
        }

        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.*,up.* "
                . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id"
                . " WHERE u.status='$type' AND user_role!='A' $filter  ORDER BY u.pk_user_id desc LIMIT $start,$per_page");
        $arr = $data->result_array();
        foreach ($arr as $key => $user) {

            $arr[$key]['profile_pic'] = (is_file(IMG_PROFILE . $user['profile_pic'])) ? site_url() . IMG_PROFILE . $user['profile_pic'] : site_url() . "public/fassets/images/user.jpg";
            $arr[$key]['name'] = $user['firstname'] . ' ' . $user['lastname'];
        }
        return array('data' => $arr, 'count' => getFoundRows(), 'start' => ($start + 1));
    }

    function getFootyFanz($page, $per_page, $sk, $user_id) {
        $req_page = $page;

        $tot_records = $page * $per_page;
        $page -= 1;
        $start = $page * $per_page;
        $search = $sk['search_key'];
        $league = $sk['league'];
        $club = $sk['club'];
        $filter = "";
        if ($league > 0) {
            $filter.= " AND u.fk_league_id='$league' ";
        }
        if ($club > 0) {
            $filter.= " AND up.fk_club_id='$club' ";
        }
        if ($search != "") {
            $sc1 = getSearchKeys($search, "u.firstname");
            $sc2 = getSearchKeys($search, "u.lastname");
            $filter.= " AND ((" . implode(" OR ", $sc1) . ") OR (" . implode(" OR ", $sc2) . ") ) ";
        }

        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.*,up.*,master_club.title AS club_title, master_club.logo AS club_logo,master_club.slug AS club_slug "
                . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id"
                . " LEFT OUTER JOIN master_club ON master_club.pk_club_id = up.fk_club_id"
                . " WHERE u.status='1' AND ff_plus='1' $filter  ORDER BY u.pk_user_id desc LIMIT $start,$per_page");
        $count = getFoundRows();
        $arr = $data->result_array();
        foreach ($arr as $key => $user) {

            $arr[$key]['profile_pic'] = (is_file(IMG_PROFILE . $user['profile_pic'])) ? site_url() . IMG_PROFILE . $user['profile_pic'] : site_url() . "public/fassets/images/user.jpg";
            $arr[$key]['name'] = $user['firstname'] . ' ' . $user['lastname'];
            $arr[$key]['club_id'] = $user['fk_club_id'];
            $arr[$key]['club_slug'] = $user['club_slug'];
            $arr[$key]['follow'] = $this->checkFollowStatus($user_id, $user['pk_user_id']);
            $club_image = CLUB_IMG_DIR . $user['club_logo'];
            if (is_file($club_image)) {
                $arr[$key]['club_image'] = site_url() . $club_image;
            } else {
                $arr[$key]['club_image'] = "";
            }
        }
        $load = true;
        if ($tot_records >= $count) {
            $load = false;
        }

        return array('data' => $arr, 'count' => getFoundRows(), 'load' => $load, 'page' => $req_page);
    }

    function checkFollowStatus($self, $other) {
        $follow = $this->db->get_where("followers", array("fk_user_id" => $self, "fk_ref_id" => $other))->result_array();
        if (count($follow) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getUserById($id) {
        $data = $this->db->query("SELECT u.*,up.* "
                . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id"
                . " WHERE u.pk_user_id='$id'");
        $arr = $data->result_array();
        $user = $arr[0];
        $user['profile_pic'] = (is_file(IMG_PROFILE . $user['profile_pic'])) ? site_url() . IMG_PROFILE . $user['profile_pic'] : site_url() . "public/fassets/images/user.jpg";
        $user['name'] = $user['firstname'] . ' ' . $user['lastname'];

        return $user;
    }

    function getUserByFeedID($feed_id) {
        $data = $this->db->query("SELECT u.firstname,u.lastname,u.slug,up.profile_pic "
                . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id"
                . " WHERE u.pk_user_id IN (SELECT fk_user_id FROM news_feed WHERE pk_feed_id='$feed_id')");
        $arr = $data->result_array();
        $user = $arr[0];
        $user['profile_pic'] = (is_file(IMG_PROFILE . $user['profile_pic'])) ? site_url() . IMG_PROFILE . $user['profile_pic'] : site_url() . "public/fassets/images/user.jpg";
        $user['name'] = $user['firstname'] . ' ' . $user['lastname'];
        if (count($arr) > 0) {
            return $user;
        }
        return false;
    }

    function getUserPosition($id) {
        $result = $this->db->query("SELECT users.pk_user_id, SUM( points ) AS points FROM user_points RIGHT JOIN users ON user_points.fk_user_id = users.pk_user_id GROUP BY pk_user_id ORDER BY points DESC , pk_user_id ASC")->result_array();
        if (count($result) > 0) {
            $pos = 1;
            foreach ($result as $row) {
                if ($id == $row['pk_user_id']) {
                    break;
                }
                $pos++;
            }
        }
        return $pos;
    }

    function followStatus($art_user_id, $user_id, $type = 'U') {
        $data = $this->db->query("SELECT COUNT(*) f_count FROM  `followers` WHERE fk_ref_id = '$art_user_id' AND fk_user_id='$user_id' AND follow_type='$type'")->result_array();
        return $data[0]['f_count'];
    }

    function follow($following_id, $user_id, $type = 'U') {
        $follow_status = $this->followStatus($following_id, $user_id);
        if ($follow_status == 0) {
            $save_data = array(
                "fk_ref_id" => $following_id,
                "fk_user_id" => $user_id,
                'follow_type' => $type
            );
            $this->db->insert("followers", $save_data);


            $noti = array(
                "type" => "FOLLOW",
                "fk_user_id" => $following_id,
                "content" => $this->session->userdata("name") . " is following you.",
                "ref_id" => $user_id,
                "read_status" => "0",
                "sent_by" => $user_id
            );
            $this->db->delete("notifications", array("type" => "FOLLOW", "fk_user_id" => $following_id, "sent_by" => $user_id));
            $this->db->insert("notifications", $noti);
        }

        return true;
    }

    function unfollow($following_id, $user_id, $type = 'U') {
        $delete_data = array(
            "fk_ref_id" => $following_id,
            "fk_user_id" => $user_id,
            'follow_type' => $type
        );
        $this->db->delete("followers", $delete_data);

        return true;
    }

    function getUserIDBySlug($slug) {
        $arr = $this->db->query("SELECT pk_user_id FROM users WHERE slug='$slug'")->result_array();
        if (count($arr) > 0) {
            return $arr[0]['pk_user_id'];
        }
        return false;
    }

    function getUserDetails($id) {
        $query = $this->db->query("SELECT u.*,up.* FROM users u LEFT JOIN user_profile up ON u.pk_user_id=up.fk_user_id WHERE pk_user_id='$id'");
        if ($query->num_rows > 0) {
            $arr = $query->result_array();
            $arr[0]['thumb'] = base_url() . NO_USER;
            if (is_file(IMG_PROFILE . $arr[0]['profile_pic'])) {
                $arr[0]['thumb'] = site_url() . IMG_PROFILE . $arr[0]['profile_pic'];
            }
            $arr[0]['name'] = $arr[0]['firstname'] . ' ' . $arr[0]['lastname'];

            $finaldata = $arr[0];
            $club = $this->db->get_where("master_club", array("pk_club_id" => $finaldata['fk_club_id']))->result_array();
            $league_id = "0";
            $club_image = "";
            if (count($club) > 0) {
                $league_id = $club[0]['fk_league_id'];
                $club_image = CLUB_IMG_DIR . $club[0]['logo'];
                if (is_file($club_image)) {
                    $club_image = site_url() . $club_image;
                } else {
                    $club_image = "";
                }
            }
            $finaldata['club_image'] = $club_image;
            $finaldata['club_name'] = (isset($club[0]['title'])) ? $club[0]['title'] : "";
            $finaldata['fk_league_id'] = $league_id;
            $finaldata['laurals'] = $this->db->query("SELECT * FROM laurals WHERE fk_user_id='$id' ORDER BY l_year DESC,l_month DESC LIMIT 0,4")->result_array();
            return $finaldata;
        }
        return false;
    }

    function SavePoints($id, $ref_id, $type, $one_time = false) {
        if ($type == "") {
            return false;
        }
        $logged_in_user = $this->session->userdata("user_id");
        if (($logged_in_user > 0) == false) {
            return true;
        }
        $save_data = array('point_type' => $type, 'points' => $this->ptype[$type], 'fk_ref_id' => $ref_id, 'fk_user_id' => $id, "ref_user_id" => $logged_in_user);
        if ($one_time) {
            $arr = $this->db->query("SELECT count(*) as row_count FROM user_points WHERE  point_type='$type' AND fk_ref_id='$ref_id' AND fk_user_id='$id'")->result_array();
            if ($arr[0]['row_count'] == 0) {
                $this->db->insert("user_points", $save_data);
            }
        } else {
            if ($type == "W_ART_READS") {
                $exist = $this->db->get_where("user_points", array('point_type' => $type, 'fk_ref_id' => $ref_id, 'fk_user_id' => $id, "ref_user_id" => $logged_in_user))->result_array();
                if (count($exist) == 0) {
                    $this->db->insert("user_points", $save_data);
                }
            } else {
                $this->db->insert("user_points", $save_data);
            }
        }


        return true;
    }

    function sendMessage($post) {
        $filter = "";
        if ($post['league'] == 0) {
            $filter = "";
        } else if ($post['league'] > 0) {
            if (is_array($post['club']) and count($post['club']) > 0) {
                $filter = " AND up.fk_club_id IN('" . implode("','", $post['club']) . "')";
            } else {
                $filter = " AND up.fk_club_id IN(SELECT pk_club_id FROM master_club fk_league_id='" . $post['league'] . "')";
            }
        }
        $query = $this->db->query("SELECT u.pk_user_id FROM users u LEFT JOIN user_profile up ON u.pk_user_id=up.fk_user_id AND u.status='1' AND u.is_deleted='0' WHERE  u.user_role!='A' $filter");
        $arr = $query->result_array();
        $insert = array();
        if (count($arr) > 0) {
            foreach ($arr as $row) {
                $insert[] = array('title' => $post['title'], 'content' => $post['content'], 'fk_user_id' => $row['pk_user_id'], 'sent_by' => $this->session->userdata("admin_id"));
            }
            $this->db->insert_batch("messages", $insert);
        }
        return true;
    }

    public function getMessages($user_id, $page, $per_page, $markreads = FALSE) {
        $tot_records = $page * $per_page;
        $page -= 1;
        $start = $page * $per_page;
        $query = "SELECT SQL_CALC_FOUND_ROWS messages.*,users.firstname,users.lastname,up.profile_pic "
                . " FROM messages "
                . " LEFT JOIN users ON users.pk_user_id=messages.sent_by "
                . " LEFT JOIN user_profile up ON users.pk_user_id=up.fk_user_id"
                . " WHERE messages.fk_user_id='" . $user_id . "' ORDER BY pk_message_id DESC LIMIT $start,$per_page";
        $arr = $this->db->query($query)->result_array();
        $count = getFoundRows();
        $ids = array();
        if (count($arr) > 0) {
            foreach ($arr as $key => $ar) {
                $ids[] = $ar['pk_message_id'];
                $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
                $image = PUBLIC_DIR . PROFILE_IMG_DIR . $ar['profile_pic'];
                if (is_file($image)) {
                    $arr[$key]['thumb'] = base_url() . $image;
                } else {
                    $arr[$key]['thumb'] = base_url() . "public/fassets/images/user.jpg";
                }
            }
            $load = true;
            if ($tot_records >= $count) {
                $load = false;
            }
            if ($markreads and count($ids) > 0) {
                $this->db->update("messages", array("read_status" => "1"), "pk_message_id IN ('" . implode("','", $ids) . "')");
            }
//        $arr = array_reverse($arr);
            return array('data' => $arr, 'count' => $count, 'load' => $load, "ids" => $ids);
        } else {
            return false;
        }
    }

    public function getUserHighlights($user_id) {
        $points = $this->db->query("SELECT sum(points) as points FROM user_points WHERE fk_user_id='$user_id'")->result_array();
        $data = array();
        $data['points'] = intval($points[0]['points']);

        $points_month = $this->db->query("SELECT sum(points) as points FROM user_points WHERE fk_user_id='$user_id' AND DATE_FORMAT(created_at,'%Y-%m') = '" . date("Y-m") . "' ")->result_array();
        $data['points_month'] = intval($points_month[0]['points']);

        $laurals = $this->db->query("SELECT * FROM laurals WHERE fk_user_id='$user_id' ORDER BY l_year DESC,l_month DESC ")->result_array();

        $data['laurals'] = $laurals;


        $art = $this->db->query("SELECT count(*) as art_count FROM post WHERE is_deleted='0' AND fk_user_id='$user_id' ")->result_array();
        $data['art_count'] = $art[0]['art_count'];

        $followers = $this->db->query("SELECT count(*) as f_count FROM followers WHERE fk_ref_id='$user_id' AND follow_type='U' ")->result_array();
        $data['followers'] = $followers[0]['f_count'];

        $reads = $this->db->query("SELECT count(*) as row_count FROM post_views WHERE fk_post_id IN (SELECT fk_user_id FROM post WHERE is_deleted='0' AND fk_user_id='$user_id') ")->result_array();

        $data['reads'] = $reads[0]['row_count'];
        $following = $this->db->query("SELECT count(*) as f_count FROM followers WHERE fk_user_id='$user_id' AND follow_type='U'")->result_array();
        $data['following'] = $following[0]['f_count'];
        return $data;
    }

    public function getHeaderCounts() {
        $ret = array("noti" => 0, "msg" => 0);
        $user_id = $this->session->userdata("user_id");
        if ($user_id > 0) {
            $data = $this->db->query("SELECT count(*) as cnt FROM messages WHERE fk_user_id='$user_id' AND read_status='0'")->result_array();
            $ret['msg'] = $data[0]['cnt'];
            $data = $this->db->query("SELECT count(*) as cnt FROM notifications WHERE fk_user_id='$user_id' AND read_status='0'")->result_array();
            $ret['noti'] = $data[0]['cnt'];
        }
        return $ret;
    }

    public function getFollowers($user_id, $limit = 9) {
        $query = $this->db->query("SELECT u.pk_user_id,u.slug,u.firstname,u.lastname,up.profile_pic ,(SELECT sum(points) as points FROM user_points WHERE user_points.fk_user_id=u.pk_user_id) AS points "
                . " FROM users u "
                . "LEFT JOIN user_profile up ON u.pk_user_id=up.fk_user_id "
                . "WHERE pk_user_id IN (SELECT fk_user_id FROM followers WHERE fk_ref_id='$user_id') ORDER BY points DESC LIMIT 0,$limit");
        if ($query->num_rows > 0) {
            $arr = $query->result_array();
            foreach ($arr as $key => $ar) {
                $arr[$key]['thumb'] = base_url() . NO_USER;
                if (is_file(IMG_PROFILE . $ar['profile_pic'])) {
                    $arr[$key]['thumb'] = site_url() . IMG_PROFILE . $ar['profile_pic'];
                }
                $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            }
            return $arr;
        }
        return false;
    }

    public function getFollowings($user_id, $limit = 9) {
        $query = $this->db->query("SELECT u.pk_user_id,u.slug,u.firstname,u.lastname,up.profile_pic, (SELECT sum(points) as points FROM user_points WHERE user_points.fk_user_id=u.pk_user_id) AS points "
                . "FROM users u LEFT JOIN user_profile up ON u.pk_user_id=up.fk_user_id WHERE pk_user_id IN (SELECT fk_ref_id FROM followers WHERE fk_user_id='$user_id')  ORDER BY points DESC LIMIT 0,$limit");
        if ($query->num_rows > 0) {
            $arr = $query->result_array();
            foreach ($arr as $key => $ar) {
                $arr[$key]['thumb'] = base_url() . NO_USER;
                if (is_file(IMG_PROFILE . $ar['profile_pic'])) {
                    $arr[$key]['thumb'] = site_url() . IMG_PROFILE . $ar['profile_pic'];
                }
                $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            }
            return $arr;
        }
        return false;
    }

    public function getUserGallery($user_id, $page, $per_page) {
        $tot_records = $page * $per_page;
        $page -= 1;
        $start = $page * $per_page;
        $query = "SELECT * FROM user_uploads WHERE fk_user_id='$user_id' ORDER BY pk_upload_id DESC  LIMIT $start,$per_page";
        $arr = $this->db->query($query)->result_array();
        $count = getFoundRows();
        $data = array();
        foreach ($arr as $key => $ar) {
            if (is_file(USER_GALLERY . $ar['file_name'])) {
                $ar['large_image'] = site_url() . USER_GALLERY . $ar['file_name'];
                $ar['id'] = $ar['pk_upload_id'];
                $ar['user_id'] = $user_id;
                $ar['thumb'] = site_url() . USER_GALLERY . "thumb/" . $ar['file_name'];
                $ar['vid_link'] = "";
                $ar['vid'] = false;
                if ($ar['file_type'] == 'V') {
                    $vid_data = explode(".", $ar['file_name']);
                    $ar['vid_link'] = "http://www.youtube.com/embed/" . $vid_data[0];
                    $ar['vid'] = true;
                }
                $data[] = $ar;
            }
        }
        return array('data' => $data, 'count' => $count);
    }

    function convertYoutube($string) {
        return preg_replace(
                "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>", $string
        );
    }

    public function getNewsFeedLikeCount($post_id, $user_id = "") {
        $filter = "";
        if ($user_id > 0) {
            $filter.= " AND fk_user_id='$user_id'";
        }
        $data = $this->db->query("SELECT count(*) as like_count FROM feed_likes WHERE fk_feed_id='$post_id' $filter")->result_array();
        return $data[0]['like_count'];
    }

    private function getFeedUser($post_id) {
        $data = $this->db->query("SELECT fk_user_id FROM news_feed WHERE pk_feed_id='$post_id'")->result_array();
        return $data[0]['fk_user_id'];
    }

    public function getNewsFeedComments($post_id, $last_id = 0, $type = "first") {
        $filter = "";
        $limit = 5;
        if ($post_id != "") {
            $filter = " AND comments.fk_ref_id='$post_id'";
        }
        $com_last_id = $last_id;
        $feed_user = 0;
        $feed_user = $this->getFeedUser($post_id);
        if ($last_id > 0) {
            if ($type == "first") {
                $filter.=" AND pk_comment_id<'$last_id' ";
            } else {
                $filter.=" AND pk_comment_id>'$last_id' ";
            }

            $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS comments.*,users.slug,users.pk_user_id,users.firstname,users.lastname,users.email,up.profile_pic,up.phone_no"
                    . " FROM comments LEFT OUTER JOIN users ON users.pk_user_id=comments.fk_user_id "
                    . " LEFT OUTER JOIN user_profile up ON users.pk_user_id=up.fk_user_id "
                    . " WHERE  type='F' $filter   ORDER BY pk_comment_id");
        } else {
            $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS comments.*,users.slug,users.pk_user_id,users.firstname,users.lastname,users.email,up.profile_pic,up.phone_no"
                    . " FROM comments LEFT OUTER JOIN users ON users.pk_user_id=comments.fk_user_id "
                    . " LEFT OUTER JOIN user_profile up ON users.pk_user_id=up.fk_user_id "
                    . " WHERE  type='F' $filter  ORDER BY pk_comment_id DESC LIMIT 0,$limit");
        }
        $arr = $data->result_array();
        $count = getFoundRows();

        foreach ($arr as $key => $ar) {
            $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            if (is_file(IMG_PROFILE . $ar['profile_pic'])) {
                $arr[$key]['thumb'] = site_url() . IMG_PROFILE . $ar['profile_pic'];
            } else {
                $arr[$key]['thumb'] = site_url() . "public/fassets/images/user.jpg";
            }
            $arr[$key]['remove_com'] = false;


            if ($ar['fk_user_id'] == $this->session->userdata("user_id") or $feed_user == $this->session->userdata("user_id") or $this->session->userdata("utype") == 'A') {
                $arr[$key]['remove_com'] = true;
            }
            $com_last_id = $ar['pk_comment_id'];
        }
        if (count($arr) > 0) {
            if ($last_id == 0) {
                $arr = array_reverse($arr);
            }
            return array('data' => $arr, 'rem_comments' => $count - $limit, 'tot_comments' => $count, 'last_id' => $com_last_id);
        } else {
            return array('data' => false, 'rem_comments' => 0, 'tot_comments' => $count, 'last_id' => $com_last_id);
        }
    }

    public function getTagsByPostID($id, $type = "String") {
        if ($id > 0) {
            if ($type == "String") {
                $result = $this->db->query("SELECT GROUP_CONCAT(tag SEPARATOR ', ') as tags FROM post_tags WHERE fk_post_id='$id' GROUP BY fk_post_id")->result_array();
                return $result[0]['tags'];
            } else {
                $result = $this->db->query("SELECT tag FROM post_tags WHERE fk_post_id='$id'")->result_array();
                return $result;
            }
        }
        return "";
    }

    public function getArticleByID($id, $type = '') {
        $type_filter = "";
        if ($type != "") {
            $type_filter = "p.post_type='$type' AND ";
        }
        if ($id > 0) {
            $data = $this->db->query("SELECT p.pk_post_id, p.title,p.content,p.file_type,p.file_name,p.slug,p.comment_allow,p.fk_user_id, COUNT( pv.fk_post_id ) pv_count FROM  `post` p LEFT JOIN post_views pv ON p.pk_post_id = pv.fk_post_id WHERE $type_filter  p.pk_post_id =  '$id'  AND p.status='1' AND is_deleted='0'  GROUP BY pv.fk_post_id")->result_array();
            if (count($data)) {
                $ar = $data[0];
                $ar['tags'] = $this->getTagsByPostID($id, "array");
                $ar['vid_link'] = "";
                $ar['vid'] = "";
                if ($ar['file_type'] == 'V') {
                    $vid_data = explode(".", $ar['file_name']);
                    $ar['vid_link'] = "http://www.youtube.com/embed/" . $vid_data[0];
                    $ar['vid'] = $vid_data[0];
                }
                $image = IMG_ARTICLE . $ar['file_name'];
                if (is_file($image)) {
                    $ar['image'] = base_url() . $image;
                } else {
                    $ar['image'] = base_url() . NO_IMAGE;
                }

                return $ar;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getNewsFeedArticleByID($id, $type = '') {
        $type_filter = "";
        if ($type != "") {
            $type_filter = "p.post_type='$type' AND ";
        }
        if ($id > 0) {
            $data = $this->db->query("SELECT p.pk_post_id, p.title,p.content,p.file_type,p.file_name,p.slug,p.comment_allow,p.fk_user_id, COUNT( pv.fk_post_id ) pv_count FROM  `post` p LEFT JOIN post_views pv ON p.pk_post_id = pv.fk_post_id WHERE $type_filter  p.pk_post_id =  '$id'  AND p.status='1' AND is_deleted='0'  GROUP BY pv.fk_post_id")->result_array();
            if (count($data)) {
                $ar = $data[0];
                $ar['tags'] = $this->getTagsByPostID($id, "array");
                $ar['vid_link'] = "";
                $ar['vid'] = "";
                if ($ar['file_type'] == 'V') {
                    $vid_data = explode(".", $ar['file_name']);
                    $ar['vid_link'] = "http://www.youtube.com/embed/" . $vid_data[0];
                    $ar['vid'] = $vid_data[0];
                }
                $image = IMG_ARTICLE . $ar['file_name'];
                if (is_file($image)) {
                    $ar['image'] = base_url() . $image;
                } else {
                    $ar['image'] = base_url() . NO_IMAGE;
                }

                return $ar;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function getPremiumWritersByLeague($league_id) {
        $arr = $this->db->query("SELECT GROUP_CONCAT(pk_user_id) as ids FROM users WHERE fk_league_id='$league_id'")->result_array();
        $ids = rtrim($arr[0]['ids'], ",");
        return $ids;
    }

    public function getNewsFeeds($user_id, $page, $per_page, $last_id = 0, $ref_id = 0) {
        $tot_records = $page * $per_page;
        $page -= 1;
        $start = $page * $per_page;
//        $user_id=$this->session->userdata("user_id");
        //************************Geting Premuim Writers of Users League for News Feed****************
        $prem_writers = $this->getPremiumWritersByLeague($this->session->userdata("league_id"));

        $prem_filter = "";
        if (trim($prem_writers) != "") {
            $prem_filter = " OR (news_feed.fk_user_id IN($prem_writers) AND news_feed.feed_ref_id='0') ";
        }
        //********************************End**********************************************
        //********************************Admin Filter for News Feed*************************************
        $admin_filter = " OR (news_feed.fk_user_id IN (SELECT pk_user_id FROM users WHERE user_role='A') AND news_feed.feed_ref_id='0') ";
        //**************************************End****************************************
        //********************************Article Filter for League******************
        //$prem_art_filter = " OR (art.fk_league_id='0' AND (users.pk_user_id IN($prem_writers) OR (art.fk_user_id IN (SELECT pk_user_id FROM users WHERE user_role='A'))) AND feed_ref_id>'0') ";
        //$art_filter = " OR (art.fk_league_id='" . $this->session->userdata("league_id") . "' OR art.fk_club_id='" . $this->session->userdata("club_id") . "') ";
        
        $user_league_id = $this->session->userdata("league_id");
        $user_club_id = $this->session->userdata("club_id");
        $user_role_type = $this->session->userdata("utype");
        
        if ($user_league_id == '' or $user_club_id == '' or $user_role_type == '') {
            $user_data = $this->getUserDetails($user_id);
            $user_league_id = $user_data['fk_league_id'];
            $user_club_id = $user_data['fk_club_id'];
            $user_role_type = $user_data['user_role'];
        } // echo "$user_league_id | $user_club_id | $user_role_type";

        $prem_art_filter = " OR (art.fk_league_id='0' AND (users.pk_user_id IN($prem_writers) OR (art.fk_user_id IN (SELECT pk_user_id FROM users WHERE user_role='A'))) AND feed_ref_id>'0') ";
        $art_filter = " OR (art.fk_league_id='" . $user_league_id . "' OR art.fk_club_id='" . $user_club_id . "') ";
        //***************************************************************************

        $filter = "";
        if ($user_role_type == 'U') {
            $filter.=" AND (users.pk_user_id IN (SELECT fk_ref_id FROM followers WHERE fk_user_id='$user_id') OR users.pk_user_id='$user_id'  $prem_filter $admin_filter $prem_art_filter $art_filter OR news_feed.fk_user_id='$user_id')";
        }
        if ($last_id == 0) {
            $query = "SELECT SQL_CALC_FOUND_ROWS news_feed.*,users.firstname,users.slug,users.pk_user_id,users.lastname,up.profile_pic "
                    . " FROM news_feed "
                    . " LEFT JOIN users ON users.pk_user_id=news_feed.fk_user_id "
                    . " LEFT JOIN user_profile up ON users.pk_user_id=up.fk_user_id"
                    . " LEFT JOIN post art ON art.pk_post_id=news_feed.feed_ref_id AND news_feed.feed_ref_id>0"
                    . " WHERE news_feed.status='1' $filter ORDER BY pk_feed_id DESC LIMIT $start,$per_page";
        } else {
            $query = "SELECT SQL_CALC_FOUND_ROWS news_feed.*,users.firstname,users.slug,users.pk_user_id,users.lastname,up.profile_pic "
                    . " FROM news_feed "
                    . " LEFT JOIN users ON users.pk_user_id=news_feed.fk_user_id "
                    . " LEFT JOIN user_profile up ON users.pk_user_id=up.fk_user_id"
                    . " LEFT JOIN post art ON art.pk_post_id=news_feed.feed_ref_id AND news_feed.feed_ref_id>0"
                    . " WHERE news_feed.status='1' AND pk_feed_id>'$last_id' $filter ORDER BY pk_feed_id DESC";
        }
        //  echo $query;
        $arr = $this->db->query($query)->result_array();
        $count = getFoundRows();
        if ($ref_id > 0) {
            $query_ref = "SELECT SQL_CALC_FOUND_ROWS news_feed.*,users.firstname,users.slug,users.pk_user_id,users.lastname,up.profile_pic "
                    . " FROM news_feed "
                    . " LEFT JOIN users ON users.pk_user_id=news_feed.fk_user_id "
                    . " LEFT JOIN user_profile up ON users.pk_user_id=up.fk_user_id"
                    . " WHERE news_feed.status='1' AND pk_feed_id='$ref_id'";
            $arr_ref = $this->db->query($query_ref)->result_array();
            $arr = array_merge($arr_ref, $arr);
        }



        foreach ($arr as $key => $ar) {
            $arr[$key]['art_data'] = false;
            if ($ar['feed_ref_id'] > 0) {
                $arr[$key]['art_data'] = $this->getNewsFeedArticleByID($ar['feed_ref_id']);
            }
            $arr[$key]['content'] = $this->convertYoutube(strip_tags($ar['content']));
            $arr[$key]['name'] = $ar['firstname'] . ' ' . $ar['lastname'];
            $image = IMG_PROFILE . 'thumb/' . $ar['profile_pic'];
            if (is_file($image)) {
                $arr[$key]['thumb'] = base_url() . $image;
            } else {
                $arr[$key]['thumb'] = base_url() . "public/fassets/images/user.jpg";
            }
            if ($ar['file_type'] == 'I') {
                $arr[$key]['image'] = "";
                $image = IMG_NEWSFEED . $ar['file_name'];
                if (is_file($image)) {
                    $arr[$key]['image'] = base_url() . $image;
                }
            }
            if ($ar['file_type'] == 'V') {
                $arr[$key]['video'] = "";
                $vid = VID_NEWSFEED . $ar['file_name'];
                if (is_file($vid)) {
                    $arr[$key]['video'] = base_url() . $vid;
                }
            }
            $arr[$key]['remove_feed'] = false;

            if ($ar['pk_user_id'] == $this->session->userdata("user_id") or $this->session->userdata("utype") == 'A') {
                $arr[$key]['remove_feed'] = true;
            }
            $com_data = $this->getNewsFeedComments($ar['pk_feed_id'], 0);
            $arr[$key]['comments'] = $com_data['data'];
            $arr[$key]['rem_comments'] = $com_data['rem_comments'];
            $arr[$key]['com_count'] = $com_data['tot_comments'];
            $arr[$key]['like_count'] = $this->getNewsFeedLikeCount($ar['pk_feed_id']);
            $arr[$key]['like_status'] = $this->getNewsFeedLikeCount($ar['pk_feed_id'], $this->session->userdata("user_id"));

            $arr[$key]['shared_by'] = false;
            if ($ar['fk_feed_id'] > 0) {
                $arr[$key]['shared_by'] = $this->getUserByFeedID($ar['fk_feed_id']);
            }
        }
        $load = true;
        if ($tot_records >= $count) {
            $load = false;
        }

//        $arr = array_reverse($arr);
        return array('data' => $arr, 'count' => $count, 'load' => $load);
    }

    public function shareNewsFeeds($feed_id, $user_id) {
        $feed = $this->db->get_where("news_feed", array("pk_feed_id" => $feed_id))->result_array();
        $save_data = array(
            'content' => $feed[0]['content'],
            'file_name' => $feed[0]['file_name'],
            'file_type' => $feed[0]['file_type'],
            'fk_user_id' => $user_id,
            'fk_feed_id' => $feed_id,
            'feed_ref_id' => $feed[0]['feed_ref_id']
        );
        $this->db->insert("news_feed", $save_data);
    }

    private function deleteFeedFiles($ftype, $file) {
        if ($ftype == 'I' and $file != "") {
            @unlink(IMG_NEWSFEED . $file);
        } else if ($ftype == 'V' and $file != "") {
            @unlink(VID_NEWSFEED . $file);
        }
//        echo '--------Files Deleted------';
    }

    private function deleteFeedComments($feed_ids, $type = "F") {
        if (is_array($feed_ids) and count($feed_ids) > 0) {

            $this->db->query("DELETE FROM comments WHERE type='$type' AND fk_ref_id IN (" . getImplode($feed_ids) . ")");
//            echo '--------Comments Deleted------';
        }
    }

    private function deleteFeedLikes($feed_ids) {
        if (is_array($feed_ids) and count($feed_ids) > 0) {
            $this->db->query("DELETE FROM feed_likes WHERE fk_feed_id IN (" . getImplode($feed_ids) . ")");
//            echo '--------Likes Deleted------';
        }
    }

    private function deleteFeedNotifications($feed_ids) {
        if (is_array($feed_ids) and count($feed_ids) > 0) {
            $this->db->query("DELETE FROM notifications WHERE type IN ('COM_POST','LIKE_POST') AND ref_id IN (" . getImplode($feed_ids) . ")");
//            echo '--------Feed Deleted------';
        }
    }

    private function deleteFeed($feed_ids) {
        if (is_array($feed_ids) and count($feed_ids) > 0) {
            $this->db->query("DELETE FROM news_feed WHERE pk_feed_id IN (" . getImplode($feed_ids) . ")");
//            echo '--------Feed Deleted------';
        }
    }

    public function deleteAllFeedData($feed_id) {
        $feeds = $this->db->query("SELECT pk_feed_id,file_name,file_type,fk_feed_id FROM news_feed WHERE pk_feed_id='$feed_id' OR fk_feed_id='$feed_id'")->result_array();

        $ids = array();
        foreach ($feeds as $key => $feed) {
            if ($feed['fk_feed_id'] == 0) {
                $this->deleteFeedFiles($feed['file_type'], $feed['file_name']);
            }
            $ids[] = $feed['pk_feed_id'];
        }
        //*********Deleting All Comments Likes and then All News Feeds***********
        $this->deleteFeedComments($ids);
        $this->deleteFeedLikes($ids);
        $this->deleteFeedNotifications($ids);
        $this->deleteFeed($ids);
        $this->db->query("DELETE FROM user_points WHERE fk_ref_id IN (" . getImplode($feed_id) . ",'$feed_id') AND point_type IN ('NF_TEXT','NF_IMG','NF_VID','NF_SHARE','NF_COM','W_NF_COM','NF_LIKE','W_NF_LIKE')");


        // public $ptype = array('ART_COM' => '1', 'NF_TEXT' => '3', 'NF_IMG' => '4', 'NF_VID' => '5', 'ART_SHARE' => '2', 'W_ART_WRT' => '7', 'W_ART_READS' => '1', 'ART_SHARE' => '2', 'W_ART_SHARE' => '3', 'W_ART_COM' => '1', 'NF_COM' => '1', 'NF_SHARE' => '2', 'W_NF_COM' => '1', 'NF_LIKE' => '1', 'W_NF_LIKE' => '1');
    }

    public function getNotifications($user_id, $page, $per_page, $markreads = FALSE) {
        $tot_records = $page * $per_page;
        $page -= 1;
        $start = $page * $per_page;
        $query = "SELECT SQL_CALC_FOUND_ROWS notifications.*"
                . " FROM notifications "
                . " WHERE notifications.fk_user_id='" . $user_id . "' ORDER BY pk_noti_id DESC LIMIT $start,$per_page";
        $arr = $this->db->query($query)->result_array();
        $count = getFoundRows();
        $ids = array();

        $image = array(
            'COM_ART' => site_url() . "public/fassets/images/noti_comment.png",
            'COM_POST' => site_url() . "public/fassets/images/noti_comment.png",
            'COM_NEWS' => site_url() . "public/fassets/images/noti_comment.png",
            'LIKE_POST' => site_url() . "public/fassets/images/noti_like.png",
            'FOLLOW' => site_url() . "public/fassets/images/noti_follow.png",
            'ART_NEW' => site_url() . "public/fassets/images/noti_article.png",
            'FEED_NEW' => site_url() . "public/fassets/images/noti_article.png",
        );
        if (count($arr) > 0) {
            foreach ($arr as $key => $ar) {
                $ids[] = $ar['pk_noti_id'];
                if (isset($image[$ar['type']])) {
                    $arr[$key]['thumb'] = $image[$ar['type']];
                } else {
                    $arr[$key]['thumb'] = $image['ART_NEW'];
                }
                if ($ar['type'] == "COM_NEWS") {
                    $data = $this->db->get_where("post", array('pk_post_id' => $ar['ref_id']))->result_array();
                    $arr[$key]['url'] = site_url() . "news/" . $data[0]['slug'];
                }
                if ($ar['type'] == "COM_ART") {
                    $data = $this->db->get_where("post", array('pk_post_id' => $ar['ref_id']))->result_array();
                    $arr[$key]['url'] = site_url() . "article/" . $data[0]['slug'];
                }
                if ($ar['type'] == "COM_POST") {
                    $arr[$key]['url'] = site_url() . "dashboard/" . $ar['ref_id'];
                }
                if ($ar['type'] == "LIKE_POST") {
                    $arr[$key]['url'] = site_url() . "dashboard/" . $ar['ref_id'];
                }
                if ($ar['type'] == "FOLLOW") {
                    $data = $this->db->get_where("users", array('pk_user_id' => $ar['ref_id']))->result_array();
                    $arr[$key]['url'] = site_url() . "articles/listing/" . $data[0]['slug'];
                }
                if ($ar['type'] == "ART_NEW") {
                    $data = $this->db->get_where("post", array('pk_post_id' => $ar['ref_id']))->result_array();
                    if (count($data) > 0) {
                        $arr[$key]['url'] = site_url() . "article/" . $data[0]['slug'];
                    } else {
                        $this->db->delete("notifications", array("pk_noti_id" => $ar['pk_noti_id']));
                        unset($arr[$key]);
                    }
                }
                if ($ar['type'] == "FEED_NEW") {
                    $arr[$key]['url'] = site_url() . "dashboard/" . $ar['ref_id'];
                }
            }
            $load = true;
            if ($tot_records >= $count) {
                $load = false;
            }
            if ($markreads and count($ids) > 0) {
                $this->db->update("notifications", array("read_status" => "1"), "pk_noti_id IN ('" . implode("','", $ids) . "')");
            }
//        $arr = array_reverse($arr);
            return array('data' => $arr, 'count' => $count, 'load' => $load, "ids" => $ids);
        } else {
            return array('data' => array(), 'count' => 0, 'load' => false, "ids" => array());
        }
    }

    function getFootyFanzArticles($page, $per_page, $sk, $user_id) {
        $req_page = $page;
        $tot_records = $page * $per_page;
        $page -= 1;
        $start = $page * $per_page;
        $search = $sk['search_key'];
        $league = $sk['league'];
        $club = $sk['club'];
        $filter = "";
        if ($league > 0) {
            $filter.= " AND u.fk_league_id='$league' ";
        }
        if ($club > 0) {
            $filter.= " AND up.fk_club_id='$club' ";
        }
        if ($search != "") {
            $sc1 = getSearchKeys($search, "u.firstname");
            $sc2 = getSearchKeys($search, "u.lastname");
            $filter.= " AND ((" . implode(" OR ", $sc1) . ") OR (" . implode(" OR ", $sc2) . ") ) ";
        }

        $f2 = "SELECT u.pk_user_id"
                . " FROM users u INNER JOIN user_profile up ON u.pk_user_id=up.fk_user_id"
                . " WHERE u.status='1' AND ff_plus='1' $filter";


        $data = $this->db->query("SELECT SQL_CALC_FOUND_ROWS post.*"
                . " FROM `post`"
                . " WHERE is_deleted='0' AND fk_user_id IN ($f2)   ORDER BY created_at DESC  LIMIT $start,$per_page");
        $arr = $data->result_array();
        $count = getFoundRows();
        foreach ($arr as $key => $ar) {
            $image = IMG_ARTICLE . $ar['file_name'];
            if (is_file($image)) {
                $arr[$key]['thumb'] = base_url() . $image;
            } else {
                $arr[$key]['thumb'] = base_url() . NO_IMAGE;
            }
            $user = $this->getArticlePostedBy($ar['fk_user_id']);
            $arr[$key]['posted_by'] = "";
            $arr[$key]['posted_by_email'] = "";
            if ($user) {
                $arr[$key]['posted_by'] = $user['name'];
                $arr[$key]['user_image'] = $user['image'];
                $arr[$key]['user_slug'] = $user['slug'];
                $arr[$key]['posted_by_email'] = $user['email'];
            }
        }
        $load = true;
        if ($tot_records >= $count) {
            $load = false;
        }
        return array('data' => $arr, 'load' => $load, 'count' => $count, 'page' => $req_page);
    }

    function getArticlePostedBy($user_id) {
        $user = $this->db->query("SELECT CONCAT(u.firstname,' ',u.lastname) as name,email,up.profile_pic,u.slug FROM users u LEFT JOIN user_profile up ON u.pk_user_id=up.fk_user_id WHERE u.pk_user_id='" . $user_id . "'")->result_array();
        if (count($user) > 0) {
            if (is_file(PUBLIC_DIR . PROFILE_IMG_DIR . $user[0]['profile_pic'])) {
                $user[0]['image'] = site_url() . PUBLIC_DIR . PROFILE_IMG_DIR . $user[0]['profile_pic'];
            } else {
                $user[0]['image'] = site_url() . "public/fassets/images/user.jpg";
            }
            return $user[0];
        }
        return false;
    }

}

?>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Change Password
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Account
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Change Password
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <style type="text/css">
        .form-horizontal .control-label{
            width: 180px;
        }
        .form-horizontal .controls {
            margin-left: 200px;
        }
    </style>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        
        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Change Password</div>
                </div>
                <div class="portlet-body">
                     <div class="row-fluid">
                         <div class="span6">
        <div class="alert alert-error error_block hide"></div>
    </div>
                         </div>
                    <div class="row-fluid">
                  <form action="#" id="profile_password" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">Old Password <span class="required">*</span></label>
                        <div class="controls">
                            <input type="password" placeholder="Old Password" id="old_password" name="old_password" class="m-wrap required span4">
                        </div>
                    </div>
                     <div class="control-group">
                        <label class="control-label">New Password <span class="required">*</span></label>
                        <div class="controls">
                            <input type="password" placeholder="new Password" id="new_password" name="new_password" class="m-wrap required span4">
                        </div>
                    </div>
                      <div class="control-group">
                        <label class="control-label">Confirm Password <span class="required">*</span></label>
                        <div class="controls">
                            <input type="password" placeholder="Confirm Password" equalTo="#new_password" id="conf_password" name="conf_password" class="m-wrap required span4">
                        </div>
                    </div>
                      <div class="control-group margin-top-10">
                        <label class="control-label"></label>
                        <div class="controls">
                            <button type="button" id="btn_change_password" onclick="$('#profile_password').submit();" class="btn green"><i class="icon-save"></i> Update Password</button>
                        </div>
                    </div>
                    
                </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(e) {
        setTimeout(function(){
         $('#profile_password').validate({
        submitHandler: function(form) {
           update_password();
            return false;
        }
    });
        },500)
});
function update_password(){
    var post_data = $("#profile_password").serialize();
    $.post(base_url + "admin/account/password_save", post_data, function(res) {
        if (res.status == 'OK') {
            $(".error_block").hide();
          $.gritter.add({
                title: 'Profile',
                text: 'New password has been Updated'
            });
        }else {
            $(".error_block").html(res.msg).hide().fadeIn('fast');
        } 
    }, 'json');
}
    
</script>


<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB -->
            <h3 class="page-title">
                Profile
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    Account
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Profile                    
                </li>                
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> My Profile</div>
            </div>
            <div class="portlet-body">
                <div class="profile" style="min-height: 200px">
                    <div class="span2">
                        <div class="">
                            <img src="<?php echo $data['thumb']; ?>" class="admin_image img-circle img-rounded img-thumbnail img-polaroid" style="width: 100%;" />
                        </div>
                    </div>
                    <div class="span10 ">              
                        <div class="row-fluid">                       
                            <div class="span2 pull-right">
                                <div class="btn blue mini pull-right profile_view" onclick="$('.profile_view').hide();
                                        $('.profile_edit').fadeIn();"><i class="icon-edit"></i> Edit</div>  
                                <div class="btn gray mini pull-right profile_edit" onclick="$('.profile_edit').hide();
                                        $('.profile_view').fadeIn();" style="display:none"><i class="icon-remove"></i> Cancel Edit</div>
                            </div>
                        </div>
                        <div class="row-fluid profile_view">
                            <div class="clearfix"></div>
                            <blockquote class="hero">
                                <div class="clearfix"></div>                            
                                <h4>Name: <span class="text admin_name"> <?php echo $data['name']; ?> </span></h4>
                                <div class="clearfix"></div>
                            </blockquote>

                            <div class="clearfix"></div>
                            <blockquote class="hero">
                                <div class="clearfix"></div>                            
                                <h4>Email: <span class="text admin_email"> <?php echo $data['email']; ?></span></h4>
                                <div class="clearfix"></div>
                            </blockquote>
                        </div>
                        <div class="row-fluid profile_edit" style="display:none">
                            <form action="#" class="form-horizontal" id="form_profile_edit" name="form_profile_edit" enctype="multypart/form-data">
                                <div class="control-group">
                                    <label class="control-label">First Name <span class="required">*</span> </label>
                                    <div class="controls">
                                        <input type="text" placeholder="First Name" id="firstname" name="firstname" value="<?php echo $data['firstname']; ?>" class="m-wrap span7 required" />                                
                                    </div>                        
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Last Name <span class="required">*</span> </label>
                                    <div class="controls">
                                        <input type="text" placeholder="Last Name" id="lastname" name="lastname" value="<?php echo $data['lastname']; ?>" class="m-wrap span7 required" />                                
                                    </div>                        
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Email <span class="required">*</span> </label>
                                    <div class="controls">
                                        <input type="text" placeholder="Email" id="email" name="email" value="<?php echo $data['email']; ?>" class="m-wrap span7 required" />                                
                                    </div>                        
                                </div>

                                <div class="control-group">
                                    <label class="control-label" style="">Image</label>
                                    <div class="controls">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Select file to upload</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" class="default file_file required" name="profile_image" id="file_file" data-msg-accept="Choose a valid file" accept="jpg|jpeg|gif|png">
                                            </span>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                            <span class="help-inline file_alert file_img_alert1" for="file_file" style="margin-top: -2px; margin-left: 10px; display:none"></span>
                                        </div>
                                        <span class="label label-info">Note: </span> <small>Only jpg, gif, jpeg and png files are allowed</small>
                                    </div>
                                </div>

                                <div class="control-group margin-top-10">
                                    <label class="control-label"> </label>
                                    <div class="controls">
                                        <button type="button" class="btn green" onclick="$('#form_profile_edit').submit();"><i class="icon-save"></i> Update</button>               
                                    </div>                        
                                </div>

                            </form>
                        </div>

                    </div>      

                </div>
                <div class="clearfix"></div>
            </div>
        </div>   
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(e) {
        $(".profile_view").live('click', function() {
            $('#form_profile_edit').validate({
                submitHandler: function(form) {
                    update_profile();
                    return false;
                }
            });
        });

    });
    function update_profile() {

        var formData = new FormData($("#form_profile_edit")[0]);
        App.blockUI($("#form_profile_edit"));
        $.ajax({
            url: base_url + "admin/account/profile_save",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                var res = $.parseJSON(data);
                App.unblockUI($("#form_profile_edit"));
                if (res.status == 'OK') {
                    $(".admin_name").html($("#admin_name").val());
                    $(".admin_email").html($("#admin_email").val());
                    $(".admin_image").attr("src", res.image);
                    $.gritter.add({
                        title: 'Profile',
                        text: 'Profile has been Updated'
                    });
                    $(".profile_edit").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#form_profile_edit"));
                $.gritter.add({title: 'Account Error', text: 'Error in Saving Profile'});
            },
            cache: false,
            contentType: false,
            processData: false
        });


    }

</script>


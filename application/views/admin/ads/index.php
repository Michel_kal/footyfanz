<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                <?php echo ucfirst($type); ?> Ads
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Ads
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <?php echo ucfirst($type); ?> Ads
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> <?php echo ucfirst($type); ?> Ads List</div>
                <a href="<?php echo base_url() . ADMIN_DIR; ?>ads/ads_form/<?php echo $type ?>/0" data-target="#modal_ads_add" data-toggle="modal"  class="btn yellow pull-right remote" style="margin-top: -5px;">Add New</a>
            </div>
            <div class="portlet-body">
                <div id="ads_list"></div>
                <?php
                paginationScript(base_url() . "admin/ads/ads_list", "ads_list", "type=$type");
                ?>
            </div>
        </div>
    </div>

    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<!-- Modal on Page-->
<div id="modal_ads_add" class="modal hide fade" data-width="750" data-height="420">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Add New</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:420px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn close_button">Close</button>
        <button type="button" class="btn green" onclick='$("#ads_add").submit();'>Save</button>
    </div>
</div>
<div id="modal_ads_edit" class="modal hide fade" data-width="750" data-height="420">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Edit</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:420px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn close_button">Close</button>
        <button type="button" class="btn green" onclick='$("#ads_edit").submit();'>Update</button>
    </div>
</div>

<script type="text/javascript">
    function add_ads() {
        App.blockUI($("#ads_add"));

        var formData = new FormData($("#ads_add")[0]);
        $.ajax({
            url: $("#ads_add").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#ads_add"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Ads",
                        text: jdata.msg
                    });
                    $("#modal_ads_add .close_button").click();
                     $(".searchBtn").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#ads_add"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function update_ads() {
        App.blockUI($("#ads_edit"));

        var formData = new FormData($("#ads_edit")[0]);
        $.ajax({
            url: $("#ads_edit").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#ads_edit"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Ads",
                        text: jdata.msg
                    });
                    $("#modal_ads_edit .close_button").click();
                   
                    $(".pagination li.active.cpageval").addClass("inactive").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#ads_edit"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

</script>


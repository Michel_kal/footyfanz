<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Footer Text
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    CMS
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Footer Text
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Edit Footer Text</div>
            </div>
            <div class="portlet-body">
                <form action="#" id="footer_text" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">About </label>
                        <div class="controls">
                            <textarea placeholder="About" id="ft_about" name="ft_about" class="m-wrap  span8"  rows="4"><?php echo getCMSContent("ft_about") ?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Contact </label>
                        <div class="controls">
                             <textarea placeholder="Contact" id="ft_contact" name="ft_contact" class="m-wrap  span8"  rows="4"><?php echo getCMSContent("ft_contact") ?></textarea>
                        </div>
                    </div>
                    <div class="control-group margin-top-10">
                        <label class="control-label"></label>
                        <div class="controls">
                            <button type="button" id="btn_change_password" onclick="update_footer();" class="btn green"><i class="icon-save"></i> Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">

    function update_footer() {
        if ($("#footer_text").valid()) {
            var post_data = $("#footer_text").serialize();
            $.post(base_url + "admin/cms/savefooter", post_data, function(res) {
                $.gritter.add({
                    title: 'Footer Text',
                    text: 'Footer has been Updated'
                });
            });
        }

    }
</script>

<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Home Meta
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    CMS
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Home Meta
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Edit Tags</div>
            </div>
            <div class="portlet-body">
                <form action="#" id="metaform" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">Keywords </label>
                        <div class="controls">
                            <textarea placeholder="Keywords" id="homemeta_key" rows="10" name="homemeta_key" class="m-wrap  span12" ><?php echo getCMSContent("homemeta_key") ?></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Description </label>
                        <div class="controls">
                            <textarea placeholder="Description" id="homemeta_desc" rows="10" name="homemeta_desc" class="m-wrap  span12" ><?php echo getCMSContent("homemeta_desc") ?></textarea>
                        </div>
                    </div>



                    <div class="control-group margin-top-10">
                        <label class="control-label"></label>
                        <div class="controls">
                            <button type="button" id="btn_change_password" onclick="update_meta();" class="btn green"><i class="icon-save"></i> Update Meta</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">

    function update_meta() {

        var post_data = $("#metaform").serialize();
        $.post(base_url + "admin/cms/savehomemeta", post_data, function(res) {
            $.gritter.add({
                title: 'Home Meta',
                text: 'Tag Contents has been Updated'
            });
        });

    }
</script>

<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Home Page Tiles
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    CMS
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Home Page Tiles
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Edit Links</div>
                <div class="btn  green pull-right" style="margin:-5px 0 -5px 0; " onclick="addNewTile();">Add New</div>
            </div>
            <div class="portlet-body">

                <div class="loaddata" data-url="<?php echo site_url() . "admin/cms/gettiles" ?>">

                </div>
            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">

    function update_tiles($form) {
        var parent = $form.parent("div");
        App.blockUI(parent);
        var formData = new FormData($form[0]);
        $.ajax({
            url: base_url + "admin/cms/updateTile",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                var res = $.parseJSON(data)
                App.unblockUI(parent);
                if (res.status == 'OK') {
                    $.gritter.add({
                        title: 'Tile Update',
                        text: 'Tile has been Updated'
                    });
                    App.loadData($(".loaddata"));
                }
            },
            error: function(data) {

            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    }

    function addNewTile() {
        App.unblockUI($(".portlet"));
        var post_data = {};
        $.post(base_url + "admin/cms/addNewTile", post_data, function(res) {
            $.gritter.add({
                title: 'Tile Added',
                text: 'Tile has been Added'
            });
            App.loadData($(".loaddata"));
        });
    }
    $(document).ready(function(e) {
        $(".remove_tile").live("click", function() {
            App.unblockUI($(".portlet"));
            var post_data = $(this).data();
            $.post(base_url + "admin/cms/removeTile", post_data, function(res) {
                $.gritter.add({
                    title: 'Tile Removed',
                    text: 'Tile has been Removed'
                });
                App.loadData($(".loaddata"));
            });
        });
    });
</script>

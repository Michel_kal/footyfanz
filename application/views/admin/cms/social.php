<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Social Links
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    CMS
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Social Links
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Edit Links</div>
            </div>
            <div class="portlet-body">
                <form action="#" id="social_links" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">Facebook </label>
                        <div class="controls">
                            <input type="text" placeholder="Facebook Link" id="fb_link" name="fb_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("fb_link") ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Twitter </label>
                        <div class="controls">
                            <input type="text" placeholder="Twitter Link" id="tw_link" name="tw_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("tw_link") ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Linkedin </label>
                        <div class="controls">
                            <input type="text" placeholder="Linkedin Link" id="ln_link" name="ln_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("ln_link") ?>">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Google Plus </label>
                        <div class="controls">
                            <input type="text" placeholder="Google Plus Link" id="gp_link" name="gp_link" class="m-wrap  span4 url"  value="<?php echo getCMSContent("gp_link") ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Youtube </label>
                        <div class="controls">
                            <input type="text" placeholder="Youtube Link" id="yt_link" name="yt_link" class="m-wrap  span4 url"  value="<?php echo getCMSContent("yt_link") ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Instagram </label>
                        <div class="controls">
                            <input type="text" placeholder="Instagram Link" id="inst_link" name="inst_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("inst_link") ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Flickr </label>
                        <div class="controls">
                            <input type="text" placeholder="Flickr Link" id="fl_link" name="fl_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("fl_link") ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Vimeo </label>
                        <div class="controls">
                            <input type="text" placeholder="Vimeo Link" id="vimio_link" name="vimio_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("vimio_link") ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Pinterest </label>
                        <div class="controls">
                            <input type="text" placeholder="Pinterest Link" id="pintrest_link" name="pintrest_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("pintrest_link") ?>">
                        </div>
                    </div>
                    <div class="control-group" style="display: none">
                        <label class="control-label">Vine </label>
                        <div class="controls">
                            <input type="text" placeholder="Vine Link" id="vine_link" name="vine_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("vine_link") ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Pingler </label>
                        <div class="controls">
                            <input type="text" placeholder="Pingler Link" id="vine_link" name="pingler_link" class="m-wrap  span4 url" value="<?php echo getCMSContent("pingler_link") ?>">
                        </div>
                    </div>



                    <div class="control-group margin-top-10">
                        <label class="control-label"></label>
                        <div class="controls">
                            <button type="button" id="btn_change_password" onclick="update_links();" class="btn green"><i class="icon-save"></i> Update Links</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">

    function update_links() {
        if ($("#social_links").valid()) {
            var post_data = $("#social_links").serialize();
            $.post(base_url + "admin/cms/savesocial", post_data, function(res) {
                $.gritter.add({
                    title: 'Social Links',
                    text: 'Links has been Updated'
                });
            });
        }

    }
</script>

<?php

function getDateRangeBM($dt_from, $dt_to, $range = 10) {
    $dd = get_date_diff($dt_from, $dt_to);
    $gap_days = ceil($dd / $range);
    $dt_range['gap_days'] = $gap_days;
    $arr_date[] = $dt_to;
    for ($i = 1; $i < $range; $i++) {
        $ad_days = $gap_days * $i;
        $arr_date[] = sub_days($dt_to, $ad_days);
    }
//    $dt_array = ;
    $dt_range['arr_date'] = array_reverse($arr_date);
    return $dt_range;
}

$date_range_info = getDateRangeBM($dt_from, $dt_to);
$gap_days = $date_range_info['gap_days'];

$dates = array();
$datas = array();
foreach ($graph as $gp) {
    $dates[] = date("Y-m-d", strtotime($gp['date']));
    $datas[] = $gp['cnt'];
}
$data_vals = $datas;
$dates = implode("|", $dates);
$datas = implode("|", $datas);
$min = 10;
$max = 100;
if (count($data_vals) > 0) {
    $min = min($data_vals);
    $max = max($data_vals);
}
$chart_data_all = array(
    "title" => "Daily News/Article Views",
    "container" => "graph_stat",
    "unit" => "View",
    "gap" => $gap_days,
    "min_val" => $min,
    "max_val" => $max,
);
?>

<script type="text/javascript">

    var chart_data_all = <?php echo json_encode($chart_data_all); ?>;
    var datacategory = '<?php echo $dates ?>'.split("|");
    var datarecord = '<?php echo $datas ?>'.split("|");
    var chart_array = new Array();
    $.each(datarecord, function(k, val) {
        chart_array[k] = [phpDateToUTC(datacategory[k]), parseFloat(val)];
    });
    var chart_series = [{name: 'Total Views', data: chart_array}];
    print_chart(chart_data_all, chart_series);
</script>
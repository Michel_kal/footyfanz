<?php $typo = array("head" => "Mashhead", "poll" => "Poll", "bg" => "Background"); ?>
<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: none">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?php if (count($data) > 0) { ?>
            <?php
            $i = $start + 1;
            foreach ($data as $key => $row) {

                // print_r($row);
                ?>
                <div class="portfolio-block">
                    <div class="row-fluid ">
                        <div class="span2">
                            <img src="<?php echo $row['thumb'] ?>" style="width: 100%" />
                        </div>
                        <div class="span10 portfolio-text" style="overflow: visible">
                            <div class="row-fluid">
                                <div class="span10">
                                    <div class="span12" style="float: left;">
                                        <h3 style="margin:0px 0; width:auto; float: left;"><?php echo $typo[$type] . " "; ?><?php echo $i++; ?></h3>

                                        <?php if ($type == "bg") { ?>
                                            <div class="clear clearfix"></div>


                                            <div class="control-group" >
                                                <label class="control-label">Color</label>
                                                <div class="controls">
                                                    <input type="text" class="colorpicker-default m-wrap bgcolors" value="<?php echo $row['color'] ?>" data-id="<?php echo $row['pk_image_id'] ?>" />
                                                </div>
                                            </div>


                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="span2 pull-right">
                                    <div class="task-config-btn btn-group pull-right">
                                        <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                        <ul class="dropdown-menu pull-right">



                                            <li><a href="<?php echo base_url() . ADMIN_DIR . "placement/manage/" . $row['image_type'] . "/" . $row['pk_image_id'] . "/" . $i; ?>"> <i class="icon icon-file"></i> Manage Placement</a></li>
                                            <li><a class="article_action" href="#" data-image="<?php echo $row['thumb'] ?>" data-href="<?php echo base_url() . ADMIN_DIR . "images/deleteaction"; ?>" data-action="3" data-id="<?php echo $row['pk_image_id']; ?>"> <i class="icon icon-remove"></i> Delete</a></li>


                                        </ul>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix"></div>
                            <div class="row-fluid">
                                <div class="span10">
                                    <h3>Site Placement:</h3>
                                    <div class="clearfix"></div>
                                    Location:
                                    <?php
                                    $available = false;
                                    $curr_sub_type = array_keys($row['placement']['location']);
                                    $curr_leagues = array();
                                    if (isset($row['placement']['location'][6])) {
                                        $curr_leagues = $row['placement']['location'][6];
                                    }
                                    $curr_clubs = "";
                                    if (isset($row['placement']['location'][5])) {
                                        $curr_clubs = implode(',', $row['placement']['location'][5]);
                                    }

                                    foreach ($row['placement']['sub_types'] as $key => $value) {
                                        if (in_array($key, $curr_sub_type)) {
                                            $available = TRUE;
                                            ?>
                                            <div class="label label-mini label-info"> <?php echo $value; ?></div>

                                            <?php
                                        }
                                    }
                                    if ($curr_leagues != "" and count($curr_leagues) > 0) {
                                        ?>
                                        <br>
                                        Leagues:
                                        <?php
                                        foreach ($row['placement']['leagues'] as $key => $value) {
                                            if (in_array($key, $curr_leagues)) {
                                                $available = TRUE;
                                                ?>
                                                <div class="label label-mini label-info"> <?php echo $value; ?></div>

                                                <?php
                                            }
                                        }
                                    }

                                    if ($curr_clubs != "" and count($curr_clubs) > 0) {
                                        ?>
                                        <br>
                                        Clubs:
                                        <?php
                                        // var_dump($row);
                                        foreach ($row['placement']['current_club_names'] as $key => $value) {
                                            $available = TRUE;
                                            ?>
                                            <div class="label label-mini label-info"> <?php echo $value; ?></div>
                                            <?php
                                        }
                                        if (in_array("5", $curr_sub_type) && count($row['placement']['current_club_names']) == 0) {
                                            $available = TRUE;
                                            ?>
                                            <div class="label label-mini label-info"> All Clubs</div>
                                            <?php
                                        }
                                    }
                                    if ($available == FALSE) {
                                        ?>
                                        Not Available
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <?php
            }
            ?>
            <?php
        }
        if (count($data) == 0) {
            ?>
            <h4 style="text-align: center">No <?php echo $typo[$type] ?> Available</h4>
        <?php }
        ?>

    </div>
    <?php echo $page; ?>
</div>


<script type="text/javascript">
    $(".article_action[data-action=3]").easyconfirm({locale: {
            title: 'Delete Image',
            text: 'Are you sure to delete this image',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
    $(".article_action").click(function(e) {
        e.preventDefault();
        $container = $(this).parents(".portfolio-block");
        App.blockUI($container);
        var post_data = $(this).data();
        $.post($(this).data("href"), post_data, function(res) {
            var jdata = $.parseJSON(res);
            $.gritter.add({
                title: jdata.title,
                text: jdata.text
            });
            App.unblockUI($container);
            $(".pagination li.cpageval").removeClass("active").addClass("inactive").click();
        });
    });
    $('.colorpicker-default').colorpicker({
        format: 'hex'
    });
    $(".bgcolors").blur(function() {
        var id = $(this).data('id');
        var post_data = {id: id, value: $(this).val()};
        $.post(base_url + "admin/images/updatecolor", post_data, function(res) {
            var jdata = $.parseJSON(res);
            $.gritter.add({
                title: jdata.title,
                text: jdata.text
            });
        });
    });
</script>
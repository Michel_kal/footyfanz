<?php
$typo = array("head" => "Mashhead", "poll" => "Poll", "bg" => "Background");

function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val) - 1]);
    switch ($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }

    return $val;
}
?>
<style>
    .sp_progress{width: 100%; }
    .sp_progress .sp_prgress_in{width: 100%; height: 10px; background: #EDEDED; margin-right: 80px; border-radius: 3px;}
    .sp_progress .sp_prgress_in .sp_progress_bar{ height: 10px; background: #10a062; }
    .sp_progress .sp_progress_val{width: 70px; float: right; margin-left: 10px; line-height: 10px; font-size: 10px; text-align: right; margin-right: 10px;}
</style>

<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                <?php echo $typo[$type] ?> Image
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-group"></i>
                    Images
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <?php echo $typo[$type] ?> Image
                </li>
                <li style="float: right;">Server Max Upload Size: <?php echo return_bytes(ini_get('upload_max_filesize')) / (1024 * 1024); ?> MB &nbsp;</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">
            <div class="span10">
                <div class="sub_title logo_progress" style="display: none;">
                    <div class="sp_filename">
                        xyz.jpeg
                    </div>
                    <div class="sp_progress" style="">
                        <div class="sp_progress_val">
                            30%
                        </div>
                        <div class="sp_prgress_in">
                            <div class="sp_progress_bar" style="width: 30%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span2 pull-right">
                <div  class=" btn green pull-right" onclick="$('#logo').click();"><i class="icon-plus"></i> Add Image</div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i>  Image List</div>                                </div>
                <div class="portlet-body">
                    <div id="poll_list"></div>
                    <?php
                    paginationScript(base_url() . "admin/images/image_list", "poll_list", "imagetype=$type");
                    ?>
                </div>
            </div>
        </div>
    </div>

    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>
<div id="modal_image_add" class="modal hide fade" data-width="750" data-height="350">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Add Image</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Close</button>
        <button type="button" class="btn green" onclick='$("#image_add").submit();'>Add Image</button>
    </div>
</div>
<!-- Modal on Page-->
<form id="logoform" method="post" enctype="multipart/form-data" action="<?php echo site_url() . "admin/images/uploadmedia"; ?>">
    <input type="file" class="hide" name="logo" id="logo"  onchange="$(this.form).submit();"/>
    <input type="hidden" name="type" value="<?php echo $type; ?>" />
</form>
<script type="text/javascript">
    function add_ads() {
        App.blockUI($("#ads_add"));

        var formData = new FormData($("#ads_add")[0]);
        $.ajax({
            url: $("#ads_add").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#ads_add"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Ads",
                        text: jdata.msg
                    });
                    $("#modal_ads_add .close_button").click();
                    $(".searchBtn").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#ads_add"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function update_ads() {
        App.blockUI($("#ads_edit"));

        var formData = new FormData($("#ads_edit")[0]);
        $.ajax({
            url: $("#ads_edit").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#ads_edit"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Ads",
                        text: jdata.msg
                    });
                    $("#modal_ads_edit .close_button").click();

                    $(".pagination li.active.cpageval").addClass("inactive").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#ads_edit"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

</script>


<script type="text/javascript">
    $(document).ready(function(e) {
        $('#logoform').ajaxForm({
            //dataType: 'json',
            beforeSubmit: function() {
                $(".sf-msg-error").html("").hide();
                $(".logo_progress").show();
                $(".logo_progress .sp_progress_val").html("0%");
                $(".logo_progress .sp_progress_bar").css("width", "0%");
                $(".sp_filename").html("Uploading: " + $("#logo").val().replace(/.*[\/\\]/, ''));
            },
            uploadProgress: function(event, position, total, percentComplete) {
                if (percentComplete == 100) {
                } else {
                    $(".logo_progress .sp_progress_bar").css("width", percentComplete + '%');
                    $(".logo_progress .sp_progress_val").html(percentComplete + '%');
                }
            },
            success: function(json) {
                $(".logo_progress").hide();
                $jdata = $.parseJSON(json);
                if ($jdata.status == "1") {
                    $.gritter.add({
                        title: "Upload Image",
                        text: $jdata.msg
                    });
                    $(".searchBtn").click();
                } else {
                    $(".sf-msg-error").html($jdata.msg).show();
                }
            }
        });
    });
</script>



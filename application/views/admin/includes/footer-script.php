<!-- JS Files and Scripts Section -->
<?php $base = base_url() . PUBLIC_DIR . "assets/" ?>
<script src="<?php echo $base; ?>plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo $base; ?>plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script> 
<script src="<?php echo $base; ?>plugins/jquery.autocomplete.js" type="text/javascript"></script> 
<script src="<?php echo $base; ?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $base; ?>plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
<!--[if lt IE 9]>
<script src="<?php echo $base; ?>plugins/excanvas.min.js"></script>
<script src="<?php echo $base; ?>plugins/respond.min.js"></script>  
<![endif]-->   
<script src="<?php echo $base; ?>plugins/bootstrap-fileupload/bootstrap-fileupload.js" type="text/javascript" ></script>
<script src="<?php echo $base; ?>plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript" ></script>
<script src="<?php echo $base; ?>plugins/jquery-validation/dist/additional-methods.min.js" type="text/javascript" ></script>
<script src="<?php echo $base; ?>plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $base; ?>plugins/jquery.blockui.min.js" type="text/javascript"></script>  
<script src="<?php echo $base; ?>plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo $base; ?>plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
<script src="<?php echo $base; ?>plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
<script src="<?php echo $base; ?>plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
<script src="<?php echo $base; ?>plugins/jquery.comfirm.dialog.js" type="text/javascript"></script>
<script src="<?php echo $base; ?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript" ></script> 
<script src="<?php echo $base; ?>plugins/gritter/js/jquery.gritter.min.js"></script> 
<script src="<?php echo $base; ?>plugins/data-tables/jquery.dataTables.min.js"></script> 
<script src="<?php echo $base; ?>plugins/data-tables/DT_bootstrap.js"></script> 
<script src="<?php echo $base; ?>scripts/ui-modals.js"></script> 
<script src="<?php echo $base; ?>scripts/app.js"></script>
<script>
    jQuery(document).ready(function() {
        App.init();
    });
</script>
<!-- End of JS Files and Scripts Section --!>
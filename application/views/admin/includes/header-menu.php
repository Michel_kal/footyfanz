<?php
$base = base_url() . PUBLIC_DIR . "assets/";
$username = $this->session->userdata('admin_name');
?>
<div class="header navbar navbar-inverse navbar-fixed-top hidden-print">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div class="container-fluid">
            <!-- BEGIN LOGO -->
<!--            <a class="brand" href="index">
                <img src="<?php echo $base; ?>img/logo/logo_small.png" alt="<?php echo SITE_NAME ?>" style="height: 26px;margin-top: -2px;" />
            </a>-->
            <a class="brand" href="index">
                <h4 style="color: #fff; font-weight: 600;margin: 0px;">FootyFanz Admin</h4>
            </a>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                <img src="<?php echo $base; ?>img/menu-toggler.png" alt="" />
            </a>          
            <!-- END RESPONSIVE MENU TOGGLER -->            
            <!-- BEGIN TOP NAVIGATION MENU -->              
            <ul class="nav pull-right">

                <!-- END INBOX DROPDOWN -->
                <!-- BEGIN TODO DROPDOWN -->

                <!-- END TODO DROPDOWN -->               
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" src="<?php echo base_url() . PUBLIC_DIR . "assets/image/favicon.png"; ?>" id="avatar_image_view_small" style="visibility: hidden;">
                        <span class="username"><?php echo $username; ?></span>
                        <i class="icon-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url() . 'admin/account/profile'; ?>"><i class="icon-key"></i> Profile</a></li>
                        <li><a href="<?php echo base_url() . 'admin/login/logout'; ?>"><i class="icon-signout"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
            <!-- END TOP NAVIGATION MENU --> 
        </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
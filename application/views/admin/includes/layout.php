<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo isset($title) ? $title : ''; ?></title>
        <?php echo $this->load->view("admin/includes/seo.php") ?>
        <?php echo $this->load->view("admin/includes/header-script.php") ?>
        <?php
        if (isset($css)) {
            if (is_array($css)) {
                foreach ($css as $css_file) {
                    ?>
                    <link type="text/css" rel="stylesheet" href="<?php echo base_url() . PUBLIC_DIR . "assets/" . $css_file; ?>"/>
                    <?php
                }
            } else if ($css != '') {
                ?>
                <link type="text/css" rel="stylesheet" href="<?php echo base_url() . PUBLIC_DIR . "assets/" . $css; ?>"/>
                <?php
            }
        }
        ?>
        <style>
            .portfolio-text h5 i{
                color: #16a1f2;
            }
        </style>
    </head>
    <body class="page-header-fixed">
        <?php echo $this->load->view("admin/includes/header-menu.php") ?>
        <div class="page-container row-fluid">
            <!-- Left side column. contains and sidebar -->
            <?php echo $this->load->view("admin/includes/left-sidebar.php", array('menu' => $menu)) ?>
            <div class="page-content">
                <!-- Right side column. Contains and content of the page -->
                <?php echo isset($content) ? $content : '' ?>
            </div>
        </div>
        <!-- add new calendar event modal -->
        <?php echo $this->load->view("admin/includes/footer-script.php"); ?>
        <?php
        if (isset($js)) {
            if (is_array($js)) {
                foreach ($js as $j) {
                    ?>
                    <script type="text/javascript" src="<?php echo base_url() . PUBLIC_DIR . "assets/js_admin/" . $j; ?>"></script>
                    <?php
                }
            } else if ($js != '') {
                ?>
                <script type="text/javascript" src="<?php echo base_url() . PUBLIC_DIR . "assets/js_admin/" . $js; ?>"></script>
                <?php
            }
        }
        ?>






    </body>
</html>

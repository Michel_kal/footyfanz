<?php
if (isset($menu)) {
    $page = explode("-", $menu);
    $pages = $menu;
} else {
    $page = array('0' => '', '1' => '');
    $pages = '';
}
?>
<div class="page-sidebar nav-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->        
    <ul class="page-sidebar-menu">
        <li>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li  class="start <?php echo ($page[0] == "1") ? 'active' : ''; ?>">
            <a href="<?php echo base_url() . 'admin' ?>">
                <i class="icon-dashboard"></i> 
                <span class=" title">Dashboard</span>
                <?php if ($page[0] == "1") { ?> <span class="selected"></span><?php } ?>
            </a> 

        </li>
        <li  class=" <?php echo ($page[0] == "2") ? 'active open' : ''; ?>">
            <a href="#">
                <i class="icon-briefcase"></i> 
                <span class="arrow title <?php echo ($page[0] == "2") ? 'open' : ''; ?>">Account</span> 
                <?php if ($page[0] == "2") { ?> <span class="selected"></span><?php } ?>
            </a> 
            <ul class="sub-menu">
                <li class="<?php echo ($pages == "2-1") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/account/profile'; ?>">
                        <i class="icon-user"></i>Profile</a>
                </li>
                <li class="<?php echo ($pages == "2-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/account/password'; ?>">
                        <i class="icon-lock"></i>Change Password</a>
                </li>
                <?php if (1 == 2) { ?>
                    <li class="<?php echo ($pages == "2-3") ? 'active' : ''; ?>">
                        <a href="<?php echo base_url() . 'admin/account/emails'; ?>">
                            <i class="icon-envelope"></i> Email Setting</a>
                    </li>
                <?php } ?>
            </ul>   
        </li>   
        <li  class=" <?php echo ($page[0] == "3") ? 'active' : ''; ?>">
            <a href="#">
                <i class="icon-briefcase"></i> 
                <span class="arrow title <?php echo ($page[0] == "3") ? 'open' : ''; ?>">Master</span> 
                <?php if ($page[0] == "3") { ?> <span class="selected"></span><?php } ?>
            </a> 
            <ul class="sub-menu">

                <li class="<?php echo ($pages == "3-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/master/league'; ?>">
                        <i class="icon-briefcase"></i> Leagues</a>
                </li>
                <li class="<?php echo ($pages == "3-1") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/master/club'; ?>">
                        <i class="icon-briefcase"></i> Clubs</a>
                </li>
                <?php if (1 == 2) { ?>
                    <li class="<?php echo ($pages == "3-11") ? 'active' : ''; ?>">
                        <a href="<?php echo base_url() . 'admin/master/category'; ?>">
                            <i class="icon-briefcase"></i> Categories</a>
                    </li>
                <?php } ?>
                <li class="<?php echo ($pages == "3-3") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/master/media'; ?>">
                        <i class="icon-file"></i> Media</a>
                </li>
            </ul>   
        </li>

        <li  class=" <?php echo ($page[0] == "8") ? 'active' : ''; ?>">
            <a href="#">
                <i class="icon-briefcase"></i> 
                <span class="arrow title <?php echo ($page[0] == "8") ? 'open' : ''; ?>">Users</span> 
                <?php if ($page[0] == "8") { ?> <span class="selected"></span><?php } ?>
            </a> 
            <ul class="sub-menu">
                <li class="<?php echo ($pages == "8-1") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/users/'; ?>">
                        <i class="icon-user"></i>
                        <span class=" title"> User list</span>

                    </a>
                </li>
                <li class="<?php echo ($pages == "8-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/message/'; ?>">
                        <i class="icon-briefcase"></i> Messages</a>
                </li>

            </ul>   
        </li>


        <li  class=" <?php echo ($page[0] == "4") ? 'active' : ''; ?>">
            <a href="#">
                <i class="icon-flag"></i>
                <span class="arrow title <?php echo ($page[0] == "4") ? 'open' : ''; ?>">Articles</span>
                <?php if ($page[0] == "4") { ?> <span class="selected"></span><?php } ?>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo ($pages == "4-1") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/article/add'; ?>">
                        <i class="icon-flag"></i> Add Article</a>
                </li>
                <li class="<?php echo ($pages == "4-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/article'; ?>">
                        <i class="icon-flag"></i> Article List</a>
                </li>

            </ul>
        </li>

        <li  class=" <?php echo ($page[0] == "5") ? 'active' : ''; ?>">
            <a href="#">
                <i class="icon-flag"></i>
                <span class="arrow title <?php echo ($page[0] == "5") ? 'open' : ''; ?>">News</span>
                <?php if ($page[0] == "4") { ?> <span class="selected"></span><?php } ?>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo ($pages == "5-1") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/news/add'; ?>">
                        <i class="icon-flag"></i> Post News</a>
                </li>
                <li class="<?php echo ($pages == "5-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/news'; ?>">
                        <i class="icon-flag"></i> News List</a>
                </li>               
            </ul>
        </li>

        <li  class=" <?php echo ($page[0] == "6") ? 'active' : ''; ?>">
            <a href="#">
                <i class="icon-flag"></i>
                <span class="arrow title <?php echo ($page[0] == "4") ? 'open' : ''; ?>">Newsletter</span>
                <?php if ($page[0] == "4") { ?> <span class="selected"></span><?php } ?>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo ($pages == "6-1") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/newsletter/sendnews'; ?>">
                        <i class="icon-flag"></i> Send News</a>
                </li>
                <li class="<?php echo ($pages == "6-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/newsletter'; ?>">
                        <i class="icon-flag"></i> View Emails</a>
                </li>
                <li class="<?php echo ($pages == "6-3") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/newsletter/emaillog'; ?>">
                        <i class="icon-flag"></i> Email Log</a>
                </li>
            </ul>
        </li>

        <li  class=" <?php echo ($page[0] == "10") ? 'active' : ''; ?>">
            <a href="#">
                <i class="icon-file"></i>
                <span class=" title">CMS</span>
                <?php if ($page[0] == "10") { ?> <span class="selected"></span><?php } ?>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo ($pages == "10-1") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/cms/editor/contact'; ?>">
                        <i class="icon-text-width"></i>Contact</a>
                </li>
                <li class="<?php echo ($pages == "10-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/cms/editor/faq'; ?>">
                        <i class="icon-text-width"></i>FAQ's</a>
                </li>
                <li class="<?php echo ($pages == "10-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/cms/social'; ?>">
                        <i class="icon-text-width"></i>Social Links</a>
                </li>
                <li class="<?php echo ($pages == "10-4") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/cms/footer'; ?>">
                        <i class="icon-text-width"></i>Footer Text</a>
                </li>
                <li class="<?php echo ($pages == "10-3") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/cms/videogallery'; ?>">
                        <i class="icon-text-width"></i>Video Gallery</a>
                </li>
                <li class="<?php echo ($pages == "10-10") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/cms/editor/404'; ?>">
                        <i class="icon-text-width"></i>404 Not Found</a>
                </li>


            </ul>
        </li>
        <?php if (1 == 2) { ?>
            <li  class=" <?php echo ($page[0] == "9") ? 'active' : ''; ?>">
                <a href="<?php echo base_url() . 'admin/pages/'; ?>">
                    <i class="icon-text-height"></i>
                    <span class=" title"> Pages</span>
                    <?php if ($page[0] == "9") { ?> <span class="selected"></span><?php } ?>
                </a>

            </li>
        <?php } ?>
        <li  class="<?php echo ($page[0] == "9") ? 'active' : ''; ?>">
            <a href="<?php echo base_url() . 'admin/poll' ?>">
                <i class="icon-group"></i> 
                <span class=" title">Opinion Poll</span>
                <?php if ($page[0] == "9") { ?> <span class="selected"></span><?php } ?>
            </a> 

        </li>

        <li  class="<?php echo ($page[0] == "12") ? 'active' : ''; ?>">
            <a href="<?php echo base_url() . 'admin/images/index/bg' ?>">
                <i class="icon-group"></i> 
                <span class=" title">Site Background</span>
                <?php if ($page[0] == "12") { ?> <span class="selected"></span><?php } ?>
            </a> 

        </li>

<li  class="<?php //echo ($page[0] == "12") ? 'active' : ''; ?>">
            <a href="<?php echo base_url() . 'admin/Adminbanter' ?>">
                <i class="icon-group"></i>
                <span class=" title">Match Room</span>
                <?php if ($page[0] == "12") { ?> <span class="selected"></span><?php } ?>
            </a>

        </li>
        <li  class="<?php //echo ($page[0] == "12") ? 'active' : ''; ?>">
            <a href="<?php echo base_url() . 'admin/Features' ?>">
                <i class="icon-group"></i>
                <span class=" title">Features</span>
                <?php if ($page[0] == "12") { ?> <span class="selected"></span><?php } ?>
            </a>

        </li>


        <li  class="<?php echo ($page[0] == "11") ? 'active' : ''; ?>">
            <a href="<?php echo base_url() . 'admin/images/index/head' ?>">
                <i class="icon-group"></i> 
                <span class=" title">Mashhead</span>
                <?php if ($page[0] == "11") { ?> <span class="selected"></span><?php } ?>
            </a> 

        </li>

        <li  class=" <?php echo ($page[0] == "7") ? 'active' : ''; ?>">
            <a href="#">
                <i class="icon-expand"></i>
                <span class=" title">Ads</span>
                <?php if ($page[0] == "5") { ?> <span class="selected"></span><?php } ?>
            </a>
            <ul class="sub-menu">
                <li class="<?php echo ($pages == "7-1") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/ads/manager/square'; ?>">
                        <i class="icon-expand"></i> Square Ads</a>
                </li>
                <li class="<?php echo ($pages == "7-2") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/ads/manager/small'; ?>">
                        <i class="icon-expand"></i> Small Ads</a>
                </li>
                <li class="<?php echo ($pages == "7-4") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/ads/manager/large'; ?>">
                        <i class="icon-expand"></i> Large Ads</a>
                </li>
                <li class="<?php echo ($pages == "7-5") ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'admin/ads/google'; ?>">
                        <i class="icon-expand"></i> Google Ads</a>
                </li>


            </ul>
        </li>

    </ul>
    <!-- END SIDEBAR MENU -->
</div>


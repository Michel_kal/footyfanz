<div class="row-fluid">
    <div class="portlet box blue">
        <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
            <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Category List</div>
        </div>
        <div class="portlet-body">
            <div class="data">
                <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
                    <div class="search-form">
                        <div class="chat-form" style="margin-top: 0px">
                            <div class="input-cont span10" style="margin-right:0px">
                                <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                            </div>
                            <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
                        </div>
                    </div>            
                </div>
                <?php
                if ($data) {
                    ?> 
                    <table class="table table-striped table-bordered table-hover table-full-width tbl_data">
                        <thead>
                            <tr>
                                <th style="width:60px;">S. No.</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Status</th>                             
                                <th>Article</th>
                                <th style="width:100px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = $start;
                            foreach ($data as $row) {
                                if ($row['parent'] == "") {
                                    $row['parent'] = $row['category'];
                                    $row['category'] = "";
                                }
                                ?> 
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $row['parent']; ?></td>
                                    <td><?php echo $row['category'] ?></td>
                                    <td>                                        <?php if ($row['status'] == '0') { ?> <span class="label label-important">Inactive</span> <?php } else { ?><span class="label label-success">Active</span><?php } ?></td>                                    
                                    <td><?php echo $row['vcnt'] ?></td>
                                    <td>
                                        <div class="task-config-btn btn-group pull-right">
                                            <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a data-toggle="modal" data-target="#modal_category_edit" href="<?php echo base_url() . ADMIN_DIR; ?>/master/category_form/<?php echo $row['pk_category_id']; ?>" class="remote"><i class="icon-pencil"></i> Edit</a>
                                                </li>                                               
                                                <li>
                                                    <?php if ($row['status'] == '0') { ?>
                                                        <a href="#" data-id='<?php echo $row['pk_category_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>/master/category_status/1" class="master_status"><i class="icon-ok"></i> Activate</a>
                                                    <?php } else { ?>
                                                        <a href="#" data-id='<?php echo $row['pk_category_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>/master/category_status/0" class="master_status"><i class="icon-ban-circle"></i> Deactivate</a>
                                                    <?php } ?>
                                                </li>
                                                <?php if ($row['vcnt'] == 0) { ?>
                                                    <li>
                                                        <a href="#" data-id='<?php echo $row['pk_category_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>/master/category_delete" class="master_delete"><i class="icon-remove"></i> Delete</a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php
                } else {
                    ?> <h4 class="text-center">No Categories Available</h4> <?php
                }
                ?>
                <?php echo $page; ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
//    App.dataTable($('.tbl_data'));
</script>


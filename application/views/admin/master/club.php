<style>
    .sp_progress{width: 100%; }
    .sp_progress .sp_prgress_in{width: 100%; height: 10px; background: #EDEDED; margin-right: 80px; border-radius: 3px;}
    .sp_progress .sp_prgress_in .sp_progress_bar{ height: 10px; background: #10a062; }
    .sp_progress .sp_progress_val{width: 70px; float: right; margin-left: 10px; line-height: 10px; font-size: 10px; text-align: right; margin-right: 10px;}
</style>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Club
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Master
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Club
                </li>          
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">

            <div class="span3 pull-right">
                <a href="<?php echo base_url() . ADMIN_DIR; ?>/master/club_form" data-target="#modal_club_add" data-toggle="modal" class="remote btn green pull-right"><i class="icon-plus"></i> Add Club</a>
            </div>
        </div>
        <div class="row-fluid">
    <div class="portlet box blue">
        <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
            <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Club List</div>
              <select class="m-wrap" id="league" style="float: right; width: 240px; margin: -5px -1px -10px 0px;">
                        <option value="0">All Leagues</option>
                        <?php foreach ($leagues as $key => $league) {
                            ?>
                            <option value="<?php echo $key ?>"><?php echo $league ?></option>
                        <?php }
                        ?>
                    </select>
        </div>
        <div class="portlet-body">
            <div class="club_data"></div>
        </div>
    </div>
</div>
    </div>

    <form id="logoform" method="post" enctype="multipart/form-data" action="<?php echo site_url() . "admin/master/uploadmedia"; ?>">
        <input type="file" class="hide" name="logo" id="logo"  onchange="$(this.form).submit();"/>
    </form>

    <!-- END Modal on Page--> 
    <!-- END PAGE CONTAINER--> 
    <div id="modal_club_add" class="modal hide fade" data-width="800" data-height="450">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3><i class="icon-reorder"></i> Add Club</h3>
        </div>
        <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn">Close</button>
            <button type="button" class="btn green" onclick='$("#club_add").submit();'>Add Club</button>
        </div>
    </div>
    <div id="modal_club_edit" class="modal hide fade" data-width="800" data-height="450">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h3><i class="icon-reorder"></i> Edit Club</h3>
        </div>
        <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn">Close</button>
            <button type="button" class="btn green" onclick='$("#club_edit").submit();'>Update Club</button>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(e) {
        $("#league").on('change', function() {                   
            var post_data = {"league":$(this).val()};            
            $.post(base_url + "admin/master/clubloader", post_data, function(res) {
               $(".club_data").html(res);
            });
        });
        $("#league").change();
    });

</script>



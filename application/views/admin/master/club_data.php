
<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>            
    </div>
    <?php
    if ($data) {

        foreach ($data as $row) {
            $row['logo'] = ($row['logo'] != "") ? site_url() . CLUB_IMG_DIR . $row['logo'] : site_url() . NO_IMAGE;
            ?>
            <div class="row-fluid  portfolio-block" style="margin-bottom: 10px;">
                <div class="span1">
                    <img src="<?php echo $row['logo']; ?>" class=" img-polaroid" style="width: 100%;" />
                </div>
                <div class="span11 portfolio-text" style="overflow: visible">
                    <div class="row-fluid">
                        <div class="span10">
                            <h4 style="margin:0px 0; float: left; width: auto;"><?php echo $row['title']; ?></h4>

                            <?php if ($row['status'] == "0") { ?>
                                <div class="label label-red label-important pull-right" style="margin-top: 5px;">Inactive</div>
                            <?php } ?>

                            <div class="clearfix"></div>

                        </div>
                        <div class="span2">
                            <div class="task-config-btn btn-group pull-right">
                                <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a data-toggle="modal" data-target="#modal_club_edit" href="<?php echo base_url() . ADMIN_DIR; ?>/master/club_form/<?php echo $row['pk_club_id']; ?>" class="remote"><i class="icon-pencil"></i> Edit</a>
                                    </li> 



                                    <?php if ($row['status'] == "1") { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $row['pk_club_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>master/club_status/0" class="master_status"><i class="icon-ban-circle"></i> Deactivate </a>
                                        </li>                                       
                                    <?php } else { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $row['pk_club_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>master/club_status/1" class="master_status"><i class="icon-ok-sign"></i> Activate </a>
                                        </li>   
                                    <?php } ?>
                                    <li>
                                        <a href="#" data-id='<?php echo $row['pk_club_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>master/club_delete" class="master_delete"><i class="icon-remove"></i> Delete </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">

                            League: <span class="text-info"><?php echo $leagues[$row['fk_league_id']]; ?></span> &nbsp; &nbsp;



                            <?php if ($row['team_manager'] != "") { ?>
                                Team Manager: <span class="text-info"><?php echo $row['team_manager']; ?> </span> &nbsp; &nbsp;

                            <?php } ?>
                            <?php if ($row['captain'] != "") { ?>
                                Captain: <span class="text-info"><?php echo $row['captain']; ?></span> &nbsp; &nbsp;

                            <?php } ?>
                            <?php if ($row['stadium'] != "") { ?>
                                Stadium: <span class="text-info"><?php echo $row['stadium']; ?> </span> &nbsp; &nbsp;

                            <?php } ?>
                            <?php if ($row['stadium'] != "") { ?>
                                Trophies: <span class="text-info"><?php echo $row['trophies']; ?> </span> &nbsp; &nbsp;

                            <?php } ?>


                        </div>
                    </div>    
                    <?php if ($row['description'] != "") { ?>
                        <div class="row-fluid">
                            <p><strong>Description:</strong> <?php echo $row['description']; ?></p>
                        </div>
                    <?php } ?>



                </div>
            </div>
        <?php } ?>
        <?php
    } else {
        ?> <h4 class="text-center">No Clubs Available</h4> <?php
    }
    ?>
    <?php echo $page; ?>
</div>

<script type="text/javascript">
    $(".master_delete").easyconfirm({locale: {
            title: 'Delete Club',
            text: 'Are you sure to delete this Club',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
</script>


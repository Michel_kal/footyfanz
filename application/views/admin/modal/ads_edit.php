<style type="text/css">
    .help-inline.valid {
        display: none !important;
    }
</style>
<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="420">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="ads_edit" class="form-horizontal" enctype="multipart/form-data"
              data-url="<?php echo base_url() . ADMIN_DIR; ?>ads/ads_update/<?php echo $ads['ad_type'] . "/" . $ads['pk_ad_id'] ?>">
            <div class="control-group">
                <label class="control-label" style="">Start Date <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Start Date" id="start_date" name="start_date" value="<?php echo date("d/m/Y", strtotime($ads['start_date'])); ?>" class="m-wrap required span7 datepicker">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">End Date <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="End Date" value="<?php echo date("d/m/Y", strtotime($ads['end_date'])); ?>" id="end_date" name="end_date"  class="m-wrap required span7 datepicker">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Link <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Link" value="<?php echo $ads['ad_url']; ?>" id="ad_url" name="ad_url" class="m-wrap required span7 url">
                </div>
            </div>

            <input type="hidden" name="type" value="<?php echo $ads['ad_type']; ?>" />


            <div class="control-group">
                <label class="control-label" style="">Ads Image <span class="required">*</span></label>
                <div class="controls"  style="">

                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <input type="hidden" name="current_image" value="<?php echo $ads['image']; ?>">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 180px;">
                                <?php
                                if (is_file(ADS_DIR . "/" . $ads['image'])) {
                                    $img_title = "Change Image";
                                    ?>
                                    <img src="<?php echo base_url() . ADS_DIR . "/" . $ads['image']; ?>" alt="" />
                                    <?php
                                } else {
                                    $img_title = "Select Image";
                                    ?>
                                    <img src="<?php echo base_url() . NO_IMAGE; ?>" alt="" />
                                <?php } ?>

                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-file"><span class="fileupload-new"><?php echo $img_title; ?></span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" name="logo" accept="jpg|gif|png|JPG|GIF|PNG" data-msg-accept="Please choose jpg, gif or png image." class="default" /></span>
                                <a href="#" class="btn fileupload-exists red" data-dismiss="fileupload"><i class="icon-remove"></i> </a>
                            </div> <span for="logo" style="color: #b94a48;" class="help-inline hide">Please choose jpg, gif or png image.</span>
                        </div>


                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_ads_add .scroller");
    $(".datepicker").datepicker({'format': 'dd/mm/yyyy', autoclose: true});
    $('#ads_edit').validate({
        submitHandler: function(form) {
            update_ads();
            return false;
        }
    });
</script>



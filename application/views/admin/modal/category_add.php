<style type="text/css">
    .help-inline.valid {
        display: none !important;
    }
</style>
<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="340">
    <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="category_add" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
                <label class="control-label" style="">Category Name <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Category Name" id="category" name="category" class="m-wrap required span7">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Category Slug <span class="required">*</span></label>
                <div class="controls"  style="">
                    <input type="text" placeholder="Category Slug" value="" id="slug" name="slug" class="m-wrap required span7">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" style="">Header Meta</label>
                <div class="controls" style="">
                    <textarea class="m-wrap  span12" name="header_meta" id="header_meta" style=" min-height:100px;"><title></title><meta name="description" content=" " />
<meta name="keywords" content="" />
                    </textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Category Level</label>
                <div class="controls">
                    <select name="und">
                        <option value="">Parent Category</option>
                        <?php foreach ($cats as $cat) { ?>
                            <option value="<?php echo $cat['pk_category_id'] ?>"><?php echo $cat['category'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>           

        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_category_add .scroller");
    $('#category_add').validate({
        submitHandler: function(form) {
            add_category();
            return false;
        }
    });
</script>



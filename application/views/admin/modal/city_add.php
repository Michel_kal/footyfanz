<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="250">
         <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
    <div class="row-fluid">
        <form action="#" id="city_add" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" style="">City Name <span class="required">*</span></label>
                <div class="controls">
                    <input type="text" placeholder="City Name" id="city" name="city" class="m-wrap required span7">

                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Select Country <span class="required">*</span></label>                                     
                <div class="controls">
                    <select name="country" id="country" class="span7 required dynamic">
                        <option value="">Select Country</option>
                        <?php foreach ($con_data as $row) { ?>
                            <option value="<?php echo $row['pk_country_id']; ?>"><?php echo $row['country']; ?></option>
                        <?php } ?>


                    </select>
                </div>                    </div>
            <div class="control-group">
                <label class="control-label" style="">Select State <span class="required">*</span></label>
                <div class="controls">
                    <select name="state" id="state" class="span7 required">
                        <option value="">Select State</option>
                    </select>

                </div>
            </div>

        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_city_add .scroller");
    $('#city_add').validate({
        submitHandler: function(form) {
            add_city();
            return false;
        }
    });
</script>



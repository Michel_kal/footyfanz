<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="400">
    <div class="row-fluid">
        <form action="#" id="user_edit" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" style="">Name <span class="required">*</span></label>
                <div class="controls">
                    <input type="text" placeholder="First Name" value="<?php echo $user['name']; ?>" id="name" name="name" class="m-wrap required span7">
                </div>
            </div>                   
            <div class="control-group">
                <label class="control-label" style="">Email <span class="required">*</span></label>
                <div class="controls">
                    <input type="text" placeholder="Email" value="<?php echo $user['email']; ?>" data-rule-email="true" id="email" name="email" class="m-wrap user_email required span7">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Contact No. <span class="required">*</span></label>
                <div class="controls">
                    <input type="text" placeholder="Contact No." value="<?php echo $user['contact_no']; ?>" id="contact_no" name="contact_no" class="m-wrap required span7">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Designation <span class="required">*</span></label>
                <div class="controls">
                    <input type="text" placeholder="Designation" value="<?php echo $user['designation']; ?>" id="designation" name="designation" class="m-wrap required span7">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" style="">Username <span class="required">*</span></label>
                <div class="controls">
                    <input type="text" placeholder="Username" value="<?php echo $user['username']; ?>" id="username" name="username" class="m-wrap required user_name span7">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" >Set New Password </label>
                <div class="controls">
                    <input type="text" placeholder="Password" id="password" name="password" class="m-wrap span7">
                    <span class="help-inline" style="display: inline-block !important;"><span class="text-error">Note: </span> Keep Blank the Password Field in case of No Change in Password</span>
                </div>
            </div>
            <input type="hidden" name="id" value="<?php echo $user['id']; ?>" />
            <input type="hidden" name="college_id" value="<?php echo $user['college_id']; ?>" />
        </form>
    </div>
</div>
<script type="text/javascript">
    init_scroll("#modal_user_edit .scroller");
    $('#user_edit').validate({
        submitHandler: function(form) {
            update_user();
            return false;
        }
    });
</script>

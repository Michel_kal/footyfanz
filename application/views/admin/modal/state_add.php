<div class="scroller" style=" padding-right: 0px !important;" data-always-visible="1" data-rail-visible1="1" data-height="250">
     <div class="row-fluid">
        <div class="alert alert-error error_block hide"></div>
    </div>
                <div class="row-fluid">
                    <form action="#" id="state_add" class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label" style="">State Name <span class="required">*</span></label>
                            <div class="controls">
                            <input type="text" placeholder="State Name" id="state" name="state" class="m-wrap required span7">

                            </div>
                        </div>
                         <div class="control-group">
                        <label class="control-label">Select Country <span class="required">*</span></label>
                        <div class="controls">
                            <select name="country" id="country" class="span7 required">
                                <option value="">Select Country</option>
                                <?php foreach($con_data as $row) {?>
                                <option value="<?php echo $row['pk_country_id']; ?>"><?php echo $row['country']; ?></option>
                                <?php } ?>
                                
                              
                            </select>
                        </div>
                    </div>
                        
                    </form>
                </div>
            </div>
<script type="text/javascript">
  init_scroll("#modal_state_add .scroller");
    $('#state_add').validate({
        submitHandler: function(form) {
            add_state();
            return false;
        }
    });
</script>



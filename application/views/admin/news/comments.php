<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Comments
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    News
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Comments
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Comments List</div>
            </div>
            <div class="portlet-body">
                <div class="portlet box">
                    <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                        <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> <span style="color: #333;">Article</span></div>
                    </div>
                    <div class="portlet-body">
                        <div class="row-fluid">
                            <div class="row-fluid  portfolio-block">
                                <div class="span2">
                                    <img src="<?php echo $article['thumb'] ?>" class="img-polaroid" style="width: 100%">
                                </div>
                                <div class="span10 portfolio-text" style="overflow: visible">
                                    <div class="row-fluid">
                                        <div class="span10">
                                            <div class="span12" style="float: left;">
                                                <h3 style="margin:0px 0;font-size: 21px;"><?php echo $article['title']; ?></h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div>

                                        <?php if ($article['club'] != '') { ?> <div class="text-success" style="font-weight: 600; width: auto; float: left; margin-right: 20px;">Club:
                                            <?php echo $article['club']; ?>
                                        <?php } ?>

                                            <?php if ($article['posted_by'] != '') { ?>
                                                <div class="text-info" style="font-weight: 600; width: auto; float: left; margin-right: 20px;">Posted By:
                                                    <?php echo $article['posted_by']; ?>                           </div>
                                                <div class="text-warning" style="font-weight: 600; width: auto; float: left; margin-right: 20px;">Email:
                                                    <?php echo $article['posted_by_email']; ?>                           </div>
                                            <?php } ?>
                                            <span style="width: auto; float: left; margin-right: 20px;" class="event_details">
                                                <i class="icon-calendar"></i> <?php echo date("M d, Y h:i A", strtotime($article['created_at'])); ?>                           </span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <p></p>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>


                    <div id="comment_list"></div>
                    <?php
                    paginationScript(base_url() . "admin/article/comment_list", "comment_list", "post_id=" . $post_id);
                    ?>

                </div>
            </div>
        </div>




        <!-- END Modal on Page-->
        <!-- END PAGE CONTAINER-->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(e) {
        $("#category").on('change', function() {
            $("#article_loader").data("scategory", '');
            $("#sub_category").html('');
            var post_data = {"category": $(this).val()};
            $.post(base_url + "admin/article/getsubcategory", post_data, function(res) {
                $("#sub_category").html(res);
                $("#sub_category").change();
            });
        });
        $("#home_page").on('change', function() {
            if ($(this).is(":checked")) {
                $("#article_loader").data("home", "1");
            } else {
                $("#article_loader").data("home", "");
            }
            App.loadData($("#article_loader"));
        });

        $(document).on('change', "#sub_category", function() {
            $("#article_loader").data("category", $("#category").val());
            $("#article_loader").data("scategory", $("#sub_category").val());
            App.loadData($("#article_loader"));
        });
    });
</script>

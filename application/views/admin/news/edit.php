<?php $file_type = array("I" => "Image", "V" => "Video Link"); ?>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Edit News
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    News
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Edit News
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Edit News</div>
            </div>
            <div class="portlet-body">
                <form class="form-vertical" id="article_form" style="margin-bottom: 0px;" enctype="multipart/form-data">
                    <div class="alert alert alert-success article_success" style="display: none;">News has been saved.</div>
                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Club </label>
                                <div class="controls">
                                    <select name="club" id="club" class="m-wrap span12">
                                        <option value="">Select Club</option>
                                        <?php foreach ($clubs as $key => $club) { ?>
                                            <option value="<?php echo $key; ?>" <?php echo ($article['fk_club_id'] == $key) ? "selected" : ""; ?>><?php echo $club; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label class="control-label">&nbsp;</label>
                                <div class="controls">
                                    <label class="checkbox line span6">
                                        <input type="checkbox" name="featured_status" value="1" id="featured_status" <?php echo ($article['featured_status'] == "1") ? "checked" : ""; ?>   /> <span class="text-success"> Mark Featured</span>
                                    </label>
                                    <label class="checkbox line span6">
                                        <input type="checkbox" name="sponser_status" value="1" id="sponser_status" <?php echo ($article['sponser_status'] == "1") ? "checked" : ""; ?>    /> <span class="text-info">Mark Sponsored</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="span4 offset1">
                            <div class="control-group">
                                <label class="control-label">&nbsp;</label>
                                <div class="controls">
                                    <label class="checkbox line pull-right">
                                        <input type="checkbox" name="comment_allow" id="comment_allow"   value="1" <?php echo ($article['comment_allow'] == "1") ? "checked" : ""; ?> /> <span class="text-warning">Allow Comments from Users</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="control-group">
                        <label class="control-label" style="">News Title <span class="required">*</span></label>
                        <div class="controls" style="">
                            <input type="text" placeholder="News Title" id="article_title" name="title" value="<?php echo $article['title'] ?>" class="m-wrap required span12">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" style="">Article Tags </label>
                        <div class="controls" style="">
                            <input type="text" placeholder="News Tags" id="article_tags" name="tags" class="m-wrap span12 article_tags" value="<?php echo $article['tags']; ?>">

                        </div>
                    </div>
                    <div class="row-fluid" style="height: 115px;">
                        <div class="span3">
                            <div class="control-group">
                                <label class="control-label">Media Type </label>
                                <div class="controls">
                                    <select name="file_type" id="file_type" class="m-wrap span12">
                                        <option value="">Select Media</option>
                                        <?php foreach ($file_type as $key => $type) { ?>
                                            <option value="<?php echo $key; ?>" <?php echo ($article['file_type'] == $key) ? "selected" : ""; ?>><?php echo $type; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="span6 media_video hide">
                            <div class="control-group ">
                                <label class="control-label" style="">Youtube Video Link<span class="required">*</span></label>
                                <div class="controls" style="">
                                    <input type="text" placeholder="Youtube Link" id="youtube_link2" name="youtube_link2" value="<?php echo $article['vid_link']; ?>" class="m-wrap span12">
                                    <input type="text" placeholder="Youtube Link" id="youtube_link" name="youtube_link" value="<?php echo $article['vid']; ?>" class="m-wrap span12" style="display: none;">
                                    <span for="youtube_link" class="help-inline hide">This field is required.</span>

                                </div>
                                <span class="label label-info">Note: </span> <small>http://youtube.com/vid?abcd</small>

                            </div>
                        </div>
                        <div class="span6 media_img hide">
                            <div class="span10">
                                <div class="control-group ">
                                    <label class="control-label" style="">Article Image </label>
                                    <div class="controls">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-file">
                                                <span class="fileupload-new">Select file to upload</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="file" class="default file_file" name="file_name" id="file_file" data-msg-accept="Choose a valid file" accept="jpg|jpeg|gif|png">
                                            </span>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                            <span class="help-inline file_alert file_img_alert1" for="file_file" style="margin-top: -2px; margin-left: 10px; display:none"></span>
                                        </div>
                                        <span class="label label-info">Note: </span> <small>Only jpg, gif, jpeg and png files are allowed</small>
                                    </div>
                                </div> 
                            </div>
                            <div class="span2">
                                <img src="<?php echo $article['thumb']; ?>" class="img-polaroid" style="width: 100%;" />
                            </div>
                        </div>
                    </div>
                    <div class="clearfix clear"></div>
                    <div class="row-fluid" >
                        <div class="control-group">
                            <label class="control-label">News Content </label>
                        </div>

                    </div>                 
                    <div class="row-fluid" style="margin-top: 20px;">
                        <textarea class="editor_content" name="content" id="editor_content" style="visibility: hidden; min-height:500px;"><?php echo $article['content']; ?></textarea>

                    </div>
                    <div style="margin-top: 10px;">
                        <div class="btn green save_article pull-right">Save</div>
                    </div>
                    <div class="clearfix"></div>
                    <input type="hidden" name="pk_post_id" value="<?php echo $article['pk_post_id']; ?>" />
                    <input type="hidden" name="image" value="<?php echo $article['file_name']; ?>" />
                </form>

            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>



<script type="text/javascript">
    var editor;
    function matchYoutubeUrl(url) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var matches = url.match(p);
        if (matches) {
            return matches[1];
        }
        return false;
    }

    $(document).ready(function(e) {
        $(".article_tags").select2({tags: [""]});
        $("#youtube_link2").on("blur", function() {
            var url = $("#youtube_link2").val();
            var id = matchYoutubeUrl(url);
            if (id != false) {
                $("#youtube_link").val(id).blur();
                $("#youtube_link").closest(".control-group").addClass("success").removeClass("error");
                $("#youtube_link").closest(".control-group").find("span.help-inline").html("").addClass("hide");
            } else {
                $("#youtube_link").val('').blur();
                $("#youtube_link").closest(".control-group").addClass("error").removeClass("success");
                $("#youtube_link").closest(".control-group").find("span.help-inline").html("Invalid youtube URL").removeClass("hide");
            }
        });
        if (editor)
            editor.destroy();
        editor = CKEDITOR.replace("editor_content");

        $(".save_article").click(function() {
            var content = editor.getData();
            $(".editor_content").val(content);
            if ($("#article_form").valid() == false) {
                return;
            }
            App.blockUI($("#article_form"));
            var formData = new FormData($("form#article_form")[0]);
            $.ajax({
                url: base_url + "admin/article/updatearticle",
                type: 'POST',
                data: formData,
                async: false,
                success: function(data) {
                    App.unblockUI($("#article_form"));
                    $.gritter.add({
                        title: 'News',
                        text: 'News has been Updated'
                    });
                    window.location.reload();
                },
                error: function(data) {
                    App.unblockUI($("#article_form"));
                    $.gritter.add({title: 'News Error', text: 'Error in Saving News'});
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
        $("#file_type").change(function() {
            if ($(this).val() == 'I') {
                $(".media_img").removeClass("hide");
                $(".media_img input#youtube_link").addClass("required");
                $(".media_video input#youtube_link").removeClass("required");
                $(".media_video").addClass("hide");
            } else if ($(this).val() == 'V') {
                $(".media_video").removeClass("hide");
                $(".media_video input#youtube_link").addClass("required");
                $(".media_img input#youtube_link").removeClass("required");
                $(".media_img").addClass("hide");
            } else {

                $(".media_img input,.media_video input").removeClass("required");
                $(".media_img,.media_video").addClass("hide");
            }
        });
        $("#file_type").change();

    });
</script>

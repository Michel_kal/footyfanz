<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; display: block">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?php if (count($data) > 0) { ?>
            <table class="table table-bordered table-hover table-full-width table-striped">
                <tr>
                    <th style="width: 50px;">#</th>
                    <th>Subject</th>
                    <th>Club</th>
                    <th style="width: 70px;">Action</th>
                </tr>

                <?php
                $i = 1;
                foreach ($data as $email) {
                    ?>
                    <tr>
                        <td><?php echo ($page_number * 30 + $i++); ?></td>
                        <td><?php echo $email['subject']; ?></td>
                        <td><?php
                            if ($email['fk_club_id'] > 0) {
                                echo $email['title'];
                            } else {
                                echo "All Club";
                            }
                            ?></td>
                        <td>
                            <div class="task-config-btn btn-group pull-right">
                                <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a  href="<?php echo site_url() . ADMIN_DIR . "newsletter/sendnews/" . $email['pk_log_id']; ?>"> <i class="icon icon-arrow-right"></i> Resend</a></li>

                                    <li><a class="article_action" href="#"  data-href="<?php echo base_url() . ADMIN_DIR . "newsletter/deleteaction"; ?>" data-action="3" data-id="<?php echo $email['pk_log_id']; ?>"> <i class="icon icon-remove"></i> Delete</a></li>

                                </ul>
                            </div>

                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        }
        if (count($data) == 0) {
            ?>
            <h4 style="text-align: center">No Log Available</h4>
        <?php }
        ?>

    </div>
    <?php echo $page; ?>
</div>


<script type="text/javascript">
    $(".article_action[data-action=3]").easyconfirm({locale: {
            title: 'Delete Log',
            text: 'Are you sure to delete this log',
            button: [' No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
    $(".article_action").click(function(e) {
        e.preventDefault();
        $container = $(this).parents(".portfolio-block");
        App.blockUI($container);
        var post_data = $(this).data();
        $.post($(this).data("href"), post_data, function(res) {
            var jdata = $.parseJSON(res);
            $.gritter.add({
                title: jdata.title,
                text: jdata.text
            });
            App.unblockUI($container);
            $(".pagination li.cpageval").removeClass("active").addClass("inactive").click();
        });
    });
</script>

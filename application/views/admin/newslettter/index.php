<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Email List
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Newsletter
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Email List
                </li>
                <li style="float: right; margin-right: 15px">
                    <a href="<?php echo site_url() ?>admin/newsletter/downloademail">Download</a>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>    
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Email List</div>
                    <select class="m-wrap" id="email_selector" style="float: right; width: 200px; margin: -5px -1px -10px 0px;">
                        <option value="0">All Clubs</option>
                        <?php foreach ($categories as $key => $category) {
                            ?>

                            <option value="<?php echo $key ?>"><?php echo $category ?></option>
                        <?php }
                        ?>
                    </select>
                    <a href="<?php echo base_url() . ADMIN_DIR; ?>/newsletter/add_form" data-target="#modal_email_add" data-toggle="modal" class="remote btn green pull-right" style="margin-top: -5px;"><i class="icon-plus"></i> Add Email</a>
                </div>
                <div class="portlet-body">
                    <div class="row-fluid">
                        <div class=" loaddata" data-url="<?php echo site_url() . "admin/newsletter/emailloader" ?>" data-category="0">

                        </div>
                    </div>                 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal on Page-->
<div id="modal_email_add" class="modal hide fade" data-width="750" data-height="350">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Add Email</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:320px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Close</button>
        <button type="button" class="btn green" onclick='$("#email_add").submit();'>Add Email</button>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(e) {
        $("#email_selector").on('change', function() {
            $(".loaddata").data("category", $(this).val());
            App.loadData($(".loaddata"));
        });
        $(".email_delete").live('click', function() {
            $container = $(".portlet")
            var post_data = $(this).data();
            App.blockUI($container);
            $.post(base_url + "admin/newsletter/deletemail", post_data, function(res) {
                var jdata = $.parseJSON(res);
                $.gritter.add({
                    title: jdata.title,
                    text: jdata.text
                });
                App.unblockUI($container);
                $(".pagination li.cpageval").addClass("inactive").click();
            });
        });
    });


    function add_email() {
        var formData = new FormData($("form#email_add")[0]);
        $.ajax({
            url: base_url + "admin/newsletter/email_save",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                var res = $.parseJSON(data)
                if (res.status == 'OK') {
                    $(".error_block").hide();
                    $(".searchBtn").click();
                    $.gritter.add({
                        title: 'Newsletter',
                        text: 'Email has been Added'
                    });
                    $('#modal_email_add .close').click();
                    $(".cpageval").removeClass("active").addClass("inactive").click();
                } else {
                    $(".error_block").html(res.msg).hide().fadeIn('fast');
                }
            },
            error: function(data) {

            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    }


</script>

<script type="text/javascript">
    var editor;
</script>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Pages
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Dashboard
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Pages
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Pages List</div>
                <a class="btn  green pull-right remote" style="margin:-5px 0 -5px 0; " data-toggle="modal" data-target="#modal_page_add" href="<?php echo base_url() . ADMIN_DIR; ?>/pages/page_add"  >Add New</a>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-hover table-full-width">
                    <tr>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Order</th>
                        <th style="width:100px;">Status</th>
                        <th></th>
                    </tr>
                    <?php foreach ($pages as $page) {
                        ?>
                        <tr>
                            <td><?php echo $page['title']; ?></td>
                            <td><?php echo $page['slug']; ?></td>
                            <td><?php echo $page['menu_order']; ?></td>
                            <td><?php echo ($page['status'] == "1") ? "<div class='label label-success'>Active</div>" : "<div class='label label-important'>Inactive</div>"; ?></td>
                            <td style="width: 120px;">
                                <div class="task-config-btn btn-group pull-right">
                                    <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a class=" remote"  data-toggle="modal" data-target="#modal_page_edit" href="<?php echo base_url() . ADMIN_DIR; ?>/pages/page_edit/<?php echo $page['pk_page_id']; ?>"  ><i class="icon-pencil"></i> Edit</a>
                                        </li>
                                        <li>
                                            <a class="" href="<?php echo base_url() . ADMIN_DIR; ?>/pages/editor/<?php echo $page['pk_page_id']; ?>"  ><i class="icon-file"></i> Content</a>
                                        </li>
                                        <li>
                                            <?php if ($page['status'] == "1") { ?>
                                                <a href="#" data-id="<?php echo $page['pk_page_id']; ?>" data-url="<?php echo base_url() . ADMIN_DIR; ?>/pages/page_status/<?php echo $page['pk_page_id'] . "/0"; ?>" class="page_status"><i class="icon-ban-circle"></i> Deactivate </a>
                                            <?php } else { ?>
                                                <a href="#" data-id="<?php echo $page['pk_page_id']; ?>" data-url="<?php echo base_url() . ADMIN_DIR; ?>/pages/page_status/<?php echo $page['pk_page_id'] . "/1"; ?>" class="page_status"><i class="icon-check"></i> Activate </a>
                                            <?php } ?>
                                        </li>
                                        <li>
                                            <a href="#" data-id="<?php echo $page['pk_page_id']; ?>" data-url="<?php echo base_url() . ADMIN_DIR; ?>/pages/page_delete/<?php echo $page['pk_page_id']; ?>" class="page_delete"><i class="icon-remove"></i> Delete </a>
                                        </li>
                                    </ul>
                                </div>




                            </td>
                        </tr>
                    <?php }
                    ?>
                </table>
            </div>
        </div>
    </div>




    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<script type="text/javascript">

    function update_tiles($form) {
        var parent = $form.parent("div");
        App.blockUI(parent);
        var formData = new FormData($form[0]);
        $.ajax({
            url: base_url + "admin/pages/updateTile",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                var res = $.parseJSON(data)
                App.unblockUI(parent);
                if (res.status == 'OK') {
                    $.gritter.add({
                        title: 'Tile Update',
                        text: 'Tile has been Updated'
                    });
                    App.loadData($(".loaddata"));
                }
            },
            error: function(data) {

            },
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    }

    function add_page() {
        App.unblockUI($(".portlet"));
        var post_data = $('#page_add').serialize();
        $.post(base_url + "admin/pages/page_add_action", post_data, function(res) {
            $.gritter.add({
                title: 'Page Added',
                text: 'Page has been Added'
            });
            window.location.reload();
        });
    }
    function edit_page() {
        App.unblockUI($(".portlet"));
        var post_data = $('#page_edit').serialize();
        $.post(base_url + "admin/pages/page_edit_action", post_data, function(res) {
            $.gritter.add({
                title: 'Page Updated',
                text: 'Page has been Updated'
            });
            window.location.reload();
        });
    }
    $(document).ready(function(e) {

        $(".page_delete").easyconfirm({locale: {
                title: 'Delete Page?',
                text: 'Are you sure to delete this page?',
                button: ['No', ' Yes'],
                action_class: 'btn red',
                closeText: 'Cancel'
            }});

        $(".page_delete").click(function() {
            App.unblockUI($(".portlet"));
            var post_data = $(this).data();
            $.post(base_url + "admin/pages/pade_delete", post_data, function(res) {
                $.gritter.add({
                    title: 'Page Deleted',
                    text: 'Page has been Deleted'
                });
                window.location.reload();
            });
        });
        $(".page_status").click(function() {
            App.unblockUI($(".portlet"));
            var post_data = $(this).data();
            $.post($(this).data('url'), post_data, function(res) {
                $.gritter.add({
                    title: 'Page Status',
                    text: 'Page Status has been Updated'
                });
                window.location.reload();
            });
        });

    });
</script>


<div id="modal_page_add" class="modal hide fade" data-width="700" data-height="250">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Add Page</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:250px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Close</button>
        <button type="button" class="btn green" onclick='$("#page_add").submit();'><i class="icon-save"></i> Save</button>
    </div>
</div>
<div id="modal_page_edit" class="modal hide fade" data-width="700" data-height="250">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Edit Page</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:250px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Close</button>
        <button type="button" class="btn green" onclick='$("#page_edit").submit();'><i class="icon-save"></i> Update</button>
    </div>
</div>
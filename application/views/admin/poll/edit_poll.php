<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Opinion Poll
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-group"></i>
                    Opinion Poll
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Edit Poll
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">

            <div class="span3 pull-right">
                <a href="<?php echo base_url() . ADMIN_DIR; ?>/poll" class="btn yellow pull-right"><i class="icon-plus"></i> Back To List</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet box blue">
                <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                    <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i>  Edit Poll</div>

                </div>
                <div class="portlet-body">
                    <?php // print_r($poll); ?>

                    <div class="row-fluid">
                        <form action="#" id="poll_add" data-url="<?php echo base_url() . ADMIN_DIR; ?>/poll/save" class="form-horizontal" enctype="multipart/form-data">
                            <div class="row-fluid">
                                <div class="alert alert-success span6" style="display: none;">
                                    Poll Updated Successfully
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Title <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="Title" value="<?php echo $poll['title']; ?>" id="title" name="title" class="m-wrap required span9">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Type <span class="required">*</span></label>
                                <div class="controls">
                                    <select name="type" class="required">
                                        <option value="">Select Type</option>
                                        <option value="V" <?php if ($poll['type'] == 'V') echo 'selected'; ?>>Vertical</option>
                                        <option value="H" <?php if ($poll['type'] == 'H') echo 'selected'; ?>>Horizontal</option>

                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Start Date <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="Start Date" id="start_date" name="start_date" value="<?php echo date("d/m/Y", strtotime($poll['start_date'])); ?>" class="m-wrap required span7 datepicker">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">End Date <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="End Date" value="<?php echo date("d/m/Y", strtotime($poll['end_date'])); ?>" id="end_date" name="end_date"  class="m-wrap required span7 datepicker">
                                </div>
                            </div>
                            <?php
                            $image = POLL_DIR . $poll['image'];
                            if (is_file($image)) {
                                $wt = array('V' => 300, 'H' => 800);
                                ?>
                                <div class="control-group">
                                    <label class="control-label" style="">Current Image </label>
                                    <div class="controls">
                                        <img src="<?php echo site_url() . $image; ?>" style="width: <?php echo $wt[$poll['type']] / 2; ?>" class="img-polaroid">
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="control-group">
                                <label class="control-label" style="">Change Image </label>
                                <div class="controls">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <span class="btn btn-file">
                                            <span class="fileupload-new">Select file to upload</span>
                                            <span class="fileupload-exists">Change</span>
                                            <input type="file" class="default file_file" name="image" id="file_file" data-msg-accept="Choose a valid file" accept="jpg|jpeg|gif|png">
                                        </span>
                                        <span class="fileupload-preview"></span>
                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none"></a>
                                        <span class="help-inline file_alert file_img_alert1" for="file_file" style="margin-top: -2px; margin-left: 10px; display:none"></span>
                                    </div>
                                    <span class="label label-info">Note: </span> <small>Only jpg, gif, jpeg and png files are allowed</small>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" style="">Option1 <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="i.e. Yes" value="<?php echo $poll['option1']; ?>" id="option1" name="option1" class="m-wrap  span7 required">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Option2 <span class="required">*</span></label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="i.e. No" value="<?php echo $poll['option2']; ?>" id="option2" name="option2" class="m-wrap  span7 required">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Option3 </label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="" value="<?php echo $poll['option3']; ?>" id="option3" name="option3" class="m-wrap  span7 ">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Option4 </label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="" value="<?php echo $poll['option4']; ?>" id="option4" name="option4" class="m-wrap  span7 ">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style="">Option5 </label>
                                <div class="controls"  style="">
                                    <input type="text" placeholder="" value="<?php echo $poll['option5']; ?>" id="option5" name="option5" class="m-wrap  span7 ">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" style=""></label>
                                <div class="controls"  style="">
                                    <button type="button" class="btn green" onclick='$("#poll_add").submit();'>Update Poll</button>
                                    <input type="hidden" name="poll_id" value="<?php echo $poll['pk_poll_id']; ?>" />
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<!-- Modal on Page-->

<script type="text/javascript">
    $(document).ready(function(e) {
        $(".datepicker").datepicker({'format': 'dd/mm/yyyy', autoclose: true});
    });
    function save_poll() {
        App.blockUI($("#poll_add").parent("div"));

        var formData = new FormData($("#poll_add")[0]);
        $.ajax({
            url: $("#poll_add").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#poll_add").parent("div"));
//                var jdata = $.parseJSON(data);
                $("#poll_add .alert-success").show();
                setTimeout(function() {
                    $("#poll_add .alert-success").hide();
                }, 4000);
            },
            error: function(data) {
                App.unblockUI($("#poll_add").parent("div"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    $(document).ready(function(e) {
        setTimeout(function() {
            $('#poll_add').validate({
                submitHandler: function(form) {
                    save_poll();
                    return false;
                }
            });
        }, 500);

    });
</script>


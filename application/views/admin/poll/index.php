<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Opinion Poll
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-group"></i>
                   Opinion Poll
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Polls
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">
        <div class="row-fluid" style="margin-bottom: 10px;">

            <div class="span3 pull-right">
                <a href="<?php echo base_url() . ADMIN_DIR; ?>poll/create_poll" class="btn green pull-right"><i class="icon-plus"></i> Create New Poll</a>
            </div>
        </div>
        <div class="row-fluid">
        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i>  Poll List</div>
               
            </div>
            <div class="portlet-body">
                <div id="poll_list"></div>
                <?php
                paginationScript(base_url() . "admin/poll/poll_list", "poll_list", "");
                ?>
            </div>
        </div>
    </div>
    </div>

    <!-- END Modal on Page-->
    <!-- END PAGE CONTAINER-->
</div>

<!-- Modal on Page-->

<script type="text/javascript">
    function add_ads() {
        App.blockUI($("#ads_add"));

        var formData = new FormData($("#ads_add")[0]);
        $.ajax({
            url: $("#ads_add").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#ads_add"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Ads",
                        text: jdata.msg
                    });
                    $("#modal_ads_add .close_button").click();
                     $(".searchBtn").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#ads_add"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function update_ads() {
        App.blockUI($("#ads_edit"));

        var formData = new FormData($("#ads_edit")[0]);
        $.ajax({
            url: $("#ads_edit").data("url"),
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#ads_edit"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Ads",
                        text: jdata.msg
                    });
                    $("#modal_ads_edit .close_button").click();
                   
                    $(".pagination li.active.cpageval").addClass("inactive").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#ads_edit"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

</script>


<?php echo '<?xml version="1.0" encoding="UTF-8" ?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo base_url(); ?></loc>
        <priority>1.0</priority>
    </url>
    <?php
    foreach ($data['categories'] as $slug => $categories) {
        if ($categories['category'] == "") {
            continue;
        }
        ?>
        <url>
            <loc><?php echo base_url() . CATEGORY_TAG . "/" . $slug; ?></loc>
            <priority>0.9</priority>
        </url>
        <?php
        if (count($categories['sub']) > 0) {
            foreach ($categories['sub'] as $slug2 => $categories2) {
                ?>
                <url>
                    <loc><?php echo base_url() . CATEGORY_TAG . "/" . $slug2; ?></loc>
                    <priority>0.8</priority>
                </url>
                <?php
            }
        }
    }
    foreach ($data['post'] as $dt) {
        ?>
        <url>
            <loc><?php echo base_url() . ARTICLE_TAG . "/" . $dt['cslug'] . "/" . $dt['slug']; ?></loc>
            <priority>0.7</priority>
        </url>
    <?php } ?>
    <?php $menus = getMenus(); ?>


    <?php foreach ($menus as $mnu) {
        ?>
        <url>
            <loc><?php echo base_url() . PAGES_TAG . "/" . $mnu['slug']; ?></loc>
            <priority>0.6</priority>
        </url>
    <?php }
    ?>

</urlset>
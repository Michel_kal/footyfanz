<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Users
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Dashboard
                    <i class="icon-angle-right"></i>
                </li>                
                <li>
                    Users
                </li>
                <li style="float: right; margin-right: 15px">
                    <a href="<?php echo site_url() ?>admin/users/downloaduser">Download</a>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Users List</div>

                <a class="btn  green pull-right remote" style="margin:-5px 0 -5px 0; display: none; " data-toggle="modal" data-target="#modal_page_add" href="<?php echo base_url() . ADMIN_DIR; ?>users/user_add"  >Add User</a>
            </div>
            <div class="portlet-body">







                <div class="tabbable tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1_1" data-toggle="tab" class="tab-loader" data-vtype='1'>Active Users</a></li>
                        <li><a href="#tab_1_1" data-toggle="tab"  class="tab-loader" data-vtype='2'>Deactivated Users</a></li>
                        <li><a href="#tab_1_1" data-toggle="tab"  class="tab-loader" data-vtype='0'>Email Not Verified Users</a></li>
                    </ul>
                    <div class="tab-content" style="overflow:visible">
                        <div class="tab-pane active" id="tab_1_1">
                            <div class="row-fluid">
                                <div class="content_loader loaddata" data-url="<?php echo base_url() . "admin/users/loader" ?>" data-vtype="1" >


                                </div>
                            </div>
                        </div>

                    </div>
                </div>










            </div>
        </div>
    </div>




    <!-- END Modal on Page--> 
    <!-- END PAGE CONTAINER--> 
</div>

<script type="text/javascript">
    $(".tab-loader").click(function() {
        $(".content_loader").data("vtype", $(this).data('vtype'));
        App.loadData($(".content_loader"));
    });
    $(document).on('click', ".user_delete", function(e) {
        e.preventDefault();
        var post_data = $(this).data();
        $.post($(this).data('url'), post_data, function(res) {
            $.gritter.add({
                title: "User deleted",
                text: "User has been deleted."
            });
            $(".cpageval").removeClass("active").addClass("inactive").click();
        });
    });

    function add_user() {
        App.blockUI($("#user_add"));
        var post_data = $("#user_add").serialize();
        $.post($("#user_add").data("url"), post_data, function(res) {
            App.unblockUI($("#user_add"));
            var jdata = $.parseJSON(res);
            if (jdata.status == "1") {
                $.gritter.add({
                    title: "User registration",
                    text: jdata.msg
                });
                $(".success_user").html(jdata.msg).fadeIn("fast");
                $(".alert_user").hide();
                window.location.href = $("#user_add").data("nexturl") + jdata.id;
            } else {
                $(".alert_user").html(jdata.msg).fadeIn("fast");
                $(".success_user").hide();
            }
        });
    }
</script>

<div id="modal_page_add" class="modal hide fade" data-width="700" data-height="350">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Add User</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:350px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Close</button>
        <button type="button" class="btn green" onclick='$("#user_add").submit();'><i class="icon-save"></i> Save</button>
    </div>
</div>

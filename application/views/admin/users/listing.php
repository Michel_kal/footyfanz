
<div class="data">
    <div class="row-fluid search-forms search-default" style="margin-top: 0px; margin-bottom: 10px; ">
        <div class="search-form">
            <div class="chat-form" style="margin-top: 0px">
                <div class="input-cont span10" style="margin-right:0px">
                    <input type="text" placeholder="Search..." value="<?php echo $search; ?>" id="txtSearchBox" class="m-wrap searchbox span6">
                </div>
                <button type="button" class="searchBtn btn blue span2">Search &nbsp; <i class="m-icon-swapright m-icon-white"></i></button>
            </div>
        </div>            
    </div>
    <?php
    if ($data) {
        foreach ($data as $invest) {
            ?>
            <div class="row-fluid  portfolio-block" style="margin-bottom: 10px;">
                <div class="span1">
                    <img src="<?php echo $invest['profile_pic']; ?>" class=" img-polaroid" style="width: 100%;" />
                </div>
                <div class="span11 portfolio-text" style="overflow: visible">
                    <div class="row-fluid">
                        <div class="span10">
                            <h3 style="margin:0px 0; float: left; width: auto;"><?php echo $invest['name']; ?></h3>

                            <?php if ($invest['premium'] == "1") { ?>
                                <div class="label label-success pull-right" style="margin-top: 5px;margin-right: 5px;">Premium</div>
                            <?php } ?>
                            <?php if ($invest['status'] == "0") { ?>
                                <div class="label label-red label-important pull-right" style="margin-top: 5px; margin-right: 5px;">Inactive</div>
                            <?php } ?>

                            <?php if ($invest['ff_plus'] == "1") { ?>
                                <div class="label label-warning pull-right" style="margin-top: 5px;margin-right: 5px;">FootyFanz+</div>
                            <?php } ?>

                            <div class="clearfix"></div>

                        </div>
                        <div class="span2">
                            <div class="task-config-btn btn-group pull-right">
                                <a class="btn mini blue" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">Actions <i class="icon-angle-down"></i></a>
                                <ul class="dropdown-menu pull-right" style="width: 175px;">
                                    <li>
                                        <a href="<?php echo base_url() . ADMIN_DIR; ?>users/profile/<?php echo $invest['pk_user_id']; ?>" ><i class="icon-file-text"></i> View Profile </a>
                                    </li>

                                    <?php if ($invest['status'] == "0") { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/resendmail/1" class="master_status"><i class="icon-ok"></i> Resend Verification</a>
                                        </li>
                                        <li>
                                            <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/updatestatus/1" class="master_status"><i class="icon-ok"></i> Verify Email</a>
                                        </li>

                                    <?php } else if ($invest['status'] == "1") { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/updatestatus/2" class="master_status"><i class="icon-ban-circle"></i> Deactivate </a>
                                        </li>                                       
                                    <?php } else if ($invest['status'] == "2") { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/updatestatus/1" class="master_status"><i class="icon-ok-sign"></i> Activate </a>
                                        </li>   
                                    <?php } ?>

                                    <?php if ($invest['premium'] == "1") { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/premstatus/0" class="master_status"><i class="icon-ban-circle"></i> Remove Premium </a>
                                        </li>                                       
                                    <?php } else { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/premstatus/1" class="master_status"><i class="icon-ok-sign"></i> Make Premium </a>
                                        </li>   
                                    <?php } ?>
                                    <?php if ($invest['ff_plus'] == "1") { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/ffstatus/0" class="master_status"><i class="icon-ban-circle"></i> Remove FootyFanz+ </a>
                                        </li>                                       
                                    <?php } else { ?>
                                        <li>
                                            <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/ffstatus/1" class="master_status"><i class="icon-ok-sign"></i> Make FootyFanz+ </a>
                                        </li>   
                                    <?php } ?>
                                    <?php if ($invest['ff_plus'] == "1") { ?>
                                        <?php if ($invest['ff_verify'] == "1") { ?>
                                            <li>
                                                <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/ffverifystatus/0" class="master_status"><i class="icon-ban-circle"></i> Unverify FootyFanz+ </a>
                                            </li>
                                        <?php } else { ?>
                                            <li>
                                                <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/ffverifystatus/1" class="master_status"><i class="icon-ok-sign"></i> Verify FootyFanz+ </a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <li>
                                        <a href="#" data-id='<?php echo $invest['pk_user_id']; ?>' data-url="<?php echo base_url() . ADMIN_DIR; ?>users/deleteuser" class="user_delete"><i class="icon-remove"></i> Delete </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <?php if ($invest['gender'] != "") { ?>
                                Gender: <span class="text-info"><?php echo $invest['gender']; ?> </span> &nbsp; &nbsp;

                            <?php } ?>

                            <?php if ($invest['username'] != "") { ?>
                                Username: <span class="text-info"><?php echo $invest['username']; ?> </span> &nbsp; &nbsp;

                            <?php } ?>
                        </div>
                    </div>  

                    <div class="row-fluid">
                        <div class="span12">

                            Email: <span class="text-info"><?php echo $invest['email']; ?></span> &nbsp; &nbsp;
                            <?php if ($invest['phone_no'] != "") { ?>
                                Phone No.: <span class="text-info"><?php echo $invest['phone_no']; ?></span> &nbsp; &nbsp;

                            <?php } ?>
                            <?php if ($invest['fk_club_id'] > 0 && isset($clubs[$invest['fk_club_id']])) { ?>
                                Club: <span class="text-info"><?php echo $clubs[$invest['fk_club_id']]; ?> </span> &nbsp; &nbsp;

                            <?php } ?>


                        </div>
                    </div>



                </div>
            </div>

        <?php } ?>
        <?php
    } else {
        ?> <h4 class="text-center">No User Available</h4> <?php
    }
    ?>
    <?php echo $page; ?>
</div>

<script type="text/javascript">
    $(".action_btn").on('click', function() {

    });

    $(".user_delete").easyconfirm({locale: {
            title: 'Delete User?',
            text: 'Are you sure to delete this user?',
            button: ['No', ' Yes'],
            action_class: 'btn red',
            closeText: 'Cancel'
        }});
</script>

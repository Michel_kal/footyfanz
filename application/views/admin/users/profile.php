<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">						
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                <?php echo $user['name'] ?>
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-dashboard"></i>
                    Dashboard
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    Users <i class="icon-angle-right"></i>
                </li>
                <li>
                    <?php echo $user['name'] ?>
                </li>
                <li class="pull-right" style="margin-right: 15px;">
                    <?php echo $user['username'] ?>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <!-- END PAGE CONTENT-->

    <div class="page-content-body">

        <div class="portlet box blue">
            <div class="portlet-title" style="padding: 5px 0px 0px 10px;">
                <div class="caption glyphicons no-js history" style="margin-top: 2px;"><i class="icon-reorder"></i> Hashtags</div>
                <a class="btn  green pull-right remote" style="margin:-5px 0 -5px 0; " data-toggle="modal" data-target="#modal_tag_add" href="<?php echo base_url() . ADMIN_DIR; ?>users/tag_add"  >Add Hashtag</a>
            </div>
            <div class="portlet-body">


            </div>
        </div>
    </div>




    <!-- END Modal on Page--> 
    <!-- END PAGE CONTAINER--> 
</div>

<script type="text/javascript">
    $(".tab-loader").click(function() {
        $(".content_loader").data("vtype", $(this).data('vtype'));
        App.loadData($(".content_loader"));
    });

    function add_tag() {
        App.blockUI($("#tag_add"));

        var formData = new FormData($("#tag_add")[0]);
        $.ajax({
            url: $("#tag_add").data("url") + "/<?php echo $user['id']; ?>",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#tag_add"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Hashtag",
                        text: jdata.msg
                    });
                    $("#modal_tag_add .close_button").click();
                    $(".nav-tabs a[data-vtype=1]").click();
                } else {
                    $(".alert_user").html(jdata.msg).fadeIn("fast");
                    $(".success_user").hide();
                }
            },
            error: function(data) {
                App.unblockUI($("#tag_add"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function update_tag() {
        App.blockUI($("#tag_edit"));

        var formData = new FormData($("#tag_edit")[0]);
        $.ajax({
            url: $("#tag_edit").data("url") + "/<?php echo $user['id']; ?>",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($("#tag_edit"));
                var jdata = $.parseJSON(data);
                if (jdata.status == "1") {
                    $.gritter.add({
                        title: "Hashtag",
                        text: jdata.msg
                    });
                    $("#modal_tag_edit .close_button").click();
                    $(".nav-tabs a[data-vtype=1]").click();
                }
            },
            error: function(data) {
                App.unblockUI($("#tag_edit"));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }


</script>


<div id="modal_tag_add" class="modal hide fade" data-width="700" data-height="400">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Add Hashtag</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:250px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn close_button">Close</button>
        <button type="button" class="btn green" onclick='$("#tag_add").submit();'><i class="icon-save"></i> Save</button>
    </div>
</div>

<div id="modal_tag_edit" class="modal hide fade" data-width="700" data-height="400">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3><i class="icon-reorder"></i> Edit Hashtag</h3>
    </div>
    <div class="modal-body"  style="overflow:hidden; min-height:250px; ">

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn close_button">Close</button>
        <button type="button" class="btn green" onclick='$("#tag_edit").submit();'><i class="icon-save"></i> Update</button>
    </div>
</div>

<?php
if ($type == 'check') {
    if ($clubs) {
        foreach ($clubs as $key => $value) {
            ?> 
            <label class="checkbox">
                <input type="checkbox" name="club[]" style="display: none" value="<?php echo $key; ?>" /> <?php echo $value; ?>
            </label>           

            <?php
        }
    } else {
        ?> 
        <h4>No Clubs Available</h4>
        <?php
    }
}

if ($type == 'check_selected') {
    if ($clubs) {
        foreach ($clubs as $key => $value) {
            ?> 
            <label class="checkbox">
                <input type="checkbox" name="club[]" <?php if(in_array($key, $curr_clubs)) echo 'checked'; ?> style="display: none" value="<?php echo $key; ?>" /> <?php echo $value; ?>
            </label>           

            <?php
        }
    } else {
        ?> 
        <h4>No Clubs Available</h4>
        <?php
    }
}
?>

<?php
if ($type == 'select') {
    ?> 
<option value="0">All Club</option>
 <?php
    if ($clubs) {
        foreach ($clubs as $key => $value) {
            ?> 
<option value="<?php echo $key; ?>" <?php if($club_id==$key) echo 'selected'; ?>><?php echo $value; ?></option>        

            <?php
        }
    } 
}
?>
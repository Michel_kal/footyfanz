<?php
$templates = getEmailFormat("4");

$template = str_replace("[username]", $data['comment_by'], $templates);
$template = str_replace("[sitename]", SITE_NAME, $template);
$template = str_replace("[postname]", $data['article_title'], $template);
$template = str_replace("[comment]", $data['comment'], $template);

$vbutton = '<a href="' . $data['article_link'] . '" style="width:120px; margin:0 auto; height:20px;float:left; margin-top:15px; margin-right:20px; color: #FFF; background: #35aa47;  text-align:center; text-decoration:none; padding:5px 3px 3px 3px; " >View Post</a>';
$template = str_replace("[postbutton]", $vbutton, $template);
?>

<div><?php echo $template; ?></div>
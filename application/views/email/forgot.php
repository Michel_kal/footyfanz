<?php
$templates = getEmailFormat("3");
$template = str_replace("[firstname]", $data['firstname'], $templates);
$template = str_replace("[lastname]", $data['lastname'], $template);
$template = str_replace("[email]", $data['email'], $template);
$template = str_replace("[username]", $data['username'], $template);
$template = str_replace("[sitename]", SITE_NAME, $template);
$resetlink=site_url()."user/resetpassword/".  $data['pk_user_id']."/".$data['reset_pass_code'];
$template = str_replace("[resetlink]", $resetlink, $template);
$vbutton = '<a href="' . $resetlink . '" style="width:120px; margin:0 auto; height:20px;float:left; margin-top:15px; margin-right:20px; color: #FFF; background: #35aa47;  text-align:center; text-decoration:none; padding:5px 3px 3px 3px; " >Reset Password</a>';
$template = str_replace("[resetbutton]", $vbutton, $template);
?>
<div><?php echo $template; ?></div>
<?php
$templates = getEmailFormat("2");

$template = str_replace("[username]", $data['name'], $templates);

$template = str_replace("[sitename]", SITE_NAME, $template);

$vlink = base_url() . "user/activation/?auth=" . base64_encode($data['email']) . "&act_code=" . base64_encode($data['vcode']);

$vbutton = '<a href="' . $vlink . '" style="width:120px; margin:0 auto; height:20px;float:left; margin-top:15px; margin-right:20px; color: #FFF; background: #35aa47;  text-align:center; text-decoration:none; padding:5px 3px 3px 3px; " >Activate Account</a>';

$template = str_replace("[verificationlink]", $vlink, $template);

$template = str_replace("[verificationbutton]", $vbutton, $template);
?>

<div><?php echo $template; ?></div>
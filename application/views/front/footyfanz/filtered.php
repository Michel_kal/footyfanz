<?php
$base = site_url() . PUBLIC_DIR . "fassets/images/";
$backall = getBackground("2", "0");
$back6 = getBackground("7", "0");

$bg = "";
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back6) > 0) {
    $bg = $back6[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>");}
    </style>
    <?php
}
?>


<div class="row ff-result">

</div>

<div class="commentloader loading">
    <div class="ff-more loadmore">More FootyFanz Plus</div>
    <div class="loaderview"><img src="<?php echo site_url() . PUBLIC_DIR; ?>images/loadingBar.gif" /></div>
</div>

<input type="hidden" id="league" value="<?php echo $league ?>" />
<input type="hidden" id="club" value="<?php echo $club ?>" />
<input type="hidden" id="txt_search" value="<?php echo $name; ?>" />
<script type="text/javascript">
    var page = 1;
    function getMembers(type) {
        var post_data = {page: page, league: $("#league").val(), club: $("#club").val(), search_key: $("#txt_search").val()};
        $('.commentloader').addClass('loading');
        $.post(base_url + "footyfanz/getlistfiltered", post_data, function(res) {
//            alert(res);
            $('.commentloader').removeClass('loading');
            if (type == "new") {
                $(".ff-result").html("");
            }
            $(".ff-result").append(res);
        });
    }
    $(document).on('click', ".loadmore", function(e) {
        page++;
        getMembers("append");
    });
    $(document).ready(function(e) {

        $("#league").on("change", function() {
            page = 1;
            getMembers("new");
            var post_data = {league_id: $("#league").val()};
            $.post(base_url + "footyfanz/getclubs", post_data, function(res) {
                $("#club").html(res);
                $("#club").closest("div").hide().fadeIn();

            });
        });
        $("#club").on("change", function() {
            page = 1;
            getMembers("new");
        });
        $(".searchBtn").on("click", function() {
            page = 1;
            getMembers("new");
        });
        $("#league").change();
    });
    $(document).on("click", ".ff_follow", function() {
        var follow_user_id = $(this).data("id");
        var fname = $(this).closest(".ff-image").find(".ff-name").html();
        $.confirm({
            title: 'Follow ' + fname,
            content: 'Are you sure to follow ' + fname,
            confirmButton: 'Yes',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "footyfanz/follow", {follow_user_id: follow_user_id}, function(res) {
                    var jres = $.parseJSON(res);
                    if (jres.status == "1") {
                        $(".searchBtn").click();
                    } else {
                        alert("Please login to follow " + $.trim(fname));
                    }

                });
            }
        });
    });
</script>

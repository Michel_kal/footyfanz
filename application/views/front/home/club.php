<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
//print_r($hp_news);
$main_news = $hp_news[0];
$trend_news = getAticles('N', 0, 0, $club['pk_club_id'], 0, 9, false);
$top_news = getTopAticles('N', $club['pk_club_id'], 0, 9);
$backall = getBackground("2", "0");
$back1 = getBackground("6", $club['fk_league_id']);
$back2 = getBackground("5", $club['pk_club_id'], true);
$back3 = getBackground("5", $club['pk_club_id']);
$bg = "";
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back1) > 0) {
    $bg = $back1[0];
}
if (count($back2) > 0) {
    $bg = $back2[0];
}
if (count($back3) > 0) {
    $bg = $back3[0];
}
if ($bg != "") {
    ?>
    <style>
    body{background-color: <?php echo $bg['color']; ?>;}
    body{background-image: url("<?php echo $bg['image']; ?>");}
    </style>
    <?php
}
?>
<section class="homefirstcol">

    <div class="row">
        <div class="col-md-8">
            <div class="row strip">
                <div class="col-md-3" style="padding: 0;">
                    <a class="blacklabel" href="<?php echo site_url() . "articles/topwriters/" . $club['slug']; ?>">
                        Top Writers
                    </a>
                </div>
                <div class="col-md-9" style="padding-left:  0;">
                    <div class="stripicons">
                        <?php
                        if ($hp_writers) {
                            foreach ($hp_writers as $pos => $row) {
                                ?>
                                <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>" title="<?php echo $row['name']; ?>">
                                    <img src="<?php echo $row['thumb']; ?>" />
                                </a>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row strip">
        <div class="col-md-8">


            <div class="clearfix"></div>





            <a href="<?php echo site_url() . NEWS_TAG . "/"; ?><?php echo $main_news['slug'] ?>">
                <div class="homeslider">
                    <img src="<?php echo $main_news['image']; ?>" />

                    <div class="sliderfooter">
                        <div class="line"></div>

                        <div>
                            <div class="hs_heading"><?php echo $main_news['title']; ?></div>
                            <div class="clear clearfix"></div>
                            <div class="uname">by <a href="<?php echo site_url() . "articles/listing/" . $main_news['user_slug']; ?>"><?php echo $main_news['posted_by']; ?></a></div>
                            <div class="uimage"><img src="<?php echo $main_news['user_image']; ?>" /></div>                                            
                            <div class="ucomments"><i class="fa fa-comments-o"></i> <?php echo $main_news['comments']; ?></div>
                            <div class="uviews"><i class="fa fa-eye"></i> <?php echo $main_news['views']; ?></div>
                        </div>
                    </div>
                </div>
            </a>
            <div class="clearfix"></div>
            <div class="row slidersmall">
                <?php
                if (is_array($hp_news)) {
                    foreach ($hp_news as $key => $row) {
                        if ($key == 0)
                            continue;
                        ?> 
                        <div class="col-md-4 ">
                            <a href="<?php echo site_url() . NEWS_TAG . "/"; ?><?php echo $main_news['slug'] ?>">
                                <div class="homeslidersmall">
                                    <img src="<?php echo $row['image']; ?>" />
                                    <div class="smalltitle">
                                        <?php echo $row['title']; ?>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <?php
                    }
                }
                ?>


            </div>

            <div class="clearfix"></div>

        </div>
        <div class="col-md-4">
            <div class="writerboard">
                <div class="boardheader">
                    Top Fanz                   


                    <a href="<?php echo site_url() . "articles/topfanz/" . $club['slug']; ?>">More</a>
                </div>
                <table class="table postable" style="margin-bottom:0px;">
                    <tr>
                        <th>Pos</th>
                        <th>Fanz</th>
                        <th>Pts</th>
                    </tr>
                    <?php
                    if ($hp_fanz) {
                        foreach ($hp_fanz as $pos => $row) {
                            ?> 
                            <tr>
                                <td><?php echo $pos + 1; ?> <span class="symbolup"><i class="fa fa-sort-up"></i></span></td>
                                <td><a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>"><?php echo $row['name']; ?> <span class="boarduser"><img src="<?php echo $row['thumb']; ?>" /></span></a></td>
                                <td><?php echo $row['points']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>                  
                </table>
            </div>


        </div>
    </div>


    <div class="row">
        <div class="col-md-8">
            <div class="clubinfo">
                <div class="clubinfoin row">

                    <div class="clubmain col-md-9">
                        <h3><?php echo $club['title']; ?></h3>
                        <div class="clearfix"></div>
                        <div>
                            <?php if ($club['team_manager'] != "") { ?>
                            <span><strong>Team Manager:</strong> <?php echo $club['team_manager']; ?> </span>
                            <?php } ?>
                            <?php if ($club['captain'] != "") { ?>
                            <span style="margin-left: 5px;"> <strong>Captain:</strong> <?php echo $club['captain']; ?> </span>
                            <?php } ?>
                        </div>
                        <?php if ($club['stadium'] != "") { ?>
                        <div style="margin-top:4px;margin-bottom: 4px;"><span><strong>Stadium:</strong> <?php echo $club['stadium']; ?> </span></div>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <div style="margin-top:1px;"><?php echo $club['description']; ?></div>


                    </div>
                    <div class="clublogo col-md-3">
                        <img src="<?php echo $club['logo']; ?>"/>
                    </div>
                </div>
                <div class="cinfofooter">
                    <ul>
                        <li style="cursor:pointer; " onclick="window.location.href = '<?php echo site_url() . "articles/topfanz/" . $club['slug']; ?>'">
                            <div class="value"><?php echo $fan_count; ?></div>
                            <div class="title">Fanz</div>
                        </li>
                        <li style="cursor:pointer; " onclick="window.location.href = '<?php echo site_url() . "articles/topwriters/" . $club['slug']; ?>'">
                            <div class="value"><?php echo $wr_count; ?></div>
                            <div class="title">Writers</div>
                        </li>
                        <li style="cursor:pointer; " onclick="window.location.href = '<?php echo site_url() . "articles/clublisting/" . $club['slug']; ?>'" >
                            <div class="value"><?php echo $art_count; ?></div>
                            <div class="title">Articles</div>
                        </li>
                        <li>
                            <div class="value"><?php echo $art_reads; ?></div>
                            <div class="title">Reads</div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix" ></div>
            </div>
            <div class="clearfix" style="margin-bottom: 15px;" ></div>
            <div class="profilenav" >
                <?php for ($i = 0; $i < $club['trophies']; $i++) { ?>
                <div class="trophyicon pull-left"><img src="<?php echo $base; ?>images/victor.png"></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-4">
            <?php
            $gc = trim(getCMSContent("ad_ggl_square"));
            $ads = getSiteAds(5, $club['pk_club_id'], "square");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <div class="leftadd">
                    <a href="<?php echo $ads[$num]['link'] ?>" target="_blank">
                        <img src="<?php echo $ads[$num]['image']; ?>" />
                    </a>
                </div>
                <?php
            } else {
                echo $gc;
            }
            ?>
        </div>
    </div>




    <div class="row strip">
        <div class="col-md-3">
            <div class="blacklabel">
                Top News
            </div>
        </div>
        <div class="col-md-9">
            <div class="stripicons">             
                <img src="<?php echo $base; ?>images/iconfire.png" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row trendnews">
                <div class="col-md-8">
                    <?php if ($top_news) { ?>
                    <a href="<?php echo site_url() . NEWS_TAG . "/" . $top_news[0]['slug']; ?>">
                        <div class="tnewsblock">
                            <img src="<?php echo $top_news[0]['large']; ?>" />

                            <div class="tnbfooter">
                                <div class="heading"><?php echo $top_news[0]['title']; ?></div>
                                <div class="uname">by <?php echo $top_news[0]['user']['name']; ?></div>
                                <div class="uimage"><img src="<?php echo $top_news[0]['user']['profile_pic']; ?>"></div>
                            </div>
                        </div>
                    </a>
                    <?php } ?>
                </div>
                <div class="col-md-4">
                    <div class="promoted">
                        <ul>
                            <li>
                                <a>More News...</a>

                            </li>
                            <?php
                            if ($top_news and count($top_news) > 1) {
                                foreach ($top_news as $key => $news) {
                                    if ($key == 0)
                                        continue;
                                    ?> <li>
                                    <a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>"><?php echo $news['title']; ?></a>
                                    </li> <?php
                                }
                                ?>

                                <?php } ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="col-md-4">
                <?php
                $gc = trim(getCMSContent("ad_ggl_square"));
                $ads = getSiteAds(5, $club['pk_club_id'], "square");
                $cnt = count($ads);
                if ($cnt > 0) {
                    $num = array_rand($ads);
                    ?>
                    <div class="leftadd">
                        <a href="<?php echo $ads[$num]['link'] ?>" target="_blank">
                            <img src="<?php echo $ads[$num]['image']; ?>" />
                        </a>
                    </div>
                    <?php
                } else {
                    echo $gc;
                }
                ?>
            </div>
        </div>



    </section>
    <section>

        <?php if (count($ffplus) > 0) { ?>
        <div class="row strip">
            <div class="col-md-3">
                <div class="blacklabel">
                    Footyfanz+
                </div>
            </div>
            <div class="col-md-9 hide_mobile">
                <div class="stripicons">
                    <img src="<?php echo $base; ?>images/iconfire.png" />
                </div>
            </div>
        </div>
        <div class="row strip">
            <div class="col-md-12">

                <div class="homeffpluscon">
                    <?php foreach ($ffplus as $ffp) { ?>
                    <div class="homeffplusitem">
                        <a href="<?php echo site_url() . "articles/listing/" . $ffp['slug']; ?>">
                            <img src="<?php echo $ffp['profile_pic']; ?>" />

                            <div class="textname">
                                <div class="texttitle"><?php echo $ffp['art_title'] ?></div>
                                <div><?php echo $ffp['name'] ?></div>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                </div>

            </div>
        </div>
        <?php } ?>






        <div class="row strip">
            <div class="col-md-3">
                <div class="blacklabel">
                    Trending News
                </div>
            </div>
            <div class="col-md-9">
                <div class="stripicons">

                    <img src="<?php echo $base; ?>images/iconfire.png" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="row trendnews">
                    <div class="col-md-8">
                        <?php if ($trend_news) { ?>
                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $trend_news[0]['slug']; ?>">
                            <div class="tnewsblock">
                                <img src="<?php echo $trend_news[0]['large']; ?>" />

                                <div class="tnbfooter">
                                    <div class="heading"><?php echo $trend_news[0]['title']; ?></div>
                                    <div class="uname">by <?php echo $trend_news[0]['user']['name']; ?></div>
                                    <div class="uimage"><img src="<?php echo $trend_news[0]['user']['profile_pic']; ?>"></div>
                                </div>
                            </div>
                        </a>
                        <?php } ?>
                    </div>
                    <div class="col-md-4">
                        <div class="promoted">
                            <ul>
                                <li>
                                    <a>More News...</a>

                                </li>
                                <?php
                                if ($trend_news and count($trend_news) > 1) {
                                    foreach ($trend_news as $key => $news) {
                                        if ($key == 0)
                                            continue;
                                        ?> <li>
                                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>"><?php echo $news['title']; ?></a>
                                        </li> <?php
                                    }
                                    ?>

                                    <?php } ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                    <?php
                    $gc = trim(getCMSContent("ad_ggl_square"));
                    $ads = getSiteAds(5, $club['pk_club_id'], "square");
                    $cnt = count($ads);
                    if ($cnt > 0) {
                        $num = array_rand($ads);
                        ?>
                        <div class="leftadd">
                            <a href="<?php echo $ads[$num]['link'] ?>" target="_blank">
                                <img src="<?php echo $ads[$num]['image']; ?>" />
                            </a>
                        </div>
                        <?php
                    } else {
                        echo $gc;
                    }
                    ?>
                </div>
            </div>
            <div class="row strip">
                <div class="col-md-12">

                    <?php
                    $gc = trim(getCMSContent("ad_ggl_square"));
                    $ads = getSiteAds(5, $club['pk_club_id'], "large");
                    $cnt = count($ads);
                    if ($cnt > 0) {
                        $num = array_rand($ads);
                        ?>
                        <div class="leftadd">
                            <a href="<?php echo $ads[$num]['link'] ?>" target="blank">
                                <img src="<?php echo $ads[$num]['image']; ?>" />
                            </a>
                        </div>
                        <?php
                    } else {
                        echo $gc;
                    }
                    ?>
                </div>
            </div>

        </section>
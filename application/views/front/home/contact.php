<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>");}
    </style>
    <?php
}
?> 

<section class="articlecontent">
    <div class="row strip">
        <div class="col-md-12">
            <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                Contact
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row strip">
                <div class="col-md-12">
                    <?php echo getCMSContent("contact"); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row strip">
                <div class="col-md-12">
                    <div class="alert alert alert-success profile_success" style="display: none;">Your message has been send. We will contact you soon.</div>
                    <form id="profileForm" action="" name="profileForm">

                        <div class="form-group">
                            <label class="form-control-label">Name *</label>
                            <input type="text" class="form-control required" id="email" name="name" placeholder="Name" value="<?php echo html_escape($this->session->userdata('name')) ?>">
                        </div>

                        <div class="form-group">
                            <label class="form-control-label">Email *</label>
                            <input type="text" class="form-control required" id="emails" name="email" placeholder="Email"  value="<?php echo html_escape($this->session->userdata('email')) ?>" />
                        </div>
                        <div class="form-group">
                            <label  class="form-control-label">Phone</label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone"  value="">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Subject *</label>
                            <input type="text" class="form-control required" id="subject" name="subject" placeholder="Subject"  value="">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="form-control-label">Message</label>
                            <textarea class="form-control" id="message" name="message" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-success send_contact pull-right">Send Message</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(".send_contact").on('click', function() {
        if ($("#profileForm").valid()) {
            App.blockUI($("#profileForm"));
            var post_data = $("#profileForm").serialize();
            $.post(base_url + "home/contactsend", post_data, function(res) {
                App.unblockUI($("#profileForm"));
                $(".profile_success").fadeIn();
                $("#profileForm")[0].reset();
            });
        } else {

        }
    });
</script>
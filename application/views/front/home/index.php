<?php
$base = base_url() . PUBLIC_DIR . "update_assets/";
$main_news = $hp_news[0];
$trend_news = getHomePageNews('N', 0, 0, 0, 0, 9, false);

$club_data = getLatestClubNews();

/*print_r($main_news);
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
*/
/*print_r($trend_news);
echo "<br>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
exit();*/

$sp_news = getHomePageNews('N', 1, 0, 0, 0, 9, false);
?>
<div id="main-content">

    <div class="wrapper mb-30">

        <div class="widget-area-1">

            <div class="widget kopa-ads-widget style1">
                <?php
                $gc = trim(getCMSContent("ad_ggl_large"));
                $ads = getSiteAds(1, "0", "large");
                $cnt = count($ads);
                if ($cnt > 0) {
                    $num = array_rand($ads);
                    ?>
                    <a href="<?php echo $ads[$num]['link'] ?>" target="blank">
                        <img src="<?php echo $ads[$num]['image']; ?>" style="width: 920px; max-width: 920px; max-height: 115px;" />
                    </a>
                    <?php
                } else {
                    echo $gc;
                }
                ?>
            </div>
            <!-- widget --> 

            <?php
            $lg_id = $this->session->userdata("site_league");
            if ($lg_id == "" or $lg_id == "0") {
                $lg_id = "1";
            }
            ?>

            <div class="widget kopa-tab-score-widget style1">
                <div class="kopa-tab style1">
                    <ul class="nav nav-tabs">
                        <?php 
                        $i = 0;
                        foreach ($club_list['clubs'] as $key => $dt) {
                            $i++;
                            ?>
                            <li <?php if($i == 1) echo 'class="active"'; ?> ><a href="#<?php echo $dt['id']; ?>"><?php echo $dt['name']; ?></a></li>
                            <?php }
                            ?>   
                        </ul>
                        <!-- nav-tabs -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="nfl">
                                <div class="owl-carousel owl-carousel-1">

                                    <div class="item">
                                        <div class="entry-item">
                                            <a href="#">
                                                <p><?php echo $data_league[$lg_id]['name'] ?></p>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- item -->
                                    <?php foreach ($data_league as $key => $dt):
                                    ?>
                                    <div class="item">
                                        <div class="entry-item">
                                            <a href="#" data-keyid='<?php echo $key ?>' data-id='<?php echo $dt['id'] ?>' data-image='<?php echo $dt['image'] ?>'>
                                                <p><?php echo $dt['name'] ?></p>
                                            </a>
                                        </div>
                                    </div>
                                    <?php 
                                    endforeach;
                                    ?>
                                </div>
                                <!-- owl-carousel-1 -->   
                            </div>
                        </div>
                    </div>
                    <!-- kopa-tab -->

                </div>
                <!-- widget --> 

                <div class="widget kopa-scroll-slider-widget">
                    <h3 class="widget-title"><span><b>Top Stories</b></span></h3>
                    <span class="thumb-left"></span>
                    <span class="thumb-right"></span>
                    <div class="scroll-slider">
                        <a href="#" class="s-prev fa fa-chevron-left"></a>
                        <a href="#" class="s-next fa fa-chevron-right"></a>
                        <div class="loading">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <ul class="clearfix">
                            <?php foreach ($hp_news as $k => $main_news): ?>
                            <li class="s-item">
                                <div class="item-wrap">
                                    <article class="entry-item image-post">
                                        <div class="item-bg"></div>
                                        <h4 class="entry-title">
                                            <span class="fa"></span>
                                            <a href="<?php echo site_url() . NEWS_TAG . "/"; ?><?php echo $main_news['slug'] ?>">
                                                <?php echo substr($main_news['title'], 0, 15).'...'; ?>
                                            </a>
                                        </h4>
                                        <div class="entry-thumb">
                                            <a href="<?php echo site_url() . NEWS_TAG . "/"; ?><?php echo $main_news['slug'] ?>">
                                                <img src="<?php echo $main_news['image']; ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="entry-date style1">
                                            <span class="s-point left"></span>
                                            <span class="s-point right"></span>
                                            <p class="entry-day"><?php echo date('d', strtotime($main_news['created_at'])); ?></p> 
                                            <p class="entry-month"><?php echo date('M', strtotime($main_news['created_at'])); ?></p>
                                        </div>
                                    </article>
                                </div>
                            </li>
                        <?php endforeach; ?>

                    </ul>
                </div>
                <!-- scroll-slider --> 
            </div>
            <!-- widget --> 

        </div>
        <!-- widget-area-1 -->

    </div>
    <!-- wrapper -->

    <div class="wrapper">

        <div class="main-top">
            <div class="kopa-ticker">
                <span class="ticker-title"><i class="fa fa-angle-double-right"></i>Breaking news</span>
                <div class="ticker-wrap">                        
                    <dl class="ticker-1">
                        <dt></dt>
                        <dd><a href="#">Quarterback Tony Romo returns. Lewis Hamilton on the legacy of Ayrton Senna</a></dd>
                        <dd><a href="#">Story of the FA Cup first round - as seen by you</a></dd>
                        <dd><a href="#">Lewis Hamilton on the legacy of Ayrton Senna in Brazil</a></dd>
                    </dl>
                    <!--ticker-1-->
                </div>
            </div>
            <!-- kopa-ticker -->

            <div class="social-links style1">
                <ul class="clearfix">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-google-plus"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                </ul>
            </div>

        </div>
        <!-- main-top -->
        <div class="widget kopa-ads-widget style1">
            <?php
            $gc = trim(getCMSContent("ad_ggl_large"));

            $ads = getAds("small");
            $cnt = count($ads);
            if ($cnt > 0) {
                $num = array_rand($ads);
                ?>
                <a target="_blank" href="<?php echo $ads[$num]['link'] ?>">
                    <img src="<?php echo $ads[$num]['image']; ?>" />
                </a>
                <?php
            }

            ?>
        </div>

        <div class="row">

            <div class="kopa-main-col">

                <div class="widget-area-2">

                    <div class="widget kopa-article-list-widget article-list-1">
                        <h3 class="widget-title style12">the Latest news<span class="ttg"></span></h3>
                        <ul class="clearfix">
                            <?php
                            if ($trend_news and count($trend_news) > 1) {
                                foreach ($trend_news as $key => $news) {
                                    if ($key == 0)
                                        continue;
                                    ?> 
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-thumb">
                                                <a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>">
                                                    <img src="<?php echo $news['thumb']; ?>" alt="">
                                                </a>
                                            </div>
                                            <div class="entry-content">
                                                <div class="content-top">
                                                    <h4 class="entry-title">
                                                        <a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>">
                                                            <?php echo substr($news['title'], 0, 80).'...'; ?>
                                                        </a>
                                                    </h4>
                                                    <p class="entry-comment"><a href="#">52</a></p>
                                                </div>
                                                <p>
                                                    <?php echo substr($news['content'], 0, 180).'...'; ?>
                                                </p> 
                                                <footer>
                                                    <p class="entry-author">by <a href="#"><?php echo $news['user']['firstname'].' '.$news['user']['lastname'] ?></a></p>
                                                </footer>
                                            </div>
                                            <div class="post-share-link style-bg-color">
                                                <span><i class="fa fa-share-alt"></i></span>
                                                <ul>
                                                    <li><a href="#" class="fa fa-facebook"></a></li>
                                                    <li><a href="#" class="fa fa-twitter"></a></li>
                                                    <li><a href="#" class="fa fa-google-plus"></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </li>
                                    <?php
                                }
                                ?>

                                <?php } ?>
                            </ul>
                        </div>
                        <!-- widget --> 

                    </div>
                    <!-- widget-area-2 -->

                </div>
                <!-- main-col -->

                <div class="sidebar widget-area-11">

                    <div class="widget widget_search style1">
                        <h3 class="widget-title style3"><span class="fa fa-search"></span>search</h3>
                        <div class="search-box">
                            <form action="#" class="search-form clearfix" method="get">
                                <input type="text" onblur="if (this.value == '') this.value = this.defaultValue;" onfocus="if (this.value == this.defaultValue) this.value = '';" value="Search..." name="s" class="search-text">
                                <button type="submit" class="search-submit">
                                    <span class="fa fa-search"></span>
                                </button>
                            </form>
                            <!-- search-form -->
                        </div>
                    </div>
                    <!-- widget -->

                    <div class="widget kopa-charts-widget style1">
                        <h3 class="widget-title style14">
                            <span>Top Writers</span>
                            <a href="<?php echo site_url() . "articles/topwriters/" . $league_id . "/0"; ?>">more <span class="fa fa-chevron-right"></span></a>
                        </h3>
                        <div class="widget-content">
                            <header>
                                <div class="t-col">Pos</div>
                                <div class="t-col width1">Writer</div>
                                <div class="t-col">Profile</div>
                                <div class="t-col">pts</div>
                            </header>
                            <ul class="clearfix">
                                <?php
                                if ($hp_writers) {
                                    foreach ($hp_writers as $pos => $row) {
                                        ?> 
                                        <li>
                                            <div class="t-col"><?php echo $pos + 1; ?> <i class="fa fa-sort-up"></i></div>
                                            <div class="t-col width1">
                                                <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">
                                                    <?php 
                                                    $writerName = "";
                                                    if (str_word_count($row['name']) > 1) {
                                                        $name = explode(' ', $row['name']);
                                                        $writerName = $name[0];
                                                    }else{
                                                        $writerName = $row['name'];
                                                    }

                                                    echo $writerName; 
                                                    ?>
                                                </a>
                                            </div>
                                            <div class="t-col"><img src="<?php echo $row['thumb']; ?>" style="height: 20px;" class="img-circle"/></div>
                                            <div class="t-col"><?php echo $row['points']; ?></div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>    

                            </ul>
                            <a class="kopa-view-all" href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">View all<span class="fa fa-chevron-right"></span></a>
                        </div>
                    </div>
                    <!-- widget --> 

                    <div class="widget kopa-tab-1-widget">
                        <div class="kopa-tab style6">
                            <?php
                            echo getCMSContent("ad_ggl_square");
                            $ads = getAds("square");
                            $cnt = count($ads);
                            if ($cnt > 0) {
                                $num = array_rand($ads);
                                ?>
                                <a href="<?php echo $ads[$num]['link'] ?>">
                                    <img src="<?php echo $ads[$num]['image']; ?>" />
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                        <!-- kopa-tab -->

                    </div>
                    <!-- widget --> 

                    <div class="widget kopa-article-list-widget article-list-5">
                        <h3 class="widget-title style14">
                            <span>planet futbol</span>
                            <a href="#">more <span class="fa fa-chevron-right"></span></a>
                        </h3>
                        <ul class="clearfix">
                            <li>
                                <article class="entry-item video-post">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="http://placehold.it/360x210" alt=""></a>
                                        <a class="thumb-icon style1" href="#"></a>
                                    </div>
                                    <div class="entry-content">
                                        <div class="content-top">
                                            <h4 class="entry-title"><a href="#">USMNT friendly roster about Dollovan, but also</a></h4>
                                            <p class="entry-comment"><a href="#">52</a></p>
                                        </div> 
                                        <footer>
                                            <p class="entry-author">by <a href="#">Michel bellar</a></p>
                                        </footer>
                                    </div>
                                    <div class="post-share-link style-bg-color">
                                        <span><i class="fa fa-share-alt"></i></span>
                                        <ul>
                                            <li><a href="#" class="fa fa-facebook"></a></li>
                                            <li><a href="#" class="fa fa-twitter"></a></li>
                                            <li><a href="#" class="fa fa-google-plus"></a></li>
                                        </ul>
                                    </div>
                                </article>
                            </li>
                        </ul>
                    </div>
                    <!-- widget --> 
                </div>
                <!-- sidebar -->

            </div>
            <!-- row -->

            <div class="row">

                <div class="widget-area-4 col-md-8 col-sm-8 col-xs-8">

                    <div class="widget kopa-article-list-widget article-list-3">
                        <div class="widget kopa-article-list-widget article-list-4">
                            <h3 class="widget-title style11">Sponsored News<span class="ttg"></span></h3>
                            <ul class="clearfix">
                                <?php if ($sp_news) { ?>
                                <li>
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <a href="<?php echo site_url() . NEWS_TAG . "/" . $sp_news[0]['slug']; ?>">
                                                <img src="<?php echo $sp_news[0]['large']; ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="entry-content">
                                            <div class="content-top">
                                                <h4 class="entry-title">
                                                    <a href="<?php echo site_url() . NEWS_TAG . "/" . $sp_news[0]['slug']; ?>">
                                                        <?php echo $sp_news[0]['title']; ?>
                                                    </a>
                                                </h4>
                                                <p class="entry-comment"><a href="#">52</a></p>
                                            </div>
                                            <div class="kopa-rating">
                                                <ul>
                                                    <li><span class="fa fa-star"></span></li>
                                                    <li><span class="fa fa-star"></span></li>
                                                    <li><span class="fa fa-star"></span></li>
                                                    <li><span class="fa fa-star"></span></li>
                                                    <li class="inactive"><span class="fa fa-star"></span></li>
                                                </ul>
                                            </div>
                                            <p>
                                                <?php echo substr($sp_news[0]['content'], 0, 165).'...'; ?> 
                                            </p> 
                                            <footer>
                                                <p class="entry-author">by <a href="#"><?php echo $sp_news[0]['user']['name']; ?></a></p>
                                            </footer>
                                        </div>
                                        <div class="post-share-link style-bg-color">
                                            <span><i class="fa fa-share-alt"></i></span>
                                            <ul>
                                                <li><a href="#" class="fa fa-facebook"></a></li>
                                                <li><a href="#" class="fa fa-twitter"></a></li>
                                                <li><a href="#" class="fa fa-google-plus"></a></li>
                                            </ul>
                                        </div>
                                    </article>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <ul class="clearfix">
                            <?php
                            if ($sp_news and count($sp_news) > 1) {
                                foreach ($sp_news as $key => $news) {
                                    if ($key == 0)
                                        continue;
                                    ?> 
                                    <li>
                                        <article class="entry-item">
                                            <span class="kopa-list-icon"></span>
                                            <h4 class="entry-title"><a href="<?php echo site_url() . NEWS_TAG . "/" . $news['slug']; ?>"><?php echo $news['title']; ?></a></h4>
                                        </article>
                                    </li>
                                    <?php
                                }
                                ?>

                                <?php } ?>
                            </ul>
                        </div>
                        <!-- widget --> 

                    </div>
                    <!-- widget-area-4 -->
                    <div class="widget-area-3 col-md-4 col-sm-4 col-xs-4">
                        <div class="widget kopa-charts-widget style1">
                            <h3 class="widget-title style14">
                                <span>TABLE</span>
                                <a href="<?php echo site_url() . "articles/topwriters/" . $league_id . "/0"; ?>">more <span class="fa fa-chevron-right"></span></a>
                            </h3>
                            <div class="widget-content">
                                <header>
                                    <div class="t-col">#</div>
                                    <div class="t-col width1" style="width: 70%;">Team</div>
                                    <div class="t-col">pts</div>
                                </header>
                                <ul class="clearfix">
                                   <?php
                                   $tot_teams = count($team_points);
                                   foreach ($team_points as $key => $row) {
                                    $cls = "";
                                    if ($key == 0) {
                                        $cls = "yellow";
                                    }
                                    if ($key >= $tot_teams - 3) {
                                        $cls = "red";
                                    }
                                    ?>
                                    <li>
                                        <div class="t-col"><?php echo $key + 1; ?> <i class="fa fa-sort-up"></i></div>
                                        <div class="t-col width1 <?php echo $cls; ?>" style="width: 70%;">
                                            <a href="<?php echo site_url() . "club/" . $row['slug']; ?>">
                                                <?php echo $row['title']; ?>
                                            </a>
                                        </div>
                                        <div class="t-col"><?php echo intval($row['points']); ?></div>
                                    </li>
                                    <?php }
                                    ?>    

                                </ul>
                                <a class="kopa-view-all" href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">View all<span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>
                        <!-- widget --> 

                    </div>
                    <!-- widget-area-3 -->
                </div>
                <!-- row --> 
                <div class="widget-area-24">

                    <div class="widget kopa-product-list-widget">
                        <h3 class="widget-title style10">Club News</h3>
                        <div class="content-wrap">

                          <div class="row">

                           <div class="owl-carousel owl-carousel-4">
                            <?php
                            if ($club_data) {
                                foreach ($club_data as $row) {
                                    ?>
                                    <div class="item">
                                     <div class="widget kopa-article-list-widget article-list-1">
                                        <h3 class="widget-title style12"><?php echo $row['club_name']; ?><span class="ttg"></span></h3>

                                        <ul class="clearfix">
                                            <?php
                                            if ($row['club_news']) {
                                                foreach ($row['club_news'] as $club) {
                                                    ?>
                                                    <li>
                                                        <article class="entry-item">
                                                            <div class="entry-thumb">
                                                                <a href="<?php echo site_url() . NEWS_TAG . "/" ?><?php echo $club['slug'] ?>">
                                                                    <img src="<?php echo $club['thumb']; ?>" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="entry-content">
                                                                <div class="content-top">
                                                                    <h4 class="entry-title">
                                                                        <a href="<?php echo site_url() . NEWS_TAG . "/" ?><?php echo $club['slug'] ?>">
                                                                            <?php echo $club['title']; ?>
                                                                        </a>
                                                                    </h4>
                                                                    <p class="entry-comment"><a href="#">52</a></p>
                                                                </div>
                                                                <p>
                                                                    <?php echo substr($club['title'], 0, 50).'...'; ?>
                                                                </p> 
                                                            </div>
                                                            <div class="post-share-link style-bg-color">
                                                                <span><i class="fa fa-share-alt"></i></span>
                                                                <ul>
                                                                    <li><a href="#" class="fa fa-facebook"></a></li>
                                                                    <li><a href="#" class="fa fa-twitter"></a></li>
                                                                    <li><a href="#" class="fa fa-google-plus"></a></li>
                                                                </ul>
                                                            </div>
                                                        </article>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <!-- widget --> 
                                </div>
                                <!-- item -->
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <!-- owl-carousel-4 -->

                </div>
                <!-- row --> 
            </div>
        </div>
        <!-- widget --> 

        <div class="mb-30"></div>

        <div class="widget kopa-brand-widget">
            <h3 class="widget-title style14">
                <span>Top Fanz</span>
                <a href="<?php echo site_url() . "articles/topfanz"; ?>">View All <span class="fa fa-chevron-right"></span></a>
            </h3>
            <div class="owl-carousel owl-carousel-5">
                <?php
                if ($hp_fanz) {
                    foreach ($hp_fanz as $pos => $row) {
                        ?> 
                        <div class="item">
                            <a href="<?php echo site_url() . "newsfeed/" . $row['slug']; ?>"><img src="<?php echo $row['thumb']; ?>" alt=""></a>
                            <p><?php echo $row['name']; ?></p>
                        </div>
                        <?php
                    }
                }
                ?>   
            </div>
            <!-- owl-carousel-5 -->
        </div>
        <!-- widget --> 

    </div>
    <!-- widget-area-24 -->

</div>
<!-- wrapper -->

</div>
<!-- main-content -->

<div id="bottom-sidebar">

    <div class="bottom-area-1">

        <div class="wrapper">

            <div class="kopa-logo">
                <a href="#"><img src="images/logo.png" alt="logo"></a>
            </div>
            <!-- logo -->

            <nav class="bottom-nav">
                <ul class="bottom-menu">
                    <li><a href="#">Home</a></li>
                    <li>
                        <a href="categories5.html">nfl</a>
                        <ul class="sub-menu">
                            <li><a href="categories4.html">nba</a></li>
                            <li><a href="categories5.html">nhl</a></li>
                        </ul>   
                    </li>
                    <li><a href="categories1.html">mmqb</a></li>
                    <li><a href="categories2.html">mlb</a></li>
                    <li><a href="categories3.html">ncaaf</a></li>
                    <li><a href="categories4.html">nba</a></li>
                    <li><a href="categories5.html">nhl</a></li>
                    <li><a href="categories6.html">golf</a></li>
                    <li><a href="categories1.html">planet futbol</a></li>
                </ul>                
            </nav>
            <!--/end bottom-nav-->               

            <nav class="bottom-nav-mobile clearfix">
                <a class="pull fa fa-bars"></a>
                <ul class="main-menu-mobile bottom-menu-mobile">
                    <li><a href="#">Home</a></li>
                    <li>
                        <a href="categories5.html">nfl</a>
                        <ul class="sub-menu">
                            <li><a href="categories4.html">nba</a></li>
                            <li><a href="categories5.html">nhl</a></li>
                        </ul>   
                    </li>
                    <li><a href="categories1.html">mmqb</a></li>
                    <li><a href="categories2.html">mlb</a></li>
                    <li><a href="categories3.html">ncaaf</a></li>
                    <li><a href="categories4.html">nba</a></li>
                    <li><a href="categories5.html">nhl</a></li>
                    <li><a href="categories6.html">golf</a></li>
                    <li><a href="categories1.html">planet futbol</a></li>
                </ul>  
            </nav>
            <!--/main-menu-mobile-->

        </div>
        <!-- wrapper -->
    </div>
    <!-- bottom-area-2 -->

    <div class="bottom-area-2">

        <div class="wrapper">

            <div class="row">

                <div class="widget-area-18">

                    <div class="widget widget_recent_entries">
                        <h3 class="widget-title">sports</h3>
                        <ul class="clearfix">
                            <li>
                                <a href="#">FIFA</a>
                            </li>
                            <li>
                                <a href="#">Madden NFL</a>
                            </li>
                            <li>
                                <a href="#">PGA TOUR</a>
                            </li>
                            <li>
                                <a href="#">NHL®</a>
                            </li>
                            <li>
                                <a href="#">NBA Live</a>
                            </li>
                            <li>
                                <a href="#">EA SPORTS UFC</a>
                            </li>
                            <li>
                                <a href="#">2014 FIFA World Cup</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- widget-area-18 -->

                <div class="widget-area-19">

                    <div class="widget widget_recent_entries">
                        <h3 class="widget-title">our team</h3>
                        <ul class="clearfix">
                            <li>
                                <a href="#">BC Lions</a>
                            </li>
                            <li>
                                <a href="#">Edmonton Eskimos</a>
                            </li>
                            <li>
                                <a href="#">Calgary Stampeders</a>
                            </li>
                            <li>
                                <a href="#">Hamilton Tiger</a>
                            </li>
                            <li>
                                <a href="#">Winnipeg Blue Bombers</a>
                            </li>
                            <li>
                                <a href="#">Hamilton Tiger-Cats</a>
                            </li>
                            <li>
                                <a href="#">Toronto Argonauts</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- widget-area-19 -->

                <div class="widget-area-20">

                    <div class="widget widget_recent_entries">
                        <h3 class="widget-title">QUICK SHOP</h3>
                        <ul class="clearfix">
                            <li>
                                <a href="#">CrossFit</a>
                            </li>
                            <li>
                                <a href="#">Reebok One Series</a>
                            </li>
                            <li>
                                <a href="#">Workout</a>
                            </li>
                            <li>
                                <a href="#">Spartan Race</a>
                            </li>
                            <li>
                                <a href="#">Les Mills</a>
                            </li>
                            <li>
                                <a href="#">NPC</a>
                            </li>
                            <li>
                                <a href="#">The Pump</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- widget-area-20 -->

                <div class="widget-area-21">

                    <div class="widget widget_recent_entries">
                        <h3 class="widget-title">Our League</h3>
                        <ul class="clearfix">
                            <li>
                                <a href="#">Tickets</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">FAQs</a>
                            </li>
                            <li>
                                <a href="#">Employment</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="#">Terms of Use</a>
                            </li>
                            <li>
                                <a href="#">Grey Cup Central</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- widget-area-21 -->

                <div class="widget-area-22">

                    <div class="widget widget_text">
                        <h3 class="widget-title">map</h3>
                        <div class="kopa-map-1-wrapper">
                            <div id="kopa-map-1" class="kopa-map-1" data-place="Ha Noi" data-latitude="21.029532" data-longitude="105.852345"></div>
                        </div>
                    </div>
                    <!-- widget --> 

                </div>
                <!-- widget-area-22 -->

            </div>
            <!-- row --> 

        </div>
        <!-- wrapper -->

    </div>
    <!-- bottom-area-2 -->

    <div class="bottom-area-3">

        <div class="wrapper">

            <div class="widget kopa-newsletter-widget">
                <div class="newsletter-intro">
                    <span class="news-icon fa fa-envelope"></span>
                    <p>Subscribe to our email newsletter</p>
                </div>
                <div class="newsletter-content">
                    <span class="input-icon fa fa-envelope"></span>
                    <form class="newsletter-form clearfix" method="post" action="#">
                        <div class="input-area">
                            <input type="text" onBlur="if (this.value == '')
                            this.value = this.defaultValue;" onFocus="if (this.value == this.defaultValue)
                            this.value = '';" value="Enter Your Email Address..." size="40" class="name"  name="name">
                        </div>
                        <button type="submit" class="search-submit">
                            <span>Sign me up</span>
                            <span class="fa fa-chevron-right"></span>
                        </button>                 
                    </form>
                    <div id="newsletter-response"></div>
                </div>
            </div>
            <!-- widget -->

        </div>
        <!-- wrapper -->

    </div>
    <!-- bottom-area-3 -->

</div>
<!-- bottom-sidebar -->

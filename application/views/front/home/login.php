<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>
<div class="container" style="margin: 0 auto;">


    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 home-signup">

            <div class="row text-center">
             <div class="col-md-6 col-md-offset-4">
                <img src="<?php echo $base; ?>images/logo_signup.png" class="img-responsive" />
            </div>
        </div>  

        <form action="#" method="post" class="form-horizontal" id="loginform">



            <div class="row">

                <div class="alert alert-danger invalidfields" style="display: none;">
                    Invalid Username or Password
                </div>
                <div class="validfields"></div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="Your Email or Username" name="email">
                </div>
            </div>


            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-unlock-alt "></i></div>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter you password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <p style="color: #ffffff; font-size: 15px;">New here?</p>
                    <p><a href="<?php echo site_url(); ?>" class="btn btn-success">Get a new account here</a></p>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <button class="btn btn-primary-home pull-right actionLogin" type="submit"><i class="fa fa-sign-in"></i> Connect</button>
                </div>
            </div>
            <div class="form-group">
              <div class="loginfield" >
                <div class="loglabel showwait" style="display:none;">Please wait...</div>
            </div>
        </div> 
        <br>
        <hr>
        <footer>
            <br>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                    <img src="<?php echo $base; ?>images/1.png">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                    <img src="<?php echo $base; ?>images/2.png">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                    <img src="<?php echo $base; ?>images/3.png">
                </div>
            </div>
        </footer> 
    </form>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
    <!-- <footer>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                <img src="<?php echo $base; ?>images/1.png">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                <img src="<?php echo $base; ?>images/2.png">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                <img src="<?php echo $base; ?>images/3.png">
            </div>
        </div>
    </footer> -->
</div>
</div>
</div>


<script type="text/javascript">
$(document).ready(function(e) {
    $(".actionLogin").on('click', function(e) {
        e.preventDefault();
        //$("#loginform .invalidfields").hide();
        var btn = $(this);
        $(this).html('<i class="fa fa-spin fa-spinner"></i> Connecting...')
        var post_data = $("#loginform").serialize();
        $.post(site_url + "user/defaultlogin", post_data, function(res) {

            if (res.status == "1") {
                <?php if ($nextpage == "index") { ?>
                    window.location.href = site_url;
                    <?php } else { ?>
                        window.location.href = res.url;
                        <?php } ?>

                    } else {
                        btn.html('<i class="fa fa-sign-in"></i> Connect');
                        $("#loginform .invalidfields").html(res.msg).show();
                    }
                }, "JSON");
    });
    $("#loginform #password").on('keypress', function(e) {
        if (e.keyCode == 13)
        {
            $(".actionLogin").click();
            e.preventDetault();
            return false;
        }
    });


    $(".actionReset").on('click', function() {
        $("#resetform .invalidfields").hide();
        $("#resetform .validfields").hide();
        var post_data = $("#resetform").serialize();
        $.post(site_url + "user/forgotpassword", post_data, function(res) {
            if (res.status == "1") {
                $("#resetemail").val('');
                $("#resetform .validfields").html(res.msg).show();
            } else {
                $("#resetform .invalidfields").html(res.msg).show();
            }
        }, "JSON");
    });
    $("#resetform").on("submit", function(e) {
        e.preventDetault();
        return false;
    });
    $("#resetemail").on('keypress', function(e) {
        if (e.keyCode == 13)
        {
            e.preventDefault();
            $("#resetform .actionReset").click();
            return false;
        }
    });


});
</script>
<script type="text/javascript">var newWin;
$(document).ready(function(e) {
    $(".btnfacebook").on('click', function() {
        var url = site_url + 'facebook/connect';
        newWin = popupwindow(url, "Facebook Login", 800, 500);
        if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
            window.location = url;
        }
    });

});
function afterLogin($status, $login_id) {
    newWin.close();
    if ($status == "1") {
        <?php if ($nextpage == "index") { ?>
            window.location.href = site_url;
            <?php } else { ?>
                window.location.href = site_url + "dashboard";
                <?php } ?>
            } else {
                window.location.href = site_url + "login";
            }

        }</script>

<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>
<div class="container" style="margin: 0 auto;">


    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 home-signup">

            <div class="row text-center">
             <div class="col-md-6 col-md-offset-4">
                <img src="<?php echo $base; ?>images/logo_signup.png" class="img-responsive" />
            </div>
        </div>  

        <form action="#" method="post" class="form-horizontal">



            <div class="row">

                <div class="alert alert-danger invalidfields" style="display: none;">
                    Email already registered
                </div>
                <div class="validfields"></div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="Your Name" name="name">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user-secret"></i></div>
                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="Pick a Username" name="username">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="Your email" name="email">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-unlock-alt "></i></div>
                    <input type="password" class="form-control" id="exampleInputAmount" placeholder="Create a new password" name="password">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-trophy"></i></div>
                    <select name="" class="form-control" name="club">
                        <option value="">Select your Favourite club</option>
                        <?php foreach ($clubs as $id => $club) { ?>
                        <option value="<?php echo $id; ?>"><?php echo $club ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-7 col-sm-7 col-xs-12">
                    <p style="color: #ffffff; font-size: 15px;">Already have an account?</p>
                    <p><a href="<?php echo site_url() . "home/login/index" ?>" class="btn btn-success">Login here</a></p>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <button class="btn btn-primary-home pull-right actionRegister" type="submit"><i class="fa fa-user-plus"></i> Create my account</button>
                </div>
            </div>
            <div class="form-group">
              <div class="loginfield" >
                <div class="loglabel showwait" style="display:none;">Please wait...</div>
            </div>
        </div>  
    </form>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
    <!-- <footer>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                <img src="<?php echo $base; ?>images/1.png">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                <img src="<?php echo $base; ?>images/2.png">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                <img src="<?php echo $base; ?>images/3.png">
            </div>
        </div>
    </footer> -->
</div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(e) {
    $(".actionRegister").on('click', function(e) {
        e.preventDefault();
        //$("#signupform .invalidfields").hide();
        $("#signupform .validfields").hide();
        $(".actionRegister").html('<i class="fa fa-spin fa-spinner"></i> Connecting...');
        var post_data = $("#signupform").serialize();
        $.post(site_url + "user/registration", post_data, function(res) {
            $("#signupform .showwait").hide();
            $("#signupform .actionRegister").show();
            if (res.status == "1") {
                $("#signupform .validfields").html(res.msg).fadeIn('slow');
                $("#signupform")[0].reset();
            } else {

                $(".invalidfields").html(res.msg).fadeIn('slow', function(){
                    $(".actionRegister").html('<i class="fa fa-user-plus"></i> Create my account');
                });
            }
        }, "JSON");
    });
    $("#signupform #password").on('keyup', function(e) {
        if (e.keyCode == 13)
        {
            $(".actionRegister").click();
        }
    });


});

</script>


<script type="text/javascript">var newWin;
$(document).ready(function(e) {
    $(".btnfacebook").on('click', function() {
        $(".close-btn").click();
        var url = site_url + 'facebook/connect';
        newWin = popupwindow(url, "Facebook Login", 800, 500);
        if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
            window.location = url;
        }
    });
});
function afterLogin($status, $login_id) {
    newWin.close();
    window.location.href = site_url + "dashboard";
}</script>
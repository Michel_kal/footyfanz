<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
?>
<div class="container" style="margin-top: 30px;">


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="loginbox">
                <h2>Welcome to FootyFanz</h2>
                <h3>By Fanz. For Fanz.</h3>
                <form id="signupform">

                    <div class="invalidfields" style="display: block;">Verification link is invalid.</div>

                </form>
                <div class="loglabel">Log In Via</div>

                <div class=" socialloginbuttons">
                    <a href="#" class="btnfacebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="btntwitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="footerlogo">
                    <img src="<?php echo $base; ?>images/logo.png" />
                </div>
                <br />

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(e) {
        $(".actionRegister").on('click', function() {
            $("#signupform .invalidfields").hide();
            $("#signupform .validfields").hide();
            var post_data = $("#signupform").serialize();
            $.post(site_url + "user/registration", post_data, function(res) {
                if (res.status == "1") {
                    $("#signupform .validfields").html(res.msg).fadeIn();
                    $("#signupform")[0].reset();
                } else {
                    $("#signupform .invalidfields").html(res.msg).fadeIn();
                }
            }, "JSON");
        });
        $("#signupform #password").on('keyup', function(e) {
            if (e.keyCode == 13)
            {
                $(".actionRegister").click();
            }
        });


    });
</script>



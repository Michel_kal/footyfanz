<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
//Type, Sponser, Featured, Club_id, User_id,Total Record,Extra->True or false
$tr_news = getAticles('N', 0, 0, 0, 0, 5);
$sp_news = getAticles('N', 1, 0, 0, 0, 5);
$ar_latest = getAticles('A', 0, 0, 0, 0, 5);
$ar_user = getAticles('A', 0, 0, 0, $this->session->userdata("user_id"), 5);
?>           

<div class="col-md-3">
    <div class="newspanel">
        <div class="clearfix"></div>
        <div class="panelheader">
            Trending News
        </div>
        <div class="panelcontent">

            <?php
            if ($tr_news) {
                foreach ($tr_news as $row) {
                    ?>  <a href="<?php echo site_url() . NEWS_TAG . "/" ?><?php echo $row['slug'] ?>">
                        <div class="newshead">
                            <div class="leftsection">
                                <img src='<?php echo $row['thumb']; ?>' />
                            </div>
                            <div class="rightsection"><?php echo wordLimiter($row['title'], 50); ?></div>
                        </div>
                    </a>
                    <?php
                }
            }
            ?>

        </div>
    </div>
</div>


<div class="col-md-3">
    <div class="newspanel">
        <div class="clearfix"></div>
        <div class="panelheader">
            Sponsored News
        </div>
        <div class="panelcontent">
            <?php
            if ($sp_news) {
                foreach ($sp_news as $row) {
                    ?>  <a href="<?php echo site_url() . NEWS_TAG . "/" ?><?php echo $row['slug'] ?>">
                        <div class="newshead">
                            <div class="leftsection">
                                <img src='<?php echo $row['thumb']; ?>' />
                            </div>
                            <div class="rightsection"><?php echo wordLimiter($row['title'], 50); ?></div>
                        </div>
                    </a>
                    <?php
                }
            }
            ?>

        </div>
    </div>
</div>


<div class="col-md-3">
    <div class="newspanel">
        <div class="clearfix"></div>
        <div class="panelheader">
            Editorial
        </div>
        <div class="panelcontent">
            <?php
            if ($ar_latest) {
                foreach ($ar_latest as $row) {
                    ?>  <a href="<?php echo site_url() . NEWS_TAG . "/" ?><?php echo $row['slug'] ?>">
                        <div class="newshead">
                            <div class="leftsection">
                                <img src='<?php echo $row['thumb']; ?>' />
                            </div>
                            <div class="rightsection"><?php echo wordLimiter($row['title'], 50); ?></div>
                        </div>
                    </a>
                    <?php
                }
            }
            ?>

        </div>
    </div>
</div>


<div class="col-md-3">
    <div class="newspanel">
        <div class="clearfix"></div>
        <div class="panelheader">
            Writer's Post
        </div>
        <div class="panelcontent">
            <?php
            if ($ar_user) {
                foreach ($ar_user as $row) {
                    ?>  <a href="<?php echo site_url() . NEWS_TAG . "/" ?><?php echo $row['slug'] ?>">
                        <div class="newshead">
                            <div class="leftsection">
                                <img src='<?php echo $row['thumb']; ?>' />
                            </div>
                            <div class="rightsection"><?php echo wordLimiter($row['title'], 50); ?></div>
                        </div>
                    </a>
                    <?php
                }
            }
            ?>

        </div>
    </div>
</div>
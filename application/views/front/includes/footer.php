﻿<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h4 style="color: #fff;">JyotishShastra.com</h4>
                <p style="    line-height: 16px;font-size: 13px;"><?php
                    echo getCMSContent("footer");
                    ;
                    ?></p>
            </div>
            <div class="col-sm-6">
                <?php $menus = getMenus(); ?>
                <ul class="pull-right">
                    <li><a href="<?php echo site_url(); ?>">Home</a></li>
                    <?php foreach ($menus as $mnu) {
                        ?>
                        <li><a href="<?php echo site_url() . PAGES_TAG . "/" . $mnu['slug'] ?>"><?php echo $mnu['title'] ?></a></li>
                    <?php }
                    ?>                  
                </ul>
                <div class="clearfix"></div>
                <ul class="header_social" style="float: right; margin-top: 30px;">
                    <?php
                    $link = getCMSContent("fb_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("tw_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("ln_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-linkedin"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("gp_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-google-plus"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("yt_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-youtube"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("inst_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("fl_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-flickr"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("vimio_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-vimeo-square"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("pintrest_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-pinterest"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("vine_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-vine"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("pingler_link");
                    if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-bullseye"></i></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                Copyright &copy;  2015 - <?php echo date("Y"); ?>, All Rights Reserved.
                <a target="_blank" href="https://jyotishshastra.com" title="JyotishShastra.com">JyotishShastra.com</a>
            </div>
            <div class="col-sm-6">
                <h3 style="text-align: right; margin: 0;">
                    <a href="http://www.dreamzkart.in">DreamzKart</a>
                </h3>
            </div>
        </div>
    </div>
</footer><!--/#footer-->


<?php if (!$this->session->userdata("user_id") > 0) { ?>
    <div class="cm-login" style="display: none;">
        <div class="overay"></div>
        <div class="login-block" style="display: none;">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-block-logo">
                        ज्योतिषशास्त्र 
                    </div>
                    <div class="login-block-inner">
                        <div class="login-section-outer row">
                            <div class="close-btn"><i class="fa fa-close"></i></div>
                            <div class="col-md-6">
                                <div class="social-login">
                                    <div >Continue With Social Logins
                                        <a href="#" class="cm-social-button facebook" >
                                            <i class="fa fa-facebook"></i>
                                            Continue with facebook
                                        </a>
                                        <a href="#" class="cm-social-button twitter" style="display: none">
                                            <i class="fa fa-twitter"></i>
                                            Continue with Twitter
                                        </a>
                                        <a href="#" class="cm-social-button google" style="display: none;">
                                            <i class="fa fa-google-plus"></i>
                                            Continue with Google
                                        </a>
                                        <div class="signup-term">
                                            By signing up you indicate that you have read and agree to the <a href="<?php echo site_url() . "page/terms-of-use" ?>"> terms of service</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="login-section">
                                    <form class="form-login" style="display: none; ">
                                        <div class="form-title">LOGIN</div>
                                        <div class="invalid_message" style="padding: 5px 10px;margin-bottom: 7px; color: #cc3300; display: none">Invalid</div>
                                        <div class="valid_message" style="padding: 5px 10px;margin-bottom: 7px; color: #009900; display: none">valid</div>
                                        <div class="form-group">
                                            <input class="form-control input-lg font-weight-200" name="email" id="log_email" placeholder="Email" required="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control input-lg font-weight-200" name="password" id="log_password" placeholder="Password" required="">
                                        </div>
                                        <div class="button btn_login" >Continue</div>
                                        <div class="">
                                            <a class="action_forgot">FORGOT PASSWORD?</a>
                                        </div>
                                        <div class="center ">
                                            <div class="login-text">Don't have an account <a class="action_signup">Sign Up</a></div>
                                        </div>
                                    </form>
                                    <form class="form-signup" style="display: none;">
                                        <div class="form-title">SIGN UP</div>
                                        <div class="invalid_message" style="padding: 5px 10px;margin-bottom: 7px; color: #cc3300; display: none">Invalid</div>
                                        <div class="valid_message" style="padding: 5px 10px;margin-bottom: 7px; color: #009900; display: none">valid</div>
                                        <div class="form-group">
                                            <input class="form-control input-lg font-weight-200" name="name" id="reg_name" placeholder="Full Name" required="">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control input-lg font-weight-200" name="email" id="reg_email" placeholder="Email" required="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control input-lg font-weight-200" name="password" id="reg_pass" placeholder="Password" required="">
                                        </div>




                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">

                                                    <input type="text" class="form-control required" placeholder="Captcha" id="captcha_sign" name="captcha_sign" data-msg-required="Please enter captcha">

                                                </div>
                                                <div class="col-md-6 captcha_container pull-right" style="padding-top: 3px;">
                                                    <img src="<?php echo site_url(); ?>general/captcha/signup">
                                                    <div class="refresh_captcha" data-type="signup"><i class="fa fa-refresh"></i></div>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="button btn_signup">Sign Up</div>

                                        <div class="center ">
                                            <div class="login-text">Already have an account?  <a class="action_login">login</a></div>
                                        </div>
                                    </form>
                                    <form class="form-forgot" style="display: none">
                                        <div class="form-title">FORGOT PASSWORD</div>
                                        <div class="invalid_message" style="padding: 5px 10px;margin-bottom: 7px; color: #cc3300; display: none">Invalid</div>
                                        <div class="valid_message" style="padding: 5px 10px;margin-bottom: 7px; color: #009900; display: none">valid</div>
                                        <div class="form-group">
                                            <input class="form-control input-lg font-weight-200" name="email" id="forgot_email" placeholder="Username or Email" required="">
                                        </div>
                                        <div class="button btn_recover">Recover</div>

                                        <div class="">
                                            <div class="login-text">Suddenly remembered?   <a class="action_login">Login here</a></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        var newWin;
        $(document).ready(function(e) {
            $(".cm-social-button.facebook").on('click', function() {
                $(".close-btn").click();
                var url = '<?php echo site_url() ?>facebook/connect';
                newWin = popupwindow(url, "Facebook Login", 800, 500);
                if (!newWin || newWin.closed || typeof newWin.closed == 'undefined')
                {
                    window.location = url;
                }
            });
        });

        function afterLogin($status, $login_id) {
            newWin.close();
            window.location.reload();

        }
    </script>

<?php } ?>
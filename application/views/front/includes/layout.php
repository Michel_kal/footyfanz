<?php

/*print_r($club_list);
exit();*/

$base = base_url() . PUBLIC_DIR . "update_assets/";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	echo $this->load->view("front/includes/update_header_script.php", (isset($header_data) ? $header_data : array()));
	?>
	<?php
	if (isset($css)) {
		if (is_array($css)) {
			foreach ($css as $css_file) {
				?>
				<link type="text/css" rel="stylesheet" href="<?php echo base_url() . PUBLIC_DIR . $css_file; ?>"/>
				<?php
			}
		} else if ($css != '') {
			?>
			<link type="text/css" rel="stylesheet" href="<?php echo base_url() . PUBLIC_DIR . $css; ?>"/>
			<?php
		}
	}
	?>

	<!-- Google Tag Manager -->
	<script>(function(w, d, s, l, i) {
		w[l] = w[l] || [];
		w[l].push({'gtm.start':
			new Date().getTime(), event: 'gtm.js'});
		var f = d.getElementsByTagName(s)[0],
		j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
		j.async = true;
		j.src =
		'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
		f.parentNode.insertBefore(j, f);
	})(window, document, 'script', 'dataLayer', 'GTM-PQKLPDF');</script>
	<!-- End Google Tag Manager -->
	<!-- Facebook Pixel Code -->
	<script>
	!function(f, b, e, v, n, t, s) {
		if (f.fbq)
			return;
		n = f.fbq = function() {
			n.callMethod ?
			n.callMethod.apply(n, arguments) : n.queue.push(arguments)
		};
		if (!f._fbq)
			f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
	}(window,
		document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '334983033546976'); // Insert your pixel ID here.
            fbq('track', 'PageView');
            </script>

        </head>

        <body class="kopa-home-6-page">

        	<noscript><img height="1" width="1" style="display:none"
        		src="https://www.facebook.com/tr?id=334983033546976&ev=PageView&noscript=1"
        		/></noscript>
        		<!-- Google Tag Manager (noscript) -->
        		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PQKLPDF"
        			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        			<!-- End Google Tag Manager (noscript) -->


        			<?php 
        			if (isset($home_page) and $home_page == 1) {

        			} else {
        				$home_page = "0";
        			}



        			echo $this->load->view("front/includes/update_top_menu.php", (isset($data_league) ? array("data_league" => $data_league, "counts" => $counts, "noti_header" => $noti_header, "home_page" => $home_page, "club_list" => $club_list) : array("counts" => $counts, "noti_header" => $noti_header, "home_page" => $home_page, "club_list" => $club_list))); 

        			?>

        			<?php echo isset($content) ? $content : '' ?>

        			
        			<footer id="kopa-footer">

        				<div class="wrapper clearfix">

        					<p id="copyright" class="">Copyright © 2013 . All Rights Reserved. Designed by <a href="#">kopatheme.com</a>.</p>

        				</div>
        				<!-- wrapper -->

        			</footer>
        			<!-- kopa-footer -->
        			<?php
        			echo $this->load->view("front/includes/update_footer_script.php", array());
        			?>

        		</body>

        		</html>

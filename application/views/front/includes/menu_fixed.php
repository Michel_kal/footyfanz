<div class="fixedheader">
    <div class="container ">
        <nav class="navbar navbar-inverse" role="banner">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="<?php echo site_url(); ?>">HOME</a></li>         
                    <li><a href="<?php echo site_url(); ?>articles/listing">ARTICLES</a></li>
                    <li><a href="<?php echo site_url(); ?>newsfeed">NEWSFEED</a></li>
                    <li><a href="<?php echo site_url(); ?>plus">FOOTYFANZ+</a></li>
                    <li><a href="<?php echo site_url(); ?>contact">CONTACT US</a></li>
                    <li><a href="<?php echo site_url(); ?>faq">FAQ'S</a></li>
                    <li>
                        <div class="search">
                            <form role="form">
                                <input type="text" class="search-form post_search" autocomplete="off" placeholder="Search...">
                                <i class="fa fa-search btn_post_search"></i>
                            </form>
                        </div>
                    </li>
                    <li class="dropdown  lastmenu">
                        <?php if ($this->session->userdata("user_id") > 0) { ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->session->userdata("name") ?> <i class="fa fa-caret-down"></i></a>

                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url(); ?>articles/post/">Write Article</a></li>
                            <li><a href="<?php echo site_url(); ?>articles/listing/<?php echo $this->session->userdata("slug"); ?>">My Articles</a></li>
                            <li><a href="<?php echo site_url(); ?>newsfeed">Newsfeed</a></li>
                            <li><a href="<?php echo site_url(); ?><?php echo $this->session->userdata("slug"); ?>">Profile</a></li>
                            <li><a href="<?php echo site_url(); ?>user/logout">Logout</a></li>
                        </ul>
                        <?php } else { ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> My Account</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url(); ?>login">Login</a></li>
                            <li><a href="<?php echo site_url(); ?>signup">Sign Up</a></li>
                        </ul>
                        <?php } ?>

                    </li>
                </ul>
            </div>

        </nav>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e) {
    $(window).scroll(function() {
        var static_height = $(".dynamicheader").offset().top;
        var window_scroll = $(window).scrollTop();
        var window_width = $("body").width();

            //console.log(devider_offset+"----"+window_scroll);
            if (static_height < window_scroll) {
                if (window_width > 960) {
                    $(".fixedheader").show();
                } else {
                    $(".fixedheader").hide();
                }
            } else {
                $(".fixedheader").hide();
            }
        });
});
</script>

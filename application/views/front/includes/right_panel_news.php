<?php
$base = base_url() . PUBLIC_DIR . "update_assets/";
//Type, Sponser, Featured, Club_id, User_id,Total Record,Extra->True or false
$club_news = getAticles('N', 0, 0, $this->session->userdata("club_id"), 0, 8);
$sp_articles = getAticles('A', 1, 0, 0, 0, 10);
?>  
<?php
$gc = trim(getCMSContent("ad_ggl_square"));

//            $ads = getAds("square");
$ads = getSiteAds(3, "0", "square");
$cnt = count($ads);
if ($cnt > 0) {
    $num = array_rand($ads);
    ?>
    <div class="widget widget_search style1">
        <div class="widget kopa-ads-widget">
            <a href="<?php echo $ads[$num]['link'] ?>">
                <img src="<?php echo $ads[$num]['image']; ?>" alt="">
            </a>
        </div>
    </div>
    <!-- widget -->
    <?php
} else {
    echo $gc;
}
?>
<div class="widget kopa-tab-1-widget sticky">
    <div class="kopa-tab style6">
        <ul class="nav nav-tabs">
            <li><a href="#news" data-toggle="tab"><span>More club news</span></a></li>
        </ul>
        <!-- nav-tabs -->
        <div class="tab-content">
            <div class="tab-pane active" id="news">

                <ul class="kopa-list clearfix">
                    <?php
                    if ($club_news) {
                        foreach ($club_news as $key => $cn) {
                            if ($key > 2) {
                                break;
                            }
                            ?> 
                            <li>
                                <div class="entry-item prev-post">
                                    <div class="entry-thumb">
                                        <a href="<?php echo site_url() . ARTICLE_TAG . "/" ?><?php echo $cn['slug'] ?>">
                                            <img src="<?php echo $cn['thumb']; ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title">
                                            <a href="<?php echo site_url() . ARTICLE_TAG . "/" ?><?php echo $cn['slug'] ?>">
                                                <?php echo $cn['title']; ?>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>

            </div>
            <!-- tab-pane -->
        </div>
    </div>
    <!-- kopa-tab -->

</div>
<!-- widget --> 
<script src="<?php echo $base; ?>js/jquery-1.11.1.js"></script> 
<script src="<?php echo $base; ?>js/sticky.min.js"></script> 
<script type="text/javascript">

var sticky = new Sticky('.sticky');
</script>
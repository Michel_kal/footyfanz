<!-- JS Files and Scripts Section -->
<?php $base = base_url() . PUBLIC_DIR . "update_assets/" ?>
<a href="#" class="scrollup"><span class="fa fa-chevron-up"></span></a>

<script src="<?php echo $base; ?>js/jquery-1.11.1.js"></script> 
<script src="<?php echo $base; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="<?php echo $base; ?>js/custom.js" charset="utf-8"></script>
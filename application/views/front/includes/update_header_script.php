<?php $base = base_url() . PUBLIC_DIR . "update_assets/"; ?>
<meta charset="utf-8">

<script type="text/javascript">
var base_url = "<?php echo base_url(); ?>";
var site_url = "<?php echo base_url(); ?>";
var user_id = "<?php echo $this->session->userdata("user_id"); ?>";
</script>

<?php if($this->session->userdata("articlemeta")){ 
	$artdata=  unserialize($this->session->userdata("articlemeta"));
	if($artdata['image']!=""){
		?>
		<meta property="og:image" content="<?php echo $artdata['image']; ?>"/>
		<?php
	}else{
		?>
		<meta property="og:image" content="<?php echo $base; ?>images/logo.png"/>
		<?php
	}
	
	
	if($artdata['title']!=""){
		?>
		<meta property="og:title" content="<?php echo str_replace('"', "'", strip_tags($artdata['title'])); ?>" />
		<title><?php echo SITE_NAME; ?>-<?php echo str_replace('"', "'", strip_tags($artdata['title'])); ?></title>
		<?php
	}else{
		?>
		<meta property="og:title" content="<?php echo SITE_NAME; ?>" />
		<title><?php echo SITE_NAME; ?></title>
		<?php
	}
	if($artdata['content']!=""){
		?>
		<meta property="og:description" content="<?php echo str_replace('"', "'", strip_tags($artdata['content'])); ?>" />
		<?php
	}    
	?>

	<?php }else{ ?>
	<meta property="og:image" content="<?php echo $base; ?>images/logo.png"/>
	<meta property="og:title" content="<?php echo SITE_NAME; ?>" /> 
	<?php } ?>
	<meta property="og:site_name" content="www.<?php echo SITE_NAME; ?>.com"/>
	<meta property="fb:app_id" content="246748612376671" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/bootstrap.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/font-awesome.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/superfish.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/megafish.css" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/jquery.navgoco.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/owl.carousel.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/owl.theme.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/jquery.mCustomScrollbar.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/custom.css">

	<script src="<?php echo $base; ?>js/modernizr.custom.js"></script>

        <!--[if lt IE 9]>
            <link rel="stylesheet" href="<?php echo $base; ?>css/ie.css" type="text/css" media="all" />
            <![endif]-->

        <!--[if IE 9]>
            <link rel="stylesheet" href="<?php echo $base; ?>css/ie9.css" type="text/css" media="all" />
            <![endif]-->

            <!-- Le fav and touch icons -->
            <link rel="shortcut icon" href="img/favicon.ico">
            <link rel="apple-touch-icon" href="<?php echo $base; ?>img/apple-touch-icon.png">
            <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $base; ?>img/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $base; ?>img/apple-touch-icon-114x114.png">

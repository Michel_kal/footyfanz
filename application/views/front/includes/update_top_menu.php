<?php
$base = base_url() . PUBLIC_DIR . "update_assets/";
?>
<header class="kopa-header">

  <div class="kopa-header-top">  

   <div class="wrapper">

    <div class="header-top-left">

     <div class="kopa-user">
      <ul class="clearfix">
        <?php
        if ($this->session->userdata("user_id") > 0){ 
          ?>
          <li><a href="<?php echo site_url(); ?><?php echo $this->session->userdata("slug"); ?>"><?php echo $this->session->userdata("name") ?></a></li>

          <?php }else{ ?>
          <li><a href="#">Sign in</a></li>
          <li>&nbsp;|&nbsp;</li>
          <li><a href="#">Register</a></li>

          <?php } ?>

        </ul>
      </div>
      <!-- kopa-user -->

      <div class="social-links style-color">
        <ul class="clearfix">
          <?php
          $link = getCMSContent("fb_link");
          if ($link) {
            ?>
            <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-facebook"></i></a></li>
            <?php } ?>
            <?php
            $link = getCMSContent("tw_link");
            if ($link) {
              ?>
              <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-twitter"></i></a></li>
              <?php } ?>
              <?php
              $link = getCMSContent("ln_link");
              if ($link) {
                ?>
                <li><a href="<?php echo $link; ?>"><i class="fa fa-linkedin"></i></a></li>
                <?php } ?>
                <?php
                $link = getCMSContent("gp_link");
                if ($link) {
                  ?>
                  <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-google-plus"></i></a></li>
                  <?php } ?>
                  <?php
                  $link = getCMSContent("yt_link");
                  if ($link) {
                    ?>
                    <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-youtube"></i></a></li>
                    <?php } ?>
                    <?php
                    $link = getCMSContent("inst_link");
                    if ($link) {
                      ?>
                      <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-instagram"></i></a></li>
                      <?php } ?>
                      <?php
                      $link = getCMSContent("fl_link");
                      if ($link) {
                        ?>
                        <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-flickr"></i></a></li>
                        <?php } ?>
                        <?php
                        $link = getCMSContent("vimio_link");
                        if ($link) {
                          ?>
                          <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-vimeo-square"></i></a></li>
                          <?php } ?>
                          <?php
                          $link = getCMSContent("pintrest_link");
                          if ($link) {
                            ?>
                            <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-pinterest"></i></a></li>
                            <?php } ?>
                            <?php
                            $link = getCMSContent("vine_link");
                            if ($link) {
                              ?>
                              <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-vine"></i></a></li>
                              <?php } ?>
                              <?php
                              $link = getCMSContent("pingler_link");
                              if ($link) {
                                ?>
                                <li><a href="<?php echo $link; ?>" title="<?php echo $link; ?>"><i class="fa fa-bullseye"></i></a></li>
                                <?php } ?>
                              </ul>
                            </div>
                            <!-- social-links -->

                          </div> 
                          <!-- header-top-left -->  

                          <div class="header-top-right">

                           <div class="header-top-list">
                            <ul class="clearfix">
                              <li class=""><a href="<?php echo site_url(); ?>user/logout"><span><i class="fa fa-sign-out "></i><span>Log out</span></span></a></li>
                            </ul>
                          </div>
                          <!-- header-top-list -->

                          <div class="kopa-search-box">
                            <a href="#"><i class="fa fa-search"></i><span>Search</span></a>
                            <form action="#" class="search-form clearfix" method="get">
                             <input type="text" onBlur="if (this.value == '')
                             this.value = this.defaultValue;" onFocus="if (this.value == this.defaultValue)
                             this.value = '';" value="Search..." name="s" class="search-text">
                             <button type="submit" class="search-submit">
                              <span>Go</span>
                            </button>
                          </form>
                        </div>
                        <!--kopa-search-box-->

                      </div>
                      <!-- header-top-right -->   

                    </div>  
                    <!-- wrapper -->

                  </div>
                  <!-- kopa-header-top -->

                  <div class="kopa-header-middle">

                   <div class="wrapper">

                    <div class="kopa-logo">
                     <a href="#"><img src="<?php echo site_url(); ?>public/logo.png" alt="logo"></a>
                   </div>
                   <!-- logo -->

                   <nav class="kopa-main-nav">
                     <ul class="main-menu sf-menu">
                      <li class="current-menu-item">
                       <a href="<?php echo site_url(); ?>"><span>Home</span></a>
                     </li>
                     <li>
                      <a href="<?php echo site_url(); ?>articles/listing"><span>Articles</span></a>
                    </li>
                    <li>
                      <a href="<?php echo site_url(); ?>newsfeed"><span>Newsfeed</span></a>
                    </li>
                    <li>
                      <a href="<?php echo site_url(); ?>plus"><span>Footyfanz+</span></a>
                    </li>
                    <li>
                      <a href="<?php echo site_url(); ?>oyo5aside"><span>Oyo5Aside</span></a>
                    </li>
                    <li><a href="<?php echo site_url(); ?>contact"><span>Contact</span></a></li>
                    <li><a href="#"><span>Shop</span></a></li>
                  </ul>                
                </nav>
                <!--/end main-nav-->

                <nav class="main-nav-mobile clearfix">
                 <a class="pull fa fa-bars"></a>
                 <ul class="main-menu-mobile">
                  <li class="current-menu-item">
                   <a href="#"><span>Home</span></a>
                   <ul class="sub-menu">
                    <li class="current-menu-item">
                     <a href="<?php echo site_url(); ?>"><span>Home</span></a>
                   </li>
                   <li>
                    <a href="<?php echo site_url(); ?>articles/listing"><span>Articles</span></a>
                  </li>
                  <li>
                    <a href="<?php echo site_url(); ?>newsfeed"><span>Newsfeed</span></a>
                  </li>
                  <li>
                    <a href="<?php echo site_url(); ?>plus"><span>Footyfanz+</span></a>
                  </li>
                  <li>
                    <a href="<?php echo site_url(); ?>oyo5aside"><span>Oyo5Aside</span></a>
                  </li>
                  <li><a href="<?php echo site_url(); ?>contact"><span>Contact</span></a></li>
                  <li><a href="#"><span>Shop</span></a></li>
                </ul>  
              </nav>
              <!--/main-menu-mobile-->

            </div>
            <!-- wrapper -->

          </div>
          <!-- kopa-header-middle -->

          <div class="kopa-header-bottom">

           <div class="wrapper">

            <nav class="kopa-main-nav-2">
              <ul class="main-menu-2 sf-menu">
                <?php
                if ($this->session->userdata("user_id") > 0){
                  if (count($noti_header['data']) == 0) {
                    ?>

                    <li>
                     <a href="#"><span><i class="fa fa-bell-o"></i> Notifications</span></a>
                     <div class="sf-mega col-md-push-0 col-xs-push-0 col-sm-push-0">
                      <!-- col-md-3 -->
                      <div class="sf-mega-section col-md-12 col-xs-12 col-sm-12">
                       <div class="widget kopa-sub-list-widget sub-list-1">
                        <ul class="row">
                          <?php 
                          foreach ($noti_header['data'] as $notification) {
                            ?>
                            <li class="col-md-4 col-xs-4 col-sm-4">
                              <article class="entry-item">
                               <h5>
                                <a href="<?php echo $notification['url'] ?>" class="noti_single">
                                  <?php
                                  echo timeAgo(strtotime($notification['created_at']));
                                  ?>
                                </a>
                              </h5>
                              <div class="entry-thumb">
                                <a href="<?php echo $notification['url'] ?>">
                                  <img src="<?php echo $notification['thumb']; ?>" alt="">
                                </a>
                              </div>
                              <h4 class="entry-title">
                                <a href="<?php echo $notification['url'] ?>">
                                  <?php echo $notification['content']; ?>
                                </a>
                              </h4>
                            </article>
                          </li>
                          <?php } ?>
                        </ul>
                      </div>
                      <!-- widget -->
                    </div>
                    <!-- col-md-9 -->
                  </div>
                </li>
                <?php 
              }
            } 
            ?>
            <li><a href="<?php echo site_url(); ?>articles/post/"><span>Write Article</span></a></li>
            <li><a href="<?php echo site_url(); ?>articles/listing/<?php echo $this->session->userdata("slug"); ?>"><span>My Articles</span></a></li>
            <li><a href="<?php echo site_url(); ?>newsfeed"><span>News feed</span></a></li>
          </ul>                
        </nav>
        <!--/end main-nav-2-->

        <nav class="main-nav-mobile style2 clearfix">
         <a class="pull">categories<i class="fa fa-angle-down"></i></a>
         <ul class="main-menu-2 sf-menu">
          <?php
          if ($this->session->userdata("user_id") > 0){ 
            if (count($noti_header['data']) == 0) {
              ?>

              <li>
               <a href="#"><span><i class="fa fa-bell-o"></i> Notifications</span></a>
               <div class="sf-mega col-md-push-0 col-xs-push-0 col-sm-push-0">
                <!-- col-md-3 -->
                <div class="sf-mega-section col-md-12 col-xs-12 col-sm-12">
                 <div class="widget kopa-sub-list-widget sub-list-1">
                  <ul class="row">
                    <?php 
                    foreach ($noti_header['data'] as $notification) {
                      ?>
                      <li class="col-md-4 col-xs-4 col-sm-4">
                        <article class="entry-item">
                         <h5>
                          <a href="<?php echo $notification['url'] ?>" class="noti_single">
                            <?php
                            echo timeAgo(strtotime($notification['created_at']));
                            ?>
                          </a>
                        </h5>
                        <div class="entry-thumb">
                          <a href="<?php echo $notification['url'] ?>">
                            <img src="<?php echo $notification['thumb']; ?>" alt="">
                          </a>
                        </div>
                        <h4 class="entry-title">
                          <a href="<?php echo $notification['url'] ?>">
                            <?php echo $notification['content']; ?>
                          </a>
                        </h4>
                      </article>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
                <!-- widget -->
              </div>
              <!-- col-md-9 -->
            </div>
          </li>
          <?php 
        }
      } 
      ?>
      <li><a href="<?php echo site_url(); ?>articles/post/"><span>Write Article</span></a></li>
      <li><a href="<?php echo site_url(); ?>articles/listing/<?php echo $this->session->userdata("slug"); ?>"><span>My Articles</span></a></li>
      <li><a href="<?php echo site_url(); ?>newsfeed"><span>News feed</span></a></li>
    </ul>
  </nav>
  <!--/main-menu-mobile-2-->

</div>
<!-- wrapper -->

</div>
<!-- kopa-header-bottom -->

</header>
        			<!-- kopa-page-header -->
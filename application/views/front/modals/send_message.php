<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="exampleModalLabel">Send message</h4>
</div>
<div class="modal-body">
    <div class="alert alert-success msg_success" style="display: none;"><i class="fa fa-check"></i> Message has been sent</div>
    <form id="messageForm" action="<?php echo site_url()."profile/send_message"; ?>" name="messageForm">
        <div class="form-group">
            <label for="recipient-name" class="form-control-label">Title</label>
            <input type="text" class="form-control required" id="title" name="title">
        </div>
        <div class="form-group">
            <label for="message-text" class="form-control-label">Message:</label>
            <textarea class="form-control required" id="content" name="content" rows="6"></textarea>
            <input type="hidden" value="<?php echo $id; ?>" name="user_id"/>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn_message">Send message</button>
</div>
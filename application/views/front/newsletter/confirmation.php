<div class="row">
    <div class="col-md-12">
        <div class="heading_block">
            Newsletter Confirmation
        </div>       
    </div>
    <div class="col-md-12">
        <div style="padding: 5px; margin-top: 20px; min-height: 300px;">
            <?php if ($status == "1") { ?>
                <div class="alert alert-success">Thanks for subscribing our Newsletter on your email <?php echo $email; ?>. </div>
            <?php } else if ($status == "3") {
                ?>
                <div class="alert alert-success">You have unsubscribed our Newsletter on your email <?php echo $email; ?>. </div>
                <?php } else {
                ?>
                <div class="alert alert-danger">Invalid Newsletter confirmation request. </div>
            <?php } ?>
        </div>
    </div>
</div>
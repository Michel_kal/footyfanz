<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>");}
    </style>
    <?php
}
?> 

<section class="newsfeed">
    <?php
    if ($ud) {
        $this->load->view("front/profile/profileheader.php", array("ud" => $ud, "follow_status" => $follow_status));
    }
    ?>


    <div class="row strip">
        <div class="col-md-12">
            <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                Followers/Following
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="writerboard">
                        <div class="boardheader">
                            Followers <span class="pull-right"><?php echo ($followers) ? count($followers) : "0"; ?></span>
                        </div>
                        <table class="table postable small">
                            <tbody> <tr>
                                    <th>Pos</th>
                                    <th>Writers</th>
                                    <th>Pts</th>
                                </tr>   
                                <?php
                                if ($followers) {
                                    $i = 1;
                                    foreach ($followers as $row) {
                                        ?>                                  
                                        <tr>
                                            <td><?php echo $i; ?> 
                                                <?php if ($i == 1) { ?>
                                                    <span class="symbol"><i class="fa fa-circle"></i></span>
                                                <?php } else { ?>
                                                    <span class="symbolup"><i class="fa fa-sort-up"></i></span>
                                                    <?php
                                                }
                                                if (1 == 2) {
                                                    ?> 
                                                    <span class="symboldown"><i class="fa fa-sort-down"></i></span>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td>  <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>"><?php echo $row['name']; ?></a> </td>
                                            <td><?php echo $row['points'] ?></td>
                                        </tr> 
                                        <?php
                                    }
                                } else {
                                    ?> 
                                    <tr class="text-center"><td colspan="3">No Followers</td></tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="writerboard">
                        <div class="boardheader">
                            Following <span class="pull-right"><?php echo ($followings) ? count($followings) : "0"; ?></span>
                        </div>
                        <table class="table postable small">
                            <tbody>
                                <tr>
                                    <th>Pos</th>
                                    <th>Writers</th>
                                    <th>Pts</th>
                                </tr>
                                <?php
                                if ($followings) {
                                    $i = 1;
                                    foreach ($followings as $row) {
                                        ?>                                  
                                        <tr>
                                            <td><?php echo $i; ?> 
                                                <?php if ($i == 1) { ?>
                                                    <span class="symbol"><i class="fa fa-circle"></i></span>
                                                <?php } else { ?>
                                                    <span class="symbolup"><i class="fa fa-sort-up"></i></span>
                                                    <?php
                                                }
                                                if (1 == 2) {
                                                    ?> 
                                                    <span class="symboldown"><i class="fa fa-sort-down"></i></span>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td>  <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>"><?php echo $row['name']; ?></a> </td>
                                            <td><?php echo $row['points'] ?></td>
                                        </tr> 
                                        <?php
                                    }
                                } else {
                                    ?> 
                                    <tr class="text-center" ><td colspan="3">No One Following</td></tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">           
            <?php
            $this->load->view("front/includes/right_panel_news.php", array('bottom' => false));
            ?>


        </div>
    </div>


</section>

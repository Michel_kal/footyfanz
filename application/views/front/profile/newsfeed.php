<?php $base = base_url() . PUBLIC_DIR . "update_assets/" ?>
<?php $baseTwo = base_url() . PUBLIC_DIR . "fassets/" ?>
<div id="main-content" style="margin-top: 20px;">

    <div class="wrapper">

        <div class="row" data-sticky-container>
            <div class="col-md-3 col-sm-3 col-xs-6">

                <div class="widget widget_search style1">
                    <h3 class="widget-title style3">
                        <a href="<?php echo site_url() . "followers/" . $ud['slug']; ?>">
                            <span class="fa fa-user"></span> Following
                        </a>
                    </h3>
                    <div class="search-box">
                        <?php
                        if ($followings) {
                            foreach ($followings as $row) {
                                ?>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>" class="kopa-button medium-button grey-button" data-toggle="tooltip" data-placement="top" title="<?php echo $row['name']; ?>">
                                        <?php echo $row['name']; ?>
                                    </a>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <h3 class="widget-title style3">

                                <span class="fa fa-frown-o"></span> You're not following
                                
                            </h3>
                            <?php
                        }
                        ?>
                    </div>
                    <script type="text/javascript">
                    $(function() {
                        $('[data-toggle="tooltip"]').tooltip();
                    });</script>
                </div>
                <!-- widget -->

                <div class="widget widget_search style1">
                    <h3 class="widget-title style3">
                        <a href="<?php echo site_url() . "followers/" . $ud['slug']; ?>">
                            <span class="fa fa-group"></span> Followers
                        </a>
                    </h3>
                    <div class="search-box">
                        <?php
                        if ($followers) {
                            foreach ($followers as $row) {
                                ?>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <a href="#" class="kopa-button medium-button grey-button">Large Button</a>
                                </div>
                                <?php
                            }
                        } else {
                            ?> 
                            <h3 class="widget-title style3">
                                <span class="fa fa-frown-o"></span> 
                                No Followers
                            </h3>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <!-- widget -->

                <div class="widget kopa-article-list-widget article-list-5">
                    <h3 class="widget-title style14">
                        <span>Gallery</span>
                        <a href="#">more <span class="fa fa-chevron-right"></span></a>
                    </h3>
                    <?php if (count($user_gallery) > 0) { ?>
                    <?php
                    foreach ($user_gallery as $gallery) {
                        if ($gallery['vid'] == false) {
                            ?> 
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="entry-thumb">
                                    <a href="#"><img src="http://placehold.it/187x128" alt=""></a>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="entry-thumb">
                                    <a href="#"><img src="http://placehold.it/187x128" alt=""></a>
                                </div>
                            </div>
                            <?php } 
                        }
                    } 
                    ?>
                </div>
                <!-- widget --> 

                <div class="widget kopa-tab-1-widget sticky">
                    <?php if ($poll_data) { ?>
                    <div class="kopa-tab style6" style="background: url(<?php echo $poll_data['image']; ?>); background-size: cover;">
                        <ul class="nav nav-tabs">
                            <li><a href="#news" data-toggle="tab"><span>club news</span></a></li>
                        </ul>
                        <!-- nav-tabs -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="news">
                                <ul class="kopa-list clearfix">
                                    <li>
                                        <span class="bg-red"><?php echo $poll_data['title']; ?></span>

                                    </li>
                                    <li>
                                        <span class="bg-blue">
                                            <?php echo $poll_data['option1']; ?> 
                                            <span class="fa fa-chevron-right"></span> 
                                            <?php echo $poll_data['votes']['perA']."%"; ?>
                                        </span>
                                        &nbsp;&nbsp;&nbsp;
                                        <span class="bg-blue">
                                            <?php echo $poll_data['option2']; ?> 
                                            <span class="fa fa-chevron-right"></span> 
                                            <?php echo $poll_data['votes']['perB']."%"; ?>
                                        </span>
                                        <?php if (trim($poll_data['option3']) != "") { ?>
                                        &nbsp;&nbsp;&nbsp;
                                        <span class="bg-blue">
                                            <?php echo $poll_data['option3']; ?> 
                                            <span class="fa fa-chevron-right"></span> 
                                            <?php echo $poll_data['votes']['perC']."%"; ?>
                                        </span>
                                        <?php } ?>

                                        <?php if (trim($poll_data['option4']) != "") { ?>
                                        &nbsp;&nbsp;&nbsp;
                                        <span class="bg-blue">
                                            <?php echo $poll_data['option4']; ?> 
                                            <span class="fa fa-chevron-right"></span> 
                                            <?php echo $poll_data['votes']['perD']."%"; ?>
                                        </span>
                                        <?php } ?>

                                        <?php if (trim($poll_data['option5']) != "") { ?>
                                        &nbsp;&nbsp;&nbsp;
                                        <span class="bg-blue">
                                            <?php echo $poll_data['option5']; ?> 
                                            <span class="fa fa-chevron-right"></span> 
                                            <?php echo $poll_data['votes']['perE']."%"; ?>
                                        </span>
                                        <?php } ?>

                                    </li>
                                    <li data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="1">
                                        <?php echo $poll_data['option1']; ?>
                                    </li>
                                    <li data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="2">
                                        <?php echo $poll_data['option2']; ?>
                                    </li>
                                    <?php if (trim($poll_data['option3']) != "") { ?>
                                    <li data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="3">
                                        <?php echo $poll_data['option3']; ?>
                                    </li>
                                    <?php } ?>
                                    <?php if (trim($poll_data['option4']) != "") { ?>
                                    <li data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4">
                                        <?php echo $poll_data['option4']; ?>
                                    </li>
                                    <?php } ?>
                                    <?php if (trim($poll_data['option5']) != "") { ?>
                                    <li data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4"><?php echo $poll_data['option5']; ?>
                                    </li>
                                    <?php } ?>
                                </ul>

                            </div>
                            <!-- tab-pane -->
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="kopa-tab style6" style="background: url(<?php echo $base; ?>images/poll/pollhomebg.jpg);">
                        <ul class="nav nav-tabs">
                            <li><a href="#" data-toggle="tab"><span>Vote Now</span></a></li>
                        </ul>
                        <!-- nav-tabs -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="news">
                                <ul class="kopa-list clearfix">
                                    <li>
                                        <span class="bg-red">Is Mourinho  Right For United?</span>
                                    </li>
                                    <li>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <a href="#" class="kopa-button small-button red-button">
                                                <span class="fa fa-check-square-o"></span> Yes
                                            </a>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <a href="#" class="kopa-button small-button red-button">
                                                <span class="fa fa-times-circle"></span> No
                                            </a>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                            <!-- tab-pane -->
                        </div>
                    </div>
                    <?php } ?>

                    <!-- kopa-tab -->
                </div>
                <!-- widget --> 

                <div class="widget kopa-tab-1-widget">
                    <div class="kopa-tab style6">
                        <ul class="nav nav-tabs">
                            <li><a href="#" data-toggle="tab"><span>Trending Now</span></a></li>
                        </ul>
                        <ul class="kopa-list clearfix">
                            <?php
                            $sp_articles = getAticles('A', 1, 0, 0, 0, 7);
                            if ($sp_articles) {
                                foreach ($sp_articles as $sp) {
                                    ?>
                                    <li>
                                        <a href="<?php echo site_url() . ARTICLE_TAG . "/" ?><?php echo $sp['slug'] ?>">
                                            <?php echo $sp['title']; ?>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6">

                <div class="kopa-breadcrumb">
                    <div class="wrapper clearfix">
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a itemprop="url" href="#">
                                <span itemprop="title">Home</span>
                            </a>
                        </span>
                        &nbsp;|&nbsp;
                        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                            <a itemprop="url" href="#">
                                <span itemprop="title">newsfeed</span>
                            </a>
                        </span>
                    </div>
                </div>
                <!--/end .breadcrumb-->

                <div class="kopa-entry-post">

                    <div class="widget kopa-comment-form-widget">
                        <div class="comment-box">
                            <h3 class="widget-title style12 widget-title-custom style12-custom">
                                <img src="<?php echo $this->session->userdata("image"); ?>" class="feed-img"/>
                                What's on your mind?
                                <span class="ttg"></span></h3>
                                <form id ="comments-form" class="clearfix" id="postform" action="<?php echo site_url() . "profile/feedsave" ?>" enctype="multipart/form-data" method="post" novalidate="novalidate"> 
                                    <input type="file" name="fileimage" id="fileimage" accept="image/x-png, image/gif, image/jpeg" style="display: none;" />
                                    <input type="file" name="filevideo" id="filevideo" accept="video/mp4,video/x-m4v,video/*"   style="display: none;" />
                                    <input type="text" name="filetype" id="filetype" value="N" style="display: none;" />
                                    <p class="textarea-block">  
                                        <textarea name="message" id="comment_message" cols="88" rows="12"></textarea>
                                    </p>
                                    <p class="comment-button clearfix feed-control">           
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3" onclick="$('#fileimage').click();">
                                                <span>
                                                    <div class="feed-upload upload-attach">
                                                        <i class="fa fa-image"></i> Image
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="col-md-3 col-sm-3 left-icon-feed upload-attach">
                                                <span>
                                                    <div class="feed-upload" onclick="$('#filevideo').click();">
                                                        <i class="fa fa-video-camera"></i> Video
                                                    </div>
                                                </span>
                                            </div>

                                            <div class="col-md-6 col-sm-6 pull-right">
                                                <span class="">
                                                    <button type="submit" class="feed-upload-btn">
                                                        <i class="fa fa-comments"></i> Post
                                                    </button>
                                                    <!-- <input type="submit" value=" Comment" id="submit-comment" > -->
                                                    &nbsp;&nbsp;&nbsp;
                                                    <span class="pull-right tweet-line">
                                                        <a href="#" data-tweet>
                                                            <i class="fa fa-check-square-o to-tweet"></i>
                                                        </a>
                                                        <i class="fa fa-twitter-square to-tweet"></i> 
                                                    </span>

                                                </span>
                                            </div>
                                        </div>
                                    </p>
                                </form>
                                <div id="response"></div>
                            </div>
                            <!-- comment-box -->



                        </div>
                        <!-- widget -->

                        <!-- Feed -->
                        <div class="feedcontainer" data-ref_id="<?php echo $ref_id ?>" data-user_id="<?php echo $user_id ?>">
                            <!--**************All Feed Container****************-->

                        </div>
                    </div>
                    <!-- kopa-entry-post -->
                    <div class="row text-center">
                        <div class="commentloader" style="display:none;">
                            <div class="loadmore">View more</div>
                            <div class="loaderview"><img src="<?php echo $baseTwo; ?>images/loadingBar.gif" /></div>
                        </div>
                    </div>

                </div>
                <!-- main-col -->

                <div class="col-md-3 col-sm-3 col-xs-6">

                    <?php
                    $this->load->view("front/includes/right_panel_news.php", array('bottom' => false));
                    ?>

                </div>
                <!-- sidebar -->

            </div>
            <!-- row -->

        </div>
        <!-- wrapper -->

    </div>
    <!-- main-content -->


    <script src="<?php echo $base; ?>js/jquery-1.11.1.js"></script> 
    <script src="<?php echo $base; ?>js/sticky.min.js"></script> 
    <script type="text/javascript">

    var sticky = new Sticky('.sticky');

    $(document).on('click', 'a[data-tweet]', function() {
        if ($(this).html() == '<i class="fa fa-check-square-o to-tweet"></i>') {
            $(this).html('<i class="fa fa-square-o to-tweet"></i>');
        }else{
            $(this).html('<i class="fa fa-check-square-o to-tweet"></i>');
        };        
        return false;
    });

    var page = 1;
    function getFeeds() {
        var post_data = {page: page, user_id: $(".feedcontainer").data("user_id"), ref_id: $(".feedcontainer").data("ref_id")};
        $('.commentloader').addClass('loading');
        $.post(base_url + "profile/getfeeds", post_data, function(res) {
            $('.commentloader').removeClass('loading');
            $(".feedcontainer").append(res);
        });
    }
    function getLatestFeeds() {
        var post_data = {last_id: $(".feedsingle:first").data("last_feed"), user_id: $(".feedcontainer").data("user_id"), ref_id: 0};
        $.post(base_url + "profile/getlatestfeeds", post_data, function(res) {
            $(".feedcontainer").prepend(res);
        });
    }
    $(document).on('click', ".loadmore", function(e) {
        page++;
        getFeeds();
    });
    $(document).on('click', ".more_comments", function(e) {
        var com_ccontainer = $(this).closest(".feedfooter").find(".feedcomments");
        var btn_more = $(this);
        var post_data = $(this).closest(".feedfooter").find(".commentsingle:first").data();
        $.post(base_url + "profile/getoldcomments", post_data, function(res) {
            com_ccontainer.prepend(res);
            $(".commentsingle", com_ccontainer).slideDown();
            btn_more.fadeOut();
        });
    });
    $(document).on('click', ".btn_remove_comment", function(e) {
        var com_container = $(this).closest(".commentsingle");
        var post_data = $(this).data();
        var com_count = $(this).closest(".feedfooter").find(".com_count");
        $.confirm({
            title: 'Remove Comment',
            content: 'Are you sure to remove this comment',
            confirmButton: 'Remove',
            confirmButtonClass: 'btn-danger',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "profile/deletecomment", post_data, function(res) {
                    com_container.fadeOut(function() {
                        com_container.remove();
                        com_count.html(parseInt(com_count.html()) - 1);
                    });
                });
            }
        });
    });
    $(document).on('click', ".poll_answer", function(e) {

        var post_data = $(this).data();
        $.confirm({
            title: 'Poll Voting',
            content: 'Are you sure to Vote your Option',
            confirmButton: 'Sure',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "home/save_poll", post_data, function(res) {
                    alert("Your Vote has been Saved");
                });
            }
        });
    });
    $(document).on('click touchend', ".btn_like", function(e) {
        //alert("Hello");
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this);
        var status = $(this).data("like_status");
        var post_data = $(this).data();
        var like_count = $(this).closest(".feedfooter").find(".like_count");
        $.post(base_url + "profile/managelike", post_data, function(res) {
            if (status == 0) {
                like_count.html(parseInt(like_count.html()) + 1);
                $this.html('<i class="fa fa-thumbs-up"></i> Liked');
                $this.data("like_status", "1");
            } else {
                like_count.html(parseInt(like_count.html()) - 1);
                $this.html('<i class="fa fa-thumbs-up"></i> Like');
                $this.data("like_status", "0");
            }

        });
    });
    $(document).on('click touchend', ".btn_share", function(e) {
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this);
        var post_data = $(this).data();
        $.confirm({
            title: 'Share Newsfeed',
            content: 'Are you sure to Share this News',
            confirmButton: 'Share',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "profile/sharefeed", post_data, function(res) {
                    getLatestFeeds();
                });
            }
        });
    });
    $(document).on('click', ".delete_feed", function(e) {
        var $this = $(this);
        var post_data = $(this).data();
        var feed_block = $(this).closest(".feedsingle");
        $.confirm({
            title: 'Remove Feed',
            content: 'Are you sure to remove this feed and all its contents',
            confirmButton: 'Remove Feed',
            confirmButtonClass: 'btn-danger',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                $.post(base_url + "profile/delete_feed", post_data, function(res) {
                    feed_block.fadeOut(function() {
                        feed_block.remove();
                    });
                });
            }
        });
    });
    $(document).on('click', ".btnpostcomment", function(e) {
        var form = $(this).closest(".feedcommentspost").find("form");
        var com_block = $(this).closest(".feedfooter").find(".feedcomments");
        var com_count = $(this).closest(".feedfooter").find(".com_count");
        var post_data = form.serialize();
        form.find(".com_text").attr("disabled", "disabled");
        $.post(base_url + "profile/savecomment", post_data, function(res) {
            com_block.append(res.data);
            com_count.html(parseInt(com_count.html()) + parseInt(res.count));
            $(".commentsingle", com_block).slideDown();
            form.find(".com_text").removeAttr("disabled");
            form.find(".com_text").val("");
            form.find(".com_last_id").val(res.last_id);
        }, 'json');
    });
    $(document).ready(function(e) {
        getFeeds();
        $(".fancybox-effects-a").fancybox();
        $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            prevEffect: 'none',
            nextEffect: 'none',
            arrows: false,
            helpers: {
                media: {},
                buttons: {}
            }
        });
        $(".btnactionfeed").on('click', function() {
            if ($.trim($("#posttext").val()) != "" || $("#filetype").val() != "N") {
                var cntval = $("#posttext").val().length;
                if (cntval > 140) {
                    $("#posttext").trigger("keyup");
                } else {
                    $('#postform').submit();
                }

            }
        });
        $(document).on("change", "#fileimage", function() {
            if ($("#filetype").val() == "I" && $(this).val() == "") {
                $(".feedfilename").html("");
                $("#filetype").val("N");
            }
            if ($(this).val() != "") {
                $("#filetype").val("I");
                $(".feedfilename").html(getFileName($(this).val()));
            }
        });
        $(document).on("change", "#filevideo", function() {
            if ($("#filetype").val() == "V" && $(this).val() == "") {
                $(".feedfilename").html("");
                $("#filetype").val("N");
            }
            if ($(this).val() != "") {
                $("#filetype").val("V");
                $(".feedfilename").html(getFileName($(this).val()));
            }
        });
    });
function getFileName(mainname) {
    var path = mainname;
    var fileName = path.match(/[^\/\\]+$/);
    return fileName;
}

$(document).ready(function(e) {
    $('#postform').ajaxForm({
            //dataType: 'json',
            beforeSubmit: function() {
                $(".feedposter").addClass("loading");
                $(".uploadpouter").show();
                $(".uploadpinner").css("width", "0%");
            },
            uploadProgress: function(event, position, total, percentComplete) {
                if (percentComplete == 100) {
                    $(".uploadpouter").hide();
                } else {
                    $(".uploadpinner").css("width", percentComplete + '%');
                }
            },
            success: function(res) {
                $(".uploadpouter").hide();
                $(".feedposter").removeClass("loading");
                $(".feedfilename").html("");
                $("#filetype").val("N");
                $('#postform')[0].reset();
                getLatestFeeds();
            },
            failed: function() {
                $(".uploadpouter").hide();
                $(".feedposter").removeClass("loading");
            }
        });
});
$(document).on('keydown', ".txtcommentbox", function(e) {
    if (e.ctrlKey && e.keyCode == 13) {
        e.preventDefault();
        $(this).closest(".feedcommentspost").find(".btnpostcomment").click();
        return false;
    }
});
$(document).on('keyup', "#posttext", function(e) {
    var cntval = $(this).val().length;
    if (cntval > 160) {
        $.confirm({
            title: 'Newsfeed limit exceed',
            content: 'Newsfeed max size is 160 character. Please write article for large content.',
            confirmButton: 'Write Article',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                var post_data = {"data": $("#posttext").val()};
                $.post(site_url + "articles/prepost", post_data, function(res) {
                    window.location.href = site_url + "articles/post";
                });
            }
        });
        return false;
    }
});
$(document).on('click', ".btn_edit_comment", function(e) {

    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_data_block").hide();
    $(".com_edit_temp .com_text").html(parent.find(".com_text_temp").html());
    $(".com_edit_temp .com_id").val($this.data("id"));
    parent.find(".com_edit_block").html($(".com_edit_temp").html());
});
$(document).on('click', ".btncancelcomment", function(e) {
    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_edit_block").html("");
    parent.find(".com_data_block").show();
});
$(document).on('click', ".btneditcomment", function(e) {
    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_text_block").html(parent.find(".com_edit_block .com_text").val());
    parent.find(".com_text_temp").html(parent.find(".com_edit_block .com_text").val());
    var post_data = parent.find(".com_edit_block form").serialize();
    $.post(site_url + "profile/commentupdate", post_data, function(res) {
        $(".btncancelcomment").click();
    });
});</script>
<script type="text/javascript">
var point_start = 0;
$(document).ready(function(e) {
    $(window).scroll(function() {
        var q_con_height = $(".feedcontainer").height() + $('.feedcontainer').offset().top - 600;
            //alert(q_con_height + "----" + $(window).scrollTop());
            if ($(window).scrollTop() > q_con_height) {
                if ($(".loadmore").css("display") != "none") {
                    $(".loadmore").click();
                }
            }


            var static_height = $(".staticcheck").offset().top;
            var window_scroll = $(window).scrollTop();
            var window_width = $("body").width();
            var devider_offset = $(".staticdevider").offset().top - 60;
            var staticstarter = $(".staticstarter").offset().top;
            //console.log(devider_offset+"----"+window_scroll);
            if (devider_offset < window_scroll) {
                if (point_start == 0) {
                    point_start = devider_offset;
                }
                if (window_width > 1170) {
                    if (window_scroll > devider_offset) {
                        if ($(".fixerinner").hasClass("fixedNow") == false) {
                            $(".fixerinner").addClass("fixedNow");
                            $(".fixerinner").css("position", "fixed");
                            $(".fixerinner").css("width", 347);
                            $(".fixerinner").css("top", ((devider_offset - staticstarter) * (-1)) + 15);
                        }
                    } else {
                        setWidthAuto();
                    }
                } else {
                    setWidthAuto();
                }
            } else {
                if (point_start > window_scroll) {
                    setWidthAuto();
                }
            }
        });
$(window).scroll(function() {
    var static_height = $(".staticcheck").offset().top;
    var window_scroll = $(window).scrollTop();
    var window_width = $("body").width();
    var devider_offset = $(".staticdevider2").offset().top + -60;
    var staticstarter = $(".staticstarter2").offset().top;
            // console.log(devider_offset + "----" + window_scroll);
            if (devider_offset < window_scroll) {
                if (point_start == 0) {
                    point_start = devider_offset;
                }
                if (window_width > 1170) {
                    if (window_scroll > devider_offset) {
                        if ($(".fixerinner2").hasClass("fixedNow2") == false) {
                            $(".fixerinner2").addClass("fixedNow2");
                            $(".fixerinner2").css("position", "fixed");
                            $(".fixerinner2").css("width", 253);
                            $(".fixerinner2").css("top", ((devider_offset - staticstarter) * (-1)) + 15);
                        }
                    } else {
                        setWidthAuto2();
                    }
                } else {
                    setWidthAuto2();
                }
            } else {
                if (point_start > window_scroll) {
                    setWidthAuto2();
                }
            }
        });
});
function setWidthAuto() {
    if ($(".fixerinner").hasClass("fixedNow")) {
        $(".fixerinner").removeClass("fixedNow");
        $(".fixerinner").css("position", "relative");
        $(".fixerinner").css("width", "auto");
        $(".fixerinner").css("top", "0");
    }
}

function setWidthAuto2() {
    if ($(".fixerinner2").hasClass("fixedNow2")) {
        $(".fixerinner2").removeClass("fixedNow2");
        $(".fixerinner2").css("position", "relative");
        $(".fixerinner2").css("width", "auto");
        $(".fixerinner2").css("top", "0");
    }
}


</script>


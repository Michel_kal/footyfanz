<?php
$base = base_url() . PUBLIC_DIR . "update_assets/";
?> 
<link rel="stylesheet" type="text/css" href="<?php echo $base; ?>css/custom.css">
<?php
//print_r($data);

if ($data) {
    foreach ($data as $feed) {
        $comments = $feed['comments'];
        $shared_by = $feed['shared_by'];
        ?> 
        <div class="kopa-entry-post feedsingle" data-last_feed="<?php echo $feed['pk_feed_id']; ?>">
            <?php
            if ($feed['art_data']) {
                $fd = $feed['art_data'];
//                print_r($fd);
                ?>
                <div class="kopa-tab-1-widget">
                    <article class="entry-item news-feed-article">
                        <div class="header-feed-post">
                            <div class="comment-avatar">
                                <img alt="" src="<?php echo $feed['thumb']; ?>">
                            </div>
                            <div>
                                <h4 class="entry-title">
                                    <a href="<?php echo site_url() . ARTICLE_TAG . "/" . $fd['slug']; ?>">
                                        <?php echo $fd['title']; ?>
                                    </a>
                                </h4>
                            </div>
                        </div>
                        
                        
                        <div class="entry-meta">
                            <br>
                            <br>
                            <br>
                            <span class="entry-author">

                                by 
                                <a href="<?php echo site_url() . "articles/listing/" . $feed['slug']; ?>">
                                    <?php echo $feed['name']; ?>
                                </a>
                            </span>
                            <?php if ($shared_by) { ?>
                            <span class="entry-author">
                                <i class="fa fa-share-alt"></i>  Shared a post from
                                <a href="<?php echo site_url() . "articles/listing/" . $shared_by['slug']; ?>">
                                    <br>
                                    <?php echo $shared_by['name']; ?>
                                </a>
                            </span>
                            <?php } ?>
                            <span class="entry-date"><?php echo timeAgo(strtotime($feed['created_at'])); ?></span>
                        </div>
                        <div class="entry-thumb">
                            <?php if ($fd['file_type'] == 'I' and $fd['image'] != "") { ?>
                            <img src="<?php echo $fd['image']; ?>" alt="">
                            <?php } ?>
                            <?php if ($fd['file_type'] == 'V' and $fd['vid_link'] != "") {  ?>
                            <iframe class="articlevideo" src="<?php echo $fd['vid_link']; ?>" frameborder="0" allowfullscreen></iframe>
                            <?php } ?>
                        </div>  
                        <p class="short-des">
                            <i>
                                <?php echo wordLimiter(strip_tags($fd['content']), 160); ?>
                            </i>
                        </p>

                        <p class="entry-comment">
                            <a href="#" class="btn btn-xs btn-default">
                                <i class="fa fa-comments"></i> 
                                <span class="com_count"><?php echo $feed['com_count']; ?></span>
                            </a>

                            <a href="#" class="btn btn-xs btn-default">
                                <i class="fa fa-thumbs-o-up"></i> <span class="like_count"><?php echo $feed['like_count']; ?></span>
                            </a>


                        </p>
                        <?php if ($feed['remove_feed']) { ?>
                        <div class="post-share-link style-bg-color">
                            <span><i class="fa fa-trash"></i></span>
                            <ul class="feed-ul">
                                <li><a href="#" class="delete_feed" data-id="<?php echo $feed['pk_feed_id']; ?>">Delete post</a></li>
                            </ul>
                        </div>
                        <!-- delete-post -->
                        <?php } ?>
                    </article> 
                    <?php } else { ?>
                    <div class="kopa-tab-1-widget">
                        <article class="entry-item news-feed-article">
                            <div class="header-feed-post">
                                <div class="comment-avatar">
                                    <img alt="" src="<?php echo $feed['thumb']; ?>">
                                </div>
                                <div>
                                    <h4 class="entry-title">
                                        <a href="<?php echo site_url() . ARTICLE_TAG . "/" . $fd['slug']; ?>">
                                            <?php echo $fd['title']; ?>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                            <div class="entry-meta">
                                <br>
                                <br>
                                <br>
                                <span class="entry-author"> 
                                    <a href="<?php echo site_url() . "articles/listing/" . $feed['slug']; ?>">
                                        <?php echo $feed['name']; ?>
                                    </a>
                                </span>
                                <?php if ($shared_by) { ?>
                                <br>
                                <span class="entry-author">
                                    <i class="fa fa-share-alt"></i>  Shared a News from 
                                    <a href="<?php echo site_url() . "articles/listing/" . $shared_by['slug']; ?>">
                                        <?php echo $shared_by['name']; ?>
                                    </a>
                                </span>
                                <?php } ?>
                                <span class="entry-date"><?php echo timeAgo(strtotime($feed['created_at'])); ?></span>
                            </div>
                            <div class="entry-thumb">
                                <?php if ($feed['file_type'] == 'I' and $feed['image'] != "") { ?>
                                <img src="<?php echo $feed['image']; ?>" />
                                <?php } ?>
                                <?php if ($feed['file_type'] == 'V' and $feed['video'] != "") { ?>

                                <video width="100%" controls>
                                    <source src="<?php echo $feed['video']; ?>" type="video/mp4">


                                    </video>
                                    <?php } ?>
                                </div>  
                                <p class="short-des">
                                    <i>
                                        <?php echo $feed['content']; ?>
                                    </i>
                                </p>

                                <p class="entry-comment">
                                    <a href="#" class="btn btn-xs btn-default">
                                        <i class="fa fa-comments"></i> 
                                        <span class="com_count"><?php echo $feed['com_count']; ?></span>
                                    </a>

                                    <a href="#" class="btn btn-xs btn-default">
                                        <i class="fa fa-thumbs-o-up"></i> <span class="like_count"><?php echo $feed['like_count']; ?></span>
                                    </a>

                                    
                                </p>
                                <?php if ($feed['remove_feed']) { ?>
                                <div class="post-share-link style-bg-color">
                                    <span><i class="fa fa-trash"></i></span>
                                    <ul class="feed-ul">
                                        <li><a href="#" class="delete_feed" data-id="<?php echo $feed['pk_feed_id']; ?>">Delete post</a></li>
                                    </ul>
                                </div>
                                <!-- delete-post -->
                                <?php } ?>
                            </article> 

                            <?php } ?>

                            <div class="kopa-tab-1-widget">

                                <div class="row feed-row-holder">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="kopa-tag-box">
                                            <span><i class="fa fa-share-alt"></i> Share On social network </span>
                                        </div>
                                        <!-- kopa-tag-box --> 

                                        <div class="kopa-share-post social-links style-bg-color">
                                            <ul class="clearfix">
                                                <li><a href="#" class="fa fa-facebook" data-feed_id="<?php echo $feed['pk_feed_id']; ?>"></a></li>
                                                <li><a href="#" class="fa fa-twitter" data-feed_id="<?php echo $feed['pk_feed_id']; ?>"></a></li>
                                                <li><a href="#" class="fa fa-rss" data-feed_id="<?php echo $feed['pk_feed_id']; ?>"></a></li>
                                                <li><a href="#" class="fa fa-google-plus" data-feed_id="<?php echo $feed['pk_feed_id']; ?>"></a></li>
                                                <li><a href="#" class="fa fa-pinterest" data-feed_id="<?php echo $feed['pk_feed_id']; ?>"></a></li>
                                                <li><a href="#" class="fa fa-instagram" data-feed_id="<?php echo $feed['pk_feed_id']; ?>"></a></li>
                                            </ul> 
                                        </div>
                                        <!-- kopa-share-post -->
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 feed-comment-like text-right">
                                        <div class="kopa-tag-box">
                                            <?php if ($feed['like_status'] == 0) { ?>
                                            <a href="#" class="btn btn-xs btn-default" data-feed_id="<?php echo $feed['pk_feed_id']; ?>" data-like_status="0">

                                                <i class="fa fa-thumbs-o-up"></i> like

                                            </a>
                                            <?php } else {
                                                ?>
                                                <a href="#" class="btn btn-xs btn-default" data-feed_id="<?php echo $feed['pk_feed_id']; ?>" data-like_status="1">

                                                    <i class="fa fa-thumbs-up"></i> liked

                                                </a>
                                                <?php }
                                                ?>
                                            </div>
                                            <div class="kopa-tag-box">

                                                <a href="#" class="btn btn-xs btn-default">
                                                    <i class="fa fa-comments-o"></i> comment
                                                </a>

                                            </div>
                                        </div>

                                    </div>


                                    <form name="com_form" class="com_form">
                                        <div class="col-md-12 col-sm-12 col-xs-12 feed-comment-entry">
                                         <div class="feed-comment-profile">
                                            <img src="<?php echo $this->session->userdata("image"); ?>"/>
                                        </div>
                                        <div class="feed-comment-input">
                                            <input type="text" placeholder="comment..." name="com_text" class="com_text">
                                        </div>
                                    </div>
                                    <input type="hidden" name="feed_id" value="<?php echo $feed['pk_feed_id']; ?>"/>
                                    <input type="hidden" name="last_id" class="com_last_id" value="<?php echo $last_id; ?>"/>
                                    <button class="btn btn-xs btn-default btnpostcomment pull-right post-feed-comment-btn">Post comment</button>

                                </form>

                                <br>
                                <br>
                                <br>
                                <br>

                                <!-- Comments -->
                                <div id="comments">
                                    <h3 class="">
                                        Comments <?php if ($feed['rem_comments'] > 0) { ?>
                                        <span class="pull-right">
                                            <a href="" class="more_comments">View <?php echo $feed['rem_comments']; ?> more comments</a></a>
                                        </span>
                                        <?php } ?> 
                                    </h3> 
                                    <ol class="comments-list clearfix">
                                        <?php
                                        $last_id = 0;
                                        if ($comments) {
                                            foreach ($comments as $com) {
                                                $last_id = $com['pk_comment_id'];
                                                ?> 
                                                <li class="comment clearfix commentsingle" data-last_id="<?php echo $com['pk_comment_id']; ?>" data-feed_id="<?php echo $feed['pk_feed_id']; ?>">
                                                    <article class="comment-wrap clearfix">
                                                        <div class="comment-avatar">
                                                            <img alt="" src="<?php echo $com['thumb']; ?>">
                                                        </div>
                                                        <div class="media-body clearfix">
                                                            <header class="clearfix">
                                                                <div class="pull-left">
                                                                    <h4>
                                                                        <a href="<?php echo site_url() . "articles/listing/" . $com['slug']; ?>">
                                                                            <?php echo $com['name']; ?>
                                                                        </a>
                                                                    </h4>
                                                                    <span class="comment-date"><?php echo timeAgo($com['created_at']); ?></span>
                                                                </div>
                                                                <div class="comment-button pull-right">
                                                                    <?php if ($feed['remove_feed'] == true or $com['remove_com'] == true) { ?>
                                                                    <a class="comment-reply-link" href="#" class="remove btn_remove_comment" data-id="<?php echo $com['pk_comment_id']; ?>"><i class="fa fa-times"></i></a>
                                                                    <?php } ?>
                                                                    <?php if ($com['fk_user_id'] == $this->session->userdata('user_id')) { ?>
                                                                    <a class="comment-reply-link" href="#" class="remove btn_edit_comment" data-id="<?php echo $com['pk_comment_id']; ?>"><i class="fa fa-pencil"></i></a>
                                                                    <?php } ?>
                                                                </div>
                                                            </header>
                                                            <p>
                                                                <?php echo nl2br(makeClickableLinks($com['content'])); ?>
                                                            </p>
                                                            <p>
                                                                <div class="com_edit_block">

                                                                </div>
                                                            </p>
                                                        </div>
                                                    </article>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- kopa-entry-post -->
                    <script src="<?php echo $base; ?>js/jquery-1.11.1.js"></script> 
                    <?php
                }
            }

            if (!$load) {
                ?>
                <script type="text/javascript">
                $(document).ready(function(e) {
                    $(".commentloader").hide();

                });

                </script>

                <?php
            }
            ?>

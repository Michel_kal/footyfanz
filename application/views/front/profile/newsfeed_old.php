<?php
$base = base_url() . PUBLIC_DIR . "fassets/";

$back2 = getBackground("3", "0", TRUE);
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if (count($back2) > 0) {
    $bg = $back2[0];
}

if ($bg != "") {
    ?>
    <style>
    body{background-color: <?php echo $bg['color']; ?>;}
    body{background-image: url("<?php echo $bg['image']; ?>");}
    </style>
    <?php
}
?>
<style>
.footer{display: none !important;}
</style>
<section class="newsfeed">
    <?php
    if ($ud) {
        // $this->load->view("front/profile/profileheader.php", array("ud" => $ud, "follow_status" => $follow_status));
    }
    ?>

    <div class="row strip " style="margin-top:10px;">
        <div class="col-md-12">
            <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                News Feed
            </div>
        </div>
    </div>

    <div class="row staticcheck">
        <div class="col-md-3 hide_mobile">
            <div class="fixer2" style="position: relative;">
                <div class="fixerinner2">
                    <div class="leftpanel">
                        <div class="staticstarter2"></div>
                        <div class="clearfix"></div>
                        <a href="<?php echo site_url() . "followers/" . $ud['slug']; ?>">
                            <div class="panelheader">
                                Following
                            </div>
                        </a>
                        <div class="panelcontent">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if ($followings) {
                                        foreach ($followings as $row) {
                                            ?>
                                            <div class="followerconout"  data-toggle="tooltip" data-placement="top" title="<?php echo $row['name']; ?>">
                                                <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">
                                                    <div class="followercon">
                                                        <img src="<?php echo $row['thumb']; ?>" />
                                                    </div>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?> 
                                        <h4 class="text-center">No Followings</h4>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <script type="text/javascript">
                                $(function() {
                                    $('[data-toggle="tooltip"]').tooltip();
                                });</script>

                            </div>
                        </div>
                    </div>


                    <div class="leftpanel">
                        <div class="clearfix"></div>
                        <a href="<?php echo site_url() . "followers/" . $ud['slug']; ?>">
                            <div class="panelheader">
                                Followers
                            </div>
                        </a>
                        <div class="panelcontent">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if ($followers) {
                                        foreach ($followers as $row) {
                                            ?>
                                            <div class="followerconout"  data-toggle="tooltip" data-placement="top" title="<?php echo $row['name']; ?>">
                                                <a href="<?php echo site_url() . "articles/listing/" . $row['slug']; ?>">
                                                    <div class="followercon">
                                                        <img src="<?php echo $row['thumb']; ?>" />
                                                    </div>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?> 
                                        <h4 class="text-center">No Followers</h4>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="leftpanel">
                        <div class="clearfix"></div>
                        <div class="panelheader">
                            <a href="<?php echo site_url() . "profile/gallery/" . $ud['slug']; ?>" style="color: #fff;">Gallery</a>
                        </div>
                        <div class="panelcontent">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if (count($user_gallery) > 0) { ?>
                                    <?php
                                    foreach ($user_gallery as $gallery) {
                                        if ($gallery['vid'] == false) {
                                            ?> 
                                            <div class="galleryconout"  data-toggle="tooltip" data-placement="top" title="">                         <div class="gallerycon">
                                                <a class="fancybox-effects-a" href="<?php echo $gallery['large_image']; ?>" title="" data-fancybox-group="gallery"> <img src="<?php echo $gallery['thumb']; ?>" /></a>
                                            </div>

                                        </div>
                                        <?php } else { ?>
                                        <div class="galleryconout"  data-toggle="tooltip" data-placement="top" title="">

                                            <div class="gallerycon">
                                                <a class="fancybox-media" href="<?php echo $gallery['vid_link']; ?>"><img src="<?php echo $gallery['thumb']; ?>" /></a> 
                                            </div>

                                        </div>

                                        <?php } ?> <?php }
                                        ?> 
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="staticdevider2"></div>
                        <?php if ($poll_data) { ?>
                        <div class="feedpoll" style="background: url(<?php echo $poll_data['image']; ?>); background-size: cover;">
                            <div class="content">
                                <div class="text">
                                    <?php echo $poll_data['title']; ?>
                                </div>
                                <div style="    text-align: center;

                                color: #fff;
                                margin-top: 5px;
                                width: 100%;
                                font-size: 12px;">
                                <?php echo $poll_data['option1'] . ": " . $poll_data['votes']['perA']; ?>%
                                &nbsp; &nbsp; <?php echo $poll_data['option2'] . ": " . $poll_data['votes']['perB']; ?>%
                                <?php if (trim($poll_data['option3']) != "") { ?>
                                &nbsp; &nbsp; <?php echo $poll_data['option3'] . ": " . $poll_data['votes']['perC']; ?>%
                                <?php } ?>
                                <?php if (trim($poll_data['option4']) != "") { ?>
                                &nbsp; &nbsp; <?php echo $poll_data['option4'] . ": " . $poll_data['votes']['perD']; ?>%
                                <?php } ?>
                                <?php if (trim($poll_data['option5']) != "") { ?>
                                &nbsp; &nbsp; <?php echo $poll_data['option5'] . ": " . $poll_data['votes']['perE']; ?>%
                                <?php } ?>

                            </div>
                            <div class="button poll_answer" style="margin-top: 5px;" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="1"><?php echo $poll_data['option1']; ?></div>
                            <div class="button poll_answer" data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="2"><?php echo $poll_data['option2']; ?></div>
                            <?php if (trim($poll_data['option3']) != "") { ?>
                            <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="3"><?php echo $poll_data['option3']; ?></div>
                            <?php } ?>
                            <?php if (trim($poll_data['option4']) != "") { ?>
                            <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4"><?php echo $poll_data['option4']; ?></div>
                            <?php } ?>
                            <?php if (trim($poll_data['option5']) != "") { ?>
                            <div class="button poll_answer"  data-id="<?php echo $poll_data['pk_poll_id']; ?>" data-voting_option="4"><?php echo $poll_data['option5']; ?></div>
                            <?php } ?>
                        </div>                       
                    </div>
                    <?php } else { ?>

                    <div class="feedpoll" style="background: url(<?php echo $base; ?>images/pollhomebg.jpg);">                         
                        <div class="content">
                            <div class="text">
                                Is Mourinho  Right For United?
                            </div>
                            <div class="button">Yes</div>
                            <div class="button">No</div>
                        </div>                                
                    </div>
                    <?php } ?>
                    <div class="promoted">
                        <ul>
                            <li>
                                <a href="#">Read This...</a>
                            </li>
                            <?php
                            $sp_articles = getAticles('A', 1, 0, 0, 0, 7);
                            if ($sp_articles) {
                                foreach ($sp_articles as $sp) {
                                    ?>
                                    <li>
                                        <a href="<?php echo site_url() . ARTICLE_TAG . "/" ?><?php echo $sp['slug'] ?>"><?php echo $sp['title']; ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 padding_0_desktop" >
            <div class="feedposter">
                <div class="postercon">
                    <div class="feeduser">
                        <img  src="<?php echo $this->session->userdata("image"); ?>" />
                        <div class="arrow"></div>
                        <div class="arrow1"></div>
                    </div>
                    <div class="feedtextcon">
                        <form method="post" id="postform" action="<?php echo site_url() . "profile/feedsave" ?>" enctype="multipart/form-data">
                            <textarea placeholder="What is in your mind?" name="posttext"  id="posttext"  class="autoline"></textarea>
                            <input type="file" name="fileimage" id="fileimage" accept="image/x-png, image/gif, image/jpeg" style="display: none;" />
                            <input type="file" name="filevideo" id="filevideo" accept="video/mp4,video/x-m4v,video/*"   style="display: none;" />
                            <input type="text" name="filetype" id="filetype" value="N" style="display: none;" />
                        </form>
                    </div>
                </div>
                <div class="line"></div>
                <div class="feedaction">
                    <div class="feedbutton" onclick="$('#fileimage').click();"><i class="fa fa-picture-o"></i> Image</div>
                    <div class="feedbutton" onclick="$('#filevideo').click();"><i class="fa fa-video-camera"></i> Video</div>
                    <div class="feedfilename"></div>
                    <div class="feedbutton">
                        <input type="checkbox" name="tweet" checked> <i class="fa fa-twitter"></i>
                    </div>
                    <div class="feedbutton pull-right" style="margin-right: 0px;" >Post</div>
                </div>
                <div class="waiting"><img src="<?php echo $base; ?>images/input-spinner.gif" /></div>
            </div>
            <div class="uploadpouter" style="display: none;">
                <div class="uploadpinner" style="width: 30%;"></div>
            </div>

            <div class="feedcontainer" data-ref_id="<?php echo $ref_id ?>" data-user_id="<?php echo $user_id ?>">
                <!--**************All Feed Container****************-->
            </div>
            <div class="commentloader">
                <div class="loadmore">View more</div>
                <div class="loaderview"><img src="<?php echo $base; ?>images/loadingBar.gif" /></div>
            </div>

        </div>
        <div class="col-md-4 hide_mobile">
            <div class="fixer" style="position: relative;">
                <div class="fixerinner">
                    <?php
                    $this->load->view("front/includes/right_panel_news.php", array('bottom' => false));
                    ?>
                </div>

            </div>
        </div>
    </div>
    <div class="com_edit_temp" style="display: none;">
        <div class="fcpostouter">
            <div class="comuser">
                <img  src="<?php echo $this->session->userdata('image'); ?>" />
            </div>
            <div class="commenttext">
                <form name="com_edit" class="com_edit">
                    <textarea placeholder="Comment... " rows="1" name="com_text"  class="txtcommentbox com_text autoline" ></textarea>
                    <input type="hidden" name="com_id" class="com_id" value=""/>

                </form>
                <div class="btneditcomment">Update </div>
                <div class="btncancelcomment">Cancel </div>
            </div>
        </div>
    </div>


</section>

<script type="text/javascript">
var page = 1;
function getFeeds() {
    var post_data = {page: page, user_id: $(".feedcontainer").data("user_id"), ref_id: $(".feedcontainer").data("ref_id")};
    $('.commentloader').addClass('loading');
    $.post(base_url + "profile/getfeeds", post_data, function(res) {
        $('.commentloader').removeClass('loading');
        $(".feedcontainer").append(res);
    });
}
function getLatestFeeds() {
    var post_data = {last_id: $(".feedsingle:first").data("last_feed"), user_id: $(".feedcontainer").data("user_id"), ref_id: 0};
    $.post(base_url + "profile/getlatestfeeds", post_data, function(res) {
        $(".feedcontainer").prepend(res);
    });
}
$(document).on('click', ".loadmore", function(e) {
    page++;
    getFeeds();
});
$(document).on('click', ".more_comments", function(e) {
    var com_ccontainer = $(this).closest(".feedfooter").find(".feedcomments");
    var btn_more = $(this);
    var post_data = $(this).closest(".feedfooter").find(".commentsingle:first").data();
    $.post(base_url + "profile/getoldcomments", post_data, function(res) {
        com_ccontainer.prepend(res);
        $(".commentsingle", com_ccontainer).slideDown();
        btn_more.fadeOut();
    });
});
$(document).on('click', ".btn_remove_comment", function(e) {
    var com_container = $(this).closest(".commentsingle");
    var post_data = $(this).data();
    var com_count = $(this).closest(".feedfooter").find(".com_count");
    $.confirm({
        title: 'Remove Comment',
        content: 'Are you sure to remove this comment',
        confirmButton: 'Remove',
        confirmButtonClass: 'btn-danger',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function() {
            $.post(base_url + "profile/deletecomment", post_data, function(res) {
                com_container.fadeOut(function() {
                    com_container.remove();
                    com_count.html(parseInt(com_count.html()) - 1);
                });
            });
        }
    });
});
$(document).on('click', ".poll_answer", function(e) {

    var post_data = $(this).data();
    $.confirm({
        title: 'Poll Voting',
        content: 'Are you sure to Vote your Option',
        confirmButton: 'Sure',
        confirmButtonClass: 'btn-success',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function() {
            $.post(base_url + "home/save_poll", post_data, function(res) {
                alert("Your Vote has been Saved");
            });
        }
    });
});
$(document).on('click touchend', ".btn_like", function(e) {
        //alert("Hello");
        e.stopPropagation();
        e.preventDefault();
        var $this = $(this);
        var status = $(this).data("like_status");
        var post_data = $(this).data();
        var like_count = $(this).closest(".feedfooter").find(".like_count");
        $.post(base_url + "profile/managelike", post_data, function(res) {
            if (status == 0) {
                like_count.html(parseInt(like_count.html()) + 1);
                $this.html('<i class="fa fa-thumbs-up"></i> Liked');
                $this.data("like_status", "1");
            } else {
                like_count.html(parseInt(like_count.html()) - 1);
                $this.html('<i class="fa fa-thumbs-up"></i> Like');
                $this.data("like_status", "0");
            }

        });
    });
$(document).on('click touchend', ".btn_share", function(e) {
    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var post_data = $(this).data();
    $.confirm({
        title: 'Share Newsfeed',
        content: 'Are you sure to Share this News',
        confirmButton: 'Share',
        confirmButtonClass: 'btn-success',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function() {
            $.post(base_url + "profile/sharefeed", post_data, function(res) {
                getLatestFeeds();
            });
        }
    });
});
$(document).on('click', ".delete_feed", function(e) {
    var $this = $(this);
    var post_data = $(this).data();
    var feed_block = $(this).closest(".feedsingle");
    $.confirm({
        title: 'Remove Feed',
        content: 'Are you sure to remove this feed and all its contents',
        confirmButton: 'Remove Feed',
        confirmButtonClass: 'btn-danger',
        animation: 'scale',
        animationClose: 'top',
        opacity: 0.5,
        confirm: function() {
            $.post(base_url + "profile/delete_feed", post_data, function(res) {
                feed_block.fadeOut(function() {
                    feed_block.remove();
                });
            });
        }
    });
});
$(document).on('click', ".btnpostcomment", function(e) {
    var form = $(this).closest(".feedcommentspost").find("form");
    var com_block = $(this).closest(".feedfooter").find(".feedcomments");
    var com_count = $(this).closest(".feedfooter").find(".com_count");
    var post_data = form.serialize();
    form.find(".com_text").attr("disabled", "disabled");
    $.post(base_url + "profile/savecomment", post_data, function(res) {
        com_block.append(res.data);
        com_count.html(parseInt(com_count.html()) + parseInt(res.count));
        $(".commentsingle", com_block).slideDown();
        form.find(".com_text").removeAttr("disabled");
        form.find(".com_text").val("");
        form.find(".com_last_id").val(res.last_id);
    }, 'json');
});
$(document).ready(function(e) {
    getFeeds();
    $(".fancybox-effects-a").fancybox();
    $('.fancybox-media')
    .attr('rel', 'media-gallery')
    .fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        prevEffect: 'none',
        nextEffect: 'none',
        arrows: false,
        helpers: {
            media: {},
            buttons: {}
        }
    });
    $(".btnactionfeed").on('click', function() {
        if ($.trim($("#posttext").val()) != "" || $("#filetype").val() != "N") {
            var cntval = $("#posttext").val().length;
            if (cntval > 140) {
                $("#posttext").trigger("keyup");
            } else {
                $('#postform').submit();
            }

        }
    });
    $(document).on("change", "#fileimage", function() {
        if ($("#filetype").val() == "I" && $(this).val() == "") {
            $(".feedfilename").html("");
            $("#filetype").val("N");
        }
        if ($(this).val() != "") {
            $("#filetype").val("I");
            $(".feedfilename").html(getFileName($(this).val()));
        }
    });
    $(document).on("change", "#filevideo", function() {
        if ($("#filetype").val() == "V" && $(this).val() == "") {
            $(".feedfilename").html("");
            $("#filetype").val("N");
        }
        if ($(this).val() != "") {
            $("#filetype").val("V");
            $(".feedfilename").html(getFileName($(this).val()));
        }
    });
});
function getFileName(mainname) {
    var path = mainname;
    var fileName = path.match(/[^\/\\]+$/);
    return fileName;
}

$(document).ready(function(e) {
    $('#postform').ajaxForm({
            //dataType: 'json',
            beforeSubmit: function() {
                $(".feedposter").addClass("loading");
                $(".uploadpouter").show();
                $(".uploadpinner").css("width", "0%");
            },
            uploadProgress: function(event, position, total, percentComplete) {
                if (percentComplete == 100) {
                    $(".uploadpouter").hide();
                } else {
                    $(".uploadpinner").css("width", percentComplete + '%');
                }
            },
            success: function(res) {
                $(".uploadpouter").hide();
                $(".feedposter").removeClass("loading");
                $(".feedfilename").html("");
                $("#filetype").val("N");
                $('#postform')[0].reset();
                getLatestFeeds();
            },
            failed: function() {
                $(".uploadpouter").hide();
                $(".feedposter").removeClass("loading");
            }
        });
});
$(document).on('keydown', ".txtcommentbox", function(e) {
    if (e.ctrlKey && e.keyCode == 13) {
        e.preventDefault();
        $(this).closest(".feedcommentspost").find(".btnpostcomment").click();
        return false;
    }
});
$(document).on('keyup', "#posttext", function(e) {
    var cntval = $(this).val().length;
    if (cntval > 160) {
        $.confirm({
            title: 'Newsfeed limit exceed',
            content: 'Newsfeed max size is 160 character. Please write article for large content.',
            confirmButton: 'Write Article',
            confirmButtonClass: 'btn-success',
            animation: 'scale',
            animationClose: 'top',
            opacity: 0.5,
            confirm: function() {
                var post_data = {"data": $("#posttext").val()};
                $.post(site_url + "articles/prepost", post_data, function(res) {
                    window.location.href = site_url + "articles/post";
                });
            }
        });
        return false;
    }
});
$(document).on('click', ".btn_edit_comment", function(e) {

    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_data_block").hide();
    $(".com_edit_temp .com_text").html(parent.find(".com_text_temp").html());
    $(".com_edit_temp .com_id").val($this.data("id"));
    parent.find(".com_edit_block").html($(".com_edit_temp").html());
});
$(document).on('click', ".btncancelcomment", function(e) {
    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_edit_block").html("");
    parent.find(".com_data_block").show();
});
$(document).on('click', ".btneditcomment", function(e) {
    var $this = $(this);
    var parent = $this.closest(".commentsingle");
    parent.find(".com_text_block").html(parent.find(".com_edit_block .com_text").val());
    parent.find(".com_text_temp").html(parent.find(".com_edit_block .com_text").val());
    var post_data = parent.find(".com_edit_block form").serialize();
    $.post(site_url + "profile/commentupdate", post_data, function(res) {
        $(".btncancelcomment").click();
    });
});</script>
<script type="text/javascript">
var point_start = 0;
$(document).ready(function(e) {
    $(window).scroll(function() {
        var q_con_height = $(".feedcontainer").height() + $('.feedcontainer').offset().top - 600;
            //alert(q_con_height + "----" + $(window).scrollTop());
            if ($(window).scrollTop() > q_con_height) {
                if ($(".loadmore").css("display") != "none") {
                    $(".loadmore").click();
                }
            }


            var static_height = $(".staticcheck").offset().top;
            var window_scroll = $(window).scrollTop();
            var window_width = $("body").width();
            var devider_offset = $(".staticdevider").offset().top - 60;
            var staticstarter = $(".staticstarter").offset().top;
            //console.log(devider_offset+"----"+window_scroll);
            if (devider_offset < window_scroll) {
                if (point_start == 0) {
                    point_start = devider_offset;
                }
                if (window_width > 1170) {
                    if (window_scroll > devider_offset) {
                        if ($(".fixerinner").hasClass("fixedNow") == false) {
                            $(".fixerinner").addClass("fixedNow");
                            $(".fixerinner").css("position", "fixed");
                            $(".fixerinner").css("width", 347);
                            $(".fixerinner").css("top", ((devider_offset - staticstarter) * (-1)) + 15);
                        }
                    } else {
                        setWidthAuto();
                    }
                } else {
                    setWidthAuto();
                }
            } else {
                if (point_start > window_scroll) {
                    setWidthAuto();
                }
            }
        });
$(window).scroll(function() {
    var static_height = $(".staticcheck").offset().top;
    var window_scroll = $(window).scrollTop();
    var window_width = $("body").width();
    var devider_offset = $(".staticdevider2").offset().top + -60;
    var staticstarter = $(".staticstarter2").offset().top;
            // console.log(devider_offset + "----" + window_scroll);
            if (devider_offset < window_scroll) {
                if (point_start == 0) {
                    point_start = devider_offset;
                }
                if (window_width > 1170) {
                    if (window_scroll > devider_offset) {
                        if ($(".fixerinner2").hasClass("fixedNow2") == false) {
                            $(".fixerinner2").addClass("fixedNow2");
                            $(".fixerinner2").css("position", "fixed");
                            $(".fixerinner2").css("width", 253);
                            $(".fixerinner2").css("top", ((devider_offset - staticstarter) * (-1)) + 15);
                        }
                    } else {
                        setWidthAuto2();
                    }
                } else {
                    setWidthAuto2();
                }
            } else {
                if (point_start > window_scroll) {
                    setWidthAuto2();
                }
            }
        });
});
function setWidthAuto() {
    if ($(".fixerinner").hasClass("fixedNow")) {
        $(".fixerinner").removeClass("fixedNow");
        $(".fixerinner").css("position", "relative");
        $(".fixerinner").css("width", "auto");
        $(".fixerinner").css("top", "0");
    }
}

function setWidthAuto2() {
    if ($(".fixerinner2").hasClass("fixedNow2")) {
        $(".fixerinner2").removeClass("fixedNow2");
        $(".fixerinner2").css("position", "relative");
        $(".fixerinner2").css("width", "auto");
        $(".fixerinner2").css("top", "0");
    }
}


</script>


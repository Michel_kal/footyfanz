<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
$positions = array("1" => "1st", "2" => "2nd", "3" => "3rd");
$months = array("1" => "Jan", "2" => "Feb", "3" => "Mar", "4" => "Apr", "5" => "May", "6" => "Jun", "7" => "Jul", "8" => "Aug", "9" => "Sep", "10" => "Oct", "11" => "Nov", "12" => "Dec");
?> 
<div class="userprofile userprofiledata" data-uname="<?php echo html_escape($ud['name']); ?>" data-user_id="<?php echo $ud['pk_user_id']; ?>">
    <div class="row">
        <div class="col-md-2 col-sm-4 col-xs-4 ">
            <div class="user">
                <div class="manouter">
                    <div class="maninner">
                        <img src="<?php echo $ud['thumb']; ?>">
                    </div>
                </div>
                <div class="footyouter">
                    <div class="footyinner" style="display:none;">
                        <img src="<?php echo $ud['club_image']; ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-10 col-sm-8 col-xs-8">
            <div class="prorightcon">
                <div class="row">                                    
                    <div class="col-md-4 pull-right" >
                        <h2><?php echo $ud['name']; ?></h2>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-6  col-sm-6 col-sm-offset-4 col-xs-6 col-xs-offset-4 ">
                                <div class="clublogo">
                                    <img src="<?php echo $ud['club_image']; ?>">
                                </div>
                                <div class="clubrank" style="text-align: center;">
                                    #<?php echo $pos; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 pull-left">
                        <p><?php echo $ud['about_me'] ?></p>
                    </div>
                </div>

                <div class="profootercon">
                    <ul>
                        <li> <a href="<?php echo site_url() . "followers/" . $ud['slug']; ?>">
                                <div class="proval followers"><?php echo $uh['followers']; ?></div>
                                <div class="prolabel">Followers</div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url() . "followers/" . $ud['slug']; ?>">
                                <div class="proval"><?php echo $uh['following']; ?></div>
                                <div class="prolabel">Following</div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo site_url(); ?>articles/listing/<?php echo $ud['slug']; ?>">
                                <div class="proval"><?php echo $uh['art_count']; ?></div>
                                <div class="prolabel">Articles</div>
                            </a>
                        </li>
                        <li>
                            <div class="proval"><?php echo $uh['reads']; ?></div>
                            <div class="prolabel">Reads</div>
                        </li> 
                        <li> 
                            <a href="<?php echo site_url(); ?>user/points/<?php echo $ud['slug']; ?>">
                                <div class="proval"><?php echo $uh['points']; ?></div>
                                <div class="prolabel">Total Points</div>
                            </a>
                        </li>
                        <li>
                            <div class="proval"><?php echo $uh['points_month']; ?></div>
                            <div class="prolabel">Monthly Points</div>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="profilenav">
    <?php foreach ($uh['laurals'] as $lr) { ?>
        <div class="trophyicon pull-left" data-toggle="tooltip" data-placement="top" data-original-title="<?php echo $positions[$lr['position']] . " rank in " . $months[$lr['l_month']] . " " . $lr['l_year']; ?>"><img src="<?php echo $base; ?>images/victor.png"></div>
    <?php } ?>
    <?php if ($follow_status == "NA") { ?>
        <a href="<?php echo site_url() . "user/edit"; ?>" class="borderbutton pull-right">Edit</a>
    <?php } ?>
    <a href="<?php echo site_url() . "articles/listing/" . $ud['slug']; ?>" class="borderbutton pull-right"> Articles </a>
    <?php if ($follow_status != "NA") { ?>
        <div data-toggle="modal" data-target="#messageModal" class="borderbutton pull-right ajax" data-url="<?php echo site_url() . "profile/send_message_form/" . $ud['pk_user_id']; ?>">Message</div>
    <?php } ?>   

    <div class="borderbutton btn_unfollow pull-right"  <?php if ($follow_status == "0" or $follow_status == "NA") { ?> style="display: none;" <?php } ?> data-id="<?php echo $ud['pk_user_id']; ?>"><i class="fa fa-check"></i> Following</div>
    <div class="borderbutton btn_follow pull-right" <?php if ($follow_status == "1" or $follow_status == "NA") { ?> style="display: none;" <?php } ?> data-id="<?php echo $ud['pk_user_id']; ?>">Follow</div>



</div>
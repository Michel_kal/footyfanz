<?php
$base = base_url() . PUBLIC_DIR . "fassets/";
//$ud = $data['ud'];
//$uh = $data['uh'];
$bg = "";
$backall = getBackground("2", "0");
if (count($backall) > 0) {
    $bg = $backall[0];
}
if ($bg != "") {
    ?>
    <style>
        body{background-color: <?php echo $bg['color']; ?>;}
        body{background-image: url("<?php echo $bg['image']; ?>");}
    </style>
    <?php
}
?> 

<section class="newsfeed">
    <div class="userprofile">
        <div class="row">
            <div class="col-md-2">
                <div class="user">
                    <div class="editimage" onclick="$('.awesome-cropper input[type=file]').click();"><i class="fa fa-instagram"></i> Change</div>
                    <div class="editimagekloader" style="display: none;"><img src="<?php echo $base; ?>images/input-spinner.gif" /></div>
                    <div class="manouter">
                        <div class="maninner">
                            <img src="<?php echo $ud['thumb']; ?>" id="userimage" />
                        </div>
                    </div>
                    <div class="footyouter">
                        <div class="footyinner">
                            <img src="<?php echo $ud['club_image']; ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="prorightcon">
                    <div class="row">                                    
                        <div class="col-md-4 pull-right" >
                            <h2><?php echo $ud['name']; ?></h2>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6">
                                    <?php if ($ud['club_image'] != "") { ?>
                                        <div class="clublogo">
                                            <img src="<?php echo $ud['club_image']; ?>">
                                        </div>
                                    <?php } ?>
                                    <div class="clubrank">
                                        #<?php echo $pos; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 pull-left">
                            <p><?php echo $ud['about_me']; ?></p>
                        </div>
                    </div>

                    <div class="profootercon">
                        <ul>
                            <li>
                                <div class="proval followers"><?php echo $uh['followers']; ?></div>
                                <div class="prolabel">Followers</div>
                            </li>
                            <li>
                                <div class="proval"><?php echo $uh['following']; ?></div>
                                <div class="prolabel">Following</div>
                            </li>
                            <li>
                                <div class="proval"><?php echo $uh['points']; ?></div>
                                <div class="prolabel">Points</div>
                            </li>
                            <li>
                                <div class="proval"><?php echo $uh['art_count']; ?></div>
                                <div class="prolabel">Articles</div>
                            </li>
                            <li>
                                <div class="proval"><?php echo $uh['reads']; ?></div>
                                <div class="prolabel">Reads</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row strip">
        <div class="col-md-12">
            <div class="blacklabel" style="text-align: left; padding-left: 15px;">
                Edit Profile
            </div>
        </div>
    </div>

    <div class="row">                      
        <div class="col-md-8">

            <div class="alert alert alert-success profile_success" style="display: none;">Profile updated.</div>
            <form id="profileForm" action="" name="profileForm">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">First Name *</label>
                            <input type="text" class="form-control required" id="firstname" name="firstname" placeholder="First Name" value="<?php echo html_escape($ud['firstname']) ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Last Name</label>
                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name"  value="<?php echo html_escape($ud['lastname']) ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Username *</label>
                            <input type="text" class="form-control required" id="username" name="username" value="<?php echo html_escape($ud['username']) ?>">
                            <span for="username1" class="help-block username1" style="color: #a94442;display: none;">Username</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Email</label>
                            <input type="text" class="form-control " id="email" name="email" <?php echo ($ud['email'] != "" and $ud['status'] != "0") ? "readonly" : ""; ?>  value="<?php echo html_escape($ud['email']) ?>">
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="message-text" class="form-control-label">About Me</label>
                    <textarea class="form-control" id="about_me" name="about_me" rows="5"><?php echo $ud['about_me']; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Gender</label>
                    <div class="clearfix"></div>
                    <label class="radio-inline"><input type="radio" name="gender" <?php echo ($ud['gender'] != "Female") ? "checked" : ""; ?>>Male</label>
                    <label class="radio-inline"><input type="radio" name="gender" <?php echo ($ud['gender'] == "Female") ? "checked" : ""; ?>>Female</label>
                </div>
                <div class="row">                    
                    <div class="col-md-6">
                        <div class="form-group" style="position: relative;">
                            <label for="recipient-name" class="form-control-label">Phone</label>
                            <input type="text" class="form-control " id="phone_no" name="phone_no" value="<?php echo html_escape($ud['phone_no']); ?>" >
                        </div>
                    </div>
                </div>
                <div class="form-group" >
                    <label for="recipient-name" class="form-control-label">Club</label>
                    <select class="form-control " id="fk_club_id" name="fk_club_id" >
                        <!--<option value="">Pick a Club</option>-->
                        <?php foreach ($clubs as $id => $club) { ?>
                            <option value="<?php echo $id; ?>" <?php echo ($ud['fk_club_id'] == $id) ? "selected" : "" ?>><?php echo $club ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Facebook Profile Link</label>
                            <input type="text" class="form-control " id="fb_link" name="fb_link"  placeholder="Facebook Profile Link"  value="<?php echo html_escape($ud['fb_link']); ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="recipient-name" class="form-control-label">Twitter Profile Link</label>
                            <input type="text" class="form-control " id="tw_link" name="tw_link" placeholder="Twitter Profile Link"  value="<?php echo html_escape($ud['tw_link']); ?>" >
                        </div>
                    </div>
                </div>
                <div>
                    <button type="button" class="btn btn-success save_profile pull-right">Update Profile</button>
                </div>



            </form>

        </div>
        <div class="col-md-4">

            <?php
            $this->load->view("front/includes/right_panel_news.php", array('bottom' => false));
            ?>


        </div>
    </div>


</section>
<form role="form" class="uploaderform">
    <input id="image_input" type="hidden" name="image_input" style="display: none;">
</form>
<script type="text/javascript">
    $(function() {

    });
</script>
<script>
    $(document).ready(function() {
        $('#dob').datetimepicker({
            viewMode: 'years',
            format: 'DD/MM/YYYY',
            maxDate: 'now'
        });
        $('#image_input').awesomeCropper(
                {width: 150, height: 150, debug: true}
        );
    });


    $(".save_profile").on('click', function() {
        if ($("#profileForm").valid()) {
            App.blockUI($("#profileForm"));
            var post_data = $("#profileForm").serialize();
            $.post(base_url + "user/updateprofile", post_data, function(res) {
                App.unblockUI($("#profileForm"));
                $(".profile_success").fadeIn();
                $('html, body').animate({scrollTop: 500}, 800);
            });
        } else {

        }
    });
    $("#username").on('blur', function() {
        var uname = $(this).val();
        if (uname == "") {
            return false;
        }
        App.blockUI($("#profileForm"));
        var post_data = $("#profileForm").serialize();
        $.post(base_url + "user/checkusername", {uname: uname}, function(res) {
//                alert(res);
            if (res.status == 0) {
                $("#username").val("").focus();
                $(".username1").html(res.msg).show();
            } else {
                $(".username1").hide();
            }
            App.unblockUI($("#profileForm"));
//                $(".profile_success").fadeIn();
//                $('html, body').animate({scrollTop: 500}, 800);
        }, 'json');

    });

    function uploadimagetoserver() {
        $(".editimagekloader").show();
        var post_data = {"image": $("#image_input").val()};
        $.post(site_url + "user/updateimage", post_data, function(res) {
            $(".editimagekloader").hide();
            if ($.trim(res) != "") {
                $("#userimage").attr("src", res);
                $(".maninner img").attr("src", res);
            }
        });
    }
</script> 
<?php $base = site_url() . "public/uassets/" ?>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page_heading">Account Settings</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="left_sidebar">
                <ul>
                    <li>
                        <a href="<?php echo site_url() . "user/index/" ?>"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url() . "user/password" ?>"><i class="fa fa-key"></i> Passsword</a>
                    </li>
                    <li class="active">
                        <a href="<?php echo site_url() . "user/setting" ?>"><i class="fa fa-cog"></i> Setting</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="main_content_box">
                <h3 class="sub_heading">Account Setting</h3>
                <p class="sub_text">Delete your accounts.</p>
                <div class="line_single"></div>
                <div class="row">

                    <div class="col-md-12">
                        <form class="form-vertical" id="profile_form">
                            <p>If you do not want to continue your account at <?php echo SITE_NAME; ?>. You can delete your account.<br>Once your account is deleted, all comments written by you will delete.</p>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn_account_delete  pull-right">Delete Account</button>
                            </div>
                        </form>
                        <div class="clearfix" style="margin-bottom: 35px;"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(e) {
        $(".btn_account_delete").on('click', function(e) {
            e.preventDefault();
            if (confirm("Are you sure you want to delete this account.")) {
                var post_data = {action: "delete"};
                $.post(site_url + "user/deleteaccount", post_data, function(res) {
                    window.location.href = site_url;
                });
            }
        });
    });
</script>

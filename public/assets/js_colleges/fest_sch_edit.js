$(document).ready(function(e) {


    $("#fest_category").live('change', function() {
        updateCategoryFest();
    });




    $("#fest_state").live('change', function() {
        App.blockUI($(".fest_city"));
        var post_data = {state_id: $(this).val()};
        $.post(base_url + "colleges/fest/getcity", post_data, function(res) {
            $("#fest_city").html(res);
            App.unblockUI($(".fest_city"));
        });
    });

    $(".contact_persons .btn_add").live('click', function() {
        addContactPerson();
    });

    $(".contact_persons .btn_remove").live('click', function() {
        $(this).closest(".contact_row").remove();
        resetContact();
    });

    $(".events .btn_add").live('click', function() {
        addEvents();
    });

    $(".events .btn_remove").live('click', function() {
        $(this).closest(".event_row").remove();
        resetEvent();
    });

    $(".fest_schedule_delete").live('click', function() {
        App.blockUI($("#fest_list"));
        var post_data = $(this).data();
        $.post($(this).data("url"), post_data, function(res) {
            $(".searchBtn").click();
            App.unblockUI($(".fest_list"));
        });
    });

    $(".reg_type").live("change", function() {
        if ($(".reg_type:checked").val() == "paid") {
            $(".reg_charge_con").slideDown();
        } else {
            $(".reg_charge_con").slideUp();
        }
    });

    $(".add_poster").click(function() {
        $("#posters").click();
    });
    $("#posters").change(function() {
        $container = $(this).parents("portlet");
        App.blockUI($container);
        var formData = new FormData($("form#form_fest_poster")[0]);
        $.ajax({
            url: base_url + "colleges/fest/update_schedule/poster",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($container);
                App.loadData($(".loadposter"));
                $.gritter.add({title: 'Fest Schedule', text: 'Fest Schedule Updated'});
                $("form#form_fest_poster")[0].reset();
            },
            error: function(data) {
                App.unblockUI($container);
                $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $(".delete_poster").live('click', function() {
        App.blockUI($(".loadposter"));
        var post_data = $(this).data();
        $.post(base_url + "colleges/fest/update_schedule/deleteposter", post_data, function(res) {
            App.loadData($(".loadposter"));
            App.unblockUI($(".loadposter"));
        });
    });

    $(".add_schedule").click(function() {
        $("#schedules").click();
    });

    $("#schedules").change(function() {
        $container = $(this).parents("portlet");
        App.blockUI($container);
        var formData = new FormData($("form#form_fest_schedule")[0]);
        $.ajax({
            url: base_url + "colleges/fest/update_schedule/schedule",
            type: 'POST',
            data: formData,
            async: false,
            success: function(data) {
                App.unblockUI($container);
                App.loadData($(".loadschedule"));
                $.gritter.add({title: 'Fest Schedule', text: 'Fest Schedule Updated'});
                $("form#form_fest_schedule")[0].reset();
            },
            error: function(data) {
                App.unblockUI($container);
                $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
    $(".delete_schedule").live('click', function() {
        App.blockUI($(".loadschedule"));
        var post_data = $(this).data();
        $.post(base_url + "colleges/fest/update_schedule/deleteschedule", post_data, function(res) {
            App.loadData($(".loadschedule"));
            App.unblockUI($(".loadschedule"));
        });
    });

});

function updateCategoryFest() {
    $category = $("#fest_category").val();
    var post_data = {cat_id: $category};
    $.post(base_url + "colleges/fest/festbycategory", post_data, function(res) {
        $("#fest_select").html(res);
    });
}

function addContactPerson() {
    $(".contact_persons").append($(".template_contact").html());
    resetContact();
}

function addEvents() {
    $(".events").append($(".template_event").html());
    resetEvent();
}

function resetContact() {
    $i = 1;
    $length = $(".contact_persons .contact_row").length;
    $(".contact_persons .contact_row").each(function() {
        $(this).find(".num_id").html($i);
        if ($i < $length) {
            $(this).find(".btn_add").hide();
        } else {
            $(this).find(".btn_add").show();
        }
        if ($length == 1) {
            $(this).find(".btn_remove").hide();
        } else {
            $(this).find(".btn_remove").show();
        }
        $i++;
    });
}

function resetEvent() {
    $i = 1;
    $length = $(".events .event_row").length;
    $(".events .event_row").each(function() {
        $(this).find(".num_id").html($i);
        if ($i < $length) {
            $(this).find(".btn_add").hide();
        } else {
            $(this).find(".btn_add").show();
        }
        if ($length == 1) {
            $(this).find(".btn_remove").hide();
        } else {
            $(this).find(".btn_remove").show();
        }
        $i++;
    });
}

function saveFestSchedule() {
    App.blockUI($("#form_fest_schedule"));
    var formData = new FormData($("form#form_fest_schedule")[0]);
    $.ajax({
        url: base_url + "colleges/fest/save_schedule",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            App.unblockUI($("#form_fest_schedule"));
            $(".fest_form_container").hide();
            $(".fest_list_container").show();
            $(".tabdata").first().click();
        },
        error: function(data) {
            App.unblockUI($("#form_fest_schedule"));
            $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

function update_fest_basic() {
    App.blockUI($("#form_fest_basic"));
    var formData = new FormData($("form#form_fest_basic")[0]);
    $.ajax({
        url: base_url + "colleges/fest/update_schedule/basic",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            App.unblockUI($("#form_fest_basic"));
            App.loadData($(".loadbasic"));
            $.gritter.add({title: 'Fest Schedule', text: 'Fest Schedule Updated'});
        },
        error: function(data) {
            App.unblockUI($("#form_fest_basic"));
            $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
function update_fest_additional() {
    App.blockUI($("#form_fest_additional"));
    var formData = new FormData($("form#form_fest_additional")[0]);
    $.ajax({
        url: base_url + "colleges/fest/update_schedule/additional",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            App.unblockUI($("#form_fest_additional"));
            App.loadData($(".loadadditional"));
            $.gritter.add({title: 'Fest Schedule', text: 'Fest Schedule Updated'});
        },
        error: function(data) {
            App.unblockUI($("#form_fest_additional"));
            $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
function update_fest_contact() {
    App.blockUI($("#form_fest_contact"));
    var formData = new FormData($("form#form_fest_contact")[0]);
    $.ajax({
        url: base_url + "colleges/fest/update_schedule/contact",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            App.unblockUI($("#form_fest_contact"));
            App.loadData($(".loadcontact"));
            $.gritter.add({title: 'Fest Schedule', text: 'Fest Schedule Updated'});
        },
        error: function(data) {
            App.unblockUI($("#form_fest_contact"));
            $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
        },
        cache: false,
        contentType: false,
        processData: false
    });
}

function update_fest_event() {
    App.blockUI($("#form_fest_event"));
    var formData = new FormData($("form#form_fest_event")[0]);
    $.ajax({
        url: base_url + "colleges/fest/update_schedule/event",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            App.unblockUI($("#form_fest_event"));
            App.loadData($(".loadevent"));
            $.gritter.add({title: 'Fest Schedule', text: 'Fest Schedule Updated'});
        },
        error: function(data) {
            App.unblockUI($("#form_fest_event"));
            $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
        },
        cache: false,
        contentType: false,
        processData: false
    });
}


function update_fest_other() {
    App.blockUI($("#form_fest_other"));
    var formData = new FormData($("form#form_fest_other")[0]);
    $.ajax({
        url: base_url + "colleges/fest/update_schedule/other",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            App.unblockUI($("#form_fest_other"));
            App.loadData($(".loadother"));
            $.gritter.add({title: 'Fest Schedule', text: 'Fest Schedule Updated'});
        },
        error: function(data) {
            App.unblockUI($("#form_fest_other"));
            $.gritter.add({title: 'Fest Error', text: 'Error in Saving Fest Schedule'});
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
function loadTab() {
    $(".tabdata").closest("li.active").find('a').click();
}

function update_college_profile() {
    var parent = $("form#form_profile_edit").parent("div");
    App.blockUI(parent);
    var formData = new FormData($("form#form_profile_edit")[0]);
    $.ajax({
        url: base_url + "colleges/account/profile_save",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            var res = $.parseJSON(data)
            App.unblockUI(parent);
            if (res.status == 'OK') {
                $(".tabdata.active").click();
                $.gritter.add({
                    title: 'College Profile',
                    text: 'Profile has been Updated'
                });
            } else {
                App.unblockUI(parent);
            }
        },
        error: function(data) {

        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
}

function check_exist(input, url, pdata) {
    input.attr("readonly", true).addClass("spinner");
    $.post(base_url + url, pdata, function(res) {
        input.attr("readonly", false).removeClass("spinner");
        input.popover('destroy');
        input.popover({
            'html': true,
            'placement': App.isRTL() ? 'left' : 'right',
            'title': res.title,
            'content': res.msg,
        });
        if (res.status == 'OK') {
            input.closest('.control-group').removeClass('error').addClass('success');
            //input.after('<span class="help-inline ok"></span>');                    
            input.popover('destroy');
        } else {
            input.val("");
            input.closest('.control-group').removeClass('success').addClass('error');
            $('.help-inline.ok', input.closest('.control-group')).remove();
            input.popover('show');
            input.focus();
        }

    }, 'json');
}

function check_email(input, url, pdata, call_back) {
    var form = input.closest("form");
    input.attr("readonly", true).addClass("spinner");
    input.popover('destroy');
    App.blockUI(form);
    $.post(base_url + url, pdata, function(res) {
        input.attr("readonly", false).removeClass("spinner");

        App.unblockUI(form);
        input.popover({
            'html': true,
            'placement': App.isRTL() ? 'left' : 'right',
            'title': res.title,
            'content': res.msg,
        });
        if (res.status == 'OK') {
            input.closest('.control-group').removeClass('error').addClass('success');
            //input.after('<span class="help-inline ok"></span>');                    
            input.popover('destroy');
            call_back();
        } else {
            input.val("");
            input.closest('.control-group').removeClass('success').addClass('error');
            $('.help-inline.ok', input.closest('.control-group')).remove();
            input.popover('show');
            input.focus();
        }
    }, 'json');
}

$(document).ready(function(e) {
    $(".college_action").live('click', function(e) {
        e.preventDefault();
        var post_data = $(this).data();
        var url = $(this).data('url');
        $.post(url, post_data, function(res) {
            loadTab();
            $.gritter.add({
                title: res.title,
                text: res.text
            });
        }, 'json');

    });

    $(".user_email").live('blur', function() {
        var input = $(this);
        var email = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (input.val() === "" || !email.test(input.val())) {
            input.popover('destroy');
            return;
        }
        var url = "colleges/account/check_user_email";
        var pdata = {email: input.val(), user_id: $("#id").val()};
        check_exist(input, url, pdata);
    });

});

var wo_editor, wo_html = '';

function wo_createEditor() {
    //if (wo_editor)
    // wo_editor.destroy();
    wo_html = $('#wo_editorcontents').val();
    // Create a new editor inside the <div id="editor">, setting its value to html
    var config = {};
    wo_editor = CKEDITOR.appendTo('wo_editor', config, wo_html);
}

function wo_removeEditor() {
    if (!wo_editor)
        return;

    // Retrieve the editor contents. In an Ajax application, this data would be
    // sent to the server or used in any other way.
    document.getElementById('wo_editorcontents').innerHTML = wo_html = wo_editor.getData();
    document.getElementById('wo_contents').style.display = '';

    // Destroy the editor.
    wo_editor.destroy();
    wo_editor = null;
}

$(document).ready(function(e) {
    $(".master_delete").live("click", function() {
        parent_block = $(this).closest(".portlet");
        portfolio_block = $(this).closest("tr");
        App.blockUI(parent_block);
        var post_data = $(this).data();
        $.post($(this).data("url"), post_data, function(res) {
            portfolio_block.slideUp(function() {
                $(this).remove();
                App.unblockUI(parent_block);
            });
        });
    });
    $(".master_status").live("click", function() {
        parent_block = $(this).closest(".portlet");
        portfolio_block = $(this).closest(".portfolio-block");
        App.blockUI(parent_block);
        var post_data = $(this).data();
        $.post($(this).data("url"), post_data, function(res) {
            App.unblockUI(parent_block);
            $(".searchBtn").click();
        });
    });
});

function update_fest() {
    var formData = new FormData($("form#fest_edit")[0]);
    $.ajax({
        url: base_url + "colleges/account/fest_save",
        type: 'POST',
        data: formData,
        async: false,
        success: function(data) {
            var res = $.parseJSON(data)
            if (res.status == 'OK') {
                $(".error_block").hide();
                $(".searchBtn").click();
                $.gritter.add({
                    title: 'Fest',
                    text: 'Fest has been Updated'
                });
                $('#modal_fest_edit .close').click();
                $('.festtab').click();
            } else {
                $(".error_block").html(res.msg).hide().fadeIn('fast');
            }
        },
        error: function(data) {
        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
}



//************************** Hint *****************************


var search_json = {};

$(document).ready(function() {
    var last_lenght = 3;
    var min_length = 2;

    $("#txt_search").live("keyup", function(event) {
        if (event.keyCode == 38 || event.keyCode == 40) {
            return;
        }
        var stext = $(this).val();
        if (stext.length > min_length && stext.length > last_lenght) {
            var post_data = {key: stext, type: $("input[name=stype]:checked").val(), category: $(".selected_category").data('id')};
            $.post(base_url + "user/profile/hint", post_data, function(res) {
                search_json = $.parseJSON(res);
                renderResult(stext);
            });
        } else {
            renderResult(stext);
        }
        last_lenght = stext.length;
    });
});

function renderResult($term) {
    var data_hint = "";
    $.each(search_json, function(k, v) {
        $text = db_highlighter(v, $term);
        data_hint = data_hint + "<li>\
            <a>" + $text + "</a><value data-key='" + k + "'>" + v + "</value>\
        </li>";
    });
    $(".db_hint ul").html(data_hint);
    if (data_hint != '') {
        $(".db_hint").slideDown();
    } else {
        $(".db_hint").hide();
    }
    active = -1;
}

function db_highlighter(value, term) {
    return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
}


$(document).ready(function(e) {
    $("#txt_search").live("keydown", function(event) {

        if ($(".db_hint").css("display") != "none" && event.keyCode == "38") {
            db_moveSelect(-1);
            return false;
        }
        if ($(".db_hint").css("display") != "none" && event.keyCode == "40") {
            db_moveSelect(1);
            return false;

        }
    });

});
var active = -1, list, listItems;
function db_moveSelect(step) {
    list = $(".db_hint ul");
    listItems = list.find("li");
    listItems.slice(active, active + 1).removeClass("active");
    movePosition(step);
    var activeItem = listItems.slice(active, active + 1).addClass("active");
}
function movePosition(step) {
    active += step;
    if (active < 0) {
        active = listItems.size() - 1;
    } else if (active >= listItems.size()) {
        active = 0;
    }
}
$(document).ready(function(e) {
    $(".db_hint ul li").live("click", function() {
        $(this).addClass("active");
        $("#txt_search").val($(".db_hint ul li.active value").html());
        $("#college").val($(".db_hint ul li.active value").data("key"));
        $(".db_hint").hide();
        $('.searchBtn').focus().click();
    });
    $("body:not(.db_hint)").click(function() {
        $(".db_hint").hide();
    });
});

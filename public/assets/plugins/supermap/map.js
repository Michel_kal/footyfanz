/*!jQuery superMap*/
/**
 * Version: 1.0.0 (14/02/2014)
 * Requires: jQuery v1.7+ and Google Map http://maps.googleapis.com/maps/api/js?sensor=false
 *
 * Copyright (c) 2012 Sheetal Kumar Maurya
 * Under MIT and GPL licenses:
 *  http://www.opensource.org/licenses/mit-license.php
 *  http://www.gnu.org/licenses/gpl.html
 * 
 */
jQuery.fn.superMap = function(options) {
    var defaults = {
        long: 80.954269578125,
        lat: 26.8394671,
        draggable: true,
        container: $("#mapcanvas"),
        lat_box: '',
        long_box: '',
        infoContainer: '',
        zoom: 8,
        markerTitle: 'Your Location',
        useaddress: false,
        address: ''
    };
    var geocoder;
    var map;
    var pos_marker;
    var geocoder = new google.maps.Geocoder();
    var settings = $.extend({}, defaults, options);
    settings = $.extend({}, settings, $(this).data());
    function init(self_pointer) {

        var latLng = new google.maps.LatLng(settings.lat, settings.long);
        map = new google.maps.Map(self_pointer, {
            zoom: settings.zoom,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        // Update current position info.
        if (settings.useaddress == true) {
            _codeAddress(settings.address)
        }
        pos_marker = new google.maps.Marker({
            position: latLng,
            title: settings.markerTitle,
            map: map,
            draggable: settings.draggable
        });
        _updateMarker(latLng);
        google.maps.event.addListener(pos_marker, 'drag', function() {
            _updateMarker(pos_marker.getPosition());
        });





    }

    function _codeAddress(address) {
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                _post_value(results[0].geometry.location.lat(), results[0].geometry.location.lng());

            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }

    function _updateMarker(latLng) {
        if (settings.infoContainer !== '' && $("#" + settings.infoContainer).html() !== 'undefined') {
            $("#" + settings.infoContainer).html([
                latLng.lat(),
                latLng.lng()
            ].join(', '));
        }
        if (settings.lat_box !== '' && $("#" + settings.lat_box).val() !== 'undefined') {
            $("#" + settings.lat_box).val(latLng.lat());
        }
        if (settings.long_box !== '' && $("#" + settings.long_box).val() !== 'undefined') {
            $("#" + settings.long_box).val(latLng.lng());
        }
    }
    function _post_value(lt, lg) {
        var latLngPos = new google.maps.LatLng(lt, lg);
        pos_marker.position = latLngPos;
        pos_marker.map = map;
        map.setCenter(latLngPos);
        pos_marker.setPosition(latLngPos);
        _updateMarker(pos_marker.getPosition());
    }
    function _get_position() {
        var g_pos = pos_marker.getPosition();
        var return_data = new Array();
        return_data[0] = g_pos.lat();
        return_data[1] = g_pos.lng();
        return return_data;
    }
    function _get_location(callback_func) {
        geocoder.geocode({
            latLng: pos_marker.getPosition()
        }, function(responses) {
            if (responses && responses.length > 0) {
                callback_func(responses[0].formatted_address);
            } else {
                callback_func('');
            }
        });
    }
    this.each(function() {
        init(this);
    });
    return {
        updateMarker: function(lt, lg) {
            _post_value(lt, lg);
        },
        getLong: function() {
            var pos_data = _get_position();
            return pos_data[1];
        },
        getLat: function() {
            var pos_data = _get_position();
            return pos_data[0];
        },
        getPos: function(callback_func) {
            _get_location(callback_func);
        },
        updateAddress: function(address) {
            _codeAddress(address);
        }
    }
};